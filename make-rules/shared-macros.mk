#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
# Copyright 2015, Igor Kozhukhov.
# Copyright (c) 2010, 2015, Oracle and/or its affiliates. All rights reserved.
#

# The location of a mirror of community source archives that we build in this
# gate.
#ARCHIVE_MIRROR ?= http://dlc.openindiana.org/oi-build/source-archives
#DOWNLOAD_SEARCH_PATH +=	$(ARCHIVE_MIRROR)
DILOS_ARCHIVE_MIRROR ?= http://dlc.dilos.org/srcarch
DOWNLOAD_SEARCH_PATH +=	$(DILOS_ARCHIVE_MIRROR)
export DOWNLOAD_SEARCH_PATH

# The workspace starts at the mercurial root
ifeq ($(origin WS_TOP), undefined)
#export WS_TOP ?=		$(shell hg root)
export WS_TOP ?=		$(shell git rev-parse --show-toplevel)
endif

WS_MACH =	$(WS_TOP)/$(MACH)
WS_LOGS =	$(WS_MACH)/logs
WS_REPO =	$(WS_MACH)/repo
WS_TOOLS =	$(WS_TOP)/tools
WS_MAKE_RULES =	$(WS_TOP)/make-rules
WS_COMPONENTS =	$(WS_TOP)/components
WS_INCORPORATIONS =	$(WS_TOP)/incorporations
WS_LINT_CACHE =	$(WS_TOP)/$(MACH)/pkglint-cache

# we want our pkg piplines to fail if there is an error
# (like if pkgdepend fails in the middle of a pipe), but
# we don't want the builds or ./configure's failing as well.
# so we only set pipefail for the publish target and have
# to reset it for the others since they might be invoked
# as dependencies of publish.
#export SHELLOPTS
#build:			SHELLOPTS=
#test:			SHELLOPTS=
#install:		SHELLOPTS=
#publish:		SHELLOPTS=pipefail
#deb:			SHELLOPTS=

# This can be overridden to avoid rebuilding when you touch a Makefile
MAKEFILE_PREREQ =	Makefile

INSTALLED_BUILD_DEPENDS=
BUILD_COMPONENTS_DEPENDS=

# some things don't build properly in non-C locales,
# so lets stay there
export LC_ALL=C

SHELL=			/bin/bash

CONSOLIDATION ?=	du.dilos.org
PUBLISHER ?=		$(CONSOLIDATION)
PUBLISHER_LOCALIZABLE ?=	$(CONSOLIDATION)-localizable

ROOT =			/

PRIV_CMD ?=		sudo -E

# Native OS version
OS_VERSION :=		$(shell uname -r)
SOLARIS_VERSION =	$(OS_VERSION:5.%=2.%)
# Target OS version
PKG_SOLARIS_VERSION ?= 5.11
BUILD_NUM ?=		1.3.7.0
BUILD_VERSION ?=	$(OS_VERSION)-$(BUILD_NUM)
DEB_VERSION ?= 		$(BUILD_NUM)

WS_DEBS ?=		$(WS_TOP)/$(MACH)/debs

#include $(WS_MAKE_RULES)/ips-buildinfo.mk

COMPILER =		gcc
BITS ?=			32

# The values of BITS changes during the build process for components that
# are built 32-bit and 64-bit.  This macro makes it possible to determine
# which components are only built 64-bit and allow other make-rules files
# to adjust accordingly.
INITIAL_BITS=		$(BITS)

# The default version should go last.
PYTHON_VERSION =	2.7
#PYTHON_VERSIONS =	2.7 2.6
PYTHON_VERSIONS =	2.7

BASS_O_MATIC =	$(WS_TOOLS)/bass-o-matic

CLONEY =	$(WS_TOOLS)/cloney

CONFIG_SHELL =	/bin/bash

PKG_REPO =	file:$(WS_REPO)

COMPONENT_SRC_NAME =	$(COMPONENT_NAME)

COMPONENT_DIR :=	$(shell pwd)
#SOURCE_DIR =	$(COMPONENT_DIR)/$(COMPONENT_SRC)
BUILD_DIR :=	$(COMPONENT_DIR)/build
#SRC_DIR =	$(COMPONENT_DIR)/src
SOURCE_DIR ?=	$(BUILD_DIR)/$(COMPONENT_SRC)
#DEBIAN_DIR ?=	$(BUILD_DIR)/debian
DEBIAN_DIR ?=	$(SOURCE_DIR)/debian
PROTO_DIR ?=	$(DEBIAN_DIR)/tmp

# Set a default reference repository against which pkglint is run, in case it
# hasn't been set in the environment.

# NOTE: Commenting out CANONICAL_REPO due to issue #1310
#CANONICAL_REPO ?=             http://pkg.openindiana.org/experimental/

ETCDIR =		/etc
USRDIR =		/usr
BINDIR =		/bin
SBINDIR =		/sbin
LIBDIR =		/lib
VARDIR =		/var
USRBINDIR =		$(USRDIR)/bin
USRBINDIR32 =		$(USRDIR)/bin/$(MACH32)
USRBINDIR64 =		$(USRDIR)/bin/$(MACH64)
USRSBINDIR =		$(USRDIR)/sbin
USRSBINDIR32 =		$(USRDIR)/sbin/$(MACH32)
USRSBINDIR64 =		$(USRDIR)/sbin/$(MACH64)
USRLIBDIR =		$(USRDIR)/lib
USRLIBDIR64 =		$(USRDIR)/lib/$(MACH64)
USRSHAREDIR =		$(USRDIR)/share
USRINCDIR =		$(USRDIR)/include
USRSHARELOCALEDIR =	$(USRSHAREDIR)/locale
USRSHAREMANDIR =	$(USRSHAREDIR)/man
USRSHAREDOCDIR =	$(USRSHAREDIR)/doc
USRSHAREFONTSDIR =	$(USRSHAREDIR)/fonts
USRSHARETTFONTSDIR =	$(USRSHAREFONTSDIR)/TrueType
USRSHARELIBDIR =	$(USRSHAREDIR)/lib
USRSHAREMAN1DIR =	$(USRSHAREMANDIR)/man1
USRSHAREMAN1MDIR =	$(USRSHAREMANDIR)/man1m
USRSHAREMAN3DIR =	$(USRSHAREMANDIR)/man3
USRSHAREMAN4DIR =	$(USRSHAREMANDIR)/man4
USRSHAREMAN5DIR =	$(USRSHAREMANDIR)/man5
USRSHAREMAN6DIR =	$(USRSHAREMANDIR)/man6
USRSHAREMAN7DIR =	$(USRSHAREMANDIR)/man7
USRSHAREMAN8DIR =	$(USRSHAREMANDIR)/man8
PROTOBINDIR =		$(PROTO_DIR)$(BINDIR)
PROTOETCDIR =		$(PROTO_DIR)$(ETCDIR)
PROTOETCSECDIR = 	$(PROTO_DIR)$(ETCDIR)/security
PROTOUSRDIR =		$(PROTO_DIR)$(USRDIR)
PROTOLIBDIR =		$(PROTO_DIR)$(LIBDIR)
PROTOLIBDIR64 =		$(PROTO_DIR)$(LIBDIR64)
PROTOUSRBINDIR =	$(PROTO_DIR)$(USRBINDIR)
PROTOUSRBINDIR32 =	$(PROTO_DIR)$(USRBINDIR32)
PROTOUSRBINDIR64 =	$(PROTO_DIR)$(USRBINDIR64)
PROTOUSRSBINDIR =	$(PROTO_DIR)$(USRSBINDIR)
PROTOUSRSBINDIR32 =	$(PROTO_DIR)$(USRSBINDIR32)
PROTOUSRSBINDIR64 =	$(PROTO_DIR)$(USRSBINDIR64)
PROTOUSRLIBDIR =	$(PROTO_DIR)$(USRLIBDIR)
PROTOUSRLIBDIR64 =	$(PROTO_DIR)$(USRLIBDIR64)
PROTOUSRINCDIR =	$(PROTO_DIR)$(USRINCDIR)
PROTOUSRSHAREDIR =	$(PROTO_DIR)$(USRSHAREDIR)
PROTOUSRSHARELIBDIR =	$(PROTO_DIR)$(USRSHARELIBDIR)
PROTOUSRSHAREMANDIR =	$(PROTO_DIR)$(USRSHAREMANDIR)
PROTOUSRSHAREDOCDIR =	$(PROTO_DIR)$(USRSHAREDOCDIR)
PROTOUSRSHAREMAN1DIR =	$(PROTO_DIR)$(USRSHAREMAN1DIR)
PROTOUSRSHAREMAN1MDIR =	$(PROTO_DIR)$(USRSHAREMAN1MDIR)
PROTOUSRSHAREMAN3DIR =	$(PROTO_DIR)$(USRSHAREMAN3DIR)
PROTOUSRSHAREMAN4DIR =	$(PROTO_DIR)$(USRSHAREMAN4DIR)
PROTOUSRSHAREMAN5DIR =	$(PROTO_DIR)$(USRSHAREMAN5DIR)
PROTOUSRSHAREMAN6DIR =	$(PROTO_DIR)$(USRSHAREMAN6DIR)
PROTOUSRSHAREMAN7DIR =	$(PROTO_DIR)$(USRSHAREMAN7DIR)
PROTOUSRSHAREMAN8DIR =	$(PROTO_DIR)$(USRSHAREMAN8DIR)
PROTOUSRSHARELOCALEDIR = $(PROTO_DIR)$(USRSHARELOCALEDIR)

GNUBIN =		/usr/gnu/bin
GNULIB =		/usr/gnu/lib
GNULIB64 =		/usr/gnu/lib/$(MACH64)
GNUSHARE =		/usr/gnu/share
GNUSHAREMAN =		/usr/gnu/share/man
GNUSHAREMAN1 =		/usr/gnu/share/man/man1
PROTOGNUBIN =		$(PROTO_DIR)/$(GNUBIN)
PROTOGNUSHARE =		$(PROTO_DIR)/$(GNUSHARE)
PROTOGNUSHAREMAN =	$(PROTO_DIR)/$(GNUSHAREMAN)
PROTOGNUSHAREMAN1 =	$(PROTO_DIR)/$(GNUSHAREMAN1)

# work around _TIME, _DATE, embedded date chatter in component builds
# to use, set TIME_CONSTANT in the component Makefile and add $(CONSTANT_TIME)
# to the appropriate {CONFIGURE|BUILD|INSTALL}_ENV
CONSTANT_TIME =		LD_PRELOAD_32=$(WS_TOOLS)/time-$(MACH32).so
CONSTANT_TIME +=	LD_PRELOAD_64=$(WS_TOOLS)/time-$(MACH64).so
CONSTANT_TIME +=	TIME_CONSTANT="$(TIME_CONSTANT)"

PATH.32=/usr/bin:$(GNUBIN):/sbin:/usr/sbin
PATH.64=/usr/bin/$(MACH64):/usr/bin:$(GNUBIN):/sbin:/usr/sbin
PATH=$(PATH.$(BITS))

# set MACH from uname -p to either sparc or i386
MACH :=		$(shell uname -p)

# set MACH32 from MACH to either sparcv7 or i86
MACH32_1 =	$(MACH:sparc=sparcv7)
MACH32 =	$(MACH32_1:i386=i86)

# set MACH64 from MACH to either sparcv9 or amd64
MACH64_1 =	$(MACH:sparc=sparcv9)
MACH64 =	$(MACH64_1:i386=amd64)

CONFIGURE_NO_ARCH =	$(BUILD_DIR_NO_ARCH)/.configured
CONFIGURE_32 =		$(BUILD_DIR_32)/.configured
CONFIGURE_64 =		$(BUILD_DIR_64)/.configured
CONFIGURE_32_and_64 =	$(CONFIGURE_32) $(CONFIGURE_64)

BUILD_DIR_NO_ARCH =	$(BUILD_DIR)/$(MACH)
#BUILD_DIR_32 =		$(BUILD_DIR)/$(MACH32)
BUILD_DIR_32 =		$(BUILD_DIR)/32
#BUILD_DIR_64 =		$(BUILD_DIR)/$(MACH64)
BUILD_DIR_64 =		$(BUILD_DIR)/64

$(BUILD_DIR_32)/.configured:	BITS=32
$(BUILD_DIR_64)/.configured:	BITS=64

BUILD_NO_ARCH =		$(BUILD_DIR_NO_ARCH)/.built
BUILD_32 =		$(BUILD_DIR_32)/.built
BUILD_64 =		$(BUILD_DIR_64)/.built
BUILD_32_and_64 =	$(BUILD_32) $(BUILD_64)
# NO_ARCH uses BITS=32 because some path setting macros use $(BITS)
$(BUILD_DIR_NO_ARCH)/.built:	BITS=32
$(BUILD_DIR_32)/.built:		BITS=32
$(BUILD_DIR_64)/.built:		BITS=64

INSTALL_NO_ARCH =	$(BUILD_DIR_NO_ARCH)/.installed
INSTALL_32 =		$(BUILD_DIR_32)/.installed
INSTALL_64 =		$(BUILD_DIR_64)/.installed
INSTALL_32_and_64 =	$(INSTALL_32) $(INSTALL_64)
$(BUILD_DIR_NO_ARCH)/.installed:  BITS=32
$(BUILD_DIR_32)/.installed:       BITS=32
$(BUILD_DIR_64)/.installed:       BITS=64

# set the default target for installation of the component
COMPONENT_INSTALL_TARGETS =	install

# set the default build test results directory
COMPONENT_TEST_BUILD_DIR =	$(BUILD_DIR)/test/$(MACH$(BITS))

# set the default master test results directory
COMPONENT_TEST_RESULTS_DIR =	$(COMPONENT_DIR)/test
COMPONENT_SYSTEM_TEST_RESULTS_DIR =	$(COMPONENT_DIR)/test

# set the default master test results file
COMPONENT_TEST_MASTER =		$(COMPONENT_TEST_RESULTS_DIR)/results-$(BITS).master

# set the default test results output file
COMPONENT_TEST_OUTPUT =		$(COMPONENT_TEST_BUILD_DIR)/test-$(BITS)-results

# set the default test results comparison diffs file
COMPONENT_TEST_DIFFS =		$(COMPONENT_TEST_BUILD_DIR)/test-$(BITS)-diffs

# set the default test snapshot file
COMPONENT_TEST_SNAPSHOT =	$(COMPONENT_TEST_BUILD_DIR)/results-$(BITS).snapshot

# Normally $(GSED) is simplest, but some results files need more power.
COMPONENT_TEST_TRANSFORMER =	$(GSED)

# The set of default transforms to be applied to the test results to try
# to normalize them.
COMPONENT_TEST_TRANSFORMS = \
	'-e "s|$(@D)|\\$$(@D)|g" ' \
	'-e "s|$(PERL)|\\$$(PERL)|g" ' \
	'-e "s|$(SOURCE_DIR)|\\$$(SOURCE_DIR)|g" '

# set the default commands used to generate the file containing the set
# of transforms to be applied to the test results to try to normalize them.
COMPONENT_TEST_CREATE_TRANSFORMS = \
	@if [ -e $(COMPONENT_TEST_MASTER) ]; \
	then \
		print "\#!/bin/sh" > $(COMPONENT_TEST_TRANSFORM_CMD); \
        	print '$(COMPONENT_TEST_TRANSFORMER) ' \
			$(COMPONENT_TEST_TRANSFORMS) \
                	' \\' >> $(COMPONENT_TEST_TRANSFORM_CMD); \
        	print '$(COMPONENT_TEST_OUTPUT) \\' \
                	>> $(COMPONENT_TEST_TRANSFORM_CMD); \
        	print '> $(COMPONENT_TEST_SNAPSHOT)' \
                	>> $(COMPONENT_TEST_TRANSFORM_CMD); \
	else \
		print 'Cannot find $(COMPONENT_TEST_MASTER)'; \
		exit 2; \
	fi

# set the default command for performing any test result munging
COMPONENT_TEST_TRANSFORM_CMD =	$(COMPONENT_TEST_BUILD_DIR)/transform-$(BITS)-results

# set the default operation to run to perform test result normalization
COMPONENT_TEST_PERFORM_TRANSFORM = \
	@if [ -e $(COMPONENT_TEST_MASTER) ]; \
	then \
		$(SHELL) $(COMPONENT_TEST_TRANSFORM_CMD); \
	else \
		print 'Cannot find $(COMPONENT_TEST_MASTER)'; \
		exit 2; \
	fi

# set the default command used to compare the master results with the snapshot
COMPONENT_TEST_COMPARE_CMD =	$(GDIFF) -uN

# set the default way that master and snapshot test results are compared
COMPONENT_TEST_COMPARE = \
	@if [ -e $(COMPONENT_TEST_MASTER) ]; \
	then \
		$(COMPONENT_TEST_COMPARE_CMD) \
			$(COMPONENT_TEST_MASTER) $(COMPONENT_TEST_SNAPSHOT) \
			> $(COMPONENT_TEST_DIFFS); \
		print "Test results in $(COMPONENT_TEST_OUTPUT)"; \
		if [ -s $(COMPONENT_TEST_DIFFS) ]; \
		then \
			print "Differences found."; \
			$(CAT) $(COMPONENT_TEST_DIFFS); \
			exit 2; \
		else \
			print "No differences found."; \
		fi \
	else \
		print 'Cannot find $(COMPONENT_TEST_MASTER)'; \
		exit 2; \
	fi

# set the default env command to use for test of the component
COMPONENT_TEST_ENV_CMD =        $(ENV)
COMPONENT_SYSTEM_TEST_ENV_CMD =	$(ENV)

# set the default command to use for test of the component
COMPONENT_TEST_CMD =		$(GMAKE)
COMPONENT_SYSTEM_TEST_CMD =	$(GMAKE)

# set the default target for test of the component
COMPONENT_TEST_TARGETS =	check
COMPONENT_SYSTEM_TEST_TARGETS =	check

# set the default directory for test of the component
COMPONENT_TEST_DIR =		$(@D)
COMPONENT_SYSTEM_TEST_DIR =	$(@D)

# determine the type of tests we want to run.
ifeq ($(strip $(wildcard $(COMPONENT_SYSTEM_TEST_RESULTS_DIR)/results-*.master)),)
SYSTEM_TEST_NO_ARCH =		$(BUILD_DIR_NO_ARCH)/.system-tested
SYSTEM_TEST_32 =		$(BUILD_DIR_32)/.system-tested
SYSTEM_TEST_64 =		$(BUILD_DIR_64)/.system-tested
else
SYSTEM_TEST_NO_ARCH =		$(BUILD_DIR_NO_ARCH)/.system-tested-and-compared
SYSTEM_TEST_32 =		$(BUILD_DIR_32)/.system-tested-and-compared
SYSTEM_TEST_64 =		$(BUILD_DIR_64)/.system-tested-and-compared
endif
SYSTEM_TEST_32_and_64 =	$(SYSTEM_TEST_32) $(SYSTEM_TEST_64)
ifeq ($(strip $(wildcard $(COMPONENT_TEST_RESULTS_DIR)/results-*.master)),)
TEST_NO_ARCH =		$(BUILD_DIR_NO_ARCH)/.tested
TEST_32 =		$(BUILD_DIR_32)/.tested
TEST_64 =		$(BUILD_DIR_64)/.tested
else
TEST_NO_ARCH =		$(BUILD_DIR_NO_ARCH)/.tested-and-compared
TEST_32 =		$(BUILD_DIR_32)/.tested-and-compared
TEST_64 =		$(BUILD_DIR_64)/.tested-and-compared
endif
TEST_32_and_64 =	$(TEST_32) $(TEST_64)
$(BUILD_DIR_32)/.tested:       BITS=32
$(BUILD_DIR_64)/.tested:       BITS=64

# When running tests at the top level, skip those tests,
# by redefining the above TEST_* targets,
# when a component Makefile includes $(SKIP_TEST_AT_TOP_LEVEL).
# It's done in separate skip-test.mk file, to allow inclusion of 
# a multi-line ifdef statement which is evaluated at the component
# Makefile level

SKIP_TEST_AT_TOP_LEVEL = $(eval include $(WS_MAKE_RULES)/skip-test.mk)

$(BUILD_DIR_NO_ARCH)/.system-tested:			BITS=32
$(BUILD_DIR_32)/.system-tested:				BITS=32
$(BUILD_DIR_64)/.system-tested:				BITS=64
$(BUILD_DIR_NO_ARCH)/.system-tested-and-compared:	BITS=32
$(BUILD_DIR_32)/.system-tested-and-compared:		BITS=32
$(BUILD_DIR_64)/.system-tested-and-compared:		BITS=64
$(BUILD_DIR_NO_ARCH)/.tested:				BITS=32
$(BUILD_DIR_32)/.tested:				BITS=32
$(BUILD_DIR_64)/.tested:				BITS=64
$(BUILD_DIR_NO_ARCH)/.tested-and-compared:		BITS=32
$(BUILD_DIR_32)/.tested-and-compared:			BITS=32
$(BUILD_DIR_64)/.tested-and-compared:			BITS=64

$(BUILD_DIR_NO_ARCH)/.system-tested:			$(BUILD_DIR_32)
$(BUILD_DIR_32)/.system-tested:				$(BUILD_DIR_32)
$(BUILD_DIR_64)/.system-tested:				$(BUILD_DIR_64)
$(BUILD_DIR_NO_ARCH)/.system-tested-and-compared:	$(BUILD_DIR_32)
$(BUILD_DIR_32)/.system-tested-and-compared:		$(BUILD_DIR_32)
$(BUILD_DIR_64)/.system-tested-and-compared:		$(BUILD_DIR_64)

$(BUILD_DIR_32) $(BUILD_DIR_64):
	$(MKDIR) $(@)

# set the default target for test of the component
COMPONENT_TEST_TARGETS =	check

# BUILD_TOOLS is the root of all tools not normally installed on the system.
#BUILD_TOOLS ?=	/ws/on11update-tools
BUILD_TOOLS ?=		/opt

#SPRO_ROOT =		$(BUILD_TOOLS)/SUNWspro
SPRO_ROOT =		$(BUILD_TOOLS)
SPRO_VROOT =		$(SPRO_ROOT)/sunstudio12.1
#SPRO_VROOT =		$(SPRO_ROOT)/solstudio12.2
#SPRO_VROOT =		$(SPRO_ROOT)/solarisstudio12.3

PARFAIT_ROOT =	$(BUILD_TOOLS)/parfait/parfait-tools-1.2.0.2
PARFAIT_TOOLS=	$(WS_TOOLS)/$(MACH)/parfait
PARFAIT= $(PARFAIT_ROOT)/bin/parfait
export PARFAIT_NATIVESUNCC=$(SPRO_VROOT)/bin/cc
export PARFAIT_NATIVESUNCXX=$(SPRO_VROOT)/bin/CC
export PARFAIT_NATIVEGCC=$(GCC_ROOT)/bin/gcc
export PARFAIT_NATIVEGXX=$(GCC_ROOT)/bin/g++

GCC_VERSION ?= 		6
GCC_ROOT ?=		/usr/gcc/$(GCC_VERSION)

CC.studio.32 =		$(SPRO_VROOT)/bin/cc
CXX.studio.32 =		$(SPRO_VROOT)/bin/CC

CC.studio.64 =		$(SPRO_VROOT)/bin/cc
CXX.studio.64 =		$(SPRO_VROOT)/bin/CC

CC.gcc.32 =		$(GCC_ROOT)/bin/gcc
CXX.gcc.32 =		$(GCC_ROOT)/bin/g++

CC.gcc.64 =		$(GCC_ROOT)/bin/gcc
CXX.gcc.64 =		$(GCC_ROOT)/bin/g++

CC =			$(CC.$(COMPILER).$(BITS))
CXX =			$(CXX.$(COMPILER).$(BITS))

lint.32 =		$(SPRO_VROOT)/bin/lint -m32
lint.64 =		$(SPRO_VROOT)/bin/lint -m64

LINT =			$(lint.$(BITS))

LD =			/usr/bin/ld

ifeq   ($(strip $(PARFAIT_BUILD)),yes)
CC.studio.32 =	$(WS_TOOLS)/parfait/cc
CXX.studio.32 =	$(WS_TOOLS)/parfait/CC
CC.studio.64 =	$(WS_TOOLS)/parfait/cc
CXX.studio.64 =	$(WS_TOOLS)/parfait/CC
CC.gcc.32 =	$(WS_TOOLS)/parfait/gcc
CXX.gcc.32 =	$(WS_TOOLS)/parfait/g++
CC.gcc.64 =	$(WS_TOOLS)/parfait/gcc
CXX.gcc.64 =	$(WS_TOOLS)/parfait/g++
LD =		$(WS_TOOLS)/parfait/ld
endif

CC =		$(CC.$(COMPILER).$(BITS))
CXX =		$(CXX.$(COMPILER).$(BITS))

RUBY_VERSION =	1.9
RUBY_LIB_VERSION =	1.9.1
RUBY.1.8 =	/usr/ruby/1.8/bin/ruby
RUBY.1.9 =	/usr/ruby/1.9/bin/ruby
RUBY =		$(RUBY.$(RUBY_VERSION))
# Use the ruby lib versions to represent the RUBY_VERSIONS that
# need to get built.  This is done because during package transformations
# both the ruby version and the ruby library version are needed. 
RUBY_VERSIONS = $(RUBY_LIB_VERSION)

PYTHON.2.6.VENDOR_PACKAGES.32 = /usr/lib/python2.6/vendor-packages
PYTHON.2.6.VENDOR_PACKAGES.64 = /usr/lib/python2.6/vendor-packages/64
PYTHON.2.6.VENDOR_PACKAGES = $(PYTHON.2.6.VENDOR_PACKAGES.$(BITS))

PYTHON.2.7.VENDOR_PACKAGES.32 = /usr/lib/python2.7/vendor-packages
PYTHON.2.7.VENDOR_PACKAGES.64 = /usr/lib/python2.7/vendor-packages/64
PYTHON.2.7.VENDOR_PACKAGES = $(PYTHON.2.7.VENDOR_PACKAGES.$(BITS))

PYTHON.3.4.VENDOR_PACKAGES.64 = /usr/lib/python3.4/vendor-packages/64
PYTHON.3.4.VENDOR_PACKAGES = $(PYTHON.3.4.VENDOR_PACKAGES.$(BITS))

PYTHON.3.5.VENDOR_PACKAGES.64 = /usr/lib/python3.5/vendor-packages
PYTHON.3.5.VENDOR_PACKAGES = $(PYTHON.3.5.VENDOR_PACKAGES.$(BITS))

PYTHON_VENDOR_PACKAGES.32 = /usr/lib/python$(PYTHON_VERSION)/vendor-packages
PYTHON_VENDOR_PACKAGES.64 = /usr/lib/python$(PYTHON_VERSION)/vendor-packages/64
PYTHON_VENDOR_PACKAGES = $(PYTHON_VENDOR_PACKAGES.$(BITS))

PYTHON.2.6.32 =	/usr/bin/python2.6
PYTHON.2.6.64 =	/usr/bin/$(MACH64)/python2.6

PYTHON.2.7.32 =	/usr/bin/python2.7
PYTHON.2.7.64 =	/usr/bin/$(MACH64)/python2.7

PYTHON.32 =	$(PYTHON.$(PYTHON_VERSION).32)
PYTHON.64 =	$(PYTHON.$(PYTHON_VERSION).64)
PYTHON =	$(PYTHON.$(PYTHON_VERSION).$(BITS))

# The default is site-packages, but that directory belongs to the end-user.
# Modules which are shipped by the OS but not with the core Python distribution
# belong in vendor-packages.
PYTHON_LIB= /usr/lib/python$(PYTHON_VERSION)/vendor-packages
PYTHON_DATA= $(PYTHON_LIB)

JAVA8_HOME =	/usr/jdk/instances/jdk1.8.0
JAVA7_HOME =	/usr/jdk/instances/jdk1.7.0
JAVA6_HOME =	/usr/jdk/instances/jdk1.6.0
JAVA_HOME ?= $(JAVA6_HOME)

# This is the default BUILD version of perl
# Not necessarily the system's default version, i.e. /usr/bin/perl
#PERL_VERSION =  5.16
PERL_VERSION =  5.24

#PERL_VERSIONS = 5.16 5.12
PERL_VERSIONS = 5.24
#PERL_VERSION.64 = 5.16
PERL_VERSION.64 = 5.24

#PERL.5.16 =	/usr/perl5/5.16/bin/perl
PERL.5.24 =     /usr/bin/perl

PERL.64 = 	$(PERL.5.24)
PERL =          $(PERL.$(PERL_VERSION))

PERL_ARCH :=	$(shell $(PERL) -e 'use Config; print $$Config{archname}')
#PERL_ARCH516 :=	$(shell $(PERL.5.16) -e 'use Config; print $$Config{archname}')
#PERL_ARCH64 := $(PERL_ARCH516)
PERL_ARCH_FUNC=	$(shell $(1) -e 'use Config; print $$Config{archname}')
P_ARCH :=	$(shell arch)
# Optimally we should ask perl which C compiler was used but it doesn't
# result in a full path name.  Only "c" is being recorded
# inside perl builds while we actually need a full path to
# the studio compiler.
#PERL_CC :=      $(shell $(PERL) -e 'use Config; print $$Config{cc}')
PERL_OPTIMIZE := $(shell $(PERL) -e 'use Config; print $$Config{optimize}')
#PERL_OPTIMIZE.5.16 := $(shell $(PERL.5.16) -e 'use Config; print $$Config{optimize}')
PERL_OPTIMIZE.5.24 := $(shell $(PERL.5.24) -e 'use Config; print $$Config{optimize}')

PKG_MACROS +=   PERL_ARCH=$(PERL_ARCH)
#PKG_MACROS +=   PERL_ARCH512=$(PERL_ARCH512)
#PKG_MACROS +=   PERL_ARCH516=$(PERL_ARCH516)
PKG_MACROS +=   P_ARCH=$(P_ARCH)
PKG_MACROS +=   PERL_VERSION=$(PERL_VERSION)
PKG_MACROS +=   PERL_VERSION.32=$(PERL_VERSION.32)
PKG_MACROS +=   PERL_VERSION.64=$(PERL_VERSION.64)

# This is the default BUILD version of tcl
# Not necessarily the system's default version, i.e. /usr/bin/tclsh
TCL_VERSION =  8.5
TCLSH.8.5.i386.32 =	/usr/bin/i86/tclsh8.5
TCLSH.8.5.i386.64 =	/usr/bin/amd64/tclsh8.5
TCLSH.8.5.sparc.32 =	/usr/bin/sparcv7/tclsh8.5
TCLSH.8.5.sparc.64 =	/usr/bin/sparcv9/tclsh8.5
TCLSH =		$(TCLSH.$(TCL_VERSION).$(MACH).$(BITS))

CCSMAKE =	/usr/bin/dmake
DMAKE =		/usr/bin/dmake
GMAKE =		/usr/bin/gmake
GPATCH =	/usr/bin/patch
PATCH_LEVEL =	1
GPATCH_BACKUP =	--backup --version-control=numbered
GPATCH_FLAGS =	-p$(PATCH_LEVEL) $(GPATCH_BACKUP)
GSED =		/usr/bin/sed
SED =		/usr/bin/gsed

PKGREPO =	/usr/bin/pkgrepo
PKGSEND =	/usr/bin/pkgsend
ifeq   ($(strip $(PKGLINT_COMPONENT)),)
PKGLINT =	/usr/bin/pkglint
else
PKGLINT =	$(WS_TOOLS)/pkglint
endif

ACLOCAL =	/usr/bin/aclocal
ACLOCAL-1.11 =	/usr/bin/aclocal-1.11
ACLOCAL-1.14 =	/usr/bin/aclocal-1.14
AUTOMAKE =	/usr/bin/automake
AUTOMAKE-1.11 =	/usr/bin/automake-1.11
AUTOMAKE-1.14 =	/usr/bin/automake-1.14
AUTORECONF = 	/usr/bin/autoreconf
AUTOCONF = 	/usr/bin/autoconf

KSH93 =         /usr/bin/ksh93
TOUCH =		/usr/bin/touch
MKDIR =		/usr/bin/mkdir -p
RM =		/usr/bin/rm -f
CP =		/usr/bin/cp -f
MV =		/usr/bin/mv -f
LN =		/usr/bin/ln
CAT =		/usr/bin/cat
SYMLINK =	/isr/bin/ln -s
#ENV =		/usr/bin/genv
ENV =		/usr/bin/env
INSTALL =	/usr/bin/install
CHMOD =		/usr/bin/chmod
NAWK =		/usr/bin/nawk
AWK =		/usr/bin/gawk
GAWK =		/usr/bin/gawk
TEE =		/usr/bin/tee
TAR =		/usr/bin/tar
FIND =		/usr/bin/find
GUNZIP =	/usr/bin/gunzip
GZIP =		/usr/bin/gzip
GREP =		/usr/bin/grep
ELFEDIT = 	/usr/bin/elfedit
#RPATH = 	$(WS_TOOLS)/rpath
GMSGFMT = 	/usr/gnu/bin/msgfmt
GM4 = 		/usr/bin/m4
XARGS = 	/usr/bin/xargs
CTFMERGE = 	/usr/bin/ctfmerge
CTFCONVERT = 	/usr/bin/ctfconvert
GSORT = 	/usr/bin/sort
SORT = 		/usr/bin/sort
OBJCOPY = 	/usr/bin/objcopy
OBJDUMP = 	/usr/bin/objdump
READELF = 	/usr/bin/readelf
NM = 		/usr/bin/nm
AR = 		/usr/bin/ar

INS.dir=        $(INSTALL) -d $@
INS.file=       $(INSTALL) -m 444 $< $(@D)

PKG_CONFIG_PATH.32 = /usr/lib/pkgconfig
PKG_CONFIG_PATH.64 = /usr/lib/$(MACH64)/pkgconfig
PKG_CONFIG_PATH = $(PKG_CONFIG_PATH.$(BITS))


#
# C preprocessor flag sets to ease feature selection.  Add the required
# feature to your Makefile with CPPFLAGS += $(FEATURE_MACRO) and add to
# the component build with CONFIGURE_OPTIONS += CPPFLAGS="$(CPPFLAGS)" or
# similiar.
#

# Enables visibility of some c99 math functions that aren't visible by default.
# What other side-effects are there?
CPP_C99_EXTENDED_MATH =	-D_STDC_99

# Enables large file support for components that have no other means of doing
# so.  Use CPP_LARGEFILES and not the .32/.64 variety directly
CPP_LARGEFILES.32 :=	$(shell getconf LFS_CFLAGS)
CPP_LARGEFILES.64 :=	$(shell getconf LFS64_CFLAGS)
CPP_LARGEFILES =		$(CPP_LARGEFILES.$(BITS))

# Enables some #pragma redefine_extname to POSIX-compliant Standard C Library
# functions. Also avoids the component's #define _POSIX_C_SOURCE to some value
# we currently do not support.
CPP_POSIX =	-D_POSIX_C_SOURCE=200112L -D_POSIX_PTHREAD_SEMANTICS

# XPG6 mode.  This option enables XPG6 conformance, plus extensions.
# Amongst other things, this option will cause system calls like
# popen (3C) and system (3C) to invoke the standards-conforming
# shell, /usr/xpg4/bin/sh, instead of /usr/bin/sh.  Add studio_XPG6MODE to
# CFLAGS instead of using this directly
CPP_XPG6MODE=	-D_XOPEN_SOURCE=600 -D__EXTENSIONS__=1 -D_XPG6

# XPG5 mode. These options are specific for C++, where _XPG6,
# _XOPEN_SOURCE=600 and C99 are illegal. -D__EXTENSIONS__=1 is legal in C++.
CPP_XPG5MODE=   -D_XOPEN_SOURCE=500 -D__EXTENSIONS__=1 -D_XPG5

#
# Studio C compiler flag sets to ease feature selection.  Add the required
# feature to your Makefile with CFLAGS += $(FEATURE_MACRO) and add to the
# component build with CONFIGURE_OPTIONS += CFLAGS="$(CFLAGS)" or similiar.
#

# Generate 32/64 bit objects
CC_BITS =	-m$(BITS)

# Code generation instruction set and optimization 'hints'.  Use studio_XBITS
# and not the .arch.bits variety directly.
studio_XBITS.sparc.32 =	-xtarget=ultra2 -xarch=sparcvis -xchip=ultra2
studio_XBITS.sparc.64 =	
ifneq   ($(strip $(PARFAIT_BUILD)),yes)
studio_XBITS.sparc.64 += -xtarget=ultra2
endif
studio_XBITS.sparc.64 += -xarch=sparcvis -xchip=ultra2
studio_XBITS.i386.32 =	-xchip=pentium
studio_XBITS.i386.64 =	-xchip=generic -Ui386 -U__i386
studio_XBITS = $(studio_XBITS.$(MACH).$(BITS))

# Turn on recognition of supported C99 language features and enable the 1999 C
# standard library semantics of routines that appear in	both the 1990 and
# 1999 C standard. To use set studio_C99MODE=$(studio_99_ENABLE) in your
# component Makefile.
studio_C99_ENABLE =		-xc99=all

# Turn off recognition of C99 language features, and do not enable the 1999 C
# standard library semantics of routines that appeared in both the 1990	and
# 1999 C standard.  To use set studio_C99MODE=$(studio_99_DISABLE) in your
# component Makefile.
studio_C99_DISABLE =	-xc99=none

# Use the compiler default 'xc99=all,no_lib'
studio_C99MODE =

# For C++, compatibility with C99 (which is technically illegal) is
# enabled in a different way. So, we must use a different macro for it.
studio_cplusplus_C99_ENABLE = 	-xlang=c99

# Turn it off.
studio_cplusplus_C99_DISABLE =

# And this is the macro you should actually use
studio_cplusplus_C99MODE = 

# Allow zero-sized struct/union declarations and void functions with return
# statements.
studio_FEATURES_EXTENSIONS =	-features=extensions

# Control the Studio optimization level.
studio_OPT.sparc.32 =	-xO4
studio_OPT.sparc.64 =	-xO4
studio_OPT.i386.32 =	-xO4
studio_OPT.i386.64 =	-xO4
studio_OPT =		$(studio_OPT.$(MACH).$(BITS))

# Studio PIC code generation.  Use CC_PIC instead to select PIC code generation.
studio_PIC = 	-KPIC -DPIC

# The Sun Studio 11 compiler has changed the behaviour of integer
# wrap arounds and so a flag is needed to use the legacy behaviour
# (without this flag panics/hangs could be exposed within the source).
# This is used through studio_IROPTS, not the 'sparc' variety.
studio_IROPTS.sparc =	-W2,-xwrap_int
studio_IROPTS =		$(studio_IROPTS.$(MACH))

# Control register usage for generated code.  SPARC ABI requires system
# libraries not to use application registers.  x86 requires 'no%frameptr' at
# x04 or higher.

# We should just use -xregs but we need to workaround 7030022. Note
# that we can't use the (documented) -Wc,-xregs workaround because
# libtool really hates -Wc and thinks it should be -Wl. Instead
# we use an (undocumented) option which actually happens to be what
# CC would use.
studio_XREGS.sparc =	-Qoption cg -xregs=no%appl
studio_XREGS.i386 =	-xregs=no%frameptr
studio_XREGS =		$(studio_XREGS.$(MACH))

gcc_XREGS.sparc =	-mno-app-regs
gcc_XREGS.i386 =
gcc_XREGS =		$(gcc_XREGS.$(MACH))

# Set data alignment on sparc to reasonable values, 8 byte alignment for 32 bit
# objects and 16 byte alignment for 64 bit objects.  This is added to CFLAGS by
# default.
studio_ALIGN.sparc.32 =	-xmemalign=8s
studio_ALIGN.sparc.64 =	-xmemalign=16s
studio_ALIGN =		$(studio_ALIGN.$(MACH).$(BITS))

# Studio shorthand for building multi-threaded code,  enables -D_REENTRANT and
# linking with threadin support.  This is added to CFLAGS by default, override
# studio_MT to turn this off.
studio_MT =		-mt

# See CPP_XPG6MODE comment above.
studio_XPG6MODE =	$(studio_C99MODE) $(CPP_XPG6MODE)
XPG6MODE =		$(studio_XPG6MODE)

# See CPP_XPG5MODE comment above. You can only use this in C++, not in C99.
studio_XPG5MODE =	$(studio_cplusplus_C99MODE) $(CPP_XPG5MODE)
XPG5MODE =		$(studio_XPG5MODE)

# Default Studio C compiler flags.  Add the required feature to your Makefile
# with CFLAGS += $(FEATURE_MACRO) and add to the component build with
# CONFIGURE_OPTIONS += CFLAGS="$(CFLAGS)" or similiar.  In most cases, it
# should not be necessary to add CFLAGS to any environment other than the
# configure environment.
CFLAGS.studio +=	$(studio_OPT) $(studio_XBITS) $(studio_XREGS) \
			$(studio_IROPTS) $(studio_C99MODE) $(studio_ALIGN) \
			$(studio_MT)

#
# GNU C compiler flag sets to ease feature selection.  Add the required
# feature to your Makefile with CFLAGS += $(FEATURE_MACRO) and add to the
# component build with CONFIGURE_OPTIONS += CFLAGS="$(CFLAGS)" or similiar.
#

# GCC Compiler optimization flag
gcc_OPT.sparc.32 ?=	-O3
gcc_OPT.sparc.64 ?=	-O3
gcc_OPT.i386.32 ?=	-O3
gcc_OPT.i386.64 ?=	-O3
gcc_OPT ?=		$(gcc_OPT.$(MACH).$(BITS))

# GCC PIC code generation.  Use CC_PIC instead to select PIC code generation.
gcc_PIC =	-fPIC -DPIC

# Generic macro for PIC code generation.  Use this macro instead of the
# compiler-specific variant.
CC_PIC =	$($(COMPILER)_PIC)


# Default GNU C compiler flags.  Add the required feature to your Makefile
# with CFLAGS += $(FEATURE_MACRO) and add to the component build with
# CONFIGURE_OPTIONS += CFLAGS="$(CFLAGS)" or similiar.  In most cases, it
# should not be necessary to add CFLAGS to any environment other than the
# configure environment.
CFLAGS.gcc +=	$(gcc_OPT)
CFLAGS.gcc +=	$(gcc_XREGS)

#CFLAGS.gcc.i386.64 += -msave-args

# Build 32 or 64 bit objects.
CFLAGS +=	$(CC_BITS)

# Add compiler-specific 'default' features
CFLAGS +=	$(CFLAGS.$(COMPILER))

# Add specific 'MACH' features
CFLAGS +=	$(CFLAGS.$(MACH))

# Add specific 'MACH' features
CFLAGS +=	$(CFLAGS.$(MACH).$(BITS))

# Add specific 'BITS' features
CFLAGS +=	$(CFLAGS.$(BITS))

# Add specific 'COMPILER'.'BITS' features
CFLAGS +=	$(CFLAGS.$(COMPILER).$(BITS))

# Add specific 'COMPILER'.'MACH'.'BITS' features
CFLAGS +=	$(CFLAGS.$(COMPILER).$(MACH).$(BITS))

# Studio C++ requires -norunpath to avoid adding its location into the RUNPATH
# to C++ applications.
studio_NORUNPATH =	 -norunpath

# To link in standard mode (the default mode) without any C++ libraries
# (except libCrun), use studio_LIBRARY_NONE in your component Makefile.
studio_LIBRARY_NONE =	 -library=%none

# Don't link C++ with any C++ Runtime or Standard C++ library
studio_CXXLIB_NONE =	-xnolib

# Link C++ with the Studio C++ Runtime and Standard C++ library.  This is the
# default for "standard" mode.
studio_CXXLIB_CSTD =	-library=Cstd,Crun

# link C++ with the Studio  C++ Runtime and Apache Standard C++ library
studio_CXXLIB_APACHE =	-library=stdcxx4,Crun

# Add the C++ ABI compatibility flags for older ABI compatibility.  The default
# is "standard mode" (-compat=5)
studio_COMPAT_VERSION_4 =	-compat=4

# Tell the compiler that we don't want the studio runpath added to the
# linker flags.  We never want the Studio location added to the RUNPATH.
CXXFLAGS +=	$($(COMPILER)_NORUNPATH)

# Build 32 or 64 bit objects in C++ as well.
CXXFLAGS +=	$(CC_BITS)

# Add compiler-specific 'default' features
CXXFLAGS +=	$(CXXFLAGS.$(COMPILER))
CXXFLAGS +=	$(CXXFLAGS.$(COMPILER).$(BITS))

#
# Solaris linker flag sets to ease feature selection.  Add the required
# feature to your Makefile with LDFLAGS += $(FEATURE_MACRO) and add to the
# component build with CONFIGURE_OPTIONS += LDFLAGS="$(LDFLAGS)" or similiar.
#

# set the bittedness that we want to link
LD_BITS =	-$(BITS)

# Reduce the symbol table size, effectively conflicting with -g.  We should
# get linker guidance here.
LD_Z_REDLOCSYM =	-z redlocsym

# Cause the linker to rescan archive libraries and resolve remaining unresolved
# symbols recursively until all symbols are resolved.  Components should be
# linking in the libraries they need, in the required order.  This should
# only be required if the component's native build is horribly broken.
LD_Z_RESCAN_NOW =	-z rescan-now

LD_Z_TEXT =		-z text

# make sure that -lc is always present when building shared objects.
LD_DEF_LIBS +=		-lc

# make sure all symbols are defined.
LD_Z_DEFS =		-z defs

# eliminate unreferenced dynamic dependencies
LD_Z_IGNORE =		-z ignore

# use direct binding
LD_B_DIRECT =		-Bdirect

# use generic macro names for enabling/disabling ASLR
ASLR_ENABLE = 		-z aslr=enable
ASLR_DISABLE = 		-z aslr=disable
ASLR_NOT_APPLICABLE = 	-z aslr=disable
ASLR_MODE = 		$(ASLR_DISABLE)

# by default, turn off Address Space Layout Randomization for ELF executables;
# to explicitly enable ASLR, set ASLR_MODE = $(ASLR_ENABLE)
# in that component's Makefile
LD_Z_ASLR =		$(ASLR_MODE)

#
# More Solaris linker flags that we want to be sure that everyone gets.  This
# is automatically added to the calling environment during the 'build' and
# 'install' phases of the component build.  Each individual feature can be
# turned off by adding FEATURE_MACRO= to the component Makefile.
#

# Create a non-executable stack when linking.
LD_MAP_NOEXSTK.i386 =	-M /usr/lib/ld/map.noexstk
LD_MAP_NOEXSTK.sparc =	-M /usr/lib/ld/map.noexstk

# Create a non-executable bss segment when linking.
LD_MAP_NOEXBSS =	-M /usr/lib/ld/map.noexbss

# Create a non-executable data segment when linking.  Due to PLT needs, the
# data segment must be executable on sparc, but the bss does not.
# see mapfile comments for more information
LD_MAP_NOEXDATA.i386 =	-M /usr/lib/ld/map.noexdata
LD_MAP_NOEXDATA.sparc =	$(LD_MAP_NOEXBSS)

# Page alignment
LD_MAP_PAGEALIGN =	-M /usr/lib/ld/map.pagealign

# Linker options to add when only building libraries
LD_OPTIONS_SO +=	$(LD_Z_TEXT) $(LD_Z_DEFS) $(LD_DEF_LIBS)

# Default linker options that everyone should get.  Do not add additional
# libraries to this macro, as it will apply to everything linked during the
# component build.
LD_OPTIONS +=	$(LD_MAP_NOEXSTK.$(MACH)) $(LD_MAP_NOEXDATA.$(MACH)) \
		$(LD_MAP_PAGEALIGN) $(LD_B_DIRECT) $(LD_Z_IGNORE)

# only used on executables
LD_EXEC_OPTIONS = $(LD_Z_ASLR)

# Environment variables and arguments passed into the build and install
# environment(s).  These are the initial settings.
COMPONENT_BUILD_ENV= \
    LD_OPTIONS="$(LD_OPTIONS)" \
    LD_OPTIONS_SO="$(LD_OPTIONS_SO)"

COMPONENT_INSTALL_ENV= \
    LD_OPTIONS="$(LD_OPTIONS)" \
    LD_OPTIONS_SO="$(LD_OPTIONS_SO)"

# Add any bit-specific settings
COMPONENT_BUILD_ENV += $(COMPONENT_BUILD_ENV.$(BITS))
COMPONENT_BUILD_ARGS += $(COMPONENT_BUILD_ARGS.$(BITS))
COMPONENT_INSTALL_ENV += $(COMPONENT_INSTALL_ENV.$(BITS))
COMPONENT_INSTALL_ARGS += $(COMPONENT_INSTALL_ARGS.$(BITS))

######################
DEF_JOBS ?= yes

JOBS ?= 1
ifeq ($(DEF_JOBS),yes)
    BUILD_JOBS = $(JOBS)
else
    BUILD_JOBS = 1
endif

COMPONENT_BUILD_ENV +=		LC_ALL="C"
#COMPONENT_BUILD_ENV += 		AWK="$(AWK)"
#COMPONENT_BUILD_ENV += 		GAWK="$(GAWK)"
#COMPONENT_BUILD_ENV += 		SED="$(GSED)"
COMPONENT_INSTALL_ENV += 	LC_ALL="C"
#COMPONENT_INSTALL_ENV += 	AWK="$(AWK)"
#COMPONENT_INSTALL_ENV += 	GAWK="$(GAWK)"
#COMPONENT_INSTALL_ENV += 	SED="$(GSED)"

GNU_ARCH_NAME32 = 		$(MACH)
GNU_ARCH_NAME64 = 		$(MACH64:amd64=x86_64)
#PARCH =				$(MACH:i386=$(GNU_ARCH_NAME32)-pc)
PARCH32 =			$(MACH:i386=$(GNU_ARCH_NAME32)-pc)
PARCH64 =			$(MACH64:amd64=$(GNU_ARCH_NAME64)-pc)
#GNU_ARCH =			$(PARCH:sparc=sparc-sun)-solaris$(SOLARIS_VERSION)
GNU_ARCH32 =			$(PARCH32:sparc=sparc-sun)-solaris$(SOLARIS_VERSION)
GNU_ARCH64 =			$(PARCH64:sparcv9=sparcv9-sun)-solaris$(SOLARIS_VERSION)

PKG_MACROS += 			GNU_ARCH_NAME32=$(GNU_ARCH_NAME32)
PKG_MACROS += 			GNU_ARCH_NAME64=$(GNU_ARCH_NAME64)
#PKG_MACROS += 			GNU_ARCH=$(GNU_ARCH)
PKG_MACROS += 			GNU_ARCH32=$(GNU_ARCH32)
PKG_MACROS += 			GNU_ARCH64=$(GNU_ARCH64)

# init CONFIGURE_ENV
CONFIGURE_ENV ?= CONFIG_SHELL="$(CONFIG_SHELL)"
CONFIGURE_ENV += LC_ALL="C"

CONFIGURE_ENV += $(CONFIGURE_ENV.$(BITS))
CONFIGURE_ENV += $(CONFIGURE_ENV.$(MACH))

CONFIGURE_DEFAULT_CPPFLAGS ?= yes
ifeq ($(CONFIGURE_DEFAULT_CPPFLAGS),yes)
CPPFLAGS += $(CPPFLAGS.$(BITS))
CPPFLAGS += $(CPPFLAGS.$(MACH))
CONFIGURE_ENV += CPPFLAGS="$(CPPFLAGS)"
endif

CONFIGURE_DEFAULT_LDFLAGS ?= yes
ifeq ($(CONFIGURE_DEFAULT_LDFLAGS),yes)
LDFLAGS += $(LDFLAGS.$(MACH))
LDFLAGS += $(LDFLAGS.$(BITS))
CONFIGURE_ENV += LDFLAGS="$(LDFLAGS)"
endif

CONFIGURE_DEFAULT_CC ?= yes
ifeq ($(CONFIGURE_DEFAULT_CC),yes)
CONFIGURE_ENV += CC="$(CC)"
CONFIGURE_ENV += CXX="$(CXX)"
endif

CONFIGURE_ENV += CFLAGS="$(CFLAGS)"
CONFIGURE_ENV += CXXFLAGS="$(CXXFLAGS)"
CONFIGURE_ENV += INSTALL="$(INSTALL)"
#CONFIGURE_ENV += AWK="$(AWK)"
#CONFIGURE_ENV += GAWK="$(GAWK)"
#CONFIGURE_ENV += SED="$(GSED)"
#CONFIGURE_ENV += NM="$(NM)"
#CONFIGURE_ENV += AR="$(AR)"
#CONFIGURE_ENV += PKG_CONFIG_PATH="$(PKG_CONFIG_PATH)"
CONFIGURE_ENV += PERL="$(PERL)"
#CONFIGURE_ENV += PERL="/usr/bin/perl"

# URL roots
APACHE_ARCHIVE=		http://archive.apache.org/dist
CODE_GOOGLE_P=		http://code.google.com/p
EC_ARCHIVE=		http://dlc.everycity.com/ec-userland/source-archives
MIRRORSERVICE_ROOT=	http://www.mirrorservice.org/sites
SOURCEFORGE_PROJECTS=	http://sourceforge.net/projects
SF_ROOT=		http://downloads.sourceforge.net
XMLSOFT_FTP_ROOT=	ftp://xmlsoft.org
XORG_ROOT=		http://xorg.freedesktop.org/releases/individual

# Shared project URLs
PROJECT_APACHE=		http://$(COMPONENT_NAME).apache.org/
PROJECT_CODE_GOOGLE_P=	$(CODE_GOOGLE_P)/$(COMPONENT_NAME)
PROJECT_SOURCEFORGE1=	http://$(COMPONENT_NAME).sourceforge.net/
PROJECT_SOURCEFORGE2=	$(SOURCEFORGE_PROJECTS)/$(COMPONENT_NAME)/

# Mirrors
MIRROR_APACHE=		$(MIRRORSERVICE_ROOT)/ftp.apache.org

# Shared download URLs
DOWNLOAD_APACHE=	$(MIRROR_APACHE)/$(COMPONENT_NAME)/$(COMPONENT_ARCHIVE)
DOWNLOAD_EC=		$(EC_ARCHIVE)/$(COMPONENT_ARCHIVE)
DOWNLOAD_GITHUB=	https://github.com/downloads/$(COMPONENT_NAME)/$(COMPONENT_NAME)/$(COMPONENT_ARCHIVE)
DOWNLOAD_GNU_FTP=	http://ftp.gnu.org/gnu/$(COMPONENT_NAME)/$(COMPONENT_ARCHIVE)
DOWNLOAD_GNU_SAVANNAH=	http://download.savannah.gnu.org/releases/$(COMPONENT_NAME)/$(COMPONENT_ARCHIVE)
DOWNLOAD_LIBRDF=	http://download.librdf.org/source/$(COMPONENT_ARCHIVE)
DOWNLOAD_PERL5=		http://www.cpan.org/src/5.0/$(COMPONENT_ARCHIVE)
DOWNLOAD_PHP_PECL=	http://pecl.php.net/get/$(COMPONENT_ARCHIVE)
DOWNLOAD_PHP_UK=	http://www.php.net/get/$(COMPONENT_ARCHIVE)/from/this/mirror
DOWNLOAD_SF=		$(SF_ROOT)/$(COMPONENT_NAME)/$(COMPONENT_ARCHIVE)
DOWNLOAD_SF_PROJECT=	$(SF_ROOT)/$(SF_PROJECT_NAME)/$(COMPONENT_ARCHIVE)
DOWNLOAD_VIDEOLAN=	http://download.videolan.org/pub/$(COMPONENT_NAME)/$(COMPONENT_VERSION)/$(COMPONENT_ARCHIVE)
DOWNLOAD_XIPH=		http://downloads.xiph.org/releases/$(COMPONENT_NAME)/$(COMPONENT_ARCHIVE)
DOWNLOAD_XMLSOFT=	$(XMLSOFT_FTP_ROOT)/$(COMPONENT_NAME)/$(COMPONENT_ARCHIVE)
DOWNLOAD_XORG_LIB=	$(XORG_ROOT)/lib/$(COMPONENT_ARCHIVE)
DOWNLOAD_XORG_PROTO=	$(XORG_ROOT)/proto/$(COMPONENT_ARCHIVE)
DOWNLOAD_XORG_UTIL=	$(XORG_ROOT)/util/$(COMPONENT_ARCHIVE)
DOWNLOAD_XORG_XCB=	$(XORG_ROOT)/xcb/$(COMPONENT_ARCHIVE)

######################

# declare these phony so that we avoid filesystem conflicts.
.PHONY:	prep build install publish deb debclean debclobber apt test clean clobber
#.PHONY:	prep build install publish test clean clobber

# If there are no tests to execute
NO_TESTS =	test-nothing
test-nothing:
	@echo "There are no tests available at this time."

# If the system tests are not implemented yet
SYSTEM_TESTS_NOT_IMPLEMENTED = no-sys-tests
no-sys-tests:
	@echo "The system test target is not yet implemented."

# There are tests, but we're skipping them.
SKIP_TEST =	skip-test
skip-test:
	@echo "Skipping tests"

# default behaviour for 'component-hook' target is to echo the component
# name and version information, but more complex behaviour can be implemented
# via command line setting of the COMPONENT_HOOK macro.
COMPONENT_HOOK ?=	echo $(COMPONENT_NAME) $(COMPONENT_VERSION)

component-hook:
	@$(COMPONENT_HOOK)

CLEAN_PATHS +=		$(BUILD_DIR)
CLOBBER_PATHS +=	$(PROTO_DIR)

$(BUILD_DIR) $(SOURCE_DIR) $(DEBIAN_DIR) $(PROTO_DIR) $(WS_DEBS):
	$(MKDIR) $@

include $(WS_MAKE_RULES)/environment.mk
