INSTDEPENDS += debhelper
INSTDEPENDS += po-debconf
INSTDEPENDS += po4a
# perl
INSTDEPENDS += python
INSTDEPENDS += python3
INSTDEPENDS += gettext
INSTDEPENDS += libintl-perl
# libqtgui4-perl
