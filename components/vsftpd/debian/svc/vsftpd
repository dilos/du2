#!/sbin/sh

# Copyright (c) 2012-2019, DilOS.
#
# Permission is hereby granted, free of charge, to any person obtaining a  copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the  rights
# to use, copy, modify, merge, publish,  distribute,  sublicense,  and/or  sell
# copies of the Software, and  to  permit  persons  to  whom  the  Software  is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall  be  included  in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY  KIND,  EXPRESS  OR
# IMPLIED, INCLUDING BUT NOT LIMITED  TO  THE  WARRANTIES  OF  MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT  SHALL  THE
# AUTHORS OR COPYRIGHT HOLDERS BE  LIABLE  FOR  ANY  CLAIM,  DAMAGES  OR  OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# smf_method(5) start/stop script

. /lib/svc/share/smf_include.sh

# Read command line arguments
method="$1"	# %m
instance="$2"	# %i

if [ -z ${SMF_FMRI} ]
then
	SMF_FMRI="svc:/network/vsftpd:${instance}"
fi

DAEMON=/usr/sbin/vsftpd

# Exit if the package is not installed
[ -x "$DAEMON" ] || exit 0

# Read configuration variable file if it is present
[ -r /etc/default/vsftpd ] && . /etc/default/vsftpd

vsftpd_start()
{
	local	args
	local	o

#	[ -f ${CONFFILE} ] || cp -f ${CONFFILE}.sample ${CONFFILE}
#	[ -f $CONFIG_FILE ] && args="-c $CONFIG_FILE"
	for o in ${OPTIONS} background=1; do
		args="${args} -o${o}"
	done
	${DAEMON} ${args}
}

case "$method" in
start)
	vsftpd_start
	;;

stop)
	pkill -INT -f ${DAEMON}
	;;

reload|refresh)
	pkill -HUP -f ${DAEMON}
	;;

*)
	echo "Usage: $0 [start|stop|refresh]"
	exit 1
	;;
esac
