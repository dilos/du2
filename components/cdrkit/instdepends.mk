INSTDEPENDS += debhelper
INSTDEPENDS += libbz2-dev
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += autotools-dev
INSTDEPENDS += cmake
# libcap2-dev [linux-any]
# libcam-dev [kfreebsd-any]
INSTDEPENDS += libmagic-dev
#
INSTDEPENDS += libsocket-dev
INSTDEPENDS += libnsl-dev
INSTDEPENDS += libxnet-dev
INSTDEPENDS += libvolmgt-dev
