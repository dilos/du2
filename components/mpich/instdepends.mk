INSTDEPENDS += debhelper
INSTDEPENDS += automake
INSTDEPENDS += dh-autoreconf
# gfortran
INSTDEPENDS += hwloc-nox
# libcr-dev [amd64 armel armhf i386 powerpc ppc64 powerpcspe],
INSTDEPENDS += libhwloc-dev
INSTDEPENDS += libxt-dev
# procps
INSTDEPENDS += quilt
INSTDEPENDS += txt2man
# valgrind [i386 amd64 powerpc armhf],
INSTDEPENDS += x11proto-core-dev
#Build-Depends-Indep:
INSTDEPENDS += texlive-latex-extra
INSTDEPENDS += texlive-latex-recommended
INSTDEPENDS += doxygen-latex
