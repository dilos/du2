#!/bin/sh

# This is a template file for libmpich-dev.postinst, since it will be processed
# by debian/rules. The update-alternatives command below needs the path to the
# lib*.so files, which depends on the multiarch triplet. This can be gotten
# from dpkg-architecture, but to avoid dependence on dpkg-dev, we instead
# determine it at package build-time, and include a processed version of this
# file as the libmpich-dev.postinst, where the variable triplet has been replaced
# by its appropriate value, depending on the build host architecture.

set -e

case "$1" in
   configure)
      # Continue below
      ;;
   abort-upgrade|abort-remove|abort-deconfigure)
      exit 0;
      ;;
   *)
      echo "postinst called with unknown argument \`$1'" >&2
      exit 0;
      ;;
esac

#echo "DEBUG: postinst in libmpich-dev "

update-alternatives \
	--install /usr/include/mpi mpi /usr/include/mpich 40 \
	--slave /usr/lib/libmpi.so libmpi.so /usr/lib/TRIPLET/libmpich.so \
	--slave /usr/lib/libmpi++.so libmpi++.so /usr/lib/TRIPLET/libmpichcxx.so \
	--slave /usr/lib/libmpicxx.so libmpicxx.so /usr/lib/TRIPLET/libmpichcxx.so \
	--slave /usr/lib/libmpifort.so libmpifort.so /usr/lib/TRIPLET/libmpichfort.so \
	--slave /usr/lib/libmpif77.so libmpif77.so /usr/lib/TRIPLET/libmpichfort.so \
	--slave /usr/lib/libmpif90.so libmpif90.so /usr/lib/TRIPLET/libmpichfort.so \
	--slave /usr/bin/mpicc mpicc /usr/bin/mpicc.mpich \
	--slave /usr/bin/mpic++ mpic++ /usr/bin/mpicxx.mpich \
	--slave /usr/bin/mpicxx mpicxx /usr/bin/mpicxx.mpich \
	--slave /usr/bin/mpiCC mpiCC /usr/bin/mpicxx.mpich \
	--slave /usr/bin/mpifort mpifort /usr/bin/mpifort.mpich \
	--slave /usr/bin/mpif77 mpif77 /usr/bin/mpifort.mpich \
	--slave /usr/bin/mpif90 mpif90 /usr/bin/mpifort.mpich \
	--slave /usr/share/man/man1/mpicc.1.gz mpicc.1.gz /usr/share/man/man1/mpicc.mpich.1.gz \
	--slave /usr/share/man/man1/mpic++.1.gz mpic++.1.gz /usr/share/man/man1/mpicxx.mpich.1.gz \
	--slave /usr/share/man/man1/mpicxx.1.gz mpicxx.1.gz /usr/share/man/man1/mpicxx.mpich.1.gz \
	--slave /usr/share/man/man1/mpiCC.1.gz mpiCC.1.gz /usr/share/man/man1/mpicxx.mpich.1.gz \
	--slave /usr/share/man/man1/mpif77.1.gz mpif77.1.gz /usr/share/man/man1/mpif77.mpich.1.gz \
	--slave /usr/share/man/man1/mpif90.1.gz mpif90.1.gz /usr/share/man/man1/mpif90.mpich.1.gz \
	--slave /usr/share/man/man1/mpifort.1.gz mpifort.1.gz /usr/share/man/man1/mpifort.mpich.1.gz \
	--slave /usr/lib/TRIPLET/pkgconfig/mpi.pc mpi.pc /usr/lib/TRIPLET/pkgconfig/mpich.pc

#DEBHELPER#

exit 0
