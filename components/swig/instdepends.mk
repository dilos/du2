INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += bison
INSTDEPENDS += dh-autoreconf
# default-jdk [!hppa !mips !mipsel !alpha !arm !hurd-i386],
INSTDEPENDS += guile-2.0-dev
INSTDEPENDS += libchicken-dev
INSTDEPENDS += libperl-dev
INSTDEPENDS += libpcre3-dev
INSTDEPENDS += python-dev
INSTDEPENDS += ruby
INSTDEPENDS += ruby-dev
INSTDEPENDS += tcl-dev
INSTDEPENDS += tk-dev
