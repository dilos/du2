INSTDEPENDS += debhelper
INSTDEPENDS += cdbs
INSTDEPENDS += libxcb1-dev
INSTDEPENDS += libxcb-util0-dev
INSTDEPENDS += libxcb-shm0-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += x11proto-core-dev
INSTDEPENDS += xutils-dev
