#!/sbin/sh
#
# Copyright (c) 2012-2019, DilOS.
#
# Permission is hereby granted, free of charge, to any person obtaining a  copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the  rights
# to use, copy, modify, merge, publish,  distribute,  sublicense,  and/or  sell
# copies of the Software, and  to  permit  persons  to  whom  the  Software  is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall  be  included  in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY  KIND,  EXPRESS  OR
# IMPLIED, INCLUDING BUT NOT LIMITED  TO  THE  WARRANTIES  OF  MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT  SHALL  THE
# AUTHORS OR COPYRIGHT HOLDERS BE  LIABLE  FOR  ANY  CLAIM,  DAMAGES  OR  OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

. /lib/svc/share/smf_include.sh

ETCDIR=/etc/postgresql
HOMEDIR=/usr/lib/postgresql
DATADIR=/var/lib/postgresql

# SMF_FMRI is the name of the target service. This allows multiple instances 
# to use the same script.
getproparg() {
        val=`svcprop -p $1 $SMF_FMRI`
        [ -n "$val" ] && echo $val
}

check_data_dir() {
	if [ ! -f "$PGCONF" ]; then
		[ ! -d $ETCDIR/$PGVERS ] && (mkdir -p $ETCDIR/$PGVERS || id -a)
		cd $PGBIN && pg_createcluster $PGVERS $PGNAME || exit $SMF_EXIT_ERR
	fi

	if [ ! -d $PGDATA/base -o ! -d $PGDATA/global -o ! -f $PGDATA/PG_VERSION ]; then
		if [ `ls -a $PGDATA | wc -w` -le 2 ]; then
			echo "Error: config/data directory $PGDATA is empty, but it should be initialized"
			echo "Hint : check your mountpoints"
		else
			echo "Error: config/data directory $PGDATA is not empty, nor is it a valid PostgreSQL data directory"
		fi
		exit $SMF_EXIT_ERR_CONFIG
	fi

	if [ ! -d /var/run/postgresql ]; then
		mkdir -p /var/run/postgresql
		chown -R postgres:postgres /var/run/postgresql
	fi
}


PGVERS=`getproparg config/version`
PGNAME=`getproparg config/cluster_name`
PGCFG=`getproparg config/file`

if [ -z $SMF_FMRI ]; then
	echo "Error: SMF framework variables are not initialized"
	exit $SMF_EXIT_ERR
fi

if [ -z $PGVERS ]; then
        echo "Error: config/version property not set"
        exit $SMF_EXIT_ERR_CONFIG
fi

if [ -z $PGNAME ]; then
        echo "Error: config/cluster_name property not set"
        exit $SMF_EXIT_ERR_CONFIG
fi

if [ -z $PGCFG ]; then
        echo "Error: config/file property not set"
        exit $SMF_EXIT_ERR_CONFIG
fi

PGBIN=$HOMEDIR/$PGVERS/bin
PGDATA=$DATADIR/$PGVERS/$PGNAME
PGCONF=$ETCDIR/$PGVERS/$PGNAME/$PGCFG

case "$1" in
'start')
	check_data_dir
        pg_ctlcluster $PGVERS $PGNAME start
        ;;

'stop')
        pg_ctlcluster $PGVERS $PGNAME stop
#        $PGBIN/pg_ctl -D $PGDATA -m fast stop
        ;;

'refresh')
#        $PGBIN/pg_ctl -D $PGDATA reload
        pg_ctlcluster $PGVERS $PGNAME reload
        ;;

*)
        echo "Usage: $0 {start|stop|refresh}"
        exit 1
        ;;

esac
exit $SMF_EXIT_OK
