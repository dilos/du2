Source: fig2dev
Section: graphics
Priority: optional
Maintainer: Roland Rosenfeld <roland@debian.org>
Standards-Version: 3.9.8
Build-Depends: debhelper (>= 9),
               dh-autoreconf,
               etoolbox,
               gawk,
               ghostscript,
               latex-beamer,
               libpng-dev,
               libxpm-dev (>= 1:3.5.4.2),
               netpbm,
               texlive-font-utils,
               texlive-fonts-recommended,
               texlive-lang-german,
               texlive-latex-base,
               texlive-latex-recommended,
               texlive-pictures (>= 2013.20140314) | pgf,
               xutils-dev,
               libiconv-dev [solaris-any]
Homepage: https://sourceforge.net/projects/mcj/

Package: fig2dev
Architecture: any
Depends: gawk, x11-common, ${misc:Depends}, ${shlibs:Depends}
Recommends: ghostscript, netpbm (>= 2:10.0-4)
Replaces: transfig (<< 1:3.2.6~beta-1~)
Breaks: transfig (<< 1:3.2.6~beta-1~)
Provides: transfig
Suggests: xfig
Multi-Arch: foreign
Description: Utilities for converting XFig figure files
 This package contains utilities (mainly fig2dev) to handle XFig
 (Facility for Interactive Generation of figures) files.
 .
 It can convert files produced by xfig to box, cgm, dxf, epic, eepic,
 eepicemu, emf, eps, gbx, ge, gif, ibmgl, jpeg, latex, map (HTML image
 map), mf (MetaFont), mp (MetaPost), mmp (Multi-Meta-Post), pcx, pdf,
 pdftex, pdftex_t, pic, pict2e, pictex, png, ppm, ps, pstex, pstex_t,
 pstricks, ptk (Perl/tk), shape, sld (AutoCad slide format), svg,
 textyl, tiff, tikz, tk (Tcl/Tk), tpic, xbm and xpm.

Package: transfig
Depends: fig2dev, ${misc:Depends}
Architecture: all
Priority: extra
Section: oldlibs
Description: transitional dummy package for fig2dev
 This is a transitional dummy package. It can safely be removed.
