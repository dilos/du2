Source: python-nacl
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: Tristan Seligmann <mithrandi@debian.org>, Colin Watson <cjwatson@debian.org>,
 Scott Kitterman <scott@kitterman.com>
Section: python
Priority: optional
Build-Depends:
 debhelper (>= 9),
 dh-python,
 libsodium-dev (>= 1.0.16),
 python-all-dev,
 python-cffi (>= 1.0.0),
 python-hypothesis (>= 3.44.1),
 python-pytest,
 python-setuptools,
 python-six,
 python3-all-dev,
 python3-cffi (>= 1.0.0),
 python3-hypothesis (>= 3.44.1),
 python3-pytest,
 python3-setuptools,
 python3-six,
 python3-sphinx,
Standards-Version: 4.1.3
Homepage: https://github.com/pyca/pynacl/
Vcs-Git: https://salsa.debian.org/python-team/modules/python-nacl
Vcs-Browser: https://salsa.debian.org/python-team/modules/python-nacl
XS-Python-Version: >= 2.7
X-Python3-Version: >= 3.3

Package: python-nacl
Architecture: any
Depends: ${misc:Depends}, ${python:Depends}, ${shlibs:Depends}
Suggests: python-nacl-doc
Description: Python bindings to libsodium (Python 2)
 PyNaCl is a Python binding to the Networking and Cryptography library (in the
 form of libsodium), a crypto library with the stated goal of improving
 usability, security and speed.
 .
 This package contains the Python 2 version of pynacl.

Package: python3-nacl
Architecture: any
Depends: ${misc:Depends}, ${python3:Depends}, ${shlibs:Depends}
Suggests: python-nacl-doc
Description: Python bindings to libsodium (Python 3)
 PyNaCl is a Python binding to the Networking and Cryptography library (in the
 form of libsodium), a crypto library with the stated goal of improving
 usability, security and speed.
 .
 This package contains the Python 3 version of pynacl.

Package: python-nacl-doc
Architecture: all
Depends: ${misc:Depends}, libjs-sphinxdoc
Section: doc
Description: Python bindings to libsodium (documentation)
 PyNaCl is a Python binding to the Networking and Cryptography library (in the
 form of libsodium), a crypto library with the stated goal of improving
 usability, security and speed.
 .
 This package contains the documentation for pynacl.
