python-nacl (1.2.1-3~bpo9+3+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- DilOS Team <dilos@dilos.org>  Sat, 13 Oct 2018 17:02:37 +0300

python-nacl (1.2.1-3~bpo9+3) stretch-backports; urgency=medium

  * Bump the Python 2 hypothesis dependency (Closes: #902825).

 -- Andrej Shadura <andrewsh@debian.org>  Fri, 20 Jul 2018 20:20:15 +0200

python-nacl (1.2.1-3~bpo9+2) stretch-backports; urgency=medium

  * Build-depends on libsodium from stretch-backports.

 -- Andrej Shadura <andrewsh@debian.org>  Thu, 19 Jul 2018 15:15:05 +0200

python-nacl (1.2.1-3~bpo9+1) stretch-backports; urgency=medium

  * Rebuild for stretch-backports.

 -- Andrej Shadura <andrewsh@debian.org>  Thu, 07 Jun 2018 17:50:38 +0200

python-nacl (1.2.1-3) unstable; urgency=medium

  * Update to also set timeout=hypothesis.unlimited to further improve the
    odds of test success on slow architectures (this is the planned default
    for Hypothesis, so it is forwawrd compatible)

 -- Scott Kitterman <scott@kitterman.com>  Tue, 13 Mar 2018 18:31:45 -0400

python-nacl (1.2.1-2) unstable; urgency=medium

  * Add debian/patches/0001-bump-hypothesis-timeouts-for-slow-archs.patch to
    add time for tests to complete on slower architectures 

 -- Scott Kitterman <scott@kitterman.com>  Mon, 12 Mar 2018 22:38:19 -0400

python-nacl (1.2.1-1) unstable; urgency=medium

  [ Colin Watson ]
  * Move VCS to salsa.debian.org.

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol

  [ Scott Kitterman ]
  * Convert from git-dpm to patches unapplied format
  * Add myself to Uploaders
  * New upstream release (Closes: #892637)
    - Add python-hypothesis/python3-hypothesis to build-depends for tests
    - Update debian/rules so .hypothesis files are left scattered about the
      binaries
    - Update debian/copyright
  * Bump standards-version to 4.1.3 without further change
  * Add libjs-sphinxdoc to python-nacl-doc Depends

 -- Scott Kitterman <scott@kitterman.com>  Mon, 12 Mar 2018 08:37:37 -0400

python-nacl (1.1.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump minimum Python versions to 2.7 and 3.3, matching upstream.
  * Add myself to Uploaders.
  * Build-depend on python3-sphinx rather than python-sphinx.

 -- Colin Watson <cjwatson@debian.org>  Fri, 03 Nov 2017 10:20:19 +0000

python-nacl (1.1.1-1) unstable; urgency=medium

  [ Stefano Rivera ]
  * Drop explicit python{,3}-cffi dependencies. Upstream is now using
    out-of-line mode.

  [ Tristan Seligmann ]
  * New upstream release.
  * Bump Standards-Version to 3.9.8 (no changes).

 -- Tristan Seligmann <mithrandi@debian.org>  Sun, 19 Mar 2017 16:52:04 +0200

python-nacl (1.0.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Tristan Seligmann ]
  * Build-Depend on cffi >= 1.0.0, since this is needed since pynacl
    1.0.0.
  * Activate hardening options.

 -- Tristan Seligmann <mithrandi@debian.org>  Thu, 30 Jun 2016 02:00:16 +0200

python-nacl (1.0.1-1) unstable; urgency=medium

  * New upstream version.

 -- Tristan Seligmann <mithrandi@debian.org>  Sun, 24 Jan 2016 17:56:44 +0200

python-nacl (0.3.0-2) unstable; urgency=medium

  * Add explicit dependency on python{,3}-cffi as upstream is not yet
    using the new compilation support, thus requiring cffi to be
    importable at runtime (closes: #801786).

 -- Tristan Seligmann <mithrandi@debian.org>  Mon, 02 Nov 2015 09:13:25 +0200

python-nacl (0.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Switch to pypi.debian.net redirector.
  * Add warner's key to upstream keyring.

 -- Tristan Seligmann <mithrandi@debian.org>  Sun, 02 Aug 2015 15:29:13 +0200

python-nacl (0.2.3-2) unstable; urgency=medium

  * Add "Restrictions: needs-root" because of cffi silliness.
  * Remove outdated / unnecessary XS-Testsuite field.

 -- Tristan Seligmann <mithrandi@debian.org>  Sat, 20 Jun 2015 14:34:41 +0200

python-nacl (0.2.3-1) unstable; urgency=low

  * Initial release. (Closes: #776452)

 -- Tristan Seligmann <mithrandi@debian.org>  Mon, 02 Feb 2015 04:10:40 +0200
