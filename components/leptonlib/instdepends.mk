INSTDEPENDS += debhelper
INSTDEPENDS += libtiff-dev
INSTDEPENDS += libpng-dev
INSTDEPENDS += libgif-dev
INSTDEPENDS += libwebp-dev
INSTDEPENDS += libopenjp2-7-dev
INSTDEPENDS += dh-autoreconf
