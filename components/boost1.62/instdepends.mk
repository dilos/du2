INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += dctrl-tools
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += libbz2-dev
INSTDEPENDS += libicu-dev
INSTDEPENDS += mpi-default-dev
INSTDEPENDS += bison
INSTDEPENDS += flex
INSTDEPENDS += docbook-to-man
INSTDEPENDS += help2man
INSTDEPENDS += xsltproc
INSTDEPENDS += doxygen
INSTDEPENDS += dh-python
# g++
# g++-6
INSTDEPENDS += python
INSTDEPENDS += python-all-dev
INSTDEPENDS += python3
INSTDEPENDS += python3-all-dev
