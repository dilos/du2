Description: Support linking with system shared libraries
Author: Jonas Smedegaard <dr@jones.dk>
Last-Update: 2019-02-14
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/libatalk/talloc/Makefile.am
+++ b/libatalk/talloc/Makefile.am
@@ -1,4 +1,11 @@
 # Makefile.am for libatalk/talloc/
 
 noinst_LTLIBRARIES = libtalloc.la
+if USE_BUILTIN_TALLOC
 libtalloc_la_SOURCES = talloc.c dalloc.c
+else
+libtalloc_la_SOURCES = dalloc.c
+endif
+
+libtalloc_la_CFLAGS = @TALLOC_CFLAGS@
+libtalloc_la_LIBADD = @TALLOC_LIBS@
--- a/configure.ac
+++ b/configure.ac
@@ -188,6 +188,9 @@
 dnl Check whether bundled tdb shall be used
 AC_NETATALK_TDB
 
+dnl Check whether bundled tdb shall be used
+AC_NETATALK_TALLOC
+
 dnl Check for Tracker
 AC_NETATALK_SPOTLIGHT
 
--- a/etc/afpd/spotlight.c
+++ b/etc/afpd/spotlight.c
@@ -32,7 +32,11 @@
 #include <atalk/errchk.h>
 #include <atalk/util.h>
 #include <atalk/logger.h>
+#if USE_BUILTIN_TALLOC
 #include <atalk/talloc.h>
+#else
+#include <talloc.h>
+#endif
 #include <atalk/dalloc.h>
 #include <atalk/byteorder.h>
 #include <atalk/netatalk_conf.h>
--- a/etc/afpd/spotlight_marshalling.c
+++ b/etc/afpd/spotlight_marshalling.c
@@ -27,7 +27,11 @@
 #include <atalk/errchk.h>
 #include <atalk/util.h>
 #include <atalk/logger.h>
+#if USE_BUILTIN_TALLOC
 #include <atalk/talloc.h>
+#else
+#include <talloc.h>
+#endif
 #include <atalk/dalloc.h>
 #include <atalk/byteorder.h>
 #include <atalk/netatalk_conf.h>
--- a/include/atalk/dalloc.h
+++ b/include/atalk/dalloc.h
@@ -19,7 +19,11 @@
 #ifndef DALLOC_H
 #define DALLOC_H
 
+#if USE_BUILTIN_TALLOC
 #include <atalk/talloc.h>
+#else
+#include <talloc.h>
+#endif
 
 /* dynamic datastore */
 typedef struct {
--- a/libatalk/cnid/tdb/cnid_tdb.h
+++ b/libatalk/cnid/tdb/cnid_tdb.h
@@ -26,7 +26,11 @@
 #include <sys/mman.h>
 #include <sys/stat.h>
 #include <signal.h>
+#if USE_BUILTIN_TDB
 #include <atalk/tdb.h>
+#else
+#include <tdb.h>
+#endif
 
 #define TDB_ERROR_LINK  1
 #define TDB_ERROR_DEV   2
--- a/libatalk/talloc/dalloc.c
+++ b/libatalk/talloc/dalloc.c
@@ -121,7 +121,11 @@
 #include <atalk/errchk.h>
 #include <atalk/util.h>
 #include <atalk/logger.h>
+#if USE_BUILTIN_TALLOC
 #include <atalk/talloc.h>
+#else
+#include <talloc.h>
+#endif
 #include <atalk/bstrlib.h>
 #include <atalk/dalloc.h>
 
--- a/macros/netatalk.m4
+++ b/macros/netatalk.m4
@@ -264,6 +264,31 @@
     AM_CONDITIONAL(USE_BUILTIN_TDB, test x"$use_bundled_tdb" = x"yes")
 ])
 
+dnl Whether to disable bundled talloc
+AC_DEFUN([AC_NETATALK_TALLOC], [
+    AC_ARG_WITH(
+        talloc,
+        [AS_HELP_STRING([--with-talloc],[whether to use the bundled talloc (default: yes)])],
+        use_bundled_talloc=$withval,
+        use_bundled_talloc=yes
+    )
+    AC_MSG_CHECKING([whether to use bundled talloc])
+    AC_MSG_RESULT([$use_bundled_talloc])
+
+    if test x"$use_bundled_talloc" = x"yes" ; then
+        AC_DEFINE(USE_BUILTIN_TALLOC, 1, [Use internal talloc])
+    else
+        if test -z "$TALLOC_LIBS" ; then
+            PKG_CHECK_MODULES(TALLOC, talloc, , [AC_MSG_ERROR([couldn't find talloc with pkg-config])])
+        fi
+        use_bundled_talloc=no
+    fi
+
+    AC_SUBST(TALLOC_CFLAGS)
+    AC_SUBST(TALLOC_LIBS)
+    AM_CONDITIONAL(USE_BUILTIN_TALLOC, test x"$use_bundled_talloc" = x"yes")
+])
+
 dnl Filesystem Hierarchy Standard (FHS) compatibility
 AC_DEFUN([AC_NETATALK_FHS], [
 AC_MSG_CHECKING([whether to use Filesystem Hierarchy Standard (FHS) compatibility])
--- a/macros/summary.m4
+++ b/macros/summary.m4
@@ -160,6 +160,13 @@
 		AC_MSG_RESULT([        LIBS   = $TDB_LIBS])
 		AC_MSG_RESULT([        CFLAGS = $TDB_CFLAGS])
     fi
+    AC_MSG_RESULT([    TALLOC:])
+    if test x"$use_bundled_talloc" = x"yes"; then
+		AC_MSG_RESULT([        bundled])
+    else
+		AC_MSG_RESULT([        LIBS   = $TALLOC_LIBS])
+		AC_MSG_RESULT([        CFLAGS = $TALLOC_CFLAGS])
+    fi
 	if test x"$ac_cv_with_cnid_mysql" = x"yes"; then
 		AC_MSG_RESULT([    MySQL:])
 		AC_MSG_RESULT([        LIBS   = $MYSQL_LIBS])
--- a/etc/spotlight/sparql_parser.y
+++ b/etc/spotlight/sparql_parser.y
@@ -8,7 +8,11 @@
 
   #include <gio/gio.h>
 
+  #if USE_BUILTIN_TALLOC
   #include <atalk/talloc.h>
+  #else
+  #include <talloc.h>
+  #endif
   #include <atalk/logger.h>
   #include <atalk/errchk.h>
   #include <atalk/spotlight.h>
--- a/etc/spotlight/spotlight_rawquery_lexer.l
+++ b/etc/spotlight/spotlight_rawquery_lexer.l
@@ -7,7 +7,11 @@
 %{
 #include <stdbool.h>
 #include <gio/gio.h>
+#if USE_BUILTIN_TALLOC
 #include <atalk/talloc.h>
+#else
+#include <talloc.h>
+#endif
 #include <atalk/spotlight.h>
 #ifdef HAVE_TRACKER
 #include "sparql_parser.h"
--- a/etc/spotlight/Makefile.am
+++ b/etc/spotlight/Makefile.am
@@ -20,16 +20,16 @@
 
 libspotlight_la_CFLAGS  = \
 	-DDBUS_API_SUBJECT_TO_CHANGE \
-	@TRACKER_CFLAGS@ \
+	@TRACKER_CFLAGS@ @TALLOC_CFLAGS@ \
 	-D_PATH_STATEDIR='"$(localstatedir)/netatalk"'
 
-libspotlight_la_LDFLAGS = @TRACKER_LIBS@ @TRACKER_MINER_LIBS@
+libspotlight_la_LDFLAGS = @TRACKER_LIBS@ @TRACKER_MINER_LIBS@ @TALLOC_LIBS@
 
 srp_SOURCES = \
 	sparql_map.c \
 	sparql_parser.y \
 	spotlight_rawquery_lexer.l
 
-srp_CFLAGS = -DMAIN -I$(top_srcdir)/include @TRACKER_CFLAGS@
-srp_LDADD = $(top_builddir)/libatalk/libatalk.la @MYSQL_LIBS@
+srp_CFLAGS = -DMAIN -I$(top_srcdir)/include @TRACKER_CFLAGS@ @TALLOC_CFLAGS@
+srp_LDADD = $(top_builddir)/libatalk/libatalk.la @MYSQL_LIBS@ @TALLOC_LIBS@
 endif
--- a/etc/afpd/Makefile.am
+++ b/etc/afpd/Makefile.am
@@ -61,8 +61,8 @@
 
 if HAVE_TRACKER
 afpd_SOURCES += spotlight.c
-afpd_LDADD += $(top_builddir)/etc/spotlight/libspotlight.la
-afpd_CFLAGS += @TRACKER_CFLAGS@
+afpd_LDADD += $(top_builddir)/etc/spotlight/libspotlight.la @TALLOC_LIBS@
+afpd_CFLAGS += @TRACKER_CFLAGS@ @TALLOC_CFLAGS@
 endif
 
 if HAVE_ACLS
