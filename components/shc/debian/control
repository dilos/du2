Source: shc
Section: devel
Priority: optional
Maintainer: Tong Sun <suntong001@users.sourceforge.net>
Build-Depends: debhelper (>= 11)
Standards-Version: 4.1.5
Homepage: http://neurobin.github.io/shc
Vcs-Git: https://github.com/neurobin/shc.git
Vcs-Browser: https://github.com/neurobin/shc.git

Package: shc
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Shell script compiler
 SHC is a generic shell script compiler.
 It takes a script, which is specified on the command line and produces
 C source code. The generated source code is then compiled and linked to
 produce a stripped binary.
 .
 The compiled binary will still be dependent on the shell specified in
 the first line of the shell code, thus shc does not create completely
 independent binaries.
 .
 shc itself is not a compiler such as cc,
 it rather encodes and encrypts a shell script and generates C source
 code with the added expiration capability. It then uses the system
 compiler to compile a stripped binary which behaves exactly like the
 original script. Upon execution, the compiled binary will decrypt and
 execute the code with the shells' -c option.
