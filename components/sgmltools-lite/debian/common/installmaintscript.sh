#!/bin/sh

set -e

if [ -z "$1" ]
  then
    echo "please specify the build directory to install maintainer script"
    exit 1
fi

MAINTDIR=debian/"$1"/DEBIAN
if [ ! -d "$MAINTDIR" ]
  then
    install -g 0 -o 0 -d "$MAINTDIR"
fi

for i in preinst postinst prerm postrm
 do
   if [ -f debian/"$i" ]
     then
       install -g 0 -o 0 -m 755 -p debian/"$i" "$MAINTDIR"/"$i";
   fi
 done

for i in conffiles shlibs
 do
   if [ -f debian/"$i" ]
     then
       install -g 0 -o 0 -m 644 -p debian/"$i" "$MAINTDIR"/"$i";
   fi
 done

