#!/bin/sh

set -e

if [ -z "$1" ]
  then
    echo "please specify the build directory to install docs"
    exit 1
fi

DOCDIR=debian/"$1"/usr/share/doc/"$1"
if [ ! -d "$DOCDIR" -a ! -L "$DOCDIR" ]
  then
    install -g 0 -o 0 -d "$DOCDIR"
fi

for i in README.Debian copyright
 do
   if [ -f debian/"$i" ]
     then
       install -g 0 -o 0 -m 644 -p debian/"$i" "$DOCDIR"/"$i";
   fi
 done

if [ -f debian/docs ]
  then
    cat debian/docs | while read line
      do
        cp -a $line $DOCDIR
      done

    chown -R 0.0 $DOCDIR
    chmod -R go=rX $DOCDIR
    chmod -R u+rw $DOCDIR
fi

