sgmltools-lite (3.0.3.0.cvs.20010909-20+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- DilOS Team <dilos@dilos.org>  Fri, 18 Jan 2019 15:05:03 +0300

sgmltools-lite (3.0.3.0.cvs.20010909-20) unstable; urgency=medium

  * QA upload.
  * Remove obsolete package jade from Depends. (Closes: #803530).

 -- Neil Roeth <neil@debian.org>  Mon, 18 Jul 2016 22:06:43 -0400

sgmltools-lite (3.0.3.0.cvs.20010909-19) unstable; urgency=medium

  * QA upload.
  * Switch to "3.0 (quilt)" source format.
  * Standards-Version: 3.9.6 (no special changes for this).
  * debian/rules: Drop some useless/unused/unnecessary stuff.
  * Do not run configure script twice. Instead, run it only once and pass
    appropriate parameters to "make install". This also avoids having
    to rewrite scripts to remove build paths from them afterwards.
  * Try to be reproducible by creating md5sums in a deterministic way.

 -- Santiago Vila <sanvila@debian.org>  Wed, 14 Oct 2015 01:17:10 +0200

sgmltools-lite (3.0.3.0.cvs.20010909-18) unstable; urgency=medium

  * QA upload
  * debian/rules:
    - use option -n in gzip invocations in order to not include timestamps.
      Thanks to Chris Lamb for the patch! (closes: #777011)
    - set the mtimes of all files which are modified during the built to the
      date of the last changelog entry. Thanks to Maria Valentina Marin
      for the patch! (closes: #793720)

 -- Ralf Treinen <treinen@debian.org>  Mon, 10 Aug 2015 12:24:06 +0200

sgmltools-lite (3.0.3.0.cvs.20010909-17) unstable; urgency=low

  * QA upload.
    - Mark sgmltools-lite Multi-Arch: foreign.

 -- Adam Conrad <adconrad@debian.org>  Mon, 16 Jul 2012 18:43:33 -0600

sgmltools-lite (3.0.3.0.cvs.20010909-16) unstable; urgency=low

  * QA upload.
  * Remove postinst. It was used to execute a transitional call to
    update-catalog: such a call is a noop since the new sgml-base version.
    (Closes: #674914). Thanks, Helmut Grohne.
  * build-{arch,indep} targets added.
  * The package is architecture-independent: build files in binary-indep.
  * Maintainer field set to QA Group.
  * Standards-Version bumped to 3.9.3.
  * FSF address updated in debian/copyright.

 -- Emanuele Rocca <ema@debian.org>  Fri, 15 Jun 2012 08:48:51 +0000

sgmltools-lite (3.0.3.0.cvs.20010909-15.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix bashism in jadewhich (Closes: #489659)

 -- Matthew Johnson <mjj29@debian.org>  Sun, 24 Aug 2008 13:58:00 +0000

sgmltools-lite (3.0.3.0.cvs.20010909-15) unstable; urgency=low

  * python/utils.py: correctly handles two or more successive asterisks
    at the end or a single-quote in the file name given to Tracer.mv()
    (since a single-quoted string can't contain a single quote in a shell,
    Tracer.mv() now back-quotes every character in the file name instead of
    calling shellProtect())

 -- Oohara Yuuma <oohara@debian.org>  Wed,  9 Jan 2008 06:10:44 +0900

sgmltools-lite (3.0.3.0.cvs.20010909-14) unstable; urgency=low

  * python/utils.py: Handles the asterisk given to Tracer.mv() manually
    so that it matches any number of characters.  In a shell, 'foo'* should
    mean "any file whose name begins with 'foo' (not foo)" --- glob occurs
    before quote removal.  (closes: #459051)

 -- Oohara Yuuma <oohara@debian.org>  Tue,  8 Jan 2008 18:59:28 +0900

sgmltools-lite (3.0.3.0.cvs.20010909-13) unstable; urgency=low

  * debian/rules: made sure that the argument of --with-dbimages
    begins with / (I don't know why I call ./configure twice,
    but it seems to work) (thanks to Adam DiCarlo <aph@debian.org> for help)
    (closes: #130504)
  * debian/postinst, debian/prerm: no /usr/doc symlink

 -- Oohara Yuuma <oohara@debian.org>  Wed, 30 Jul 2003 11:04:39 +0900

sgmltools-lite (3.0.3.0.cvs.20010909-12) unstable; urgency=low

  * debian/control: added version condition to Depends: python because
    sgmltools requires python 1.5 or later
  * src/configure.in: added missing AC_SUBST
  * src/configure: regenerated

 -- Oohara Yuuma <oohara@debian.org>  Thu, 11 Apr 2002 04:04:14 +0900

sgmltools-lite (3.0.3.0.cvs.20010909-11) unstable; urgency=low

  * src/configure.in: made sure that it does not check the absolute path
    of programs (Debian follows the FHS, we know they are in /usr/bin)
    (closes: #142156)
  * src/configure: regenerated
  * debian/control: no Build-Depends-Indep: now
  * debian/.mew.el: removed because it has nothing to do with the package

 -- Oohara Yuuma <oohara@debian.org>  Thu, 11 Apr 2002 00:33:27 +0900

sgmltools-lite (3.0.3.0.cvs.20010909-10) unstable; urgency=low

  * debian/control: improved Description: (thanks Meinolf Sander
    <mesa@netcologne.de>)
  * src/python/backends/Html.py: fixed a typo in a warning message
  * src/bin/sgmltools.in: hardcoded the absolute path of jade, lynx
    and w3m to reduce build dependency
  * debian/control: dropped jade|openjade, lynx and w3m|w3mmee from
    Build-Depends-Indep: (they are needed just to check their absolute path)

 -- Oohara Yuuma <oohara@debian.org>  Sun, 31 Mar 2002 15:44:35 +0900

sgmltools-lite (3.0.3.0.cvs.20010909-9) unstable; urgency=low

  * new maintainer
  * src/python/backends/Html.py: issues a warning if the output directory
    is empty

 -- Oohara Yuuma <oohara@debian.org>  Mon, 25 Feb 2002 11:28:16 +0900

sgmltools-lite (3.0.3.0.cvs.20010909-8) unstable; urgency=low

  * Merge the work done by Oohara Yuuma.  Thanks to him.

 -- Taketoshi Sano <sano@debian.org>  Mon, 18 Feb 2002 21:52:30 +0900

sgmltools-lite (3.0.3.0.cvs.20010909-7.1) unstable; urgency=low

  * NMU
  * src/python/SGMLtools.py: assumes -b onehtml if -b html and
    -j '-V nochunks' are specified (closes: #72887)
  * src/python/SGMLtools.py: added /etc/sgml/catalog to the search path
    for a SGML super catalog (closes: #131620)
  * debian/addition/jadewhich: replaced $* with "$@" (closes: #132250)

 -- Oohara Yuuma <oohara@libra.interq.or.jp>  Fri, 15 Feb 2002 13:49:59 +0900

sgmltools-lite (3.0.3.0.cvs.20010909-7) unstable; urgency=low

  * remove python-base from Build-Depends and Depends.
    I don't know if the upgrade from potato can be done
    safely, but the bug reporter must have been certain.
    (Closes: #124019)

 -- Taketoshi Sano <sano@debian.org>  Sat, 29 Dec 2001 23:58:33 +0900

sgmltools-lite (3.0.3.0.cvs.20010909-6) unstable; urgency=low

  * Applied patch from "Martin Schwenke" <martin@meltin.net>.
    Thanks to him for providing a patch, as well as pointing
    out this problem. (Closes: #118567)

 -- Taketoshi Sano <sano@debian.org>  Wed, 14 Nov 2001 12:20:48 +0900

sgmltools-lite (3.0.3.0.cvs.20010909-5) unstable; urgency=low

  * update to keep up with the python packaging reorganization.
    As far as I checked, this can work with 2.2 as well as 1.5 and 2.1
    of python. So just symply added python to Depends: line.
    (Closes: #118129)

 -- Taketoshi Sano <sano@debian.org>  Mon,  5 Nov 2001 22:01:05 +0900

sgmltools-lite (3.0.3.0.cvs.20010909-4) unstable; urgency=low

  * update the modified part (ugly hack) by me in SGMLtools.py
    to use the temporary input file in place of the input pipe.
    openjade can't handle input from stdin correctly.
    (Closes: #116775)

 -- Taketoshi Sano <sano@debian.org>  Fri, 26 Oct 2001 19:29:59 +0900

sgmltools-lite (3.0.3.0.cvs.20010909-3) unstable; urgency=low

  * move the call of update-catalog command from preinst
    to postinst to fix the "could not install" bug.
    (Closes: #115471)

 -- Taketoshi Sano <sano@debian.org>  Tue, 16 Oct 2001 20:13:04 +0900

sgmltools-lite (3.0.3.0.cvs.20010909-2) unstable; urgency=low

  * converted not to depend debhelper (Closes: #113778)
  * Add more note into package description to inform users
    that this tool just convert DocBook SGML into HTML only
    without other recommended and suggested packages.
    This change is related to the wishlist #112796

 -- Taketoshi Sano <sano@debian.org>  Thu,  4 Oct 2001 00:23:42 +0900

sgmltools-lite (3.0.3.0.cvs.20010909-1) unstable; urgency=low

  * Change configuration for the migration to FHS (/usr/share/sgml)
    (Closes: #104145)
  * Depends on docbook-dsssl (>= 1.71-1). (Closes: #103668)
  * Preinst calls update-catalog to remove old sgmltools-2 catalog
    (Closes: #104144)
  * Openjade can't work when input file is provided via pipe.
    This is the cause of trouble.  Temporary fix is applied by me
    using a dirty quick hack, but the correct one may be needed for
    the upstream.  Are there anyone who can privide it ?
    (Closes: #109813)
  * Add note into package description to inform users that this
    tool can not handle DocBook XML. (Closes: #111895)
  * w3m/w3mmee has already Recommends line.  I think this should
    be enough.  (Closes: #112796)

 -- Taketoshi Sano <sano@debian.org>  Sun, 25 Sep 2001 01:04:32 +0900

sgmltools-lite (3.0.2.3.cvs0-8) unstable; urgency=low

  * add docbook-dsssl to dependencies. Thanks to Adam for
    his notification. (Closes #103668)

 -- Taketoshi Sano <sano@debian.org>  Fri,  6 Jul 2001 22:49:05 +0900

sgmltools-lite (3.0.2.3.cvs0-7) unstable; urgency=low

  * add w3mmee in suggests. (Closes: #98392)

 -- Taketoshi Sano <sano@debian.org>  Fri, 25 May 2001 02:27:54 +0900

sgmltools-lite (3.0.2.3.cvs0-6) unstable; urgency=low

  * revert to use Suggests for jadetex, lynx, and linuxdoc-tools.
    w3m is the default for txt backend, and is recommended now.
    (Closes: #92161, #92212)
    modification to produce some informational messages when
    the absent backend is requested, will take longer time.
    If you have nice idea, please let me know.
  * update the manpage for sgmltools.1 (Closes: #90231)

 -- Taketoshi Sano <sano@debian.org>  Tue, 24 Apr 2001 23:33:03 +0900

sgmltools-lite (3.0.2.3.cvs0-5) unstable; urgency=low

  * add python-base to build-depends (Closes: #90229)
  * There are many users who likes to make this tool depend
    other backend-related packages. So let's try to do it,
    and see if there are requests against this change.
    (Closes: #88607, #75530) This is requested also in #91981.
  * fix debian/rules to set admon_graphics_path correctly.
    This problem is reported in #91981.
  * fix src/python/backends/Ld2db.py to update for recent
    transition of Debian system-wide sgml catalog configuration.
    (Closes: #91981)

 -- Taketoshi Sano <sano@debian.org>  Thu, 29 Mar 2001 16:39:57 +0900

sgmltools-lite (3.0.2.3.cvs0-4) unstable; urgency=low

  * Remove update-catalog from postinst/prerm since it is not needed now.

 -- Taketoshi Sano <sano@debian.org>  Wed, 28 Mar 2001 21:25:49 +0900

sgmltools-lite (3.0.2.3.cvs0-3) unstable; urgency=low

  * Added "provides: sgmltools-2"
  * update package description to note the requirement
    for text output.

 -- Taketoshi Sano <sano@debian.org>  Tue,  6 Mar 2001 13:26:44 +0900

sgmltools-lite (3.0.2.3.cvs0-2) unstable; urgency=low

  * change "Conflict: sgmltools-2 (<=2.0.2-4)" to provide
    a dummy package of sgmltools-2 for smooth upgrade. 
  * remove a cruft (buildcat.1.remove.gz) from binary pacakge.
  * add lynx to builddepends.
  * add lynx to suggests.

 -- Taketoshi Sano <sano@debian.org>  Mon,  5 Mar 2001 11:41:32 +0900

sgmltools-lite (3.0.2.3.cvs0-1) unstable; urgency=low

  * Initial Release.
  * This is the upstream successor of SGML-Tools version 2, so this
    packages conflicts/replaces the Debian package of sgmltools-2.

 -- Taketoshi Sano <sano@debian.org>  Fri,  2 Mar 2001 15:09:24 +0900
