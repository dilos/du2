INSTDEPENDS += debhelper
INSTDEPENDS += autoconf-archive
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dpkg-dev
INSTDEPENDS += libdatrie-dev
INSTDEPENDS += libdatrie1-bin
INSTDEPENDS += pkg-config
# Build-Depends-Indep:
INSTDEPENDS += doxygen
