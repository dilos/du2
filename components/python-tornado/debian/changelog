python-tornado (4.4.3-1+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- Denis Kozadaev <denis@tambov.ru>  Mon, 18 Dec 2017 20:59:50 +0300

python-tornado (4.4.3-1) unstable; urgency=medium

  * New upstream bugfix release
  * No longer disable PIE during build

 -- Ondřej Nový <onovy@debian.org>  Thu, 13 Apr 2017 14:57:17 +0200

python-tornado (4.4.2-1) unstable; urgency=medium

  * New upstream release.
  * Push dh compat to 10.

 -- Julien Puydt <julien.puydt@laposte.net>  Fri, 07 Oct 2016 15:58:20 +0200

python-tornado (4.4.1-2) unstable; urgency=medium

  * Prevent internet access during build (Closes: #833351)
    - Use local objects.inv for intersphinx mapping
    - Skip domain resolving tests

 -- Ondřej Nový <onovy@debian.org>  Fri, 05 Aug 2016 15:20:50 +0200

python-tornado (4.4.1-1) unstable; urgency=medium

  * New upstream release (Closes: #830859)
  * Rearranged lintian overrides for new release
  * Added myself as uploader
  * Removed python-simplejson and python-multiprocessing from B-D/R,
    because Python < 2.6 is not supported anymore (Closes: #826455)
  * Moved python-mysqldb from Recommends to Suggests, it's only used in demo
  * Moved python{3,}-pycurl from Depends to Suggests, it's not strictly needed
    and added to B-D to run related unit tests
  * Added python-concurrent.futures to Build-Depends and Recommends
  * Added python{3,}-twisted to Build-Depends and Suggests
  * Added python{3,}-mock to Build-Depends
  * d/copyright: Added myself to Debian part
  * Package description changed to upstream (newer) version
  * Rearranged Debian tests Depends to Build-Depends
  * Run Py3 unit tests during build and enable verbose mode
  * Bumped required Python version to 2.7 and 3.3
  * Added python-tornado-doc package with docs and examples
  * Enabled all but PIE hardening
  * Generate upstream changelog file during build
  * Added python3-backports-abc to B-D for older Python 3

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Aug 2016 16:48:24 +0200

python-tornado (4.3.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Team upload
  * Added Build-Depends to dh-python and python-setuptools
  * Added ${shlibs:Depends} and removed lintian override for libc
  * Lintian tag rename:
    privacy-breach-may-use-debian-package -> privacy-breach-uses-embedded-file

  [ Gianfranco Costamagna, Ondřej Nový ]
  * debian/patches/0007-Fix-testsuite-failure-when-sockets-is-not-defined.patch:
    fix testsuite when sockets is not defined.
    - https://github.com/tornadoweb/tornado/pull/1725

 -- Ondřej Nový <novy@ondrej.org>  Tue, 24 May 2016 11:25:26 +0200

python-tornado (4.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Refreshed patches (and dropped the Python 3.5 patch -- gone upstream).
  * Added dep on singledispatch and backports_abc.
  * Added lintian override for the facebook example (not a privacy breach)
  * Added lintian overrides for sample.xml compressed versions.

 -- Julien Puydt <julien.puydt@laposte.net>  Sat, 21 May 2016 21:57:17 +0000

python-tornado (4.2.1-2) unstable; urgency=medium

  * Team upload.

  [ SVN-Git Migration ]
  * git-dpm config.
  * Update Vcs fields for git migration.

  [ Ondřej Nový ]
  * Fixed VCS URL (https).

  [ Mattia Rizzolo ]
  * Add a patch to skip failing test and fix ftbfs on hurd.
  * Bump Standards-Version to 3.9.8, no changes needed.
  * Bump debhelper compat level to 9.
  * Build using pybuild.

 -- Mattia Rizzolo <mattia@debian.org>  Thu, 28 Apr 2016 08:45:36 +0000

python-tornado (4.2.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * debian/patches/python35-compat.patch: Python 3.5 compatibility fixes
    pulled from upstream.
  * debian/tests/control: Remove an obsolete comment.

 -- Barry Warsaw <barry@debian.org>  Tue, 18 Aug 2015 16:04:34 -0400

python-tornado (4.2.0-1) unstable; urgency=medium

  * Update to latest upstream (Closes: #779035)
  * Remove patches/certs-path.patch and ignore-ca-certificates.patch
    (now useless).
  * Refresh all other patches.
  * Add patches/without-certifi.patch so we directly look where we
    know the certs are.
  * Push standards-version up.
  * Add ca-certificates as a build-dep so tests pass.
  * Add myself as uploaders.

 -- Julien Puydt <julien.puydt@laposte.net>  Mon, 22 Jun 2015 11:24:38 +0200

python-tornado (3.2.2-1.1) unstable; urgency=medium

  * Non-Maintainer Upload.
  * patches/sockopt.patch: New patch to ignore ENOPROTOOPT errors from
    SO_REUSEADDR or SO_ERROR on AF_UNIX sockets, for systems which do not
    implement them there. Thanks Svante Signell for the patch. (Closes: #748903)

 -- Samuel Thibault <sthibault@debian.org>  Sat, 25 Oct 2014 21:14:45 +0200

python-tornado (3.2.2-1) unstable; urgency=medium

  * New upstream release
    - update watchfile to new location
  * skip-timing-tests.patch:
    skip tests that need fast machines (Closes: #751343)

 -- Julian Taylor <jtaylor.debian@googlemail.com>  Mon, 19 May 2014 21:42:13 +0200

python-tornado (3.2.0-1) unstable; urgency=medium

  * New upstream release
    - now arch: any package with binary extension for faster websockets

 -- Julian Taylor <jtaylor.debian@googlemail.com>  Sun, 23 Feb 2014 20:16:37 +0100

python-tornado (3.1.1-1) unstable; urgency=low

  * New upstream release
    - fixes localhost binding on disabled network (Closes: #711806)
  * drop upstream applied random-port.patch and CVE-2013-2099.patch
  * bump standard to 3.9.5 no changes required

 -- Julian Taylor <jtaylor.debian@googlemail.com>  Sun, 03 Nov 2013 18:48:51 +0100

python-tornado (2.4.1-3) unstable; urgency=low

  * Team upload.

  [ Jakub Wilk ]
  * Run tests only if DEB_BUILD_OPTIONS=nocheck is not set.

  [ Andrew Starr-Bochicchio ]
  * Backport fix for CVE 2013-2009. Avoid allowing multiple
    wildcards in a single SSL cert hostname segment (Closes: #709069).

 -- Andrew Starr-Bochicchio <asb@debian.org>  Fri, 04 Oct 2013 19:18:05 -0400

python-tornado (2.4.1-2) unstable; urgency=low

  [ Julian Taylor ]
  * upload to unstable
  * debian/rules: replace PWD with CURDIR

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

 -- Julian Taylor <jtaylor.debian@googlemail.com>  Thu, 09 May 2013 20:08:11 +0200

python-tornado (2.4.1-1) experimental; urgency=low

  * New upstream release (Closes: #698566)
  * run python2 test during build (Closes: #663928)
  * add autopkgtests
  * ignoreuserwarning.patch:
    disable warning in test to allow testing with the package already installed
  * bump standards to 3.9.4, no changes required
  * random-port.patch: get a random free port for tests
  * don't compress example python files

 -- Julian Taylor <jtaylor.debian@googlemail.com>  Wed, 23 Jan 2013 20:26:46 +0100

python-tornado (2.3-2) unstable; urgency=low

  * debian/control
    - Replaced the python3.2 in the Depends field of python3-tornado
      by $(python3:Depends)
  * Renamed python-tornado.examples to examples
  * Removed python3-tornado.examples

 -- Carl Chenet <chaica@debian.org>  Sun, 10 Jun 2012 01:26:50 +0200

python-tornado (2.3-1) unstable; urgency=low

  [ Julian Taylor ]
  * Team upload
  * new upstream release
    - change debian/watch to download section instead of tag tarballs
      tag tarball contain lots of unnecessary website and test data
    - drop upstream applied CVE-2012-2374.patch
    - refresh patches
    - closes LP: #998615
  [ Carl Chenet]
  * debian/control
    - Replaced chaica@ohmytux.com by chaica@debian.org
    - Replace (python3:Depends) in python3-tornado Depends field
      because of https://github.com/facebook/tornado/issues/450
    - Modified the short description of python3-tornado package
  * Renamed examples to python-tornado.examples
  * Added python3-tornado.examples

 -- Carl Chenet <chaica@debian.org>  Sun, 10 Jun 2012 00:12:24 +0200

python-tornado (2.1.0-3) unstable; urgency=high

  [ Julian Taylor ]
  * Team upload
  * bump X-Python-Version to >= 2.6
    lower versions require arch any for custom epoll extension
  * move to Section: web (Closes: #665854)
    - override lintian wrong-section-according-to-package-name
  * CVE-2012-2374.patch adopted from upstream's GIT (Closes: #673987)

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 22 May 2012 19:11:51 -0400

python-tornado (2.1.0-2) unstable; urgency=low

  [ Thomas Kluyver ]
  * Package for Python 3 (Closes: #653643)

  [ Julian Taylor ]
  * make packages arch all
  * debian/control
    - wrap-and-sort fields
  * remove unnecessary Dependencies and Breaks
  * convert copyright to dep5
  * bump standard to 3.9.3 (no changes)

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 13 Mar 2012 18:08:33 -0400

python-tornado (2.1.0-1) unstable; urgency=low

  * Team upload
  * New upstream release (Closes: #636602)
  * Refreshed patches
  * Updated debian/copyright
  * Added myself to uploaders to have bugs closed with uploads

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 21 Sep 2011 19:58:11 -0400

python-tornado (1.2.1-1) unstable; urgency=low

  * New upstream release
  * debian/control
    - Bump Standards-Version to 3.9.2

 -- Carl Chenet <chaica@ohmytux.com>  Mon, 09 May 2011 16:56:04 +0200

python-tornado (1.2.0-1) unstable; urgency=low

  * New upstream release.
  * Added debian/examples
  * Added patch to ignore ca-certificates.crt from sources
  * Added patch to look for ca-certificates.crt at the right place
  * debian/copyright
    - Added copyrights and license for some files
    - some files no more in the upstream sources
  * debian/control
    - Removed python-support
    - Replaced XS-Python-Version by X-Python-Version
    - Added Breaks: ${python:Breaks}
    - Added Recommends: python (>= 2.6) | python-multiprocessing
    - Added Depends: ca-certificates
  * debian/rules
    - Replaced dh $@ by dh $@ --with python2
  * debian/watch
    - fixed the path

 -- Carl Chenet <chaica@ohmytux.com>  Tue, 01 Mar 2011 02:38:06 +0100

python-tornado (1.0.1-1) unstable; urgency=low

  * Team upload
  * New upstream release
  * Standards-Version bumped to 3.9.1 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Sun, 22 Aug 2010 15:14:57 +0200

python-tornado (0.2-1) unstable; urgency=low

  * Initial release. (Closes: #551520)

 -- Carl Chenet <chaica@ohmytux.com>  Sat, 20 Mar 2010 17:38:45 +0100
