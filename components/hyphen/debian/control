Source: hyphen
Section: libs
Priority: optional
Maintainer: Debian LibreOffice Maintainers <debian-openoffice@lists.debian.org>
Uploaders: Mattia Rizzolo <mattia@debian.org>, Rene Engelhard <rene@debian.org>
Build-Depends: debhelper (>= 10), texlive-base (>= 2013~), gawk
Standards-Version: 3.9.8
Homepage: https://github.com/hunspell/hyphen
Vcs-Git: https://anonscm.debian.org/git/pkg-openoffice/hyphen.git
Vcs-Browser: https://anonscm.debian.org/git/pkg-openoffice/hyphen.git

Package: libhyphen-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libhyphen0 (= ${binary:Version}), ${misc:Depends}
Description: ALTLinux hyphenation library - development files
 ALTLinuxhyph is a modified version of libhnj which is a high quality
 hyphenation and justification library based on the TeX
 hyphenation algorithm. The TeX hyphenation patterns could be used after
 a preprocessing step.
 .
 It is used in OpenOffice.org
 .
 This package contains the headers and necessary files for programming apps
 with ALTLinuxhyph.

Package: libhyphen0
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: ALTLinux hyphenation library - shared library
 ALTLinuxhyph is a modified version of libhnj which is a high quality
 hyphenation and justification library based on the TeX
 hyphenation algorithm. The TeX hyphenation patterns could be used after
 a preprocessing step.
 .
 It is used in OpenOffice.org
 .
 This package contains the shared library.

Package: hyphen-en-us
Section: text
Architecture: all
Multi-Arch: foreign
Depends: dictionaries-common, ${misc:Depends}
Suggests: libreoffice-writer
Provides: hyphen-hyphenation-patterns, hyphen-hyphenation-patterns-en-us
Description: English (US) hyphenation patterns
 This package contains the English (US) hyphenation patterns.
 .
 You can use these patterns with programs which take advantage of libhyphen,
 like LibreOffice.
