INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += pkg-config
INSTDEPENDS += libjpeg-dev
INSTDEPENDS += libtiff-dev
# librsvg2-bin | imagemagick
INSTDEPENDS += librsvg2-bin
