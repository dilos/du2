#!/usr/bin/perl

use warnings;
use strict;
use Text::Wrap;

my $Langs = $ARGV[0];
my $Formats = $ARGV[1];

my %Template = (
	en => {
		lang_full => 'en-us',
		artist => 'voice actress Allison Smith',
		desc_base => '"en" (English) and "en_US" (USA English)',
		desc => 'English (US, by Allison Smith)',
		name => 'US English',
		dirname => 'en_US_f_Allison',
 	},
	es => {
		lang_full => 'es-mx',
		artist => 'voice actress Allison Smith',
		desc_base => '"es" (Spanish) and "es_MX" (Mexican Spanish)',
		desc => '(MX, by Allison Smith)',
		name => 'Spanish',
		dirname => 'es_MX_f_Allison',
 	},
	fr => {
		lang_full => 'fr-ca',
		artist => 'voice actress June Wallack',
		desc_base => '"fr" (French) and "fr_CA" (Canadian French)',
		desc => 'French (Canadian, by June Wallack)',
		name => 'Canadian French',
		dirname => 'fr_CA_f_June',
	},
	it => {
		lang_full => 'it-it',
		artist => 'voice actor Carlo Flora',
		desc_base => '"it" and "it_IT" (Italian)',
		desc => 'Italian (by Carlo Flora)',
		name => 'Italian',
		dirname => 'it_IT_m_Carlo',
	},
	ru => {
		lang_full => 'ru-ru',
		artist => 'Maxim Topal',
		desc_base => '"ru" and "ru_RU" (Russian)',
		desc => 'Russian (provided by Maxim Topal)',
		name => 'Russian',
		dirname => 'ru_RU_f_IvrvoiceRU',
	},

	gsm => {
		desc => 'raw gsm-fr format (Compressed. Takes relatively little space. playable with sox)',
	},
	g722 => {
		desc => 'raw G.722 format (mildly compressed wide-band codec)',
	},
	wav => {
		desc => 'WAV format (8Khz, mono)'
	},

);

$Text::Wrap::columns = 72;
sub format_desc_wrap {
	return join "\n .\n", map {wrap(" ", " ", $_)} @_;
}

sub base_deps($) {
	my $lang = shift;

	my @formats = split / /, $Formats;
	my @deps = map {"asterisk-core-sounds-$lang-$_"} @formats;
	return join ' | ', @deps;
}

sub print_base($) {
	my $lang = shift;
	my $en_conflict = '';
	my $name = $Template{$lang}{name};
	my $artist = $Template{$lang}{artist};
	my $desc_base = $Template{$lang}{desc_base};
	my $lang_full = $Template{$lang}{lang_full};
	my $base_deps = base_deps($lang);
	print
"Package: asterisk-core-sounds-$lang
Architecture: all
Depends: \${misc:Depends}, $base_deps
";
	if($lang eq 'en') {
	       print "Conflicts: asterisk-core-sounds-en-gsm ( << 1.4.21-2 )\n";
       }
       print
"Enhances: asterisk
Provides: asterisk-prompt-$lang, asterisk-prompt-$lang_full
Description: asterisk PBX sound files - $name
"	;
	print format_desc_wrap(
		"Asterisk is an Open Source PBX and telephony toolkit. It is, in a sense, middleware between Internet and telephony channels on the bottom, and Internet and telephony applications at the top.",
		"Asterisk includes a set of standard sound files in various formats. The core part of that collection in $name, by $artist, is contained in various encodings in packages asterisk-core-sounds-$lang-*; this package registers these through the alternatives system to provide the default $desc_base sounds.",
 	),
	"\n\n";
}

sub print_pack($$) {
	my $lang = shift;
	my $format = shift;
	my $lang_full = $Template{$lang}{lang_full};
	my $lang_desc = $Template{$lang}{desc};
	my $format_desc = $Template{$format}{desc};

	print
"Package: asterisk-core-sounds-$lang-$format
Architecture: all
Depends: \${misc:Depends}
Recommends: asterisk-core-sounds-$lang
";
	if (($lang eq 'en') and ($format eq 'gsm')) {
		print
"Conflicts: asterisk-sounds-main
Replaces: asterisk-sounds-main
";
	}
	print
"Description: asterisk PBX sound files - $lang_full/$format
",
 		format_desc_wrap(
 			"Asterisk is an Open Source PBX and telephony toolkit. It is, in a sense, middleware between Internet and telephony channels on the bottom, and Internet and telephony applications at the top.",
 			"Asterisk includes a set of standard sound files in various formats. This package contains the core part of that collection in $lang_desc in $format_desc."
		),
		"\n\n";
}

sub print_header() {
	my $orig_file = "debian/control";
	open(ORIG, '<', $orig_file) or die "Can't open original $orig_file: $!";
	while (<ORIG>) {
		last if /^$/;
		print;
	}
	close ORIG;
	print "\n";
}

sub gen_scripts($$) {
	my $lang = shift;
	my $formats = shift;

	my $dirname = $Template{$lang}{dirname};
	my $dirname_full = "usr/share/asterisk/sounds/$dirname";
	my $lang_full = $Template{$lang}{lang_full};
	my $config_base = "debian/asterisk-core-sounds-$lang";
	system("echo '$lang-gsm/core-sounds-$lang.txt' >$config_base.docs");
	foreach my $format (@$formats) {
		system("echo '$lang-$format/* $dirname_full' >$config_base-$format.install");
	}

	# postinst and prerm
	my %replacements = (
		'@dirname@' => $dirname,
		'@lang@' => $lang,
		'@lang_full@' => $lang_full,
		'@lang_full_caps@' => substr($lang_full, 0, 2) . '_' . uc(substr($lang_full, 3, 2)),
	);
	my $sed = 'sed';
		foreach my $key (keys %replacements) {
		$sed .= " -e 's/$key/$replacements{$key}/g'"
	}
	system("$sed debian/template.postinst > $config_base.postinst");
	system("$sed debian/template.prerm > $config_base.prerm");
}

print_header;
foreach my $lang (split / /, $Langs) {
	my @formats = split / /, $Formats;
	print_base($lang);
	foreach my $format (@formats) {
		print_pack($lang, $format);
	}
	gen_scripts $lang, [@formats];
}
