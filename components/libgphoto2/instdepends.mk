INSTDEPENDS += debhelper
INSTDEPENDS += doxygen
INSTDEPENDS += graphviz
INSTDEPENDS += libdbus-1-dev
INSTDEPENDS += libexif-dev
INSTDEPENDS += libgd-dev
#INSTDEPENDS += libgpmg1-dev [linux-any]
INSTDEPENDS += libjpeg-dev
INSTDEPENDS += libltdl-dev
INSTDEPENDS += libtool
INSTDEPENDS += libusb-1.0-0-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += rdfind
INSTDEPENDS += symlinks
INSTDEPENDS += zlib1g-dev
