libuninameslist (20160701-2+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- Denis Kozadaev <denis@dilos.org>  Fri, 21 Sep 2018 20:10:24 +0300

libuninameslist (20160701-2) unstable; urgency=medium

  * upload to unstable 

 -- Hideki Yamane <henrich@debian.org>  Sat, 05 Nov 2016 23:41:49 +0900

libuninameslist (20160701-1) experimental; urgency=medium

  * New upstream release 
  * debian/control
    - change libuninameslist0 to libuninameslist1
    - use https for Vcs-Git
    - drop unnecessary Build-Dependends
    - set Standards-Version: 3.9.8
  * debian/compat
    - set 10

 -- Hideki Yamane <henrich@debian.org>  Sat, 17 Sep 2016 11:02:00 +0900

libuninameslist (0.5.20150701-1) unstable; urgency=medium

  * Imported Upstream version 0.5.20150701

 -- Hideki Yamane <henrich@debian.org>  Sun, 05 Jul 2015 12:27:48 +0900

libuninameslist (0.4.20140731-1) unstable; urgency=medium

  * New Upstream version 0.4.20140731
  * drop unnecessary debian/patches
  * debian/control
    - fix duplicated Section

 -- Hideki Yamane <henrich@debian.org>  Sat, 11 Apr 2015 23:27:13 +0900

libuninameslist (0.3.20130501-4) unstable; urgency=medium

  * debian/control
    - set Standards-Version: 3.9.6 
    - add Vcs-* field
  * debian/watch
    - update it to deal with upstream's change

 -- Hideki Yamane <henrich@debian.org>  Sat, 11 Apr 2015 23:19:06 +0900

libuninameslist (0.3.20130501-3) unstable; urgency=low

  [ Vasudev Kamath ]
  * Fixed the watch file

  [ Hideki Yamane ]
  * debian/control: add Homepage field

 -- Hideki Yamane <henrich@debian.org>  Sun, 06 Oct 2013 00:49:26 +0900

libuninameslist (0.3.20130501-2) unstable; urgency=low

  * upload to unstable 
  * debian/watch
    - update it to watch github 

 -- Hideki Yamane <henrich@debian.org>  Fri, 03 May 2013 21:29:10 +0900

libuninameslist (0.3.20130501-1) experimental; urgency=low

  * New upstream release
  * debian/patches
    - remove delete_aclocal_m4.patch since upstream drops aclocal_m4
    - remove libtool-shell.patch since upstream drops Makefile.in
    - drop CPPFLAGS.patch since it's unnecessary anymore
    - add use_bash.patch

 -- Hideki Yamane <henrich@debian.org>  Fri, 03 May 2013 09:40:22 +0900

libuninameslist (0.0.20091231-5) unstable; urgency=low

  * Upload to unstable 

 -- Hideki Yamane <henrich@debian.org>  Fri, 03 May 2013 09:38:48 +0900

libuninameslist (0.0.20091231-4) experimental; urgency=low

  * debian/control
    - fix lintian warning: "description-synopsis-starts-with-article"
  * debian/patches/CPPFLAGS.patch
    - more hardening with missing CPPFLAGS 

 -- Hideki Yamane <henrich@debian.org>  Sat, 20 Apr 2013 07:43:44 +0900

libuninameslist (0.0.20091231-3) experimental; urgency=low

  * debian/rules
    - enable hardening 

 -- Hideki Yamane <henrich@debian.org>  Fri, 19 Apr 2013 11:15:45 +0900

libuninameslist (0.0.20091231-2) unstable; urgency=low

  * debian/control
    - New maintainer, thanks to Kęstutis Biliūnas for the work you've done
      (Closes: #704969)
    - remove obsolete Dm-Upload-Allowed: field
    - set "Standards-Version: 3.9.4"
    - set "Build-Depends: debhelper (>= 9)
    - use "Build-Depends: dh-autoreconf"
    - add "Pre-Depends: {misc:Pre-Depends}" for Multi-Arch
  * debian/compat
    - set 9
  * debian/rules
    - use dh style
  * debian/patches
    - add delete_aclocal_m4.patch to avoid build failure
  * libuninameslist-dev.install
    - don't install *.la files
    - adjust library path for Multi-Arch
  * libuninameslist0.install
    - adjust library path for Multi-Arch

 -- Hideki Yamane <henrich@debian.org>  Fri, 19 Apr 2013 10:41:05 +0900

libuninameslist (0.0.20091231-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS: eval: 1: base_compile+= -c: not found":
    add patch from Ubuntu / Colin Watson:
    - Set SHELL in Makefile.in so that the expansion of @LIBTOOL@ works
    Closes: #621933
    LP: #829445

 -- gregor herrmann <gregoa@debian.org>  Thu, 29 Sep 2011 17:12:11 +0200

libuninameslist (0.0.20091231-1) unstable; urgency=low

  * New upstream release. Updated to Unicode 5.2 (Closes: #562596).
  * Switched to 3.0 (quilt) source format.
  * debian/control:
    - switched to debhelper v7;
    - add ${misc:Depends} to dependencies to properly cope with 
      debhelper-triggerred dependencies;
    - bumped Standards-Version to 3.8.3. No changes required.
  * debian/patches: removed the patch 001_add_libtool_mode.diff
    - fixed upstream.

 -- Kęstutis Biliūnas <kebil@kaunas.init.lt>  Fri, 01 Jan 2010 13:51:47 +0200

libuninameslist (0.0.20080409-2) unstable; urgency=low

  * debian/control:
    - added the autotools-dev and quilt packages to Build-Depends;
    - bumped Standards-Version to 3.8.1. No changes required.
  * debian/patches:
    - added the patch 001_add_libtool_mode.diff for fixing Makefile intall
      target.
  * debian/rules:
    - added copying files config.sub and config.guess from the autotools-dev
      package (Closes: #526548).

 -- Kęstutis Biliūnas <kebil@kaunas.init.lt>  Sat, 02 May 2009 10:16:44 +0300

libuninameslist (0.0.20080409-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - bumped Standards-Version to 3.8.0. No changes required.
    - added DM-Upload-Allowed: yes.
  * Fixed debian/rules clean target.
  * debian/watch: updated.

 -- Kęstutis Biliūnas <kebil@kaunas.init.lt>  Sat, 14 Feb 2009 19:12:30 +0200

libuninameslist (0.0.20060907-2) unstable; urgency=low

  * debian/control: added the libtool and automake packages to
    Build-Depends and removed autotools-dev package.
  * debian/rules: added 'libtoolize -c -f" in the configure stage
    (Closes: #401702).
  * Fixed debian/watch file.

 -- Kęstutis Biliūnas <kebil@kaunas.init.lt>  Fri, 12 Jan 2007 22:29:35 +0200

libuninameslist (0.0.20060907-1) unstable; urgency=low

  * New upstream release.
  * Bumped Standards-Version to 3.7.2. No changes required.
  * Fixed debian/watch file.

 -- Kęstutis Biliūnas <kebil@kaunas.init.lt>  Mon, 23 Oct 2006 16:15:12 +0300

libuninameslist (0.0.20050712-1) unstable; urgency=low

  * Initial release (Closes: Bug#361538).

 -- Kęstutis Biliūnas <kebil@kaunas.init.lt>  Tue,  4 Apr 2006 23:31:56 +0300

