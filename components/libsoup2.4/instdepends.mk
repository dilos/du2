INSTDEPENDS += debhelper
INSTDEPENDS += gnome-pkg-tools
INSTDEPENDS += intltool
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += glib-networking
INSTDEPENDS += libsqlite3-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += libkrb5-dev
INSTDEPENDS += gtk-doc-tools
INSTDEPENDS += shared-mime-info
INSTDEPENDS += libgirepository1.0-dev
INSTDEPENDS += gobject-introspection
INSTDEPENDS += dbus
INSTDEPENDS += curl
INSTDEPENDS += valac
#Build-Depends-Indep:
INSTDEPENDS += libglib2.0-doc
