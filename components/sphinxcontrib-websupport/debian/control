Source: sphinxcontrib-websupport
Section: python
Priority: optional
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: Dmitry Shachnev <mitya57@debian.org>
Homepage: https://github.com/sphinx-doc/sphinxcontrib-websupport
Build-Depends: debhelper (>= 11),
               dh-python,
               dpkg-dev (>= 1.17.14),
               python-all,
               python-pytest <!nocheck>,
               python-setuptools,
               python-sphinx (>= 1.6) <!nocheck>,
               python-sqlalchemy <!nocheck>,
               python-whoosh <!nocheck>,
               python-xapian <!nocheck>,
               python3-all,
               python3-pytest <!nocheck>,
               python3-setuptools,
               python3-sphinx (>= 1.6) <!nocheck>,
               python3-sqlalchemy <!nocheck>,
               python3-whoosh <!nocheck>,
               python3-xapian <!nocheck>
Standards-Version: 4.1.4
Vcs-Git: https://salsa.debian.org/python-team/modules/sphinxcontrib-websupport.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/sphinxcontrib-websupport

Package: python-sphinxcontrib.websupport
Architecture: all
Depends: python-sphinx (>= 1.6), ${misc:Depends}, ${python:Depends}
Recommends: python-sqlalchemy, python-whoosh, python-xapian
Description: API to integrate Sphinx documentation into Web applications (Python 2)
 This module provides a means for integrating documentation built with Sphinx
 into web applications. It supports comments, storage (with SQLAlchemy), and
 search engines (whoosh and xapian).
 .
 This is the Python 2 version of sphinxcontrib-websupport.

Package: python3-sphinxcontrib.websupport
Architecture: all
Depends: python3-sphinx (>= 1.6), ${misc:Depends}, ${python3:Depends}
Recommends: python3-sqlalchemy, python3-whoosh, python3-xapian
Description: API to integrate Sphinx documentation into Web applications (Python 3)
 This module provides a means for integrating documentation built with Sphinx
 into web applications. It supports comments, storage (with SQLAlchemy), and
 search engines (whoosh and xapian).
 .
 This is the Python 3 version of sphinxcontrib-websupport.
