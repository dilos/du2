Source: liblinear
Section: libs
Priority: optional
Maintainer: Christian Kastner <ckk@debian.org>
Uploaders: Chen-Tse Tsai <ctse.tsai@gmail.com>
Build-Depends:
    debhelper (>= 9),
    dh-python,
    python-all (>= 2.6.6-3~),
    python3-all,
    libblas-dev
Standards-Version: 3.9.8
Homepage: http://www.csie.ntu.edu.tw/~cjlin/liblinear/
Vcs-Git: https://anonscm.debian.org/git/debian-science/packages/liblinear.git
Vcs-Browser: https://anonscm.debian.org/cgit/debian-science/packages/liblinear.git
X-Python-Version: >= 2.5
X-Python3-Version: >= 3.0

Package: liblinear-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
    ${misc:Depends},
    liblinear3 (= ${binary:Version}),
    libblas-dev | libblas.so
Description: Development libraries and header files for LIBLINEAR
 LIBLINEAR is a library for learning linear classifiers for large scale
 applications. It supports Support Vector Machines (SVM) with L2 and L1
 loss, logistic regression, multi class classification and also Linear
 Programming Machines (L1-regularized SVMs). Its computational complexity
 scales linearly with the number of training examples making it one of
 the fastest SVM solvers around.
 .
 This package contains the header files and static libraries.

Package: liblinear3
Architecture: any
Multi-Arch: same
Pre-Depends:
    ${misc:Pre-Depends}
Depends:
    ${shlibs:Depends},
    ${misc:Depends}
Suggests:
    liblinear-tools (= ${binary:Version}),
    liblinear-dev (= ${binary:Version})
Description: Library for Large Linear Classification
 LIBLINEAR is a library for learning linear classifiers for large scale
 applications. It supports Support Vector Machines (SVM) with L2 and L1
 loss, logistic regression, multi class classification and also Linear
 Programming Machines (L1-regularized SVMs). Its computational complexity
 scales linearly with the number of training examples making it one of
 the fastest SVM solvers around. It also provides Python bindings.
 .
 This package contains the shared libraries.

Package: liblinear-tools
Section: science
Architecture: any
Multi-Arch: foreign
Depends:
    ${shlibs:Depends},
    ${misc:Depends},
    liblinear3 (= ${binary:Version})
Suggests:
    libsvm-tools
Description: Standalone applications for LIBLINEAR
 LIBLINEAR is a library for learning linear classifiers for large scale
 applications. It supports Support Vector Machines (SVM) with L2 and L1
 loss, logistic regression, multi class classification and also Linear
 Programming Machines (L1-regularized SVMs). Its computational complexity
 scales linearly with the number of training examples making it one of
 the fastest SVM solvers around. It also provides Python bindings.
 .
 This package contains the standalone applications.

Package: python-liblinear
Section: python
Architecture: all
Depends:
    ${python:Depends},
    ${misc:Depends},
    liblinear3 (>= ${binary:Version}),
    liblinear3 (<< ${binary:Version}.1~)
Description: Python bindings for LIBLINEAR
 LIBLINEAR is a library for learning linear classifiers for large scale
 applications. It supports Support Vector Machines (SVM) with L2 and L1
 loss, logistic regression, multi class classification and also Linear
 Programming Machines (L1-regularized SVMs). Its computational complexity
 scales linearly with the number of training examples making it one of
 the fastest SVM solvers around. It also provides Python bindings.
 .
 This package contains the Python bindings.

Package: python3-liblinear
Section: python
Architecture: all
Depends:
    ${python3:Depends},
    ${misc:Depends},
    liblinear3 (>= ${binary:Version}),
    liblinear3 (<< ${binary:Version}.1~)
Description: Python 3 bindings for LIBLINEAR
 LIBLINEAR is a library for learning linear classifiers for large scale
 applications. It supports Support Vector Machines (SVM) with L2 and L1
 loss, logistic regression, multi class classification and also Linear
 Programming Machines (L1-regularized SVMs). Its computational complexity
 scales linearly with the number of training examples making it one of
 the fastest SVM solvers around. It also provides Python bindings.
 .
 This package contains the Python 3 bindings.
