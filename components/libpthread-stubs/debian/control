Source: libpthread-stubs
Priority: optional
Section: libdevel
Maintainer: XCB Developers <xcb@lists.freedesktop.org>
Uploaders: Julien Danjou <acid@debian.org>
Build-Depends: debhelper (>= 8.1.3), dh-autoreconf
Standards-Version: 3.8.3
Vcs-Git: git://anonscm.debian.org/git/collab-maint/libpthread-stubs
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=collab-maint/libpthread-stubs.git

Package: libpthread-stubs0-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}
Description: pthread stubs not provided by native libc, development files
 This library provides weak aliases for pthread functions not provided
 in libc or otherwise available by default.  Libraries like libxcb rely
 on pthread stubs to use pthreads optionally, becoming thread-safe when
 linked to libpthread, while avoiding any performance hit when running
 single-threaded. libpthread-stubs supports this behavior even on
 platforms which do not supply all the necessary pthread stubs.
 .
 On platforms which already supply all the necessary pthread stubs, this
 package ships only the pkg-config file pthread-stubs.pc, to allow
 libraries to unconditionally express a dependency on pthread-stubs and
 still obtain correct behavior.
