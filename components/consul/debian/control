Source: consul
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Dmitry Smirnov <onlyjob@debian.org>,
           Martín Ferrari <tincho@debian.org>,
           Tianon Gravi <tianon@debian.org>,
           Tim Potter <tpot@hpe.com>,
Build-Depends: debhelper (>= 11~) ,dh-golang ,golang-any
    ,golang-github-armon-circbuf-dev
    ,golang-github-armon-go-metrics-dev (>= 0.0~git20171117~)
    ,golang-github-armon-go-radix-dev
#   ,golang-github-beorn7-perks-dev
    ,golang-github-bgentry-speakeasy-dev
    ,golang-github-circonus-labs-circonus-gometrics-dev (>= 1.2.0~)
    ,golang-github-circonus-labs-circonusllhist-dev
    ,golang-github-datadog-datadog-go-dev
    ,golang-github-davecgh-go-spew-dev
    ,golang-github-denverdino-aliyungo-dev
    ,golang-github-elazarl-go-bindata-assetfs-dev (>= 0.0~git20151224~)
###    ,golang-github-fsouza-go-dockerclient-dev
    ,golang-github-google-gofuzz-dev
    ,golang-github-gophercloud-gophercloud-dev
    ,golang-github-hashicorp-go-checkpoint-dev
    ,golang-github-hashicorp-go-cleanhttp-dev (>= 0.0~git20171219~)
    ,golang-github-hashicorp-go-immutable-radix-dev (>= 0.0~git20170725~)
    ,golang-github-hashicorp-golang-lru-dev (>= 0.0~git20160207~)
    ,golang-github-hashicorp-go-memdb-dev (>= 0.0~git20180224~)
    ,golang-github-hashicorp-go-msgpack-dev
    ,golang-github-hashicorp-go-multierror-dev
    ,golang-github-hashicorp-go-reap-dev
    ,golang-github-hashicorp-go-retryablehttp-dev
    ,golang-github-hashicorp-go-rootcerts-dev
    ,golang-github-hashicorp-go-sockaddr-dev
    ,golang-github-hashicorp-go-syslog-dev
    ,golang-github-hashicorp-go-uuid-dev
    ,golang-github-hashicorp-go-version-dev
    ,golang-github-hashicorp-hcl-dev
    ,golang-github-hashicorp-hil-dev (>= 0.0~git20160711~)
    ,golang-github-hashicorp-logutils-dev
    ,golang-github-hashicorp-memberlist-dev (>= 0.1.0+git20180209~)
    ,golang-github-hashicorp-net-rpc-msgpackrpc-dev
    ,golang-github-hashicorp-raft-boltdb-dev
    ,golang-github-hashicorp-raft-dev
    ,golang-github-hashicorp-scada-client-dev
    ,golang-github-hashicorp-serf-dev (>= 0.8.1+git20180508~)
    ,golang-github-hashicorp-yamux-dev (>= 0.0~git20151129~)
    ,golang-github-inconshreveable-muxado-dev
    ,golang-github-mattn-go-isatty-dev
    ,golang-github-miekg-dns-dev
    ,golang-github-mitchellh-cli-dev (>= 0.0~git20171129~)
    ,golang-github-mitchellh-copystructure-dev
    ,golang-github-mitchellh-hashstructure-dev
    ,golang-github-mitchellh-mapstructure-dev
    ,golang-github-mitchellh-reflectwalk-dev
    ,golang-github-nytimes-gziphandler-dev
    ,golang-github-pmezard-go-difflib-dev
    ,golang-github-posener-complete-dev
    ,golang-github-ryanuber-columnize-dev
    ,golang-github-sergi-go-diff-dev
    ,golang-github-shirou-gopsutil-dev
    ,golang-golang-x-sys-dev (>= 0.0~git20161012~)
Standards-Version: 4.1.4
Homepage: https://github.com/hashicorp/consul
Vcs-Browser: https://salsa.debian.org/go-team/packages/consul
Vcs-Git: https://salsa.debian.org/go-team/packages/consul.git
XS-Go-Import-Path: github.com/hashicorp/consul
Testsuite: autopkgtest-pkg-go

Package: golang-github-hashicorp-consul-dev
Architecture: all
Depends: ${misc:Depends} ,${shlibs:Depends}
    ,golang-github-armon-go-metrics-dev (>= 0.0~git20171117~)
    ,golang-github-armon-go-radix-dev
    ,golang-github-hashicorp-go-cleanhttp-dev
    ,golang-github-hashicorp-golang-lru-dev (>= 0.0~git20160207~)
    ,golang-github-hashicorp-go-memdb-dev
    ,golang-github-hashicorp-go-msgpack-dev
    ,golang-github-hashicorp-go-rootcerts-dev
    ,golang-github-hashicorp-go-uuid-dev
    ,golang-github-hashicorp-hcl-dev
    ,golang-github-hashicorp-hil-dev (>= 0.0~git20160711~)
    ,golang-github-hashicorp-memberlist-dev (>= 0.1.0+git20180209~)
    ,golang-github-hashicorp-raft-boltdb-dev
    ,golang-github-hashicorp-raft-dev
    ,golang-github-hashicorp-serf-dev (>= 0.8.1+git20180508~)
    ,golang-github-hashicorp-yamux-dev (>= 0.0~git20151129~)
    ,golang-github-inconshreveable-muxado-dev
    ,golang-github-miekg-dns-dev
    ,golang-github-mitchellh-cli-dev (>= 0.0~git20171129~)
    ,golang-github-mitchellh-copystructure-dev
    ,golang-golang-x-sys-dev (>= 0.0~git20161012~)
Description: tool for service discovery, monitoring and configuration (source)
 Consul is a tool for service discovery and configuration. Consul is
 distributed, highly available, and extremely scalable.
 .
 Consul provides several key features:
 .
  - Service Discovery - Consul makes it simple for services to register
    themselves and to discover other services via a DNS or HTTP interface.
    External services such as SaaS providers can be registered as well.
 .
  - Health Checking - Health Checking enables Consul to quickly alert operators
    about any issues in a cluster. The integration with service discovery
    prevents routing traffic to unhealthy hosts and enables service level
    circuit breakers.
 .
  - Key/Value Storage - A flexible key/value store enables storing dynamic
    configuration, feature flagging, coordination, leader election and more.
    The simple HTTP API makes it easy to use anywhere.
 .
  - Multi-Datacenter - Consul is built to be datacenter aware, and can support
    any number of regions without complex configuration.
 .
 Consul runs on Linux, Mac OS X, and Windows. It is recommended to run the
 Consul servers only on Linux, however.
 .
 This package contains the source.

Package: consul
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends} ,adduser
Built-Using: ${misc:Built-Using}
Description: tool for service discovery, monitoring and configuration
 Consul is a tool for service discovery and configuration. Consul is
 distributed, highly available, and extremely scalable.
 .
 Consul provides several key features:
 .
  - Service Discovery - Consul makes it simple for services to register
    themselves and to discover other services via a DNS or HTTP interface.
    External services such as SaaS providers can be registered as well.
 .
  - Health Checking - Health Checking enables Consul to quickly alert operators
    about any issues in a cluster. The integration with service discovery
    prevents routing traffic to unhealthy hosts and enables service level
    circuit breakers.
 .
  - Key/Value Storage - A flexible key/value store enables storing dynamic
    configuration, feature flagging, coordination, leader election and more.
    The simple HTTP API makes it easy to use anywhere.
 .
  - Multi-Datacenter - Consul is built to be datacenter aware, and can support
    any number of regions without complex configuration.
 .
 Consul runs on Linux, Mac OS X, and Windows. It is recommended to run the
 Consul servers only on Linux, however.
