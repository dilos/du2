Source: gcc-6
Section: devel
Priority: optional
Maintainer: Igor Kozhukhov <ikozhukhov@gmail.com>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 5.0.62), dpkg-dev (>= 1.17.11), 
  g++-multilib [amd64 i386 kfreebsd-amd64 mips mips64 mips64el mipsel mipsn32 mipsn32el powerpc ppc64 s390 s390x sparc sparc64 x32], 
  libc6.1-dev (>= 2.13-5) [alpha ia64] | libc0.3-dev (>= 2.13-5) [hurd-i386] | libc0.1-dev (>= 2.13-5) [kfreebsd-i386 kfreebsd-amd64] | libc6-dev (>= 2.13-5) [!solaris-any], libc6-dev (>= 2.13-31) [armel armhf], libc6-dev-amd64 [i386 x32], libc6-dev-sparc64 [sparc], libc6-dev-sparc [sparc64], libc6-dev-s390 [s390x], libc6-dev-s390x [s390], libc6-dev-i386 [amd64 x32], libc6-dev-powerpc [ppc64], libc6-dev-ppc64 [powerpc], libc0.1-dev-i386 [kfreebsd-amd64], lib32gcc1 [amd64 ppc64 kfreebsd-amd64 mipsn32 mipsn32el mips64 mips64el s390x sparc64 x32], libn32gcc1 [mips mipsel mips64 mips64el], lib64gcc1 [i386 mips mipsel mipsn32 mipsn32el powerpc sparc s390 x32], libc6-dev-mips64 [mips mipsel mipsn32 mipsn32el], libc6-dev-mipsn32 [mips mipsel mips64 mips64el], libc6-dev-mips32 [mipsn32 mipsn32el mips64 mips64el], libc6-dev-x32 [amd64 i386], libx32gcc1 [amd64 i386], libc6.1-dbg [alpha ia64] | libc0.3-dbg [hurd-i386] | libc0.1-dbg [kfreebsd-i386 kfreebsd-amd64] | libc6-dbg [!solaris-any],
  libc-dev [solaris-any],
  kfreebsd-kernel-headers (>= 0.84) [kfreebsd-any], 
  m4, libtool, autoconf2.64 | autoconf [solaris-any], 
  libunwind7-dev (>= 0.98.5-6) [ia64], libatomic-ops-dev [ia64], 
  autogen, gawk, lzma, xz-utils, patchutils, 
  zlib1g-dev, systemtap-sdt-dev [linux-any kfreebsd-any hurd-any], 
  binutils (>= 2.25-3~) | binutils-multiarch (>= 2.25-3~), binutils-hppa64 (>= 2.25-3~) [hppa], 
  gperf (>= 3.0.1), bison (>= 1:2.3), flex, gettext, 
  gdb, 
  texinfo (>= 4.3), locales [!solaris-any], sharutils, 
  procps, zlib1g-dev, libantlr-java, python, libffi-dev, fastjar, libmagic-dev, libecj-java (>= 3.3.0-2), zip, libasound2-dev [ !hurd-any !kfreebsd-any !solaris-any], libxtst-dev, libxt-dev, libgtk2.0-dev (>= 2.4.4-2), libart-2.0-dev, libcairo2-dev, netbase, 
  libcloog-isl-dev (>= 0.18), libmpc-dev (>= 1.0), libmpfr-dev (>= 3.0.0-9~), libgmp-dev (>= 2:5.0.1~), 
  dejagnu [!m68k], realpath (>= 1.9.12), chrpath, lsb-release, quilt,
  sun-as [solaris-sparc], libcpp [solaris-sparc]
Build-Depends-Indep: doxygen (>= 1.7.2), graphviz (>= 2.2), ghostscript, texlive-latex-base, xsltproc, libxml2-utils, docbook-xsl-ns, 
Homepage: http://gcc.gnu.org/
Vcs-Browser: http://svn.debian.org/viewsvn/gcccvs/branches/sid/gcc-6/
Vcs-Svn: svn://anonscm.debian.org/gcccvs/branches/sid/gcc-6

Package: gcc-6-libatomic
Architecture: any
Section: libs
Priority: required
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: GCC support library
 Shared version of the support library, a library of internal subroutines
 that GCC uses to overcome shortcomings of particular machines, or
 special needs for some languages.

Package: gcc-6-libcc1
Architecture: any
Section: libs
Priority: required
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: GCC support library
 Shared version of the support library, a library of internal subroutines
 that GCC uses to overcome shortcomings of particular machines, or
 special needs for some languages.

Package: gcc-6-libcilkrts
Architecture: any
Section: libs
Priority: required
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: GCC support library
 Shared version of the support library, a library of internal subroutines
 that GCC uses to overcome shortcomings of particular machines, or
 special needs for some languages.

Package: gcc-6-libgcc
Architecture: any
Section: libs
Priority: required
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: GCC support library
 Shared version of the support library, a library of internal subroutines
 that GCC uses to overcome shortcomings of particular machines, or
 special needs for some languages.

Package: gcc-6-libgfortran
Architecture: any
Section: libs
Priority: required
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: GCC support library
 Shared version of the support library, a library of internal subroutines
 that GCC uses to overcome shortcomings of particular machines, or
 special needs for some languages.

Package: gcc-6-libgomp
Architecture: any
Section: libs
Priority: required
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: GCC support library
 Shared version of the support library, a library of internal subroutines
 that GCC uses to overcome shortcomings of particular machines, or
 special needs for some languages.

Package: gcc-6-libitm
Architecture: any
Section: libs
Priority: required
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: GCC support library
 Shared version of the support library, a library of internal subroutines
 that GCC uses to overcome shortcomings of particular machines, or
 special needs for some languages.

Package: gcc-6-libobjc
Architecture: any
Section: libs
Priority: required
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: GCC support library
 Shared version of the support library, a library of internal subroutines
 that GCC uses to overcome shortcomings of particular machines, or
 special needs for some languages.

Package: gcc-6-libquadmath
Architecture: solaris-i386
Section: libs
Priority: required
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: GCC support library
 Shared version of the support library, a library of internal subroutines
 that GCC uses to overcome shortcomings of particular machines, or
 special needs for some languages.

Package: gcc-6-libssp
Architecture: any
Section: libs
Priority: required
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: GCC support library
 Shared version of the support library, a library of internal subroutines
 that GCC uses to overcome shortcomings of particular machines, or
 special needs for some languages.

Package: gcc-6-libstdc++
Architecture: any
Section: libs
Priority: required
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: GCC support library
 Shared version of the support library, a library of internal subroutines
 that GCC uses to overcome shortcomings of particular machines, or
 special needs for some languages.

Package: gcc-6-libgo
Architecture: any
Section: libs
Priority: required
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: GCC support library
 Shared version of the support library, a library of internal subroutines
 that GCC uses to overcome shortcomings of particular machines, or
 special needs for some languages.

Package: gcc-6
Architecture: any
Multi-Arch: same
Section: libs
Priority: required
Provides: c++-6, cpp-6, g++-6, gcc-6, gcc-ar-6, gcc-nm-6, gcc-ranlib-6, gcov-6, gfortran-6, gccgo-6, gobjc-6,
  libstdc++-6-dev
Depends: ${shlibs:Depends}, ${misc:Depends},
  gcc-6-libcc1 (= ${binary:Version}),
  gcc-6-libgcc (= ${binary:Version}),
  gcc-6-libatomic (= ${binary:Version}),
  gcc-6-libcilkrts (= ${binary:Version}),
  gcc-6-libgfortran (= ${binary:Version}),
  gcc-6-libgomp (= ${binary:Version}),
  gcc-6-libitm (= ${binary:Version}),
  gcc-6-libobjc (= ${binary:Version}),
  gcc-6-libquadmath (= ${binary:Version}) [solaris-i386],
  gcc-6-libssp (= ${binary:Version}),
  gcc-6-libstdc++ (= ${binary:Version}),
  sun-as [solaris-sparc],
  libcpp [solaris-sparc],
  binutils,
  system-header,
  system-crt,
  developer-linker,
  gcc-6-libgo (= ${binary:Version})
#Replaces: ${base:Replaces}
#Breaks: gcc-4.4-base (<< 4.4.7), gcc-4.7-base (<< 4.7.3), gcj-4.4-base (<< 4.4.6-9~), gnat-4.4-base (<< 4.4.6-3~), gcj-4.6-base (<< 4.6.1-4~), gnat-4.6 (<< 4.6.1-5~), dehydra (<= 0.9.hg20110609-2)
Description: GCC, the GNU Compiler Collection
 This package contains files common to all languages and libraries
 contained in the GNU Compiler Collection (GCC).

#Package: gcc-6-source
#Architecture: all
#Priority: optional
#Depends: make, autoconf2.64, quilt, patchutils, gawk, ${misc:Depends}
#Description: Source of the GNU Compiler Collection
# This package contains the sources and patches which are needed to
# build the GNU Compiler Collection (GCC).
