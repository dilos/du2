--- a/gcc/config/i386/i386.c
+++ b/gcc/config/i386/i386.c
@@ -2443,6 +2443,13 @@
 
    saved frame pointer			if frame_pointer_needed
 					<- HARD_FRAME_POINTER
+
+   [-msave-args]
+					<- arg_save_offset
+
+   [saveargs padding]
+					<- force-save-regs-using-mov
+
    [saved regs]
 					<- regs_save_offset
    [padding0]
@@ -2460,6 +2467,7 @@
   */
 struct ix86_frame
 {
+  int nmsave_args;
   int nsseregs;
   int nregs;
   int va_arg_size;
@@ -2471,6 +2479,7 @@
   HOST_WIDE_INT hard_frame_pointer_offset;
   HOST_WIDE_INT stack_pointer_offset;
   HOST_WIDE_INT hfp_save_offset;
+  HOST_WIDE_INT arg_save_offset;
   HOST_WIDE_INT reg_save_offset;
   HOST_WIDE_INT sse_reg_save_offset;
 
@@ -2606,6 +2615,9 @@
 
 static enum calling_abi ix86_function_abi (const_tree);
 
+static int ix86_nsaved_args (void);
+static void pro_epilogue_adjust_stack (rtx, rtx, rtx, int, bool);
+
 
 #ifndef SUBTARGET32_DEFAULT_CPU
 #define SUBTARGET32_DEFAULT_CPU "i386"
@@ -5203,6 +5215,9 @@
     opts->x_ix86_isa_flags
       |= OPTION_MASK_ISA_LZCNT & ~opts->x_ix86_isa_flags_explicit;
 
+  if (!TARGET_64BIT_P (opts->x_ix86_isa_flags) && TARGET_SAVE_ARGS)
+    error ("-msave-args makes no sense in the 32-bit mode");
+
   /* Validate -mpreferred-stack-boundary= value or default it to
      PREFERRED_STACK_BOUNDARY_DEFAULT.  */
   ix86_preferred_stack_boundary = PREFERRED_STACK_BOUNDARY_DEFAULT;
@@ -10926,7 +10941,7 @@
 
   ix86_compute_frame_layout (&frame);
   return (frame.stack_pointer_offset == UNITS_PER_WORD
-	  && (frame.nregs + frame.nsseregs) == 0);
+	  && (frame.nmsave_args + frame.nregs + frame.nsseregs) == 0);
 }
 
 /* Value should be nonzero if functions must have frame pointers.
@@ -10950,6 +10965,9 @@
   if (TARGET_32BIT_MS_ABI && cfun->calls_setjmp)
     return true;
 
+  if (TARGET_SAVE_ARGS)
+    return true;
+
   /* Win64 SEH, very large frames need a frame-pointer as maximum stack
      allocation is 4GB.  */
   if (TARGET_64BIT_MS_ABI && get_frame_size () > SEH_MAX_FRAME_SIZE)
@@ -11382,6 +11400,7 @@
 
   frame->nregs = ix86_nsaved_regs ();
   frame->nsseregs = ix86_nsaved_sseregs ();
+  frame->nmsave_args = ix86_nsaved_args ();
 
   /* 64-bit MS ABI seem to require stack alignment to be always 16,
      except for function prologues, leaf functions and when the defult
@@ -11444,7 +11463,8 @@
     }
 
   frame->save_regs_using_mov
-    = (TARGET_PROLOGUE_USING_MOVE && cfun->machine->use_fast_prologue_epilogue
+    = ((TARGET_FORCE_SAVE_REGS_USING_MOV ||
+	(TARGET_PROLOGUE_USING_MOVE && cfun->machine->use_fast_prologue_epilogue))
        /* If static stack checking is enabled and done with probes,
 	  the registers need to be saved before allocating the frame.  */
        && flag_stack_check != STATIC_BUILTIN_STACK_CHECK);
@@ -11464,6 +11484,13 @@
   /* The traditional frame pointer location is at the top of the frame.  */
   frame->hard_frame_pointer_offset = offset;
 
+  if (TARGET_SAVE_ARGS)
+    {
+      offset += frame->nmsave_args * UNITS_PER_WORD;
+      offset += (frame->nmsave_args % 2) * UNITS_PER_WORD;
+    }
+  frame->arg_save_offset = offset;
+
   /* Register save area */
   offset += frame->nregs * UNITS_PER_WORD;
   frame->reg_save_offset = offset;
@@ -11541,8 +11568,9 @@
   /* Size prologue needs to allocate.  */
   to_allocate = offset - frame->sse_reg_save_offset;
 
-  if ((!to_allocate && frame->nregs <= 1)
-      || (TARGET_64BIT && to_allocate >= (HOST_WIDE_INT) 0x80000000))
+  if (!TARGET_SAVE_ARGS &&
+      ((!to_allocate && frame->nregs <= 1)
+       || (TARGET_64BIT && to_allocate >= (HOST_WIDE_INT) 0x80000000)))
     frame->save_regs_using_mov = false;
 
   if (ix86_using_red_zone ()
@@ -11553,7 +11581,11 @@
     {
       frame->red_zone_size = to_allocate;
       if (frame->save_regs_using_mov)
-	frame->red_zone_size += frame->nregs * UNITS_PER_WORD;
+	{
+	  frame->red_zone_size += frame->nregs * UNITS_PER_WORD;
+	  frame->red_zone_size += frame->nmsave_args * UNITS_PER_WORD;
+	  frame->red_zone_size += (frame->nmsave_args % 2) * UNITS_PER_WORD;
+	}
       if (frame->red_zone_size > RED_ZONE_SIZE - RED_ZONE_RESERVE)
 	frame->red_zone_size = RED_ZONE_SIZE - RED_ZONE_RESERVE;
     }
@@ -11584,6 +11616,22 @@
 	  frame->hard_frame_pointer_offset = frame->stack_pointer_offset - 128;
 	}
     }
+
+  if (getenv("DEBUG_FRAME_STUFF") != NULL)
+    {
+      printf("nmsave_args: %d\n", frame->nmsave_args);
+      printf("nsseregs: %d\n", frame->nsseregs);
+      printf("nregs: %d\n", frame->nregs);
+
+      printf("frame_pointer_offset: %llx\n", frame->frame_pointer_offset);
+      printf("hard_frame_pointer_offset: %llx\n", frame->hard_frame_pointer_offset);
+      printf("stack_pointer_offset: %llx\n", frame->stack_pointer_offset);
+      printf("hfp_save_offset: %llx\n", frame->hfp_save_offset);
+      printf("arg_save_offset: %llx\n", frame->arg_save_offset);
+      printf("reg_save_offset: %llx\n", frame->reg_save_offset);
+      printf("sse_reg_save_offset: %llx\n", frame->sse_reg_save_offset);
+
+    }
 }
 
 /* This is semi-inlined memory_address_length, but simplified
@@ -11692,6 +11740,23 @@
   unsigned int regno;
   rtx_insn *insn;
 
+  if (TARGET_SAVE_ARGS)
+    {
+      int i;
+      int nsaved = ix86_nsaved_args ();
+      int start = cfun->returns_struct;
+
+      for (i = start; i < start + nsaved; i++)
+	{
+	  regno = x86_64_int_parameter_registers[i];
+	  insn = emit_insn (gen_push (gen_rtx_REG (word_mode, regno)));
+	  RTX_FRAME_RELATED_P (insn) = 1;
+	}
+      if (nsaved % 2 != 0)
+	pro_epilogue_adjust_stack (stack_pointer_rtx, stack_pointer_rtx,
+				   GEN_INT (-UNITS_PER_WORD), -1, false);
+    }
+
   for (regno = FIRST_PSEUDO_REGISTER - 1; regno-- > 0; )
     if (GENERAL_REGNO_P (regno) && ix86_save_reg (regno, true))
       {
@@ -11779,9 +11844,30 @@
 /* Emit code to save registers using MOV insns.
    First register is stored at CFA - CFA_OFFSET.  */
 static void
-ix86_emit_save_regs_using_mov (HOST_WIDE_INT cfa_offset)
+ix86_emit_save_regs_using_mov (struct ix86_frame *frame)
 {
   unsigned int regno;
+  HOST_WIDE_INT cfa_offset = frame->arg_save_offset;
+
+  if (TARGET_SAVE_ARGS)
+    {
+      int i;
+      int nsaved = ix86_nsaved_args ();
+      int start = cfun->returns_struct;
+
+      /* We deal with this twice? */
+      if (nsaved % 2 != 0)
+	cfa_offset -= UNITS_PER_WORD;
+
+      for (i = start + nsaved - 1; i >= start; i--)
+	{
+	  regno = x86_64_int_parameter_registers[i];
+	  ix86_emit_save_reg_using_mov(word_mode, regno, cfa_offset);
+	  cfa_offset -= UNITS_PER_WORD;
+	}
+    }
+
+  cfa_offset = frame->reg_save_offset;
 
   for (regno = 0; regno < FIRST_PSEUDO_REGISTER; regno++)
     if (GENERAL_REGNO_P (regno) && ix86_save_reg (regno, true))
@@ -12823,7 +12909,7 @@
 	}
     }
 
-  int_registers_saved = (frame.nregs == 0);
+  int_registers_saved = (frame.nregs == 0 && frame.nmsave_args == 0);
   sse_registers_saved = (frame.nsseregs == 0);
 
   if (frame_pointer_needed && !m->fs.fp_valid)
@@ -12874,7 +12960,7 @@
 	       && (! TARGET_STACK_PROBE
 		   || frame.stack_pointer_offset < CHECK_STACK_LIMIT))
 	{
-	  ix86_emit_save_regs_using_mov (frame.reg_save_offset);
+	  ix86_emit_save_regs_using_mov (&frame);
 	  int_registers_saved = true;
 	}
     }
@@ -13117,7 +13203,7 @@
     }
 
   if (!int_registers_saved)
-    ix86_emit_save_regs_using_mov (frame.reg_save_offset);
+    ix86_emit_save_regs_using_mov (&frame);
   if (!sse_registers_saved)
     ix86_emit_save_sse_regs_using_mov (frame.sse_reg_save_offset);
 
@@ -13150,6 +13236,7 @@
      relative to the value of the stack pointer at the end of the function
      prologue, and moving instructions that access redzone area via frame
      pointer inside push sequence violates this assumption.  */
+  /* XXX: We may wish to do this when SAVE_ARGS in general */
   if (frame_pointer_needed && frame.red_zone_size)
     emit_insn (gen_memory_blockage ());
 
@@ -13377,6 +13464,7 @@
 
   /* See the comment about red zone and frame
      pointer usage in ix86_expand_prologue.  */
+  /* XXX: We may wish to do this when SAVE_ARGS in general */
   if (frame_pointer_needed && frame.red_zone_size)
     emit_insn (gen_memory_blockage ());
 
@@ -13570,6 +13658,35 @@
       ix86_emit_restore_regs_using_pop ();
     }
 
+  if (TARGET_SAVE_ARGS) {
+    /*
+     * For each saved argument, emit a restore note, to make sure it happens
+     * correctly within the shrink wrapping (I think).
+     *
+     * Note that 'restore' in this case merely means the rule is the same as
+     * it was on function entry, not that we have actually done a register
+     * restore (which of course, we haven't).
+     *
+     * If we do not do this, the DWARF code will emit sufficient restores to
+     * provide balance on its own initiative, which in the presence of
+     * -fshrink-wrap may actually _introduce_ unbalance (whereby we only
+     * .cfi_offset a register sometimes, but will always .cfi_restore it.
+     * This will trip an assert.)
+     */
+    int start = cfun->returns_struct;
+    int nsaved = ix86_nsaved_args();
+    int i;
+
+    for (i = start + nsaved - 1; i >= start; i--)
+      queued_cfa_restores
+	= alloc_reg_note (REG_CFA_RESTORE,
+			  gen_rtx_REG(Pmode,
+				      x86_64_int_parameter_registers[i]),
+			  queued_cfa_restores);
+
+    gcc_assert(m->fs.fp_valid);
+  }
+
   /* If we used a stack pointer and haven't already got rid of it,
      then do so now.  */
   if (m->fs.fp_valid)
@@ -14499,6 +14616,18 @@
   return !ix86_legitimate_constant_p (mode, x);
 }
 
+/* Return number of arguments to be saved on the stack with
+   -msave-args.  */
+
+static int
+ix86_nsaved_args (void)
+{
+  if (TARGET_SAVE_ARGS)
+    return crtl->args.info.regno - cfun->returns_struct;
+  else
+    return 0;
+}
+
 /*  Nonzero if the symbol is marked as dllimport, or as stub-variable,
     otherwise zero.  */
 
--- a/gcc/config/i386/i386.opt
+++ b/gcc/config/i386/i386.opt
@@ -499,6 +499,16 @@
 Target Report Mask(TLS_DIRECT_SEG_REFS)
 Use direct references against %gs when accessing tls data.
 
+msave-args
+Target Report Mask(SAVE_ARGS)
+Save integer arguments on the stack at function entry
+
+mforce-save-regs-using-mov
+Target Report Mask(FORCE_SAVE_REGS_USING_MOV)
+Save registers using push in function prologues.  This is intentionally
+undocumented and used for msave-args testing.
+
+
 mtune=
 Target RejectNegative Joined Var(ix86_tune_string)
 Schedule code for given CPU.
--- a/gcc/dwarf2out.c
+++ b/gcc/dwarf2out.c
@@ -20841,6 +20841,11 @@
     /* Add the calling convention attribute if requested.  */
     add_calling_convention_attribute (subr_die, decl);
 
+#ifdef TARGET_SAVE_ARGS
+  if (TARGET_SAVE_ARGS)
+    add_AT_flag (subr_die, DW_AT_SUN_amd64_parmdump, 1);
+#endif
+
   /* Output Dwarf info for all of the stuff within the body of the function
      (if it has one - it may be just a declaration).
 
--- /dev/null
+++ b/gcc/testsuite/gcc.target/i386/msave-args-mov.c
@@ -0,0 +1,26 @@
+/* { dg-do run { target { { i?86-*-solaris2.* } && lp64 } } } */
+/* { dg-options "-msave-args -mforce-save-regs-using-mov -save-temps" } */
+
+#include <stdio.h>
+
+void t(int, int, int, int, int) __attribute__ ((noinline));
+
+int
+main(int argc, char **argv)
+{
+	t(1, 2, 3, 4, 5);
+	return (0);
+}
+
+void
+t(int a, int b, int c, int d, int e)
+{
+	printf("%d %d %d %d %d", a, b, c, d, e);
+}
+
+/* { dg-final { scan-assembler "movq\t%rdi, -8\\(%rbp\\)" } } */
+/* { dg-final { scan-assembler "movq\t%rsi, -16\\(%rbp\\)" } } */
+/* { dg-final { scan-assembler "movq\t%rdx, -24\\(%rbp\\)" } } */
+/* { dg-final { scan-assembler "movq\t%rcx, -32\\(%rbp\\)" } } */
+/* { dg-final { scan-assembler "movq\t%r8, -40\\(%rbp\\)" } } */
+/* { dg-final { cleanup-saved-temps } } */
--- /dev/null
+++ b/gcc/testsuite/gcc.target/i386/msave-args-push.c
@@ -0,0 +1,26 @@
+/* { dg-do run { target { { i?86-*-solaris2.* } && lp64 } } } */
+/* { dg-options "-msave-args -save-temps " } */
+
+#include <stdio.h>
+
+void t(int, int, int, int, int) __attribute__ ((noinline));
+
+int
+main(int argc, char **argv)
+{
+	t(1, 2, 3, 4, 5);
+	return (0);
+}
+
+void
+t(int a, int b, int c, int d, int e)
+{
+	printf("%d %d %d %d %d", a, b, c, d, e);
+}
+
+/* { dg-final { scan-assembler "pushq\t%rdi" } } */
+/* { dg-final { scan-assembler "pushq\t%rsi" } } */
+/* { dg-final { scan-assembler "pushq\t%rdx" } } */
+/* { dg-final { scan-assembler "pushq\t%rcx" } } */
+/* { dg-final { scan-assembler "pushq\t%r8" } } */
+/* { dg-final { cleanup-saved-temps } } */
--- a/include/dwarf2.def
+++ b/include/dwarf2.def
@@ -411,6 +411,8 @@
 /* Biased integer extension.
    See https://gcc.gnu.org/wiki/DW_AT_GNU_bias .  */
 DW_TAG (DW_AT_GNU_bias, 0x2305)
+/* Sun extension. */
+DW_AT (DW_AT_SUN_amd64_parmdump, 0x2224)
 /* UPC extension.  */
 DW_AT (DW_AT_upc_threads_scaled, 0x3210)
 /* PGI (STMicroelectronics) extensions.  */
--- a/gcc/opth-gen.awk
+++ b/gcc/opth-gen.awk
@@ -371,7 +371,7 @@
 		print "#error too many masks for " var
 		print "#endif"
 	}
-	else if (masknum[var] > 31) {
+	else if (masknum[var] > 32) {
 		if (var == "")
 			print "#error too many target masks"
 		else
