INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
# Required scons version to understand -isystem, exported by krb5-config's
# CFLAGS as of 1.12.1+dfsg-9
INSTDEPENDS += scons
INSTDEPENDS += quilt
INSTDEPENDS += libapr1-dev
INSTDEPENDS += libaprutil1-dev
INSTDEPENDS += chrpath
INSTDEPENDS += libkrb5-dev
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += libssl-dev
