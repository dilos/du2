xserver-xorg-video-mga (1:1.6.5-1+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- Denis Kozadaev <denis@tambov.ru>  Sat, 25 Aug 2018 18:59:15 +0300

xserver-xorg-video-mga (1:1.6.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Move URL from description to Homepage field.
  * Switch URLs to https.
  * Bump Standards-Version to 3.9.8.

 -- Andreas Boll <andreas.boll.dev@gmail.com>  Wed, 18 Jan 2017 15:25:36 +0100

xserver-xorg-video-mga (1:1.6.4-2) unstable; urgency=medium

  [ Andreas Boll ]
  * Cherry-pick commit df094bf (Adapt Block/WakeupHandler signature for
    ABI 23) from upstream master branch.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 18 Nov 2016 13:08:11 +0200

xserver-xorg-video-mga (1:1.6.4-1) unstable; urgency=medium

  * Let uscan verify tarball signatures.
  * New upstream release.
  * Bump debhelper compat level to 9.

 -- Julien Cristau <jcristau@debian.org>  Sat, 02 May 2015 23:25:08 +0200

xserver-xorg-video-mga (1:1.6.3-2) unstable; urgency=medium

  * Recommend xserver-xorg-video-modesetting, for kms on G200 server chips.

 -- Julien Cristau <jcristau@debian.org>  Sun, 11 May 2014 01:24:24 +0200

xserver-xorg-video-mga (1:1.6.3-1) unstable; urgency=medium

  * New upstream release.
    + Fixes dual head crash without XAA (closes: #726002)
  * Drop patches 02_tentatively_unbreak_dual_head.diff and 03_kms-g200.diff,
    included upstream.
  * Remove Cyril Brulebois from Uploaders.

 -- Julien Cristau <jcristau@debian.org>  Mon, 20 Jan 2014 13:32:08 +0100

xserver-xorg-video-mga (1:1.6.2-1) unstable; urgency=low

  [ Maarten Lankhorst ]
  * New upstream release
  * Bump Standards-Version to 3.9.3 (no changes needed).

  [ Julien Cristau ]
  * Disable silent build rules.

 -- Julien Cristau <jcristau@debian.org>  Mon, 16 Sep 2013 23:15:39 +0200

xserver-xorg-video-mga (1:1.5.0-3) unstable; urgency=medium

  * Restrict the kernel driver matching to G200 server chips.  They're the
    ones using the mgag200 kms driver, other chips could be using matroxfb
    which is compatible with xf86-video-mga (closes: 697532).

 -- Julien Cristau <jcristau@debian.org>  Sat, 12 Jan 2013 16:03:52 +0100

xserver-xorg-video-mga (1:1.5.0-2) unstable; urgency=low

  * mga: don't bind to device if there is a kernel driver, to avoid conflicts
    with the mga g200se KMS driver in Linux 3.5.

 -- Julien Cristau <jcristau@debian.org>  Mon, 16 Jul 2012 22:57:15 +0200

xserver-xorg-video-mga (1:1.5.0-1) unstable; urgency=low

  * New upstream release, compatible with xserver 1.12.

 -- Julien Cristau <jcristau@debian.org>  Sun, 13 May 2012 11:33:27 +0200

xserver-xorg-video-mga (1:1.4.13.dfsg-4) unstable; urgency=low

  [ Tormod Volden ]
  * Merge from upstream up to 01ca2186ea028b2549de509b51726aa08519fce0
    - 01ca218... Reset tagfifo for renesas.
    - c083bf0... Added support for G200SE Pilot3
    - 43280e6... Don't include xf86Priv.h
  * Bump Standards-Version to 3.9.2 (no changes needed)

 -- Julien Cristau <jcristau@debian.org>  Sun, 23 Oct 2011 17:31:21 +0200

xserver-xorg-video-mga (1:1.4.13.dfsg-3) unstable; urgency=low

  * Merge from upstream up to 5f1b04e86e79938c8158055a777280a649f95510
    - 5f1b04e... added support for G200ER.
    - f7a2ef6... Sun's copyrights now belong to Oracle
    - 636c3c8... Don't allow the config file to override BIOS location
    - 0bd44fa... Purge cvs tags.
    - 951474c... modified G200SE conditionnal statement about revision
      register for products compatibility.
  * Add myself as uploader

 -- Tormod Volden <debian.tormod@gmail.com>  Mon, 21 Feb 2011 23:54:24 +0100

xserver-xorg-video-mga (1:1.4.13.dfsg-2) unstable; urgency=low

  * Switch to dh:
    - Use debhelper 8.
    - Use dh-autoreconf.
    - Bump xserver-xorg-dev build-dep for dh_xsf_substvars and xsf
      debhelper sequence.
  * Remove xsfbs (except repack.sh) accordingly.
  * Update Uploaders list. Thanks, David & Brice!
  * Remove long obsolete Replaces/Conflicts.
  * Wrap Depends/Provides/Suggests.
  * Bump Standards-Version to 3.9.1 (no changes needed).

 -- Cyril Brulebois <kibi@debian.org>  Sat, 05 Feb 2011 14:51:41 +0100

xserver-xorg-video-mga (1:1.4.13.dfsg-1) experimental; urgency=low

  [ Robert Hooker ]
  * New upstream release.
  * Bump xutils-dev requirement for new util-macros.

  [ Cyril Brulebois ]
  * Tweak watch file to match 1.* but not 1.9* so that 1.9* versions from
    2007 are ignored.

 -- Cyril Brulebois <kibi@debian.org>  Sat, 20 Nov 2010 17:03:31 +0100

xserver-xorg-video-mga (1:1.4.11.dfsg-5) experimental; urgency=low

  * Build against Xserver 1.9.1 rc1.

 -- Cyril Brulebois <kibi@debian.org>  Sat, 16 Oct 2010 18:59:45 +0200

xserver-xorg-video-mga (1:1.4.11.dfsg-4+squeeze1) unstable; urgency=low

  * Add patch: 02_tentatively_unbreak_dual_head.diff, tested by Ferenc
    Wágner (thanks!). Patch by Andy MacLean, stolen from LP's #292214
    (hopefully closes: #562209).

 -- Cyril Brulebois <kibi@debian.org>  Mon, 22 Nov 2010 18:05:24 +0100

xserver-xorg-video-mga (1:1.4.11.dfsg-4) unstable; urgency=low

  * Add support for G200EH, cherry-picked from upstream git (closes: #575271).

 -- Julien Cristau <jcristau@debian.org>  Tue, 11 May 2010 16:03:33 +0200

xserver-xorg-video-mga (1:1.4.11.dfsg-3) unstable; urgency=low

  [ Julien Cristau ]
  * Rename the build directory to not include DEB_BUILD_GNU_TYPE for no
    good reason.  Thanks, Colin Watson!
  * Remove myself from Uploaders

  [ Cyril Brulebois ]
  * Update to new xsfbs, replace deprecated ${xserver:Depends} with
    ${xviddriver:Depends} in Depends, and bump B-D on xserver-xorg-dev
    accordingly.
  * Add myself to Uploaders.
  * Bump Standards-Version from 3.8.3 to 3.8.4 (no changes needed).

 -- Cyril Brulebois <kibi@debian.org>  Mon, 03 May 2010 23:27:47 +0200

xserver-xorg-video-mga (1:1.4.11.dfsg-2) experimental; urgency=low

  [ Timo Aaltonen ]
  * Bump Standards-Version to 3.8.3.
  * Build against Xserver 1.7.

  [ Cyril Brulebois ]
  * Upload to experimental.

 -- Cyril Brulebois <kibi@debian.org>  Sun, 06 Dec 2009 02:47:06 +0100

xserver-xorg-video-mga (1:1.4.11.dfsg-1) unstable; urgency=low

  [ David Nusinow ]
  * Remove 01_gen_pci_ids.diff. The X server now uses an internal table to
    choose a driver during autoconfiguration.
  * Renumber 03_no_nonfree.diff to 01.

  [ Brice Goglin ]
  * New upstream release.
  * Add README.source, bump Standards-Version to 3.8.2.
  * Update debian/copyright from upstream's COPYING.

 -- Brice Goglin <bgoglin@debian.org>  Thu, 30 Jul 2009 12:33:04 +0200

xserver-xorg-video-mga (1:1.4.10.dfsg-1) unstable; urgency=low

  * New upstream release.
  * xserver-xorg-video-mga now suggests firmware-linux since
    a firmware may be needed during driver initialization.

 -- Brice Goglin <bgoglin@debian.org>  Mon, 27 Apr 2009 21:12:29 +0200

xserver-xorg-video-mga (1:1.4.9.dfsg-4) unstable; urgency=low

  * Upload to unstable.

 -- Julien Cristau <jcristau@debian.org>  Thu, 09 Apr 2009 10:33:47 +0100

xserver-xorg-video-mga (1:1.4.9.dfsg-3) experimental; urgency=low

  * Run autoreconf on build; add build-deps on automake, libtool, xutils-dev.
  * Handle parallel builds.
  * debian/watch, debian/xsfbs/repack.sh: repack the upstream tarball to
    remove the non-free stuff.
  * Build against xserver 1.6 rc.

 -- Julien Cristau <jcristau@debian.org>  Mon, 09 Feb 2009 19:45:45 +0100

xserver-xorg-video-mga (1:1.4.9.dfsg-2) experimental; urgency=low

  * Build against xserver 1.5.

 -- Julien Cristau <jcristau@debian.org>  Tue, 09 Sep 2008 23:50:05 +0100

xserver-xorg-video-mga (1:1.4.9.dfsg-1) unstable; urgency=low

  * New upstream release.
  * Run dpkg-shlibdeps with --warnings=6.  Drivers reference symbols from
    /usr/bin/Xorg and other modules, and that's not a bug, so we want
    dpkg-shlibdeps to shut up about symbols it can't find.  Build-depend on
    dpkg-dev >= 1.14.17.
  * Bump Standards-Version to 3.7.3.
  * Add myself to Uploaders.

 -- Julien Cristau <jcristau@debian.org>  Thu, 10 Jul 2008 13:32:32 +0200

xserver-xorg-video-mga (1:1.4.8.dfsg.1-1) unstable; urgency=low

  * New upstream release.
  * Drop XS- prefix from Vcs-* fields.

 -- Brice Goglin <bgoglin@debian.org>  Fri, 18 Jan 2008 23:43:57 +0100

xserver-xorg-video-mga (1:1.4.7.dfsg.1-3) unstable; urgency=low

  * Upload to unstable

 -- David Nusinow <dnusinow@debian.org>  Sun, 16 Sep 2007 15:43:45 -0400

xserver-xorg-video-mga (1:1.4.7.dfsg.1-2) experimental; urgency=low

  * Add 01_gen_pci_ids.diff. This patch provides a set of PCI ID's supported
    by the driver so that it can be autoloaded by the X server

 -- David Nusinow <dnusinow@debian.org>  Thu, 13 Sep 2007 22:14:25 -0400

xserver-xorg-video-mga (1:1.4.7.dfsg.1-1) experimental; urgency=low

  * New upstream release.
    + Increase the minimal frequency on G550 so that 320x240 works,
      closes: #430112.
  * Build against xserver 1.4.
  * Add upstream URL to debian/copyright.
  * Add myself to Uploaders, add remove Branden with his permission.

 -- Brice Goglin <bgoglin@debian.org>  Thu, 13 Sep 2007 08:30:08 +0200

xserver-xorg-video-mga (1:1.4.6.1.dfsg.1-3) unstable; urgency=low

  [ Timo Aaltonen ]
  * Replaces/Conflicts: xserver-xorg-driver-mga.

  [ Brice Goglin ]
  * Don't build-dep on libdrm-dev on hurd-i386, thanks Michael Banck
    (closes: #392874).
  * Bump Build-Depends: xserver-xorg-dev to >= 2:1.2.99.902
    (needed to let xsfbs get access to serverminver).
  * Add a link to www.X.org and a reference to the xf86-video-mga
    module in the long description.

 -- Julien Cristau <jcristau@debian.org>  Sat, 19 May 2007 15:55:47 +0200

xserver-xorg-video-mga (1:1.4.6.1.dfsg.1-2) unstable; urgency=low

  * Build package non-native.
  * Remove Fabio from uploaders, with his permission.
  * Add XS-Vcs-*.
  * Drop obsolete CVS information from the description.
  * Install the upstream changelog.
  * Upload to unstable.

 -- Julien Cristau <jcristau@debian.org>  Mon, 23 Apr 2007 06:25:20 +0200

xserver-xorg-video-mga (1:1.4.6.1.dfsg.1-1) experimental; urgency=low

  * New upstream release
  * Remove obsolete patches 01_stupid_configure_error.diff and
    04-Bug-2168-Fix-graphics-corruptions-with-Mystique-rev-2.diff
  * Generate server dependencies automatically from the ABI

 -- David Nusinow <dnusinow@debian.org>  Wed, 21 Feb 2007 23:29:57 -0500

xserver-xorg-video-mga (1:1.4.4.dfsg.1-2) unstable; urgency=low

  [ Julien Cristau ]
  * Add link to xserver-xorg-core bug script, so that bugreports contain
    the user's config and log files.
  * Bump dependency on xserver-xorg-core to >= 2:1.1.1-11, as previous
    versions don't have the bug script.
  * Steal patch from upstream git which should fix a graphics corruption with
    Mystique rev2 cards (closes: #320328).

 -- David Nusinow <dnusinow@debian.org>  Tue, 12 Dec 2006 21:10:04 -0500

xserver-xorg-video-mga (1:1.4.4.dfsg.1-1) unstable; urgency=low

  * New upstream release
    + Fix some crasher bugs with DRI. Thanks Svante Signell. Closes: #395025
  * Remove manpage typos patch, which was accepted upstream
  * Remove minumum clock patch
  * Add 01_stupid_configure_error.diff

 -- David Nusinow <dnusinow@debian.org>  Mon,  6 Nov 2006 23:53:51 -0500

xserver-xorg-video-mga (1:1.4.2.dfsg.1-1) unstable; urgency=low

  * New upstream release
    + Fixes DRI locking issues. Thanks Sune Vuorela and Michel Dänzer.
      Closes: #390287

 -- David Nusinow <dnusinow@debian.org>  Fri, 13 Oct 2006 17:31:27 -0400

xserver-xorg-video-mga (1:1.4.1.dfsg.1-4) unstable; urgency=low

  [ Steve Langasek ]
  * Add missing build-dep on quilt.  Closes: #388484.

  [ David Nusinow ]
  * Manpage typo fixes. Thanks A. Costa. (closes: #364558)
  * Update standards version to 3.7.2.0. No changes necessary.

 -- David Nusinow <dnusinow@debian.org>  Thu, 21 Sep 2006 22:31:54 -0400

xserver-xorg-video-mga (1:1.4.1.dfsg.1-3) unstable; urgency=low

  [ Steve Langasek ]
  * Upload to unstable

 -- David Nusinow <dnusinow@debian.org>  Mon, 18 Sep 2006 19:57:41 -0400

xserver-xorg-video-mga (1:1.4.1.dfsg.1-2) experimental; urgency=low

  [ Drew Parsons ]
  *  Provides: xserver-xorg-video-1.0 not xserver-xorg-video.

  [ David Nusinow ]
  * Bump xserver (build-)depends epochs to 2: to deal with botched
    server upload

 -- David Nusinow <dnusinow@debian.org>  Tue, 22 Aug 2006 23:46:33 +0000

xserver-xorg-video-mga (1:1.4.1.dfsg.1-1) experimental; urgency=low

  [ Andres Salomon ]
  * Test for obj-$(DEB_BUILD_GNU_TYPE) before creating it during build;
    idempotency fix.
  * Run dh_install w/ --list-missing.

  [ David Nusinow ]
  * New upstream release
  * Bump dependency on xserver-xorg-core to >= 1:1.1.1. Do the same thing for
    the build-dep on xserver-xorg-dev.
  * Bump debhelper compat to 5
  * Remove obsolete patch 01_fix_matrox_display.diff

 -- David Nusinow <dnusinow@debian.org>  Mon, 14 Aug 2006 23:20:38 +0000

xserver-xorg-video-mga (1:1.2.1.3.dfsg.1-2) unstable; urgency=low

  * Upload to modular

 -- David Nusinow <dnusinow@debian.org>  Sun, 26 Mar 2006 20:25:42 -0500

xserver-xorg-video-mga (1:1.2.1.3.dfsg.1-1) experimental; urgency=low

  * Remove non-free mga_ucode.h and add it to the prune list. Add
    03_no_nonfree.diff to cope with its removal and autoreconf.

 -- David Nusinow <dnusinow@debian.org>  Sun, 12 Mar 2006 13:35:30 -0500

xserver-xorg-video-mga (1:1.2.1.3-2) experimental; urgency=low

  * Port patches from trunk:
    + general/032_fix_matrox_display.diff
    + general/099m_mga_increase_minimum_pixel_clock.diff

 -- David Nusinow <dnusinow@debian.org>  Sun, 26 Feb 2006 17:28:53 -0500

xserver-xorg-video-mga (1:1.2.1.3-1) experimental; urgency=low

  * First upload to Debian
  * Change source package, package, and provides names to denote the
    type of driver and that they are for xserver-xorg

 -- David Nusinow <dnusinow@debian.org>  Fri, 13 Jan 2006 00:38:10 -0500

xserver-xorg-driver-mga (1:1.2.1.3-0ubuntu1) dapper; urgency=low

  * New upstream release.
  * Add provides on xserver-xorg-driver.

 -- Daniel Stone <daniel.stone@ubuntu.com>  Wed,  4 Jan 2006 19:58:26 +1100

xserver-xorg-driver-mga (1:1.2.1.2-0ubuntu1) dapper; urgency=low

  * New upstream release.

 -- Daniel Stone <daniel.stone@ubuntu.com>  Mon, 19 Dec 2005 09:07:03 +1100

xserver-xorg-driver-mga (1:1.2.1.1-0ubuntu1) dapper; urgency=low

  * New upstream release.
  * Bump Build-Depends on libdrm-dev to >> 2.0.

 -- Daniel Stone <daniel.stone@ubuntu.com>  Mon, 12 Dec 2005 13:24:20 +1100

xserver-xorg-driver-mga (1:1.2.1-0ubuntu2) dapper; urgency=low

  * Add missing Build-Depends (x11proto-core-dev, x11proto-fonts-dev,
    x11proto-randr-dev, x11proto-render-dev, x11proto-xext-dev, libdrm
    (>> 1.0.5), x11proto-xf86dri-dev).

 -- Daniel Stone <daniel.stone@ubuntu.com>  Mon,  5 Dec 2005 12:54:39 +1100

xserver-xorg-driver-mga (1:1.2.1-0ubuntu1) dapper; urgency=low

  * New upstream release.

 -- Daniel Stone <daniel.stone@ubuntu.com>  Tue, 22 Nov 2005 13:31:50 +1100

xserver-xorg-driver-mga (1:1.2.0.1-1) dapper; urgency=low

  * New upstream version.

 -- Daniel Stone <daniel.stone@ubuntu.com>  Tue, 25 Oct 2005 18:26:55 +1000

xserver-xorg-driver-mga (1:1.1.2-1) breezy; urgency=low

  * First xserver-xorg-driver-mga release.

 -- Daniel Stone <daniel.stone@ubuntu.com>  Wed,  6 Jul 2005 15:48:17 +1000
