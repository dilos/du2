INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
# misc:Pre-Depends
INSTDEPENDS += libx11-dev
INSTDEPENDS += x11proto-core-dev
INSTDEPENDS += x11proto-xext-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += automake
INSTDEPENDS += libtool
INSTDEPENDS += xutils-dev
# specs
INSTDEPENDS += xmlto
INSTDEPENDS += xorg-sgml-doctools
INSTDEPENDS += w3m
INSTDEPENDS += xsltproc
