INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += libboost-dev
INSTDEPENDS += libcppunit-dev
INSTDEPENDS += libicu-dev
INSTDEPENDS += liblcms2-dev
INSTDEPENDS += librevenge-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += zlib1g-dev
# Build-Depends-Indep:
INSTDEPENDS += doxygen
