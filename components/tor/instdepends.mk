INSTDEPENDS += debhelper
INSTDEPENDS += quilt
INSTDEPENDS += libssl-dev
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += libevent-dev
# binutils (>= 2.14.90.0.7)
INSTDEPENDS += asciidoc
INSTDEPENDS += docbook-xml
INSTDEPENDS += docbook-xsl
INSTDEPENDS += xmlto
# dh-apparmor
# libseccomp-dev [amd64 i386]
# dh-systemd [linux-any], libsystemd-dev [linux-any]
# pkg-config [linux-any]
INSTDEPENDS += dh-autoreconf
