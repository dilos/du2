Source: opensync
Section: libs
Priority: optional
Maintainer: Michael Banck <mbanck@debian.org>
Build-Depends: debhelper (>= 7.0.50~), quilt, cdbs, python-all-dev (>= 2.3.5-11), 
 python-support (>= 0.3.9) | dh-python, libglib2.0-dev, libsqlite3-dev, libxml2-dev, check,
 swig, automake, autoconf (>= 2.60), autotools-dev, libtool, libiconv-dev, bison, byacc
Standards-Version: 3.7.3

Package: libopensync0
Architecture: any
Depends: ${shlibs:Depends}
Description: Synchronisation framework for email/pdas/and more
 A synchronisation framework for synchronising data in various format. You
 can use libopensync from programs. See multisync for an existing X11 gui
 program that uses opensync.

Package: opensyncutils
Architecture: any
Depends: ${shlibs:Depends}
Description: Command line utilities for libopensync
 Libopensync provides a number of command line debugging and utility programs.
 Install this package for the osyncdump, osyncstress, osyncplugin, osynctest,
 osyncbinary programs.

Package: libopensync0-dev
Section: libdevel
Provides: libopensync-dev
Conflicts: libopensync-dev
Architecture: any
Depends: libopensync0 (= ${binary:Version}), libglib2.0-dev, libsqlite3-dev, libxml2-dev
Description: Headers and static libraries for libopensync
 Headers and static libraries for libopensync. These are not needed to use
 opensync, but are needed to build plugins or programs like multisync that
 link against libopensync.

Package: libopensync0-dbg
Architecture: linux-any
Priority: extra
Depends: libopensync0 (= ${binary:Version})
Description: Debug symbols for libopensync0
 Debug symbols for libopensync that will allow useful tracebacks and debugging
 on end user machines.

Package: python-opensync
Section: python
Architecture: any
Depends: ${shlibs:Depends}, ${python:Depends}, libopensync0 (= ${binary:Version})
Provides: ${python:Provides}
Replaces: python2.4-opensync (<< 0.18-2.1)
Conflicts: python2.4-opensync (<< 0.18-2.1)
XB-Python-Version: ${python:Versions}
Description: Python bindings to the opensync synchronisation engine
 Python bindings to the opensync synchronisation engine.
