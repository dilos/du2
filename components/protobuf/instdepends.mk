INSTDEPENDS += debhelper
# Debian build system
INSTDEPENDS += dpkg-dev
INSTDEPENDS += dh-autoreconf
# C/C++
# g++ (>= 4:4.7)
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += google-mock
INSTDEPENDS += libgtest-dev
# Python
INSTDEPENDS += dh-python
INSTDEPENDS += python-all
INSTDEPENDS += libpython-all-dev
INSTDEPENDS += python3-all
INSTDEPENDS += libpython3-all-dev
INSTDEPENDS += python-setuptools
INSTDEPENDS += python3-setuptools
INSTDEPENDS += python-google-apputils
INSTDEPENDS += python3-six
# Manpage generator
INSTDEPENDS += xmlto
# Tests
INSTDEPENDS += unzip
#Build-Depends-Indep:
# Java
INSTDEPENDS += ant
#INSTDEPENDS += default-jdk
INSTDEPENDS += maven-repo-helper
