INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += dh-python
INSTDEPENDS += python-all
INSTDEPENDS += python3-all
INSTDEPENDS += python-setuptools
INSTDEPENDS += python3-setuptools
INSTDEPENDS += zlib1g-dev
