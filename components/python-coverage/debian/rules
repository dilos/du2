#! /usr/bin/make -f
#
# debian/rules
# Part of the Debian ‘python-coverage’ package.
#
# Copyright © 2010–2016 Ben Finney <bignose@debian.org>
#
# This is free software; you may copy, modify, and/or distribute this
# work under the terms of the Apache License, version 2.0 as published
# by the Apache Software Foundation. No warranty expressed or implied.
# See the file ‘/usr/share/common-licenses/Apache-2.0’ for details.

export DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

export PYBUILD_NAME = coverage
export PYBUILD_DISABLE = test

PYTHON2_PACKAGE_NAME = python-${PYBUILD_NAME}
PYTHON3_PACKAGE_NAME = python3-${PYBUILD_NAME}
package_working_root = debian
package_install_root = debian/{package}

DISTRIBUTION_NAME = coverage

HTMLFILES_DIR = ${DISTRIBUTION_NAME}/htmlfiles
htmlfiles_dirname = $(notdir ${HTMLFILES_DIR})
bundled_libraries += $(addprefix ${HTMLFILES_DIR}/, \
	jquery.hotkeys.js \
	jquery.isonscreen.js \
	)

RESOURCES_DIR = usr/share/{package}
package_install_resources_root = ${package_install_root}/${RESOURCES_DIR}

DOCUMENTATION_DIR = doc
MANPAGE_SUFFIX = .1
manpage_tempfile_suffix = ${MANPAGE_SUFFIX}.tmp
default_manpage_name = python-coverage${MANPAGE_SUFFIX}
python2_command_names = $(sort $(notdir $(wildcard \
	${package_working_root}/*/usr/bin/python2*-coverage)))
python3_command_names = $(sort $(notdir $(wildcard \
	${package_working_root}/*/usr/bin/python3*-coverage)))
versioned_command_names = ${python2_command_names} ${python3_command_names}
versioned_manpage_names = $(addsuffix ${MANPAGE_SUFFIX}, \
	${versioned_command_names})
versioned_manpage_paths = $(addprefix ${DOCUMENTATION_DIR}/, \
	${versioned_manpage_names})
GENERATED_FILES += ${DOCUMENTATION_DIR}/*${MANPAGE_SUFFIX}
GENERATED_FILES += ${DOCUMENTATION_DIR}/*${manpage_tempfile_suffix}

DOCUMENTATION_BUILD_DIR = ${DOCUMENTATION_DIR}/_build
DOCUMENTATION_BUILD_HTML_DIR = ${DOCUMENTATION_BUILD_DIR}/html
GENERATED_FILES += ${DOCUMENTATION_BUILD_DIR}

INSTALL = install

# Send HTTP traffic to the “discard” service during packaging actions.
export http_proxy = http://127.0.1.1:9/
export https_proxy = ${http_proxy}

RST_SUFFIX = .txt

RST2MAN = rst2man

SPHINX = sphinx-build
SPHINX_OPTS = -N


%:
	dh $@ --with python2,python3,sphinxdoc --buildsystem=pybuild


.PHONY: remove-bundled-libraries
remove-bundled-libraries:
	$(RM) ${bundled_libraries}

.PHONY: manpage-aliases
manpage-aliases: ${versioned_manpage_paths}

${DOCUMENTATION_DIR}:
	$(INSTALL) --directory "$@"

${DOCUMENTATION_DIR}/${default_manpage_name}: ${DOCUMENTATION_DIR}
${DOCUMENTATION_DIR}/${default_manpage_name}: ${package_working_root}/${default_manpage_name}
	$(INSTALL) --mode=u=rw,go=r "$<" "$@"

${DOCUMENTATION_DIR}/%${MANPAGE_SUFFIX}: ${DOCUMENTATION_DIR}/${default_manpage_name}
	$(INSTALL) --mode=u=rw,go=r "$<" "$@"

%.1: %.1${RST_SUFFIX}
	$(RST2MAN) "$<" > "$@".tmp
	cat debian/manpage_encoding_declaration.UTF-8 "$@".tmp > "$@"


.PHONY: get-packaged-orig-source
get-packaged-orig-source:
	$(uscan-download-source) --download-current-version

# Policy §4.9 strongly recommends the ‘get-orig-source’ target:
# “This target is optional, but providing it if possible is a good idea.”
#
# This target is an anomaly: per Policy §4.9, it fetches the *latest*
# upstream source, regardless of this package's version. To fetch the
# upstream source corresponding to this package's *declared* version
# in ‘debian/changelog’, use ‘get-packaged-orig-source’ instead.
.PHONY: get-orig-source
get-orig-source:
	$(uscan-download-source)

get-orig-source get-packaged-orig-source: http_proxy =
get-orig-source get-packaged-orig-source: makefile_dir = $(abspath $(dir $(firstword ${MAKEFILE_LIST})))
get-orig-source get-packaged-orig-source: package_dir = $(abspath $(dir ${makefile_dir}))

define uscan-download-source
	uscan --noconf --verbose \
	    --force-download \
	    --rename \
	    --destdir=$(CURDIR) \
	    --check-dirname-level=0 ${package_dir}
endef # uscan-download-source


override_dh_auto_build: remove-bundled-libraries
override_dh_auto_build:
	dh_auto_build --buildsystem=pybuild

# Upstream test suite doesn't currently run in our build environment.
override_dh_auto_test:

override_dh_auto_install:
	dh_auto_install --buildsystem=pybuild
	$(RM) $(addprefix ${package_working_root}/${PYTHON3_PACKAGE_NAME}/, \
	    usr/bin/python-coverage)

export PYBUILD_AFTER_INSTALL = \
	$(INSTALL) --mode=u=rwx,go=rx --directory \
	    "${package_install_resources_root}" ; \
	$(INSTALL) --mode=u=rwx,go=rx --directory \
	    "${package_install_resources_root}/${htmlfiles_dirname}" ; \
	$(INSTALL) --mode=u=rw,go=r "${HTMLFILES_DIR}"/* \
	    "${package_install_resources_root}/${htmlfiles_dirname}" ; \
	dh_link \
	    --package={package} \
	    "/${RESOURCES_DIR}/${htmlfiles_dirname}" \
	    usr/lib/python{version}/dist-packages/${HTMLFILES_DIR}

override_dh_installchangelogs:
	dh_installchangelogs CHANGES.rst

override_dh_installman: manpage-aliases
	dh_installman

override_dh_auto_clean:
	dh_auto_clean
	$(RM) -r ${GENERATED_FILES}

override_dh_auto_build:
	dh_auto_build
	$(SPHINX) ${SPHINX_OPTS} -bhtml ${DOCUMENTATION_DIR}/ \
		${DOCUMENTATION_BUILD_HTML_DIR}/


# Local variables:
# mode: makefile
# coding: utf-8
# End:
# vim: filetype=make fileencoding=utf-8 :
