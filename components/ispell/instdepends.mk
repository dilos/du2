INSTDEPENDS += debhelper
INSTDEPENDS += bison
INSTDEPENDS += dictionaries-common-dev
INSTDEPENDS += libncurses5-dev
# Build-Depends-Indep:
INSTDEPENDS += wamerican
INSTDEPENDS += wamerican-huge
INSTDEPENDS += wamerican-insane
INSTDEPENDS += wamerican-large
INSTDEPENDS += wamerican-small
INSTDEPENDS += wbritish
INSTDEPENDS += wbritish-huge
INSTDEPENDS += wbritish-insane
INSTDEPENDS += wbritish-large
INSTDEPENDS += wbritish-small
