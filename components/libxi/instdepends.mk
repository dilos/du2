INSTDEPENDS += debhelper
INSTDEPENDS += quilt
INSTDEPENDS += x11proto-core-dev
INSTDEPENDS += x11proto-xext-dev
INSTDEPENDS += x11proto-input-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += libxext-dev
INSTDEPENDS += libxfixes-dev
INSTDEPENDS += xmlto
INSTDEPENDS += asciidoc
INSTDEPENDS += pkg-config
INSTDEPENDS += xutils-dev
# specs
INSTDEPENDS += xmlto
INSTDEPENDS += xorg-sgml-doctools
INSTDEPENDS += xsltproc
INSTDEPENDS += w3m
