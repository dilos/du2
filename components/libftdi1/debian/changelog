libftdi1 (1.3-2+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- Denis Kozadaev <denis@tambov.ru>  Fri, 08 Dec 2017 20:08:52 +0300

libftdi1 (1.3-2) unstable; urgency=medium

  * debian/patches/01-cmake-multiarch.diff: update to also fix
    LIBFTDI_USE_FILE in multiarch systems. Thanks to Jochen Sprickerhof
    for the patch (closes: #832617).
  * debian/rules: enable the C++ library even for the python builds so
    that LibFTDI1Config.cmake contains both LIBFTDI_PYTHON_MODULE_PATH
    and LIBFTDIPP* entries.

 -- Aurelien Jarno <aurel32@debian.org>  Thu, 04 Aug 2016 23:06:25 +0000

libftdi1 (1.3-1) unstable; urgency=medium

  * New upstream version.
  * Stop building -dbg packages, please use the -dbgsym packages instead.
  * Move the documentation from libftdi1-dev to libftdi1-doc as
    recommended by recent policy versions.
  * Bumped Standards-Version to 3.9.8.

 -- Aurelien Jarno <aurel32@debian.org>  Wed, 01 Jun 2016 23:00:29 +0200

libftdi1 (1.2-5) unstable; urgency=medium

  * Don't link python extensions with the python interpreter (closes:
    #802809).

 -- Aurelien Jarno <aurel32@debian.org>  Sat, 24 Oct 2015 02:19:45 +0200

libftdi1 (1.2-4) unstable; urgency=medium

  * Rename libftdipp1-2 into libftdipp1-2v5 for the GCC 5 ABI transition.
  * Build-depends on g++ (>= 4:5.2) and boost (>= 1.58).
  * Force C++ standard to c++11 to prepare for the GCC 6 transition.

 -- Aurelien Jarno <aurel32@debian.org>  Mon, 03 Aug 2015 14:04:23 +0000

libftdi1 (1.2-3) unstable; urgency=medium

  * Upstream uses SWIG + CMake to build the Python bindings, and TTBOMK
    this gives no easy way to install a Python shared library whose name
    includes the ABI. To fix the installation with multiple Python
    versions, rename the shared library to include the ABI just after
    calling "make install" (closes: #793830).

 -- Aurelien Jarno <aurel32@debian.org>  Wed, 29 Jul 2015 00:02:46 +0200

libftdi1 (1.2-2) unstable; urgency=medium

  * Fix 01-cmake-multiarch.diff to correctly generate libftdi1.pc.

 -- Aurelien Jarno <aurel32@debian.org>  Sat, 18 Jul 2015 22:37:34 +0200

libftdi1 (1.2-1) unstable; urgency=low

  * New package based on libftdi providing a new upstream version with
    an incompatible API (closes: #706301, #766582, #781545).

 -- Aurelien Jarno <aurel32@debian.org>  Mon, 27 Apr 2015 23:56:27 +0200
