Source: m17n-lib
Section: utils
Priority: optional
Maintainer: Harshula Jayasuriya <harshula@debian.org>
Build-Depends: debhelper (>= 9), dh-autoreconf, libtool (>= 2.2.4), automake (>= 1.9.6), autoconf (>= 2.61), pkg-config (>= 0.22), libxt-dev, libx11-dev, libxaw7-dev, libxmu-dev, libfribidi-dev, libotf-dev (>= 0.9.11), libfreetype6-dev, libxft-dev, libxrender-dev, libfontconfig1-dev, libgd-dev, libxml2-dev, libanthy-dev, ispell, libthai-dev, m17n-db
Standards-Version: 3.9.6
Homepage: http://www.nongnu.org/m17n/
Vcs-Git: git://anonscm.debian.org/pkg-ime/m17n-lib.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-ime/m17n-lib.git

Package: libm17n-0
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, m17n-db (>= 1.6.0)
Suggests: m17n-docs
Description: multilingual text processing library - runtime
 The m17n library is a multilingual text processing library for the C
 language.  This library has following features:
  - The m17n library is an open source software.
  - The m17n library for any Linux/Unix applications.
  - The m17n library realizes multilingualization of many aspects of
    applications.
  - The m17n library represents multilingual text as an object named
    M-text.  M-text is a string with attributes called text properties,
    and designed to substitute for string in C.  Text properties carry any
    information required to input, display and edit the text.
  - The m17n library  supports functions to handle M-texts.
 .
 m17n is an abbreviation of Multilingualization.
 .
 This package contains the runtime part of m17n-lib.

Package: libm17n-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, libm17n-0 (= ${binary:Version}), libxml2-dev, libthai-dev, dpkg-dev
Suggests: m17n-docs
Description: multilingual text processing library - development
 The m17n library is a multilingual text processing library for the C
 language.  This library has following features:
 .
  - The m17n library is an open source software.
  - The m17n library for any Linux/Unix applications.
  - The m17n library realizes multilingualization of many aspects of
    applications.
  - The m17n library represents multilingual text as an object named
    M-text.  M-text is a string with attributes called text properties,
    and designed to substitute for string in C.  Text properties carry any
    information required to input, display and edit the text.
  - The m17n library  supports functions to handle M-texts.
 .
 m17n is an abbreviation of Multilingualization.
 .
 This package contains the header and development files needed to build
 programs and packages using m17n-lib.

Package: libm17n-0-dbg
Section: debug
Priority: extra
Architecture: linux-any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, libm17n-0 (= ${binary:Version})
Suggests: m17n-docs
Description: multilingual text processing library - debugging symbols
 The m17n library is a multilingual text processing library for the C
 language.  This library has following features:
 .
  - The m17n library is an open source software.
  - The m17n library for any Linux/Unix applications.
  - The m17n library realizes multilingualization of many aspects of
    applications.
  - The m17n library represents multilingual text as an object named
    M-text.  M-text is a string with attributes called text properties,
    and designed to substitute for string in C.  Text properties carry any
    information required to input, display and edit the text.
  - The m17n library  supports functions to handle M-texts.
 .
 m17n is an abbreviation of Multilingualization.
 .
 This package contains unstripped shared libraries. it is provided
 primarily to provide a backtrace with names in a debugger, this makes
 it somewhat easier to interpret core dumps.  The libraries are installed
 in /usr/lib/debug and can be used by placing that directory in
 LD_LIBRARY_PATH.
 Most people will not need this package.

Package: m17n-lib-bin
Architecture: any
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}, libm17n-0 (= ${binary:Version})
Suggests: m17n-docs
Description: multilingual text processing library - utilities
 The m17n library is a multilingual text processing library for the C
 language.  This library has following features:
 .
  - The m17n library is an open source software.
  - The m17n library for any Linux/Unix applications.
  - The m17n library realizes multilingualization of many aspects of
    applications.
  - The m17n library represents multilingual text as an object named
    M-text.  M-text is a string with attributes called text properties,
    and designed to substitute for string in C.  Text properties carry any
    information required to input, display and edit the text.
  - The m17n library  supports functions to handle M-texts.
 .
 m17n is an abbreviation of Multilingualization.
 .
 This package contains the utilities which use m17n-lib.

Package: m17n-lib-mimx
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, libm17n-0 (= ${binary:Version}), anthy, ispell
Suggests: m17n-docs
Description: multilingual text processing library - binary modules
 The m17n library is a multilingual text processing library for the C
 language.  This library has following features:
 .
  - The m17n library is an open source software.
  - The m17n library for any Linux/Unix applications.
  - The m17n library realizes multilingualization of many aspects of
    applications.
  - The m17n library represents multilingual text as an object named
    M-text.  M-text is a string with attributes called text properties,
    and designed to substitute for string in C.  Text properties carry any
    information required to input, display and edit the text.
  - The m17n library  supports functions to handle M-texts.
 .
 m17n is an abbreviation of Multilingualization.
 .
 This package contains the binary modules needed by:
 - ja-anthy.mim
 - ispell.mim
