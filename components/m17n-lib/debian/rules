#!/usr/bin/make -f
# -*- makefile -*-
# Sample debian/rules that uses debhelper.
# This file was originally written by Joey Hess and Craig Small.
# This file was modified by Harshula Jayasuriya.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

SRC_PKG = m17n-lib
LIB_PKG = libm17n-0
INSTALLDIR = $(CURDIR)/debian/$(SRC_PKG)


#dpkg_buildflags = DEB_LDFLAGS_MAINT_APPEND="-Wl,--as-needed -Wl,-z,defs" dpkg-buildflags
dpkg_buildflags = DEB_LDFLAGS_MAINT_APPEND="-Wl,-z,defs" dpkg-buildflags


# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_HOST_MULTIARCH  ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
ifneq ($(DEB_HOST_GNU_TYPE),$(DEB_BUILD_GNU_TYPE))
CROSS= --build $(DEB_BUILD_GNU_TYPE) --host $(DEB_HOST_GNU_TYPE)
else
CROSS= --build $(DEB_BUILD_GNU_TYPE)
endif


# shared library versions, option 1
#version=0.3.0
#major=0
# option 2, assuming the library is created as src/.libs/libfoo.so.2.0.5 or so
version=`ls src/.libs/lib*.so.* | \
 awk '{if (match($$0,/[0-9]+\.[0-9]+\.[0-9]+$$/)) print substr($$0,RSTART)}'`
major=`ls src/.libs/lib*.so.* | \
 awk '{if (match($$0,/\.so\.[0-9]+$$/)) print substr($$0,RSTART+4)}'`

config.status: configure
	dh_testdir
	dh_autoreconf
	./configure $(CROSS) \
			--prefix=/usr \
			--mandir=\$${prefix}/share/man \
			--infodir=\$${prefix}/share/info \
			--libdir=\$${prefix}/lib/$(DEB_HOST_MULTIARCH) \
			$(shell $(dpkg_buildflags) --export=configure)

build-arch: build
build-indep: build

build: build-stamp

build-stamp: config.status
	dh_testdir
	$(MAKE)
	touch $@

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp
	[ ! -f Makefile ] || $(MAKE) distclean
	dh_autoreconf_clean
	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs
	$(MAKE) DESTDIR=$(INSTALLDIR) install


# Build architecture-independent files here.
binary-indep: build install
# We have nothing to do by default.

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir
	dh_testroot
	dh_installchangelogs ChangeLog
	dh_installdocs
	dh_installexamples
	dh_install --sourcedir=$(INSTALLDIR) --list-missing
#	dh_installmenu
#	dh_installdebconf
#	dh_installlogrotate
#	dh_installemacsen
#	dh_installcatalogs
#	dh_installpam
#	dh_installmime
#	dh_installinit
#	dh_installcron
#	dh_installinfo
#	dh_installwm
#	dh_installudev
	dh_lintian
#	dh_bugfiles
#	dh_undocumented
	dh_installman
	dh_link
	dh_strip --dbg-package=$(LIB_PKG)-dbg
	dh_compress
	dh_fixperms
#	dh_perl
#	dh_python
	dh_makeshlibs -Xlibm17n-gd -Xlibm17n-X -Xlibmimx-anthy -Xlibmimx-ispell
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install
