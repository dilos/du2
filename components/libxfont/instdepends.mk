INSTDEPENDS += debhelper
INSTDEPENDS += quilt
INSTDEPENDS += pkg-config
INSTDEPENDS += libfontenc-dev
INSTDEPENDS += x11proto-core-dev
INSTDEPENDS += xtrans-dev
INSTDEPENDS += x11proto-fonts-dev
INSTDEPENDS += libfreetype6-dev
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += libbz2-dev
INSTDEPENDS += xutils-dev
# devel-docs
INSTDEPENDS += xmlto
INSTDEPENDS += xorg-sgml-doctools
INSTDEPENDS += lynx
