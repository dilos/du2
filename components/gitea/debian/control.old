Source: gitea
Section: utils
Priority: extra
Maintainer: DilOS team <dilos@dilos.org>
Build-Depends: debhelper (>= 11),
               dh-golang,
               golang-any,
               libpam0g-dev,
               python
Standards-Version: 4.1.4
Homepage: https://github.com/go-gitea/gitea
Vcs-Browser: https://github.com/go-gitea/debian-packaging
Vcs-Git: https://github.com/go-gitea/debian-packaging.git
Testsuite: autopkgtest-pkg-go
XS-Go-Import-Path: code.gitea.io/gitea
XS-Autobuild: yes

Package: gitea
Architecture: any
Built-Using: ${misc:Built-Using}
Pre-Depends: debconf
Depends: adduser,
         git,
         gitea-common (= ${source:Version}) [linux-any],
         libcap2-bin [linux-any],
         ${misc:Depends},
         ${shlibs:Depends}
Description: Painless self-hosted git service
 Gitea is a self-hosted git service aiming to provide a full suite of
 features similar to Gitlab or Github. It aims to be light weight,
 feature rich, and easily maintained.
 .
 Features:
   * User dashboard:
     + Context switching (organization / current user)
     + Activity timeline
     + Repository list
   * Issues dashboard
   * Pull requests
   * Notification (web / email)
   * Repository types
     + Mirror
     + Normal
     + Migrated
   * Custom templates
   * TLS support
   * Detailed logging
   * Database support:
     + MySQL
     + PostgreSQL
     + SQLite3
     + MSSQL
   * Admin panel(s)
     + Repository/Organization/User management
     + Statisticts
     + Server status
     + System notices
   * Authentication sources:
     + OAuth
     + PAM
     + LDAP
     + SMTP
   * Multi-language support
   * Release management / issue (ticket) tracking
     + Milestones
     + Labels
     + Assign
     + Search / sort / filter
     + Comments / attachments
     + Pull requests
   * Project wiki
   * Per-project settings
 .
 Documentation: https://docs.gitea.io/en-US/
