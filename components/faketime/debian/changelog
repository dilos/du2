faketime (0.9.6-7+dilos1) unstable; urgency=medium

  * Build for DilOS

 -- Denis Kozadaev <denis@tambov.ru>  Fri, 20 Oct 2017 15:47:56 +0300

faketime (0.9.6-7) unstable; urgency=medium

  * added DEP-8 test (thanks, Jakub Wilk!) (Closes: #830588)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 13 Jul 2016 03:41:16 +0200

faketime (0.9.6-6) unstable; urgency=medium

  * Fix CFLAGS (Closes: #830510) Thanks, Sven Joachim and Jakub Wilk for
    catching this!

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sat, 09 Jul 2016 00:12:07 +0200

faketime (0.9.6-5) unstable; urgency=medium

  * use -Wno-nonnull-compare for GCC 6 (Closes: #811610)
  * use ${misc:Pre-Depends} for multiarch-support,
    avoiding lintian pre-depends-directly-on-multiarch-support
  * bumped standards-version to 3.9.8 (no changes needed)
  * normalize Vcs-* fields
  * use "wrap-and-sort -as" for normalizing debian/ files

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 07 Jul 2016 13:27:29 -0400

faketime (0.9.6-4) unstable; urgency=medium

  * link to libfaketime's README from faketime's doc dir (thanks, Ximin
    Luo)
  * bump Standards-Version to 3.9.6 (no changes needed)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sat, 14 Feb 2015 19:36:04 -0500

faketime (0.9.6-3) unstable; urgency=medium

  * a better fix (approved by upstream to let a single package work with
    multiple versions of libc (Closes: #755104)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 05 Aug 2014 15:15:06 -0400

faketime (0.9.6-2) unstable; urgency=medium

  * note that this version does not work with libc6 < 2.17 due to
    the patch for #699559 (Closes: #753460)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sun, 13 Jul 2014 20:23:53 -0400

faketime (0.9.6-1) unstable; urgency=medium

  * New Upstream Release (Closes: #750721, #737571)
  * bumped Standards-Version to 3.9.5 (no changes needed)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sun, 08 Jun 2014 21:29:30 -0400

faketime (0.9.5-2) unstable; urgency=medium

  * avoid accidentally truncating LD_PRELOAD. Thanks, Antonio Terceiro
    (Closes: #743301)
  
 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 02 Apr 2014 09:45:49 -0400

faketime (0.9.5-1) unstable; urgency=low

  * new upstream release
  * dropped build system patches -- incorporated upstream
  * debian/copyright: make machine-readable
  * enabled use on multi-arch (Closes: #672376) though manual installation
    of non-native libfaketime packages is still necessary; see:
    https://wiki.debian.org/HelmutGrohne/MultiarchSpecChanges#A.60LD_PRELOAD.60

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 14 Oct 2013 19:30:40 -0400

faketime (0.9.1-2) unstable; urgency=low

  * fix FTBFS on kFreeBSD (Closes: #712709) (Thanks, Petr Salinger)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 20 Jun 2013 09:28:46 -0400

faketime (0.9.1-1) unstable; urgency=low

  * New Upstream Release
  * Moved to source format 3.0 (quilt)
  * debhelper compat level 9
  * bumped Standards-Version to 3.9.3 (no changes needed)
  * fix failures against libc 2.17 (Closes: #699559) (thanks, Gerardo
    Malazdrewicz)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sun, 03 Feb 2013 01:42:35 -0500

faketime (0.8-1) unstable; urgency=low

  * Initial release (Closes: #495630)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 26 Aug 2008 14:37:38 -0400

