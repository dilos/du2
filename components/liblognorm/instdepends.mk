INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += autotools-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += libxml2-dev
INSTDEPENDS += libestr-dev
INSTDEPENDS += libfastjson-dev
INSTDEPENDS += python-sphinx
