Source: antlr
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Torsten Werner <twerner@debian.org>
Build-Depends: debhelper (>= 9), default-jdk, python (>= 2.6.6-3~), autotools-dev, python-all-dev (>= 2.3.5-11), sharutils
Build-Depends-Indep: maven-repo-helper
Standards-Version: 3.9.6
Vcs-Svn: svn://anonscm.debian.org/pkg-java/trunk/antlr
Vcs-Browser: http://anonscm.debian.org/viewvc/pkg-java/trunk/antlr
Homepage: http://www.antlr2.org

Package: antlr
Architecture: all
Depends: ${misc:Depends}, libantlr-java (= ${binary:Version}),
 default-jre-headless | java5-runtime-headless | java6-runtime-headless
Description: language tool for constructing recognizers, compilers etc
 ANTLR, ANother Tool for Language Recognition, (formerly PCCTS) is 
 a language tool that provides a framework for constructing recognizers,
 compilers, and translators from grammatical descriptions containing C++
 or Java actions [You can use PCCTS 1.xx to generate C-based parsers].
 . 
 Computer language translation has become a common task. While 
 compilers and tools for traditional computer languages (such as C
 or Java) are still being built, their number is dwarfed by the thousands
 of mini-languages for which recognizers and translators are being 
 developed. Programmers construct translators for database formats, 
 graphical data files (e.g., PostScript, AutoCAD), text processing 
 files (e.g., HTML, SGML).  ANTLR is designed to handle all of your 
 translation tasks.

Package: libantlr-java
Architecture: all
Depends: ${misc:Depends}
Replaces: antlr (<< 2.7.7-8)
Breaks: antlr (<< 2.7.7-8)
Description: language tool for constructing recognizers, compilers etc (java library)
 ANTLR, ANother Tool for Language Recognition, (formerly PCCTS) is 
 a language tool that provides a framework for constructing recognizers,
 compilers, and translators from grammatical descriptions containing C++
 or Java actions [You can use PCCTS 1.xx to generate C-based parsers].
 .
 This package contains the java libraries without a dependency on any runtime
 to be able to bootstrap gcj without a dependency on a java runtime.

Package: libantlr-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}
Recommends: antlr
Description: language tool for constructing recognizers, compilers etc
 ANTLR, ANother Tool for Language Recognition, (formerly PCCTS) is
 a language tool that provides a framework for constructing recognizers,
 compilers, and translators from grammatical descriptions containing C++
 or Java actions [You can use PCCTS 1.xx to generate C-based parsers].
 .
 These are the static libraries for C++.

Package: antlr-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Conflicts: antlr (<< 2.7.6-8)
Description: language tool for constructing recognizers, compilers etc
 This package contains the documentation and examples for antlr.
 ANTLR stands for ANother Tool for Language Recognition,
 (formerly PCCTS). It is  a language tool that provides a framework
 for constructing recognizers, compilers, and translators from
 grammatical descriptions containing C++ or Java actions
 [You can use PCCTS 1.xx to generate C-based parsers].
 .
 See antlr package for a complete description

Package: python-antlr
Section: python
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}
Replaces: antlr (<< 2.7.7-10)
Breaks: antlr (<< 2.7.7-10)
Description: language tool for constructing recognizers, compilers etc
 This package contains the Python version of antlr. ANTLR stands for
 ANother Tool for Language Recognition, (formerly PCCTS).
 .
 See antlr package for a complete description.

