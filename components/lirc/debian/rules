#!/usr/bin/make -f

include /usr/share/dpkg/architecture.mk

export DEB_BUILD_MAINT_OPTIONS      = hardening=+all
#export DEB_LDFLAGS_MAINT_APPEND     = -Wl,--as-needed
CPPFLAGS+= -DDILOS

%:
#	dh $@ --with autoreconf,python3,systemd --parallel
	dh $@ --with autoreconf,python3 --parallel

override_dh_autoreconf:
	dh_autoreconf $(CURDIR)/autogen.sh

override_dh_auto_configure:
ifeq ($(DEB_BUILD_ARCH_OS), linux)
	dh_auto_configure -- HAVE_UINPUT=1
else
	dh_auto_configure
endif

override_dh_shlibdeps:
	dh_shlibdeps -l $(CURDIR)/debian/tmp/usr/lib/*/lirc/plugins

override_dh_auto_install:
	$(MAKE) DESTDIR=$(CURDIR)/debian/tmp \
            pythondir='$$(libdir)/python3/dist-packages' \
	    install
	py3clean debian/tmp
	find debian/tmp -name *.la -delete
ifeq ($(DEB_BUILD_ARCH_OS), linux)
	mkdir -p debian/tmp/usr/lib/tmpfiles.d
	echo "d /var/run/lirc  0755  root  root  10d" \
	    > debian/tmp/usr/lib/tmpfiles.d/lirc.conf
endif
	# Temporary postinstall 0.9.4 script.
	cp debian/lirc-old2new debian/tmp/usr/share/lirc
	# Don't overwrite existing config files.
	for f in lircd.conf lircmd.conf irexec.lircrc lirc_options.conf; do \
	    mv debian/tmp/etc/lirc/$$f debian/tmp/etc/lirc/$$f.dist; \
	done

override_dh_systemd_enable:
	dh_systemd_enable --name=lircd

override_dh_install:
	dh_install --fail-missing

override_dh_installinit:
	dh_installinit --package=lirc --name=lircd
	dh_installinit --package=lirc --name=lircmd
	dh_installinit

override_dh_python3:
	# dh_python3 fails to create deps and to compile.
	dh_python3 /usr/lib/*/python3/dist-packages/lirc/

override_dh_fixperms-arch:
	dh_fixperms
	chmod 755 debian/lirc/usr/share/lirc/contrib/irman2lirc
