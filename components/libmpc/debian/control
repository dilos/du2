Source: libmpc
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <pkg-multimedia-maintainers@lists.alioth.debian.org>
Uploaders: Sebastian Dröge <slomo@debian.org>,
           Jorge Salamero Sanz <bencer@cauterized.net>
Build-Depends: cdbs (>= 0.4.93~),
               debhelper (>= 9),
               gnulib,
               automake,
               autoconf,
               dh-autoreconf,
               libtool,
               pkg-config,
               libreplaygain-dev (>= 1.0~r412),
               libcue-dev
Standards-Version: 3.9.8
Homepage: http://www.musepack.net
Vcs-Git: https://anonscm.debian.org/git/pkg-multimedia/libmpc.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-multimedia/libmpc.git

Package: libmpcdec6
Architecture: any
Multi-Arch: same
Section: libs
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: MusePack decoder - library
 Musepack is an audio compression format with a strong emphasis on
 high quality. It's not lossless, but it is designed for transparency,
 so that you won't be able to hear differences between the original
 wave file and the much smaller MPC file.
 .
 It is based on the MPEG-1 Layer-2 / MP2 algorithms, but since 1997
 it has rapidly developed and vastly improved and is now at an advanced
 stage in which it contains heavily optimized and patentless code.

Package: libmpcdec-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libmpcdec6 (= ${binary:Version}),
         ${misc:Depends}
Description: MusePack decoder - development files
 Musepack is an audio compression format with a strong emphasis on
 high quality. It's not lossless, but it is designed for transparency,
 so that you won't be able to hear differences between the original
 wave file and the much smaller MPC file.
 .
 It is based on the MPEG-1 Layer-2 / MP2 algorithms, but since 1997
 it has rapidly developed and vastly improved and is now at an advanced
 stage in which it contains heavily optimized and patentless code.
 .
 This package contains the header files, static libraries
 and symbolic links that developers using libreplaygain will need.

Package: musepack-tools
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: MusePack commandline utilities
 Musepack is an audio compression format with a strong emphasis on
 high quality. It's not lossless, but it is designed for transparency,
 so that you won't be able to hear differences between the original
 wave file and the much smaller MPC file.
 .
 It is based on the MPEG-1 Layer-2 / MP2 algorithms, but since 1997
 it has rapidly developed and vastly improved and is now at an advanced
 stage in which it contains heavily optimized and patentless code.
 .
 This package contains various commandline  utilities for MusePack files:
  - mpcenc (encoder)
  - mpcdec (decoder)
  - mpccut (cut MPC files without reencoding)
  - mpcgain (gain calculation)
  - mpc2sv8 (conversion to MusePack SV8 from older SV)

