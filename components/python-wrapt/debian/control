Source: python-wrapt
Section: python
Priority: optional
Maintainer: PKG OpenStack <openstack-devel@lists.alioth.debian.org>
Uploaders: Thomas Goirand <zigo@debian.org>,
Build-Depends: debhelper (>= 9),
               dh-python,
               openstack-pkg-tools (>= 52~),
               python-all-dev,
               python-pytest,
               python-setuptools,
               python-six,
               python-sphinx,
               python-sphinx-rtd-theme,
               python3-all,
               python3-all-dev,
               python3-pytest,
               python3-setuptools,
               python3-six,
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/cgit/openstack/python-wrapt.git/
Vcs-Git: https://anonscm.debian.org/git/openstack/python-wrapt.git
Homepage: https://github.com/GrahamDumpleton/wrapt

Package: python-wrapt
Architecture: any
Depends: python-six,
         ${misc:Depends},
         ${python:Depends},
         ${shlibs:Depends},
Description: decorators, wrappers and monkey patching. - Python 2.x
 The aim of the wrapt module is to provide a transparent object proxy for
 Python, which can be used as the basis for the construction of function
 wrappers and decorator functions.
 .
 The wrapt module focuses very much on correctness. It therefore goes way
 beyond existing mechanisms such as functools.wraps() to ensure that decorators
 preserve introspectability, signatures, type checking abilities etc. The
 decorators that can be constructed using this module will work in far more
 scenarios than typical decorators and provide more predictable and consistent
 behaviour.
 .
 To ensure that the overhead is as minimal as possible, a C extension module is
 used for performance critical components. An automatic fallback to a pure
 Python implementation is also provided where a target system does not have a
 compiler to allow the C extension to be compiled.
 .
 This package contains the Python 2.x module.

Package: python3-wrapt
Architecture: any
Depends: python3-six,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends},
Description: decorators, wrappers and monkey patching. - Python 3.x
 The aim of the wrapt module is to provide a transparent object proxy for
 Python, which can be used as the basis for the construction of function
 wrappers and decorator functions.
 .
 The wrapt module focuses very much on correctness. It therefore goes way
 beyond existing mechanisms such as functools.wraps() to ensure that decorators
 preserve introspectability, signatures, type checking abilities etc. The
 decorators that can be constructed using this module will work in far more
 scenarios than typical decorators and provide more predictable and consistent
 behaviour.
 .
 To ensure that the overhead is as minimal as possible, a C extension module is
 used for performance critical components. An automatic fallback to a pure
 Python implementation is also provided where a target system does not have a
 compiler to allow the C extension to be compiled.
 .
 This package contains the Python 3.x module.

Package: python-wrapt-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends},
Description: decorators, wrappers and monkey patching. - doc
 The aim of the wrapt module is to provide a transparent object proxy for
 Python, which can be used as the basis for the construction of function
 wrappers and decorator functions.
 .
 The wrapt module focuses very much on correctness. It therefore goes way
 beyond existing mechanisms such as functools.wraps() to ensure that decorators
 preserve introspectability, signatures, type checking abilities etc. The
 decorators that can be constructed using this module will work in far more
 scenarios than typical decorators and provide more predictable and consistent
 behaviour.
 .
 To ensure that the overhead is as minimal as possible, a C extension module is
 used for performance critical components. An automatic fallback to a pure
 Python implementation is also provided where a target system does not have a
 compiler to allow the C extension to be compiled.
 .
 This package contains the documentation.
