INSTDEPENDS += debhelper
INSTDEPENDS += quilt
INSTDEPENDS += xsltproc
INSTDEPENDS += docbook-xsl
INSTDEPENDS += libx11-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += xutils-dev
INSTDEPENDS += libpthread-stubs0-dev
#INSTDEPENDS += libudev-dev [linux-any]
INSTDEPENDS += libpciaccess-dev
#INSTDEPENDS += valgrind
INSTDEPENDS += libbsd-dev
