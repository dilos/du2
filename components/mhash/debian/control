Source: mhash
Section: libs
Priority: optional
Maintainer: Chris Hanson <cph@debian.org>
Uploaders: Barak A. Pearlmutter <bap@debian.org>
Build-Depends: debhelper (>= 9), dh-autoreconf, pkg-config
Standards-Version: 3.9.5
Homepage: http://mhash.sourceforge.net/
Vcs-Git: git://anonscm.debian.org/collab-maint/mhash.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=collab-maint/mhash.git

Package: libmhash2
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Library for cryptographic hashing and message authentication
 Mhash is a library that provides a uniform interface to a large
 number of hash algorithms.  These algorithms can be used to compute
 checksums, message digests, and other signatures.  The HMAC support
 implements the basics for message authentication, following RFC 2104.
 Mhash also provides several key-generation algorithms, including
 those of OpenPGP (RFC 2440).
 .
 This package contains the shared library.

Package: libmhash-dev
Section: libdevel
Architecture: any
Depends: libmhash2 (= ${binary:Version}), libc-dev, ${misc:Depends}
Description: Library for cryptographic hashing and message authentication
 Mhash is a library that provides a uniform interface to a large
 number of hash algorithms.  These algorithms can be used to compute
 checksums, message digests, and other signatures.  The HMAC support
 implements the basics for message authentication, following RFC 2104.
 Mhash also provides several key-generation algorithms, including
 those of OpenPGP (RFC 2440).  Further information is available at
 http://mhash.sourceforge.net/.
 .
 This package contains header files, the man page, and the static
 library.
