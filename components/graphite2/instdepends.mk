INSTDEPENDS += debhelper
INSTDEPENDS += dh-exec
INSTDEPENDS += dpkg-dev
INSTDEPENDS += cmake
INSTDEPENDS += python
INSTDEPENDS += fonttools
# Build-Depends-Indep:
INSTDEPENDS += asciidoc-dblatex
INSTDEPENDS += doxygen
INSTDEPENDS += docbook-xsl
INSTDEPENDS += latex-xcolor
INSTDEPENDS += libxml2-utils
INSTDEPENDS += graphviz
