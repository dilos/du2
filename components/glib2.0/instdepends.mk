INSTDEPENDS += debhelper
INSTDEPENDS += cdbs
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dh-exec
INSTDEPENDS += pkg-config
INSTDEPENDS += gettext
INSTDEPENDS += autotools-dev
INSTDEPENDS += gnome-pkg-tools
INSTDEPENDS += dpkg-dev
INSTDEPENDS += libelf-dev
# libmount-dev (>= 2.28) [linux-any
INSTDEPENDS += libpcre3-dev
INSTDEPENDS += gtk-doc-tools
# libselinux1-dev [linux-any],
# linux-libc-dev [linux-any],
INSTDEPENDS += libgamin-dev
# [!linux-any]
# libfam-dev
# [!linux-any],
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += desktop-file-utils
# dbus
# shared-mime-info
# tzdata <!nocheck>,
# xterm <!nocheck>,
INSTDEPENDS += python3
# python3-dbus <!nocheck>,
# python3-gi <!nocheck>,
INSTDEPENDS += libxml2-utils
INSTDEPENDS += xsltproc
INSTDEPENDS += docbook-xml
INSTDEPENDS += docbook-xsl
INSTDEPENDS += libffi-dev
INSTDEPENDS += libsec-dev
