Source: cheetah
Section: python
Priority: optional
Maintainer: Arnaud Fontaine <arnau@debian.org>
Uploaders: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Build-Depends: debhelper (>= 10~),
	       cdbs (>= 0.4.90~),
	       python-all-dev (>= 2.6.6-9),
               dh-python,
	       python-setuptools (>= 0.6.14-5),
	       python-markdown (>= 2.0.1)
X-Python-Version: >= 2.3
Standards-Version: 3.9.8
Homepage: http://www.cheetahtemplate.org/
Vcs-Git: https://anonscm.debian.org/git/python-modules/packages/cheetah.git
Vcs-Browser: https://anonscm.debian.org/cgit/python-modules/packages/cheetah.git

Package: python-cheetah
Architecture: any
Depends: ${python:Depends},
	 ${shlibs:Depends},
	 ${misc:Depends}
Suggests: python-markdown (>= 2.0.1), python-pygments, python-memcache
Description: text-based template engine and Python code generator
 Cheetah can be used as a standalone templating utility or referenced as a
 library from other Python applications. It has many potential uses, but web
 developers looking for a viable alternative to ASP, JSP, PHP and PSP are
 expected to be its principle user group.
 .
 Features:
  * Generates HTML, SGML, XML, SQL, Postscript, form email, LaTeX, or any other
    text-based format.
  * Cleanly separates content, graphic design, and program code.
  * Blends the power and flexibility of Python with a simple template language
    that non-programmers can understand.
  * Gives template writers full access to any Python data structure, module,
    function, object, or method in their templates.
  * Makes code reuse easy by providing an object-orientated interface to
    templates that is accessible from Python code or other Cheetah templates.
    One template can subclass another and selectively reimplement sections of
    it.
  * Provides a simple, yet powerful, caching mechanism that can dramatically
    improve the performance of a dynamic website.
  * Compiles templates into optimized, yet readable, Python code.
