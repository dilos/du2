INSTDEPENDS += debhelper
INSTDEPENDS += dh-buildinfo
INSTDEPENDS += pkg-config
INSTDEPENDS += libgavl-dev
INSTDEPENDS += libcairo2-dev
INSTDEPENDS += libopencv-dev
#Build-Depends-Indep:
INSTDEPENDS += doxygen
INSTDEPENDS += graphviz
