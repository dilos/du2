INSTDEPENDS += debhelper
INSTDEPENDS += libblas-dev
INSTDEPENDS += liblapack-dev
INSTDEPENDS += cmake
INSTDEPENDS += libarpack2-dev
INSTDEPENDS += dpkg-dev
INSTDEPENDS += libsuperlu-dev
INSTDEPENDS += quilt
