#!/bin/sh
#
# Copyright (c) 2012-2019, DilOS.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"),  to deal in
# the Software without restriction, including without  limitation  the rights to
# use, copy, modify, merge, publish, distribute, sublicense,  and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT  WARRANTY  OF  ANY  KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT,  TORT  OR  OTHERWISE,  ARISING  FROM,  OUT  OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Standard prolog
#
. /lib/svc/share/smf_include.sh

#if [ -z $SMF_FMRI ]; then
#        echo "SMF framework variables are not initialized."
#        exit $SMF_EXIT_ERR
#fi

method=$1
instance=$2
contract=$3

#
# Is NTP configured?
#
if [ ! -f /etc/ntp.conf ]; then
	echo "Error: Configuration file '/etc/ntp.conf' not found." \
	    "  See ntpd(1M)."
	exit $SMF_EXIT_ERR_CONFIG
fi

# for debug, uncomment it
#set -x

LANG=C
export LANG

DAEMON=/usr/sbin/ntpd
PIDFILE=/var/run/ntpd.pid

test -x $DAEMON || exit $SMF_EXIT_ERR

if [ -r /etc/default/ntp ]; then
	. /etc/default/ntp
fi

RUNASUSER=daemon
UGID=$(getent passwd $RUNASUSER | cut -f 3,4 -d:) || true
NTPD_OPTS="$NTPD_OPTS -u $UGID"

case $method in
'start')
	[ -d /var/lib/ntp ] || mkdir -p /var/lib/ntp
	chown -R $RUNASUSER:$RUNASUSER /var/lib/ntp
	$DAEMON -p $PIDFILE $NTPD_OPTS
	;;

'stop')
	if [ -f $PIDFILE ]; then
		PID=$(cat $PIDFILE)
		kill -TERM $PID
	fi
	;;

'restart')
	$0 stop
	sleep 2
	$0 start
	;;

*)
	echo "Usage: $0 [start|stop|restart]"
	exit $SMF_EXIT_ERR
	;;
esac
exit $SMF_EXIT_OK
