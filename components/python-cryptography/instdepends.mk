INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += dpkg-dev
INSTDEPENDS += libssl-dev
INSTDEPENDS += python-all-dev
INSTDEPENDS += python-cffi
INSTDEPENDS += python-cryptography-vectors
INSTDEPENDS += python-enum34
INSTDEPENDS += python-hypothesis
INSTDEPENDS += python-idna
INSTDEPENDS += python-ipaddress
INSTDEPENDS += python-iso8601
INSTDEPENDS += python-pretend
INSTDEPENDS += python-pyasn1
INSTDEPENDS += python-pyasn1-modules
INSTDEPENDS += python-pytest
INSTDEPENDS += python-setuptools
INSTDEPENDS += python-six
INSTDEPENDS += python-sphinx-rtd-theme
INSTDEPENDS += python-tz
INSTDEPENDS += python3-all-dev
INSTDEPENDS += python3-cffi
INSTDEPENDS += python3-cryptography-vectors
INSTDEPENDS += python3-hypothesis
INSTDEPENDS += python3-idna
INSTDEPENDS += python3-iso8601
INSTDEPENDS += python3-pretend
INSTDEPENDS += python3-pyasn1
INSTDEPENDS += python3-pyasn1-modules
INSTDEPENDS += python3-pytest
INSTDEPENDS += python3-setuptools
INSTDEPENDS += python3-six
INSTDEPENDS += python3-sphinx
INSTDEPENDS += python3-tz
