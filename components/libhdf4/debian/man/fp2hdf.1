.TH FP2HDF 1 "October 30, 1999"
.\" man page by Jim Van Zandt <jrv@vanzandt.mv.com>         -*- nroff -*-
.SH NAME
fp2hdf \- convert floating point data to HDF
.SH SYNOPSIS
\fBfp2hdf\fP \fB-h\fP[\fBelp\fP]
\fBfp2hdf\fP \fIinfile\fP [\fIinfile\fP...]
\fB-o\fP[\fButfile\fP \fIoutfile\fP]
[\fB-r\fP[\fBaster\fP] [\fIras_options\fP...]]
[\fB-f\fP[\fBloat\fP]]
.SH DESCRIPTION
\fBfp2hdf\fP
converts floating point data to HDF Scientific Data Set (SDS)
and/or 8-bit Raster Image Set (RIS8) format, storing the results
in an HDF file.  The image data can be scaled about a mean value.
.P
Input file(s) contain a single two-dimensional or
three-dimensional floating point array in either ASCII text, native
floating point, or HDF SDS format.  If an HDF file is used for input,
it must contain an SDS.  The SDS need only contain a dimension record
and the data, but if it also contains maximum and minimum values
and/or scales for each axis, these will be used.  If the input format
is ASCII text or native floating point, see "Notes" below on how it
must be organized.
.SH OPTIONS
.TP
.BR -h [ elp ]
Print a helpful summary of usage, and exit.
.TP
.BR -o [ utfile "] \fIoutfile\fP"
Data from one or more input files are stored as one or more data sets
and/or images in one HDF output file, \fIoutfile\fP.
.TP
.BR -r [ aster ]
Store output as a raster image set in the output file
.BR -f [ loat ]
Store output as a scientific data set in the the output file.
This is the default if the "-r" option is not specified.
.P
\fIras_opts\fP:
.TP
.BR -e [ xpand "] \fIhoriz\fP \fIvert\fP [\fIdepth\fP]"
Expand float data via pixel replication to produce the image(s).
\fIhoriz\fP and \fIvert\fP give the horizontal and vertical resolution
of the image(s) to be produced; and optionally, \fIdepth\fP gives the
number of images or depth planes (for 3D input data).
.TP
.BR -i [ nterp "] \fIhoriz vert\fP [\fIdepth\fP]"
Apply bilinear, or trilinear, interpolation to the float data to
produce the image(s).  \fIhoriz\fP, \fIvert\fP, and \fIdepth\fP must
be greater than or equal to the dimensions of the original dataset.
.TP
.BR -p [ alfile "] \fIpalfile\fP"
Store the palette with the image.  Get the palette from
\fIpalfile\fP; which may be an HDF file containing a palette,
or a file containing a raw palette.
.TP
.BR -m [ ean "] \fImean\fP"
If a floating point mean value is given, the image will be
scaled about the mean.  The new extremes (newmax and newmin),
as given by:
.sp
  newmax = mean + max(abs(max-mean), abs(mean-min))
.br
  newmin = mean - max(abs(max-mean), abs(mean-min))
.IP
will be equidistant from the mean value.  If no mean value
is given, then the mean will be:  0.5 * (max + min)
.SH INPUT
If the input file format is ASCII text or native floating point, it
must have the following input fields:
.nf

        format
        nplanes
        nrows
        ncols
        max_value
        min_value
        [plane1 plane2 plane3 ...]
        row1 row2 row3 ...
        col1 col2 col3 ...
        data1 data2 data3 ...
        ...

.fi
Where:
.IP \fIformat\fP
Format designator ("TEXT", "FP32" or "FP64").
.IP \fInplanes\fP
Dimension of the depth axis ("1" for 2D input).
.IP \fInrows\fP
Dimension of the vertical axis.
.IP \fIncols\fP
Dimension of the horizontal axis.
.IP \fImax_value\fP
Maximum data value.
.IP \fImin_value\fP
Minimum data value.
.IP "\fIplane1\fP, \fIplane2\fP, \fIplane3\fP, ..."
Scales for depth axis.
.IP "\fIrow1\fP, \fIrow2\fP, \fIrow3\fP, ..."
Scales for the vertical axis.
.IP "\fIcol1\fP, \fIcol2\fP, \fIcol3\fP, ..."
Scales for the horizontal axis.
.IP "\fIdata1\fP, \fIdata2\fP, \fIdata3\fP, ..."
The data ordered by rows, left to right and top
to bottom; then optionally, ordered by planes,
front to back.
.IP
For FP32 and FP64 input format, \fIformat\fP, \fInplanes\fP,
\fInrows\fP, \fIncols\fP, and \fInplanes\fP are native integers; where
\fIformat\fP is the integer representation of the appropriate
4-character string (0x46503332 for "FP32" and 0x46503634 for "FP64").
The remaining input fields are composed of native 32-bit floating
point values for FP32 input format, or native 64-bit floating point
values for FP64 input format.
.SH EXAMPLE
Convert floating point data in "f1.txt" to SDS format, and store it
as an SDS in HDF file "o1":
.sp
        fp2hdf f1.txt -o o1
.sp
Convert floating point data in "f2.hdf" to 8-bit raster format, and
store it as an RIS8 in HDF file "o2":
.sp
        fp2hdf f2.hdf -o o2 -r
.sp
Convert floating point data in "f3.bin" to 8-bit raster format and
SDS format, and store both the RIS8 and the SDS in HDF file "o3":
.sp
        fp2hdf f3.bin -o o3 -r -f
.sp
Convert floating point data in "f4" to a 500x600 raster image, and
store the RIS8 in HDF file "o4".  Also store a palette from "palfile"
with the image:
.sp
        fp2hdf f4 -o o4 -r -e 500 600 -p palfile
.sp
Convert floating point data in "f5" to 200 planes of 500x600 raster
images, and store the RIS8 in HDF file "o5".  Also scale the image
data so that it is centered about a mean value of 10.0:
.sp
        fp2hdf f5 -o o5 -r -i 500 600 200 -m 10.0
.SH "SEE ALSO"
\fBhdf\fP(5)
