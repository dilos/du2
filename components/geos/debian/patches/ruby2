Description: Update include and libraries paths for Ruby 2.x
Author: Bas Couwenberg <sebastic@debian.org>
Forwarded: https://trac.osgeo.org/geos/ticket/742

--- a/configure
+++ b/configure
@@ -18601,12 +18601,16 @@ fi
 
 				RUBY_BIN_DIR=`$RUBY -rrbconfig -e 'puts RbConfig::CONFIG["bindir"] || Config::CONFIG["bindir"]'`
 
-    		RUBY_SITE_ARCH=`$RUBY -rrbconfig -e 'puts RbConfig::CONFIG["sitearch"] || Config::CONFIG["sitearch"]'`
+				RUBY_SITE_ARCH=`$RUBY -rrbconfig -e 'puts RbConfig::CONFIG["sitearch"] || Config::CONFIG["sitearch"]'`
 
 				RUBY_INCLUDE_DIR=`$RUBY -rrbconfig -e 'puts RbConfig::CONFIG["rubyhdrdir"] || Config::CONFIG["archdir"]'`
 
+				RUBY_ARCH_INCLUDE_DIR=`$RUBY -rrbconfig -e 'puts RbConfig::CONFIG["rubyarchhdrdir"] || Config::CONFIG["archdir"]'`
+
 				RUBY_LIB_DIR=`$RUBY -rrbconfig -e 'puts RbConfig::CONFIG["libdir"] || Config::CONFIG["libdir"]'`
 
+				RUBY_ARCH_LIB_DIR=`$RUBY -rrbconfig -e 'puts RbConfig::CONFIG["archlibdir"] || Config::CONFIG["archlibdir"]'`
+
 				RUBY_EXTENSION_DIR=`$RUBY -rrbconfig -e 'puts RbConfig::CONFIG["vendorarchdir"] || Config::CONFIG["vendorarchdir"]'`
 
 				RUBY_SO_NAME=`$RUBY -rrbconfig -e 'puts RbConfig::CONFIG["RUBY_SO_NAME"] || Config::CONFIG["RUBY_SO_NAME"]'`
--- a/macros/ruby.m4
+++ b/macros/ruby.m4
@@ -25,15 +25,21 @@ AC_DEFUN([AC_RUBY_DEVEL],
 		dnl Get Ruby bin directory
 		RUBY_BIN_DIR=`$RUBY -rrbconfig -e 'puts RbConfig::CONFIG[["bindir"]] || Config::CONFIG[["bindir"]]'`
 
-    dnl Get Ruby site arch
+		dnl Get Ruby site arch
 		RUBY_SITE_ARCH=`$RUBY -rrbconfig -e 'puts RbConfig::CONFIG[["sitearch"]] || Config::CONFIG[["sitearch"]]'`
 
 		dnl Get Ruby include directory
 		RUBY_INCLUDE_DIR=`$RUBY -rrbconfig -e 'puts RbConfig::CONFIG[["rubyhdrdir"]] || Config::CONFIG[["archdir"]]'`
 	
+		dnl Get Ruby config.h include directory
+		RUBY_ARCH_INCLUDE_DIR=`$RUBY -rrbconfig -e 'puts RbConfig::CONFIG[["rubyarchhdrdir"]] || Config::CONFIG[["archdir"]]'`
+	
 		dnl Get Ruby lib directory
 		RUBY_LIB_DIR=`$RUBY -rrbconfig -e 'puts RbConfig::CONFIG[["libdir"]] || Config::CONFIG[["libdir"]]'`
 
+		dnl Get Ruby arch lib directory
+		RUBY_ARCH_LIB_DIR=`$RUBY -rrbconfig -e 'puts RbConfig::CONFIG[["archlibdir"]] || Config::CONFIG[["archlibdir"]]'`
+
 		dnl Get Ruby extensions directory
 		RUBY_EXTENSION_DIR=`$RUBY -rrbconfig -e 'puts RbConfig::CONFIG[["vendorarchdir"]] || Config::CONFIG[["vendorarchdir"]]'`
 
@@ -48,7 +54,9 @@ AC_DEFUN([AC_RUBY_DEVEL],
     AC_MSG_NOTICE([Ruby bin directory is '$RUBY_BIN_DIR'])
     AC_MSG_NOTICE([Ruby site arch is '$RUBY_SITE_ARCH'])
     AC_MSG_NOTICE([Ruby include directory is '$RUBY_INCLUDE_DIR'])
+    AC_MSG_NOTICE([Ruby config.h include directory is '$RUBY_ARCH_INCLUDE_DIR'])
     AC_MSG_NOTICE([Ruby library directory is '$RUBY_LIB_DIR'])
+    AC_MSG_NOTICE([Ruby architecture library directory is '$RUBY_ARCH_LIB_DIR'])
     AC_MSG_NOTICE([Ruby extension directory is '$RUBY_EXTENSION_DIR'])
     AC_MSG_NOTICE([Ruby library is '$RUBY_SO_NAME'])
     AC_MSG_NOTICE([Ruby import library is '$RUBY_SHARED_LIB'])
@@ -57,7 +65,9 @@ AC_DEFUN([AC_RUBY_DEVEL],
     AC_SUBST([RUBY_BIN_DIR])
     AC_SUBST([RUBY_SITE_ARCH])
     AC_SUBST([RUBY_INCLUDE_DIR])
+    AC_SUBST([RUBY_ARCH_INCLUDE_DIR])
     AC_SUBST([RUBY_LIB_DIR])
+    AC_SUBST([RUBY_ARCH_LIB_DIR])
     AC_SUBST([RUBY_EXTENSION_DIR])
     AC_SUBST([RUBY_SO_NAME])
     AC_SUBST([RUBY_SHARED_LIB])
--- a/swig/ruby/Makefile.am
+++ b/swig/ruby/Makefile.am
@@ -17,18 +17,18 @@ BUILT_SOURCES = geos_wrap.cxx
 rubyextensiondirdir = $(RUBY_EXTENSION_DIR)
 
 # Setup includes
-AM_CPPFLAGS = -I$(RUBY_INCLUDE_DIR) -I$(RUBY_INCLUDE_DIR)/$(RUBY_SITE_ARCH)
+AM_CPPFLAGS = -I$(RUBY_INCLUDE_DIR) -I$(RUBY_INCLUDE_DIR)/$(RUBY_SITE_ARCH) -I$(RUBY_ARCH_INCLUDE_DIR)
 
 # Build Ruby module as shared library
 rubyextensiondir_LTLIBRARIES = geos.la
 geos_la_SOURCES = geos_wrap.cxx
-geos_la_LIBADD =  $(top_builddir)/capi/libgeos_c.la $(RUBY_SO_NAME)
+geos_la_LIBADD =  $(top_builddir)/capi/libgeos_c.la -l$(RUBY_SO_NAME)
 
 # Only need to grab the capi header files
 geos_la_CPPFLAGS = $(AM_CPPFLAGS) -I$(top_builddir)/capi
 
 # Specify -module and -avoid-version so we can create a file called geos.dll/so which is what Ruby wants
-geos_la_LDFLAGS = -no-undefined  -module -avoid-version -L$(RUBY_LIB_DIR)
+geos_la_LDFLAGS = -no-undefined  -module -avoid-version -L$(RUBY_LIB_DIR) -L$(RUBY_ARCH_LIB_DIR)
 
 if ENABLE_SWIG
 
--- a/swig/ruby/Makefile.in
+++ b/swig/ruby/Makefile.in
@@ -447,18 +447,18 @@ MAINTAINERCLEANFILES = geos_wrap.cxx
 @ENABLE_RUBY_TRUE@rubyextensiondirdir = $(RUBY_EXTENSION_DIR)
 
 # Setup includes
-@ENABLE_RUBY_TRUE@AM_CPPFLAGS = -I$(RUBY_INCLUDE_DIR) -I$(RUBY_INCLUDE_DIR)/$(RUBY_SITE_ARCH)
+@ENABLE_RUBY_TRUE@AM_CPPFLAGS = -I$(RUBY_INCLUDE_DIR) -I$(RUBY_INCLUDE_DIR)/$(RUBY_SITE_ARCH) -I$(RUBY_ARCH_INCLUDE_DIR)
 
 # Build Ruby module as shared library
 @ENABLE_RUBY_TRUE@rubyextensiondir_LTLIBRARIES = geos.la
 @ENABLE_RUBY_TRUE@geos_la_SOURCES = geos_wrap.cxx
-@ENABLE_RUBY_TRUE@geos_la_LIBADD = $(top_builddir)/capi/libgeos_c.la $(RUBY_SO_NAME)
+@ENABLE_RUBY_TRUE@geos_la_LIBADD = $(top_builddir)/capi/libgeos_c.la -l$(RUBY_SO_NAME)
 
 # Only need to grab the capi header files
 @ENABLE_RUBY_TRUE@geos_la_CPPFLAGS = $(AM_CPPFLAGS) -I$(top_builddir)/capi
 
 # Specify -module and -avoid-version so we can create a file called geos.dll/so which is what Ruby wants
-@ENABLE_RUBY_TRUE@geos_la_LDFLAGS = -no-undefined  -module -avoid-version -L$(RUBY_LIB_DIR)
+@ENABLE_RUBY_TRUE@geos_la_LDFLAGS = -no-undefined  -module -avoid-version -L$(RUBY_LIB_DIR) -L$(RUBY_ARCH_LIB_DIR)
 all: $(BUILT_SOURCES)
 	$(MAKE) $(AM_MAKEFLAGS) all-recursive
 
