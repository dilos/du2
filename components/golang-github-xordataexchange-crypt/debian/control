Source: golang-github-xordataexchange-crypt
Section: devel
Priority: extra
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Anthony Fok <foka@debian.org>, Tim Potter <tpot@hpe.com>
Build-Depends: debhelper (>= 10),
               dh-exec,
               dh-golang,
               golang-any,
               golang-github-armon-consul-api-dev,
               golang-etcd-server-dev | golang-github-coreos-etcd-dev,
               golang-golang-x-crypto-dev
Standards-Version: 4.0.0
Homepage: https://github.com/xordataexchange/crypt
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-go/packages/golang-github-xordataexchange-crypt.git
Vcs-Git: https://anonscm.debian.org/git/pkg-go/packages/golang-github-xordataexchange-crypt.git
XS-Go-Import-Path: github.com/xordataexchange/crypt
Testsuite: autopkgtest-pkg-go

Package: golang-github-xordataexchange-crypt-dev
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         golang-github-armon-consul-api-dev,
         golang-golang-x-crypto-dev,
#         golang-github-coreos-etcd-dev | golang-etcd-server-dev,
Suggests: golang-github-xordataexchange-crypt
Description: Store/retrieve encrypted configs from etcd or Consul (Go library)
 Fess up.  You have passwords and usernames hard coded in your apps.
 You have IP addresses checked in to your source code repository.
 You have entire configuration files that were created by the developer
 who wrote the app and haven’t been changed since she typed "git init".
 .
 "crypt" is here to lead you back to the Path of Enlightened Configuration.
 Store encrypted configuration values in etcd or Consul using a command-line
 application.
 .
 Decrypt them before starting your application using a wrapper script and
 the handy CLI tool, or inside the app using the "crypt/config" library.
 .
 "crypt" is built on time-tested standards like OpenPGP, base64, and gzip.
 Your data is encrypted using public key encryption, and can only be
 decrypted by when the private key is available.  After compression,
 it is encrypted, and base64-encoded so it can be stored in your key/value
 store of choice.  etcd and Consul are supported out of the box, but adding
 other storage tools is a trivial task, thanks to Go’s interfaces.
 .
 This package provides the "github.com/xordataexchange/crypt/config"
 Go library.

Package: golang-github-xordataexchange-crypt
Architecture: any
Built-Using: ${misc:Built-Using}
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: golang-github-xordataexchange-crypt-dev
Description: Store/retrieve encrypted configs from etcd or Consul (CLI tool)
 Fess up.  You have passwords and usernames hard coded in your apps.
 You have IP addresses checked in to your source code repository.
 You have entire configuration files that were created by the developer
 who wrote the app and haven’t been changed since she typed "git init".
 .
 "crypt" is here to lead you back to the Path of Enlightened Configuration.
 Store encrypted configuration values in etcd or Consul using a command-line
 application.
 .
 Decrypt them before starting your application using a wrapper script and
 the handy CLI tool, or inside the app using the "crypt/config" library.
 .
 "crypt" is built on time-tested standards like OpenPGP, base64, and gzip.
 Your data is encrypted using public key encryption, and can only be
 decrypted by when the private key is available.  After compression,
 it is encrypted, and base64-encoded so it can be stored in your key/value
 store of choice.  etcd and Consul are supported out of the box, but adding
 other storage tools is a trivial task, thanks to Go’s interfaces.
 .
 This package provides the command-line tool "bin/crypt", but renamed to
 /usr/bin/crypt-xordataexchange, to avoid filename collision with
 /usr/bin/crypt from the mcrypt package.
Section: utils
Priority: optional
