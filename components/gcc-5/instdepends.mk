INSTDEPENDS += debhelper
INSTDEPENDS +=  dpkg-dev
# m4
INSTDEPENDS += libtool
# autoconf2.64
INSTDEPENDS += autogen
# gawk
INSTDEPENDS += lzma
INSTDEPENDS += xz-utils
INSTDEPENDS += patchutils 
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += lib32z1-dev
INSTDEPENDS += binutils
INSTDEPENDS += gperf
INSTDEPENDS += bison
INSTDEPENDS += flex
INSTDEPENDS += gettext
INSTDEPENDS += gdb
INSTDEPENDS += texinfo
# locales
INSTDEPENDS += sharutils
# procps
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += libantlr-java
INSTDEPENDS += python
INSTDEPENDS += libffi-dev
INSTDEPENDS += fastjar
INSTDEPENDS += libmagic-dev
INSTDEPENDS += libecj-java
INSTDEPENDS += zip
# libasound2-dev [ !hurd-any !kfreebsd-any]
INSTDEPENDS += libxtst-dev
INSTDEPENDS += libxt-dev
INSTDEPENDS += libgtk2.0-dev
INSTDEPENDS += libart-2.0-dev
INSTDEPENDS += libcairo2-dev
# netbase, 
INSTDEPENDS += libcloog-isl-dev
INSTDEPENDS += libmpc-dev
INSTDEPENDS += libmpfr-dev
INSTDEPENDS += libgmp-dev
INSTDEPENDS += dejagnu
INSTDEPENDS += realpath
INSTDEPENDS += chrpath
INSTDEPENDS += lsb-release
INSTDEPENDS += quilt
# Build-Depends-Indep:
INSTDEPENDS += doxygen
INSTDEPENDS += graphviz
INSTDEPENDS += ghostscript
INSTDEPENDS += texlive-latex-base
INSTDEPENDS += xsltproc
INSTDEPENDS += libxml2-utils
INSTDEPENDS += docbook-xsl-ns
INSTDEPENDS += gcc-5
