INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += libssl1.0-dev
INSTDEPENDS += python-all-dev
INSTDEPENDS += python-setuptools
INSTDEPENDS += swig
INSTDEPENDS += python-pytest
# openssl
INSTDEPENDS += python-docutils
INSTDEPENDS += links
