INSTDEPENDS += debhelper
INSTDEPENDS += bison
INSTDEPENDS += chrpath
INSTDEPENDS += flex
INSTDEPENDS += gperf
INSTDEPENDS += libfontconfig1-dev
INSTDEPENDS += libgl1-mesa-dev
INSTDEPENDS += libgles2-mesa-dev
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libglu1-mesa-dev
INSTDEPENDS += libgstreamer-plugins-base1.0-dev
INSTDEPENDS += libgstreamer1.0-dev
INSTDEPENDS += libicu-dev
INSTDEPENDS += libjpeg-dev
INSTDEPENDS += libpng-dev
INSTDEPENDS += libqt5opengl5-dev
INSTDEPENDS += libqt5xmlpatterns5-dev
INSTDEPENDS += libsqlite3-dev
INSTDEPENDS += libwebp-dev
INSTDEPENDS += libxcomposite-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += libxrender-dev
INSTDEPENDS += libxslt1-dev
INSTDEPENDS += mesa-common-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += pkg-kde-tools
INSTDEPENDS += python-minimal
INSTDEPENDS += python2.7
INSTDEPENDS += qtbase5-private-dev
INSTDEPENDS += qtdeclarative5-private-dev
INSTDEPENDS += qtscript5-private-dev
INSTDEPENDS += ruby
# Build-Depends-Indep:
INSTDEPENDS += libqt5sql5-sqlite
INSTDEPENDS += qtbase5-doc-html
INSTDEPENDS += qttools5-dev-tools
