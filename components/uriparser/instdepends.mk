INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dh-exec
# Build-Depends-Indep:
INSTDEPENDS += doxygen
INSTDEPENDS += graphviz
INSTDEPENDS += qttools5-dev-tools
INSTDEPENDS += qtbase5-dev
INSTDEPENDS += libqt5sql5-sqlite
