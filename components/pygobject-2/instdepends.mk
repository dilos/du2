INSTDEPENDS += debhelper
INSTDEPENDS += gnome-pkg-tools
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += cdbs
INSTDEPENDS += quilt
INSTDEPENDS += python-all-dev
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libcairo2-dev
INSTDEPENDS += libffi-dev
INSTDEPENDS += python-cairo-dev
INSTDEPENDS += xsltproc
# xvfb
INSTDEPENDS += xauth
INSTDEPENDS += dbus-x11
INSTDEPENDS += docbook-xsl
INSTDEPENDS += autotools-dev
# python-all-dbg
# python-apt-dbg
# python-cairo-dbg
