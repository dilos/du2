Source: lv2
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <pkg-multimedia-maintainers@lists.alioth.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>,
 Jaromír Mikeš <mira.mikes@seznam.cz>
Build-Depends:
 debhelper (>= 9),
 libgtk2.0-dev,
 libsndfile1-dev,
 perl,
 pkg-config,
 python
Standards-Version: 3.9.8
Vcs-Git: https://anonscm.debian.org/git/pkg-multimedia/lv2.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-multimedia/lv2.git
Homepage: http://lv2plug.in/

Package: lv2-dev
Section: libdevel
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Breaks:
 lv2-extensions-good (<= 0~20111202+dfsg0-2~),
 lv2core (<= 6.0+dfsg0-2~)
Replaces:
 lv2-extensions-good (<= 0~20111202+dfsg0-2~),
 lv2core (<= 6.0+dfsg0-2~)
Description: LV2 audio plugin specification
 LV2 is a simple but extensible successor of LADSPA plugins,
 intended to address the limitations of LADSPA which many
 applications have outgrown.
 .
 This package contains the LV2 audio plugin specification,
 with all the official extension packages, as well as example
 plugins, lv2specgen, and additional data.
 .
 Implementations are encouraged to abandon the “copy paste
 headers” practice and depend on this package instead.

Package: lv2-examples
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Breaks: lv2-dev (<= 1.8.0~dfsg0-1+)
Replaces: lv2-dev (<= 1.8.0~dfsg0-1+)
Provides:
 lv2-plugin
Description: LV2 audio plugin specification (example plugins)
 LV2 is a simple but extensible successor of LADSPA plugins,
 intended to address the limitations of LADSPA which many
 applications have outgrown.
 .
 This package contains some LV2 example plugins.
