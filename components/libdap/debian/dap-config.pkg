#! /bin/sh
#
# Rewrite of dap-config, reading from pkg-config file.
# This is necessary under Debian, due to multiarch; 
# dap-config deprecated in favour of pkg-config
#  - Alastair McKinstry, <mckinstry@debian.org>, 2011-09-15

usage()
{
    cat <<EOF
Usage: dap-config [OPTION]

Available values for OPTION include:

  --help      	display this help message and exit
  --cc        	C compiler
  --cxx       	C++ compiler
  --cflags    	pre-processor and compiler flags
  --libs      	library linking information for libdap (both clients and servers)
  --server-libs libraries for servers
  --client-libs libraries for clients
  --prefix    	OPeNDAP install prefix
  --version   	Library version
EOF

    exit $1
}

if test $# -eq 0; then
    usage 1
fi

while test $# -gt 0; do
    case "$1" in
    # this deals with options in the style
    # --option=value and extracts the value part
    # [not currently used]
    -*=*) value=`echo "$1" | sed 's/[-_a-zA-Z0-9]*=//'` ;;
    *) value= ;;
    esac

    case "$1" in
    --help)
	usage 0
	;;

    --cc)
	echo "gcc"
	;;

    --cxx)
	echo "g++"
	;;

    --cflags)
	pkg-config --cflags libdap libdapclient libdapserver
	;;

    --libs)
       	pkg-config --libs libdap libdapclient libdapserver
       	;;

    --server-libs)
       	pkg-config --libs libdapserver
       	;;

    --client-libs)
       	pkg-config --libs libdapclient
       	;;

    --prefix)
       	pkg-config --variable=prefix libdap
       	;;

    --version)
	pkg-config --modversion libdap
	;;

    *)
        echo "unknown option: $1"
	usage
	exit 1
	;;
    esac
    shift
done

exit 0
