Source: lablgl
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
 Samuel Mimram <smimram@debian.org>,
 Stéphane Glondu <glondu@debian.org>,
 Mehdi Dogguy <mehdi@debian.org>,
 Ralf Treinen <treinen@debian.org>,
 Lifeng Sun <lifongsun@gmail.com>
Build-Depends:
 debhelper (>= 9),
 ocaml (>= 4.00.1),
 tcl8.5-dev,
 tk8.5-dev,
 liblabltk-ocaml-dev,
 libgl1-mesa-dev | libgl-dev,
 libglu1-mesa-dev | libglu-dev,
 freeglut3-dev,
 x11proto-core-dev,
 libxmu-dev,
 libx11-dev,
 dpkg-dev (>= 1.13.19),
 docbook-xml,
 docbook-xsl,
 libxml2-utils,
 xsltproc,
 camlp4,
 dh-ocaml (>= 0.9)
Standards-Version: 3.9.4
Homepage: https://forge.ocamlcore.org/projects/lablgl/
Vcs-Git: git://anonscm.debian.org/pkg-ocaml-maint/packages/lablgl.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-ocaml-maint/packages/lablgl.git

# Reasons for Build-Depends:
#
# debhelper (>= 4.0): used in debian/rules, debian/compat
# ocaml: the ocaml compiler and stdlib, with labltk
# tcl8.5-dev: #include <tcl.h>
# tk8.5-dev: #include <tk.h>
# libgl-dev: #include <GL/gl.h>
#            #include <GL/glx.h>
# libglu-dev: #include <GL/glu.h>
# freeglut3-dev: #include <GL/glut.h>
#               #include <GLUT/*>
# x11proto-core-dev: #include <X11/Xatom.h>
# libxmu-dev: #include <X11/Xmu/StdCmap.h>
# libx11-dev: #include <X11/Xlib.h>
#             #include <X11/Xutil.h>
# dpkg-dev (>= 1.13.19): binary:Version
# docbook-xml (>= 4.4), docbook-xsl, libxml2-utils, xsltproc: manpages
# camlp4: camlp4

Package: liblablgl-ocaml
Architecture: any
Depends:
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Description: Runtime libraries for lablgl
 LablGL is an OpenGL interface for Objective Caml. Since it includes
 support for the Togl widget you can comfortably use it with LablTk.
 A GtkGlarea binding for use with lablgtk is also provided.
 .
 This package contains only the dynamic libraries needed for running dynamic
 bytecode executables.

Package: liblablgl-ocaml-dev
Architecture: any
Depends:
 libgl1-mesa-dev | libgl-dev,
 libglu1-mesa-dev | xlibmesa-glu-dev | libglu-dev,
 tk8.5-dev,
 ocaml-findlib,
 libxmu-dev,
 freeglut3-dev,
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Description: OpenGL interface for Objective Caml
 LablGL gives access to the OpenGL interface from Objective Caml. Since it
 includes support for the Togl widget, you can comfortably use it with
 LablTk. A GtkGlarea binding for use with lablgtk is also provided.
