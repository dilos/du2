INSTDEPENDS += debhelper
INSTDEPENDS += quilt
INSTDEPENDS += m4
INSTDEPENDS += lsb-release
INSTDEPENDS += zip
INSTDEPENDS += unzip
INSTDEPENDS += sharutils
# gawk
# cpio
INSTDEPENDS += pkg-config
# procps
INSTDEPENDS += wdiff
# tzdata
INSTDEPENDS += fastjar
INSTDEPENDS += autoconf
INSTDEPENDS += automake
INSTDEPENDS += autotools-dev
INSTDEPENDS += ant
INSTDEPENDS += ant-optional
# g++-6
# openjdk-8-jdk | openjdk-7-jdk
INSTDEPENDS += libxtst-dev
INSTDEPENDS += libxi-dev
INSTDEPENDS += libxt-dev
INSTDEPENDS += libxaw7-dev
INSTDEPENDS += libxrender-dev
INSTDEPENDS += libcups2-dev
# libasound2-dev
INSTDEPENDS += liblcms2-dev
INSTDEPENDS += libfreetype6-dev
INSTDEPENDS += libgtk2.0-dev
INSTDEPENDS += libxinerama-dev
INSTDEPENDS += libkrb5-dev
INSTDEPENDS += xsltproc
INSTDEPENDS += libpcsclite-dev
INSTDEPENDS += libffi-dev
INSTDEPENDS += zlib1g-dev
# libattr1-dev
INSTDEPENDS += libpng-dev
INSTDEPENDS += libjpeg-dev
INSTDEPENDS += libgif-dev
INSTDEPENDS += libpulse-dev
# systemtap-sdt-dev - linux-any
INSTDEPENDS += libnss3-dev
INSTDEPENDS += mauve
INSTDEPENDS += jtreg
# xvfb - need xorg-server port
INSTDEPENDS += xauth
INSTDEPENDS += xfonts-base
INSTDEPENDS += libgl1-mesa-dri
# twm | metacity
# twm |
INSTDEPENDS += dbus-x11
INSTDEPENDS += x11-xkb-utils
INSTDEPENDS += time
# openjdk-8-jdk-headless <cross>
INSTDEPENDS += dpkg-dev
