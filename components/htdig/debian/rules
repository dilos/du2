#!/usr/bin/make -f

DEB_HOST_GNU_TYPE       ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE      ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)

CFLAGS = `dpkg-buildflags --get CFLAGS` -D__EXTENSIONS__ -D__dilos__
CXXFLAGS = `dpkg-buildflags --get CXXFLAGS`
CXXFLAGS += -Wno-deprecated
LDFLAGS = `dpkg-buildflags --get LDFLAGS`
CPPFLAGS = `dpkg-buildflags --get CPPFLAGS`

build: build-arch build-indep
build-arch: build-stamp
build-indep: build-stamp
build-stamp:
	dh_testdir
	dh_autoreconf
	rm -f libhtdigphp/config.log libhtdigphp/config.status
	export PDF_PARSER=/usr/bin/htdig-pdfparser && \
	CFLAGS="$(CFLAGS)" CPPFLAGS="$(CPPFLAGS)" CXXFLAGS="$(CXXFLAGS)" LDFLAGS="$(LDFLAGS)" ./configure \
		--host=$(DEB_HOST_GNU_TYPE) --build=$(DEB_BUILD_GNU_TYPE) \
		--with-pic \
		--with-gnu-ld \
		--prefix=/usr \
		--with-cgi-bin-dir=/usr/lib/cgi-bin \
		--with-search-dir=/usr/share/doc/htdig/examples \
		--with-image-dir=/var/lib/htdig/www \
		--with-config-dir=/etc/htdig \
		--with-common-dir=/etc/htdig \
		--with-database-dir=/var/lib/htdig
	make
	touch $@

clean:
	dh_testdir
	dh_testroot

	-rm -f build-stamp
	[ ! -f Makefile ] || $(MAKE) distclean
	-rm -f `find . -name "*~"`
	-rm -rf debian/tmp debian/files* core debian/substvars

	dh_autoreconf_clean
	dh_clean

binary-indep: build
	dh_testdir
	dh_testroot
	dh_prep -i
	dh_installdirs -i

	mkdir -p debian/htdig-doc/usr/share/doc/htdig-doc/examples
	cp -r contrib/* debian/htdig-doc/usr/share/doc/htdig-doc/examples
	mv debian/htdig-doc/usr/share/doc/htdig-doc/examples/examples \
		debian/htdig-doc/usr/share/doc/htdig-doc/examples/htdig
	mkdir -p debian/htdig-doc/usr/share/doc/htdig-doc/html
	cp -r htdoc/* debian/htdig-doc/usr/share/doc/htdig-doc/html
	rm -f debian/htdig-doc/usr/share/doc/htdig-doc/html/Makefile*
	rm -f debian/htdig-doc/usr/share/doc/htdig-doc/html/COPYING
	rm -f debian/htdig-doc/usr/share/doc/htdig-doc/examples/rtf2html/COPYING
	rmdir debian/htdig-doc/usr/share/doc/htdig-doc/examples/xmlsearch
	rmdir debian/htdig-doc/usr/share/doc/htdig-doc/examples/htdig/xmlsearch
	rm -rf debian/htdig-doc/usr/share/doc/htdig-doc/examples/php-wrapper/
	rm -rf debian/htdig-doc/usr/share/doc/htdig-doc/examples/htwrapper/
	find debian/htdig-doc/usr/share/doc -type f | xargs chmod -x

	dh_installdocs -i README
	dh_installchangelogs -i ChangeLog
	dh_strip -i
	dh_compress -i -Xhtml/C
	dh_fixperms -i
	dh_installdeb -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

binary-arch:	build
	dh_testdir
	dh_testroot
	dh_prep -a
	dh_installdirs -a

	make install DESTDIR=`pwd`/debian/tmp
	mkdir -p debian/tmp/usr/sbin
	install -m 755 debian/htdigconfig debian/tmp/usr/sbin
	mkdir -p debian/tmp/usr/share/htdig
	install -m 755 debian/parse_doc.pl debian/tmp/usr/share/htdig
	install -m 755 debian/htdig-pdfparser debian/tmp/usr/bin
	chmod -x debian/tmp/etc/htdig/*
	find debian/tmp/usr/share/doc -type f | xargs chmod -x

	dh_installdebconf
	dh_installdocs -a README
	dh_installchangelogs -a ChangeLog
	dh_installman -a
	dh_installcron -a
	dh_install -phtdig
	dh_strip -a
	dh_compress -a
	dh_fixperms -a
	dh_makeshlibs -a
	dh_installdeb -a
	dh_shlibdeps -a
	dh_gencontrol -a
	dh_md5sums -a
	dh_builddeb -a

binary:	binary-indep binary-arch

.PHONY: build build-arch build-indep binary binary-arch binary-indep clean
