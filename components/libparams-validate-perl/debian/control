Source: libparams-validate-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>,
           Krzysztof Krzyżaniak (eloy) <eloy@debian.org>,
           Damyan Ivanov <dmn@debian.org>,
           Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper (>= 9.20120312~),
               libmodule-build-perl,
               libmodule-implementation-perl,
               libreadonly-perl,
               libreadonly-xs-perl,
               libtest-fatal-perl,
               libtest-requires-perl,
               libtest-taint-perl,
               perl
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-perl/packages/libparams-validate-perl.git
Vcs-Git: https://anonscm.debian.org/git/pkg-perl/packages/libparams-validate-perl.git
Homepage: https://metacpan.org/release/Params-Validate

Package: libparams-validate-perl
Architecture: any
Depends: ${perl:Depends},
         ${shlibs:Depends},
         ${misc:Depends},
         libmodule-implementation-perl
Description: Perl module to validate parameters to Perl method/function calls
 Params::Validate is a Perl module providing a flexible way to validate method
 and function call parameters. The validation can be as simple as checking for
 the presence of required parameters, or more complex, like validating object
 classes (via isa) or capabilities (via can) and checking parameter types. It
 also provides extensibility through customized validation callbacks.
 .
 The module has been designed to work equally well with positional or named
 parameters (via a hash or hash reference) and includes experimental support
 for attributes (see Attribute::Params::Validate for details).
