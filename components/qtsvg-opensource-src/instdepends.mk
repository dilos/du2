INSTDEPENDS += debhelper
INSTDEPENDS += dbus
INSTDEPENDS += libqt5opengl5-dev
INSTDEPENDS += pkg-kde-tools
INSTDEPENDS += qtbase5-dev
INSTDEPENDS += qtbase5-private-dev
INSTDEPENDS += zlib1g-dev
# Build-Depends-Indep:
INSTDEPENDS += libqt5sql5-sqlite
INSTDEPENDS += qtbase5-doc-html
INSTDEPENDS += qttools5-dev-tools
