INSTDEPENDS += debhelper
#INSTDEPENDS += libasound2-dev [linux-any]
INSTDEPENDS += libgstreamer-plugins-base1.0-dev
INSTDEPENDS += libgstreamer1.0-dev
INSTDEPENDS += libopenal-dev
INSTDEPENDS += libpulse-dev
INSTDEPENDS += libqt5opengl5-dev
INSTDEPENDS += pkg-kde-tools
INSTDEPENDS += qtbase5-dev
INSTDEPENDS += qtbase5-private-dev
INSTDEPENDS += qtdeclarative5-private-dev
INSTDEPENDS += zlib1g-dev
# Build-Depends-Indep:
INSTDEPENDS += libqt5sql5-sqlite
INSTDEPENDS += qtbase5-doc-html
INSTDEPENDS += qttools5-dev-tools
