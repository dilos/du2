#!/usr/bin/make -f

PY2VERS := $(shell pyversions -s)
PY3VERS := $(shell py3versions -s)
LIB := $$(python -c "from distutils.command.build import build ; from distutils.core import Distribution ; b = build(Distribution()) ; b.finalize_options() ; print b.build_platlib")

%:
	dh $@ --with sphinxdoc,python2,python3

override_dh_auto_install:
	set -e ; \
	for python in $(PY2VERS); do \
		$$python setup.py install --prefix $(CURDIR)/debian/python-mpltoolkits.basemap/usr --install-layout=deb; \
	done
#		$$python-dbg setup.py install --prefix $(CURDIR)/debian/python-mpltoolkits.basemap-dbg/usr --install-layout=deb;
	for python in $(PY3VERS); do \
		$$python setup.py install --prefix $(CURDIR)/debian/python3-mpltoolkits.basemap/usr --install-layout=deb; \
	done
#		$$python-dbg setup.py install --prefix $(CURDIR)/debian/python3-mpltoolkits.basemap-dbg/usr --install-layout=deb;
	dh_numpy

override_dh_python2:
	dh_python2
	# remove namespace file, already shipped with python-matplotlib
	rm $(CURDIR)/debian/python-mpltoolkits.basemap/usr/lib/python2*/dist-packages/mpl_toolkits/__init__.py


override_dh_python3:
	dh_python3
	# remove namespace file, already shipped with python3-matplotlib
	rm $(CURDIR)/debian/python3-mpltoolkits.basemap/usr/lib/python3*/dist-packages/mpl_toolkits/__init__.py

build: build-arch build-indep ;

build-arch:
	dh build

build-indep: build-arch
	# HACK: we need to import axes_grid1 but we need to import it from the "local"
	# mpl_toolkits namespace
	ln -s $$(dirname $$(python -c "import mpl_toolkits.axes_grid1 as p; print p.__file__")) $(CURDIR)/$(LIB)/mpl_toolkits/
	# build doc only for default python version
	(export MPLCONFIGDIR=. ; cd doc ; PYTHONPATH=$(CURDIR)/$(LIB) BASEMAPDATA=$(CURDIR)/lib/mpl_toolkits/basemap/data/ python make.py html)
	# remove hack
	rm $(CURDIR)/$(LIB)/mpl_toolkits/axes_grid1

override_dh_sphinxdoc:
	dh_sphinxdoc -i

override_dh_clean:
	dh_clean
	rm -rf doc/build
	rm -f nad2bin.o nad2bin

override_dh_builddeb:
	dh_builddeb -- -Zxz

override_dh_strip:
ifeq (,$(filter nostrip,$(DEB_BUILD_OPTIONS)))
	dh_strip -ppython-mpltoolkits.basemap --dbg-package=python-mpltoolkits.basemap-dbg
	dh_strip -ppython3-mpltoolkits.basemap --dbg-package=python3-mpltoolkits.basemap-dbg
endif
