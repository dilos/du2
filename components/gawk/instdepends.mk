INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
INSTDEPENDS += bison
INSTDEPENDS += gettext
INSTDEPENDS += libmpfr-dev
INSTDEPENDS += libreadline-dev
INSTDEPENDS += libsigsegv-dev
# locales (>= 0)
