INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += libcurl4-gnutls-dev
INSTDEPENDS += libgnutls28-dev
INSTDEPENDS += libssh2-1-dev
INSTDEPENDS += python-all-dev
INSTDEPENDS += python-bottle
INSTDEPENDS += python-docutils
INSTDEPENDS += python-flaky
INSTDEPENDS += python-nose
INSTDEPENDS += python-sphinx
INSTDEPENDS += python3-all-dev
INSTDEPENDS += python3-flaky
# for libcurl4-gnutls
INSTDEPENDS += libidn2-0-dev
INSTDEPENDS += libldap2-dev
INSTDEPENDS += libnghttp2-dev
INSTDEPENDS += libpsl-dev
