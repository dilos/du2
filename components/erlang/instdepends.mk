INSTDEPENDS += debhelper
INSTDEPENDS += autoconf
# openssl
INSTDEPENDS += libssl1.0-dev
# m4
INSTDEPENDS += libncurses5-dev
INSTDEPENDS += autotools-dev
INSTDEPENDS += unixodbc-dev
INSTDEPENDS += bison
INSTDEPENDS += flex
INSTDEPENDS += ed
INSTDEPENDS += libwxgtk3.0-dev
INSTDEPENDS += dctrl-tools
INSTDEPENDS += xsltproc
INSTDEPENDS += libgl1-mesa-dev
# | libgl-dev
INSTDEPENDS += libglu1-mesa-dev
# | libglu-dev
# libsctp-dev [linux-any], libsystemd-dev [linux-any], dh-systemd
#Build-Depends-Indep:
INSTDEPENDS += libxml2-utils
INSTDEPENDS += fop
INSTDEPENDS += default-jdk
# | sun-java6-jdk
