INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += libgnomevfs2-dev
INSTDEPENDS += libbonobo2-dev
INSTDEPENDS += libpopt-dev
INSTDEPENDS += intltool
INSTDEPENDS += libgconf2-dev
INSTDEPENDS += libcanberra-dev
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += cdbs
INSTDEPENDS += gnome-pkg-tools
INSTDEPENDS += dpkg-dev
INSTDEPENDS += gtk-doc-tools
INSTDEPENDS += gnome-common
INSTDEPENDS += docbook-xml
#Build-Depends-Indep:
INSTDEPENDS += libglib2.0-doc
