Source: charls
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Mathieu Malaterre <malat@debian.org>,
           Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper (>= 9),
               dpkg-dev (>= 1.18.0),
               cmake
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/cgit/debian-med/charls.git
Vcs-Git: https://anonscm.debian.org/git/debian-med/charls.git
Homepage: https://github.com/team-charls/charls

Package: libcharls-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libcharls1 (= ${binary:Version}),
         ${misc:Depends}
Description: Implementation of the JPEG-LS standard (development libraries)
 CharLS is an optimized implementation of the JPEG-LS standard for lossless and
 near-lossless image compression
 .
 JPEG-LS (ISO-14495-1/ITU-T.87) is a standard derived from the Hewlett Packard
 LOCO algorithm. JPEG LS has low complexity (meaning fast compression) and high
 compression ratios, similar to JPEG 2000. JPEG-LS is more similar to the old
 Lossless JPEG than to JPEG 2000, but interestingly the two different techniques
 result in vastly different performance characteristics.
 .
 This package contains the development files.

Package: libcharls1
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Implementation of the JPEG-LS standard
 CharLS is an optimized implementation of the JPEG-LS standard for lossless and
 near-lossless image compression
 .
 JPEG-LS (ISO-14495-1/ITU-T.87) is a standard derived from the Hewlett Packard
 LOCO algorithm. JPEG LS has low complexity (meaning fast compression) and high
 compression ratios, similar to JPEG 2000. JPEG-LS is more similar to the old
 Lossless JPEG than to JPEG 2000, but interestingly the two different techniques
 result in vastly different performance characteristics.
