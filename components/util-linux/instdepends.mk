INSTDEPENDS += debhelper
INSTDEPENDS += dh-exec
INSTDEPENDS += dpkg-dev
INSTDEPENDS += gettext
# libcap-ng-dev [linux-any] <!stage1>,
INSTDEPENDS += libncurses5-dev
INSTDEPENDS += libncursesw5-dev
# libpam0g-dev <!stage1>,
INSTDEPENDS += libpam0g-dev
#INSTDEPENDS += libpam-dev
# libselinux1-dev [linux-any],
# libsystemd-dev [linux-any] <!stage1>,
INSTDEPENDS += libtool
# libudev-dev [linux-any] <!stage1>,
INSTDEPENDS += pkg-config
INSTDEPENDS += po-debconf
# systemd [linux-any] <!stage1>,
INSTDEPENDS += zlib1g-dev
#
INSTDEPENDS += dh-autoreconf

#INSTDEPENDS += autoconf
#INSTDEPENDS += automake
#INSTDEPENDS += autopoint

INSTDEPENDS += libkstat-dev
