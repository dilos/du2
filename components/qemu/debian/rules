#!/usr/bin/make -f
SHELL = /bin/sh -e

# get DEB_VERSION
include /usr/share/dpkg/pkg-info.mk
# get DEB_HOST_ARCH DEB_HOST_ARCH_OS DEB_HOST_GNU_TYPE DEB_HOST_MULTIARCH DEB_BUILD_GNU_TYPE
include /usr/share/dpkg/architecture.mk
# get CFLAGS LDFLAGS etc
include /usr/share/dpkg/buildflags.mk

VENDOR := DilOS
export CTFCONVERT=/usr/bin/ctfconvert
export CTFMERGE=/usr/bin/ctfmerge
export VERSION=DEB_VERSION

# support parallel build using DEB_BUILD_OPTIONS=parallel=N
#ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
#  MAKEFLAGS += -j$(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
#endif

# verbose build
V ?= 1
export CC= gcc

# list of packages we're supposed to build
BUILD_PACKAGES = $(call dpkg_late_eval,BUILD_PACKAGES,dh_listpackages)

enable_system = enable
#enable_system = $(if $(filter qemu-system,${BUILD_PACKAGES}),enable,disable)
enable_linux_user = $(if $(filter qemu-user,${BUILD_PACKAGES}),enable,disable)

FIRMWAREPATH = /usr/share/qemu:/usr/share/seabios:/usr/lib/ipxe/qemu

CFLAGS += -fno-builtin
#CFLAGS += -fident -nodefaultlibs -Wall -Werror -fno-inline-functions
CFLAGS += -fident -nodefaultlibs -fno-inline-functions
CFLAGS += -fno-aggressive-loop-optimizations
CPPFLAGS += -DTARGET_PHYS_ADDR_BITS=64 -D__EXTENSIONS__ -D__dilos__
LDFLAGS += -nodefaultlibs -Wl,-zfatal-warnings -Wl,-zassert-deflib -lz -lvnd -lm -lc

# we add another set of configure options from debian/control
common_configure_opts = \
	--with-pkgversion="$(VENDOR) $(DEB_VERSION)" \
	--extra-cflags="$(CFLAGS) $(CPPFLAGS)" --extra-ldflags="$(LDFLAGS)" \
	--prefix=/usr \
	--sysconfdir=/etc \
	--disable-blobs \
	--disable-strip \
	--interp-prefix=/etc/qemu-binfmt/%M \
	--disable-bluez \
	--disable-brlapi \
	--disable-curl \
	--enable-debug \
	--disable-docs \
	--enable-kvm \
	--enable-kvm-pit \
	--enable-vnc-png \
	--disable-kvm-device-assignment \
	--disable-sdl \
	--disable-vnc-jpeg \
	--disable-vnc-sasl \
	--disable-vnc-tls \
	--enable-trace-backend=dtrace \
	--audio-card-list= \
	--audio-drv-list= \
	--cpu=x86_64

#	--localstatedir=/var 
#	--libdir=/usr/lib/$(DEB_HOST_MULTIARCH) 
#	--libexecdir=/usr/lib/qemu 
#	--firmwarepath=${FIRMWAREPATH} 
#	--localstatedir=/var 

# Cross compiling support
#ifneq ($(DEB_BUILD_GNU_TYPE), $(DEB_HOST_GNU_TYPE))
#common_configure_opts  += --cross-prefix=$(DEB_HOST_GNU_TYPE)-
#endif

ifeq (${enable_system},enable)

# list of system (softmmu) targets, from ./configure
system_targets = x86_64
#system_targets = \
# i386 x86_64 alpha aarch64 arm cris lm32 hppa m68k microblaze microblazeel \
# mips mipsel mips64 mips64el moxie nios2 or1k ppc ppcemb ppc64 riscv32 riscv64 \
# sh4 sh4eb sparc sparc64 s390x tricore xtensa xtensaeb unicore32

# qemu-system subpackages, from d/control
#sys_systems = arm mips ppc sparc x86
sys_systems = x86
systems = ${sys_systems} misc
sysarch_arm   = $(filter aarch64 arm,${system_targets})
sysarch_mips  = $(filter mips mipsel mips64 mips64el,${system_targets})
sysarch_ppc   = $(filter ppc ppc64 ppcemb,${system_targets})
sysarch_sparc = $(filter sparc sparc64,${system_targets})
sysarch_x86   = $(filter i386 x86_64,${system_targets})
sysarch_misc  = $(filter-out $(foreach s,${sys_systems},${sysarch_$s}),${system_targets})

else

systems =

endif # enable_system

ifeq (${enable_linux_user},enable)

# list of linux-user targets, from ./configure
user_targets = \
 i386 x86_64 alpha aarch64 aarch64_be arm armeb cris hppa m68k microblaze microblazeel \
 mips mipsel mips64 mips64el mipsn32 mipsn32el nios2 or1k \
 ppc ppc64 ppc64abi32 ppc64le riscv32 riscv64 sh4 sh4eb sparc sparc64 sparc32plus \
 s390x tilegx xtensa xtensaeb

endif	# enable_linux_user

b/configure-stamp: configure
	dh_testdir
	# system build
	rm -rf b/qemu; mkdir -p b/qemu
	cd b/qemu && \
	    ../../configure ${common_configure_opts} ||\
	 { echo ===== CONFIGURE FAILED ===; tail -n 50 config.log; exit 1; }
	sed -i 's,-O2,,g' b/qemu/config-host.mak
	touch $@

build: build-arch build-indep
build-arch: b/build-stamp
build-indep: b/build-stamp
b/build-stamp: b/configure-stamp
	dh_testdir

	# system and utils build
	$(MAKE) -C b/qemu V=${V}
#ifeq (${enable_system},enable)
#	dtc -o b/qemu/pc-bios/bamboo.dtb pc-bios/bamboo.dts
#endif

ifeq ($(enable_linux_user),enable)
	# user-static build
	# we use this invocation to build just the binaries
	$(MAKE) -C b/user-static V=${V} $(foreach t,${user_targets},subdir-${t}-linux-user)
endif
	touch $@

clean:	debian/control
	dh_testdir
	rm -rf b
	find scripts/ -name '*.pyc' -delete || :
	dh_clean

# define ${ai} variable to be one of -i (indep), -a (arch) or nothing (both)
ai :=
binary-indep: ai := -i
binary-indep: install
binary-arch: ai := -a
binary-arch: install
binary: install

define inst-system
	mkdir -p debian/qemu-system-$1/usr/share/man/man1 debian/qemu-system-$1/usr/bin
	for t in ${sysarch_$1}; do \
	    mv debian/tmp/usr/bin/qemu-system-$$t debian/qemu-system-$1/usr/bin/; \
	    echo ".so man1/qemu-system.1" > debian/qemu-system-$1/usr/share/man/man1/qemu-system-$$t.1; \
	done
	echo sysarch:$1=\
$(if $(wordlist 10,20,${sysarch_$1}),\
$(wordlist 1,8,${sysarch_$1})\$${Newline}   $(wordlist 9,20,${sysarch_$1}),\
${sysarch_$1}) \
> debian/qemu-system-$1.substvars
	echo sysprovides:$1=${addprefix qemu-system-,${filter-out $1,${sysarch_$1}}} | \
	  sed -e 's/ /, /g' -e 'y/_/-/' >> debian/qemu-system-$1.substvars
	dh_link -pqemu-system-$1 usr/share/doc/qemu-system-common usr/share/doc/qemu-system-$1/common

endef

install: b/build-stamp
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs -a

	# system and utils install
	$(MAKE) -C b/qemu DESTDIR=$(CURDIR)/debian/tmp INSTALL_BLOBS=1 install
	install -m 0555 debian/kvm.bin debian/tmp/usr/bin/kvm
	mkdir -p debian/tmp/usr/share/man/man1
	install -m 0555 debian/kvm.1 debian/tmp/usr/share/man/man1
#	qemu.so (MDB)

# install whole thing so --list-missing works right
	dh_install --list-missing
#	exit 1
# install the rest for arch/indep as needed
#	dh_installdocs ${ai} -Nqemu-user-binfmt
#	dh_installchangelogs ${ai} -Nqemu-user-binfmt
#	dh_installdocs ${ai} -pqemu-user-binfmt --link-doc=qemu-user
	dh_installman ${ai}
	dh_installudev ${ai}
	dh_link ${ai}
	dh_lintian ${ai}
	dh_strip ${ai}
	dh_compress ${ai}
	dh_fixperms ${ai}
	dh_shlibdeps ${ai}
	dh_installdeb ${ai}
	dh_gencontrol ${ai}
	dh_md5sums ${ai}
	dh_builddeb ${ai}

ifneq (,$(wildcard debian/control-in))
# only include rules for debian/control if debian/control-in is present
debian/control: debian/control-in debian/rules
	echo '# autogenerated file, please edit debian/control-in' > $@.tmp
	sed -e 's/^:$(shell echo ${VENDOR} | tr '[A-Z]' '[a-z]')://' \
		-e '/^:[a-z]*:/D' $< >> $@.tmp
	mv -f $@.tmp $@
	chmod -w $@
endif

get-orig-source:
	./debian/get-orig-source.sh ${DEB_VERSION}

.PHONY: build clean binary-indep binary-arch binary install get-orig-source

