INSTDEPENDS += debhelper
INSTDEPENDS += libvnd-dev
INSTDEPENDS += driver-kvm-dev
INSTDEPENDS += developer-dtrace
INSTDEPENDS += libcapstone-dev
INSTDEPENDS += texinfo
INSTDEPENDS += device-tree-compiler
INSTDEPENDS += python
INSTDEPENDS += libcurl4-gnutls-dev
INSTDEPENDS += libfdt-dev
INSTDEPENDS += gnutls-dev
INSTDEPENDS += libgtk-3-dev
INSTDEPENDS += libncursesw5-dev
INSTDEPENDS += libcacard-dev
INSTDEPENDS += libpixman-1-dev
INSTDEPENDS += libsasl2-dev
INSTDEPENDS += uuid-dev
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += libjpeg-dev
INSTDEPENDS += libpng-dev
