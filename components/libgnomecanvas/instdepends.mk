INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += docbook-xml
INSTDEPENDS += gettext
INSTDEPENDS += gnome-common
INSTDEPENDS += gnome-pkg-tools
INSTDEPENDS += gtk-doc-tools
INSTDEPENDS += intltool
INSTDEPENDS += libart-2.0-dev
INSTDEPENDS += libgail-dev
INSTDEPENDS += libglade2-dev
INSTDEPENDS += libgtk2.0-dev
INSTDEPENDS += xauth
#INSTDEPENDS += xvfb [fixme]
# Build-Depends-Indep:
INSTDEPENDS += libglib2.0-doc
INSTDEPENDS += libgtk2.0-doc
