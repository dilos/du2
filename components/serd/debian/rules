#!/usr/bin/make -f

upstream_version ?= $(shell dpkg-parsechangelog | sed -rne 's/^Version: ([0-9.]+)(\+dfsg\d+)?.*$$/\1/p')
dfsg_version = $(upstream_version)~dfsg0
pkg = $(shell dpkg-parsechangelog | sed -ne 's/^Source: //p')

export DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow

#export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

export LINKFLAGS += $(LDFLAGS)

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

WAF = ./waf

ifeq ($(shell dpkg-query -s doxygen graphviz 1>/dev/null 2>&1 && echo yes),yes)
CONFIGURE_EXTRA_FLAGS += --docs
endif

%:
	dh $@

override_dh_auto_configure:
	$(WAF) configure \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--libdir=/usr/lib/$(DEB_HOST_MULTIARCH) \
		--strict \
		$(CONFIGURE_EXTRA_FLAGS)

override_dh_auto_build:
	$(WAF)

override_dh_auto_clean:
	$(WAF) clean || true
	find -name "*.pyc" -delete
	rm -rf build .waf*
	rm -rf .lock-*_build
	dh_auto_clean

override_dh_auto_install:
	$(WAF) install --destdir=$(CURDIR)/debian/tmp

override_dh_installchangelogs:
	dh_installchangelogs NEWS

# get-orig-source to drop waf
get-orig-source:
	uscan --noconf --force-download --rename --download-current-version --destdir=.
	tar -xf $(pkg)_$(upstream_version).orig.tar.bz2
	mv $(pkg)-$(upstream_version) $(pkg)-$(dfsg_version)
	cd $(pkg)-$(dfsg_version) ; python waf --help > /dev/null
	mv $(pkg)-$(dfsg_version)/.waf-*/* $(pkg)-$(dfsg_version)
	sed -i '/^#==>$$/,$$d' $(pkg)-$(dfsg_version)/waf
	rmdir $(pkg)-$(dfsg_version)/.waf-*
	find $(pkg)-$(dfsg_version)/ -name '*.pyc' -delete
	tar cf $(pkg)_$(dfsg_version).orig.tar $(pkg)-$(dfsg_version)
	xz -9fz $(pkg)_$(dfsg_version).orig.tar
	rm -rf $(pkg)-$(dfsg_version)
	mv $(pkg)_$(dfsg_version).orig.tar.xz ../$(pkg)_$(dfsg_version).orig.tar.xz
	mv $(pkg)_$(upstream_version).orig.tar.bz2 ../$(pkg)_$(upstream_version).orig.tar.bz2
