Description: Handle first day of week and first week of year through locale
             or command line setting.
Author: Michael Meskes <meskes@debian.org>

diff -ru bsdmainutils/usr.bin/ncal.orig/calendar.c bsdmainutils/usr.bin/ncal/calendar.c
--- bsdmainutils/usr.bin/ncal.orig/calendar.c	2016-03-09 14:48:35.918752258 +0100
+++ bsdmainutils/usr.bin/ncal/calendar.c	2016-03-09 14:48:38.270788312 +0100
@@ -25,8 +25,10 @@
  */
 
 #include <sys/cdefs.h>
+#include <langinfo.h>
 
 #include "calendar.h"
+extern int weekstart;
 
 #ifndef NULL
 #define NULL 0
@@ -257,11 +259,12 @@
 	nd = ndaysgi(&idt);
 	/*
 	 * If more than 3 days of this week are in the preceding year, the
-	 * next week is week 1 (and the next monday is the answer),
-	 * otherwise this week is week 1 and the last monday is the
+	 * next week is week 1 (and the next sunday/monday is the answer),
+	 * otherwise this week is week 1 and the last sunday/monday is the
 	 * answer.
 	 */
-	if ((wd = weekday(nd)) > 3)
+	/* 3 may or may not be correct, better use what the locale says */
+	if ((wd = weekday(nd) + 1 - weekstart) >= *nl_langinfo(_NL_TIME_WEEK_1STWEEK))
 		return (nd - wd + 7);
 	else
 		return (nd - wd);
diff -ru bsdmainutils/usr.bin/ncal.orig/ncal.1 bsdmainutils/usr.bin/ncal/ncal.1
--- bsdmainutils/usr.bin/ncal.orig/ncal.1	2016-03-09 14:48:35.918752258 +0100
+++ bsdmainutils/usr.bin/ncal/ncal.1	2016-03-09 14:53:30.835534083 +0100
@@ -47,7 +47,7 @@
 .Fl m Ar month
 .Op Ar year
 .Nm ncal
-.Op Fl 3hjJpwy
+.Op Fl 3hjJpwySM
 .Op Fl A Ar number
 .Op Fl B Ar number
 .Op Fl s Ar country_code
@@ -56,7 +56,7 @@
 .Ar year
 .Oc
 .Nm ncal
-.Op Fl 3hJeo
+.Op Fl 3hJeoSM
 .Op Fl A Ar number
 .Op Fl B Ar number
 .Op Ar year
@@ -147,6 +147,10 @@
 Use
 .Ar yyyy-mm-dd
 as the current date (for debugging of highlighting).
+.It Fl M
+Weeks start on Monday.
+.It Fl S
+Weeks start on Sunday.
 .El
 .Pp
 A single parameter specifies the year (1\(en9999) to be displayed;
diff -ru bsdmainutils/usr.bin/ncal.orig/ncal.c bsdmainutils/usr.bin/ncal/ncal.c
--- bsdmainutils/usr.bin/ncal.orig/ncal.c	2016-03-09 14:48:35.918752258 +0100
+++ bsdmainutils/usr.bin/ncal/ncal.c	2016-03-09 14:52:55.998946067 +0100
@@ -165,6 +165,7 @@
 int     nswitch;		/* user defined switch date */
 int	nswitchb;		/* switch date for backward compatibility */
 int	highlightdate;
+int	weekstart = -1;		/* day the week starts on (Sun [0] - Sat [6]) */
 
 char	*center(char *s, char *t, int w);
 wchar_t *wcenter(wchar_t *s, wchar_t *t, int w);
@@ -255,7 +256,7 @@
 
 	before = after = -1;
 
-	while ((ch = getopt(argc, argv, "3A:B:Cd:eH:hjJm:Nops:wy")) != -1)
+	while ((ch = getopt(argc, argv, "3A:B:Cd:eH:hjJm:Nops:wySM")) != -1)
 		switch (ch) {
 		case '3':
 			flag_3months = 1;
@@ -344,6 +345,16 @@
 		case 'y':
 			flag_wholeyear = 1;
 			break;
+		case 'S':
+			if (flag_backward)
+				usage();
+			weekstart = 0;
+			break;
+		case 'M':
+			if (flag_backward)
+				usage();
+			weekstart = 1;
+			break;
 		default:
 			usage();
 		}
@@ -384,6 +395,27 @@
 		usage();
 	}
 
+	/* Technically not correct, but removes the need to add 1 later on */
+	if (flag_backward) 
+		weekstart = (weekstart == -1) ? 1 : weekstart + 1;
+	/* Determine on what day the week starts. */
+#ifdef __GLIBC__
+	else if (weekstart == -1)
+	{
+		int first_week_i;
+		date first_week_d;
+		date sunday = { .y = 1997, .m = 11, .d = 30 };
+
+		first_week_i = (intptr_t) nl_langinfo(_NL_TIME_WEEK_1STDAY);
+		first_week_d.d = first_week_i % 100;
+		first_week_i /= 100;
+		first_week_d.m = first_week_i % 100;
+		first_week_i /= 100;
+		first_week_d.y = first_week_i;
+		weekstart = *nl_langinfo(_NL_TIME_FIRST_WEEKDAY) + (ndaysj(&first_week_d) - ndaysj(&sunday)) % 7 - 1;
+	}
+#endif
+
 	if (flag_month != NULL) {
 		if (parsemonth(flag_month, &m, &y)) {
 			errx(EX_USAGE,
@@ -504,8 +536,8 @@
 	fputs(
 "Usage: cal [general options] [-hjy] [[month] year]\n"
 "       cal [general options] [-hj] [-m month] [year]\n"
-"       ncal [general options] [-hJjpwy] [-s country_code] [[month] year]\n"
-"       ncal [general options] [-hJeo] [year]\n"
+"       ncal [general options] [-hJjpwySM] [-s country_code] [[month] year]\n"
+"       ncal [general options] [-hJeoSM] [year]\n"
 "General options: [-NC3] [-A months] [-B months]\n"
 "For debug the highlighting: [-H yyyy-mm-dd] [-d yyyy-mm]\n",
 	    stderr);
@@ -541,7 +573,13 @@
 	struct tm tm;
 	char    buf[MAX_WIDTH];
 #ifndef D_MD_ORDER
-	static int d_first = 1; /* XXX */
+	static int d_first = -1; /* XXX */
+	char * str = nl_langinfo(D_FMT);
+
+	if (d_first < 0) {
+		for (; *str && *str != 'd' && *str != 'm'; str++);
+		d_first = (*str == 'm') ? 0 : 1;
+	}
 #else
 	static int d_first = -1;
 
@@ -799,10 +837,13 @@
 		jan1 = firstday(y, 1);
 
 	/*
-	 * Set firstm to the day number of monday of the first week of
+	 * Set firstm to the day number of the day starting the first week of
 	 * this month. (This might be in the last month)
 	 */
-	firstm = first - weekday(first);
+	if (weekstart == 0)
+		firstm = first - (weekday(first) + 1) % 7;
+	else
+		firstm = first - weekday(first);
 
 	/* Set ds (daystring) and dw (daywidth) according to the jd_flag. */
 	if (jd_flag) {
@@ -958,7 +999,7 @@
 	memset(&tm, 0, sizeof(tm));
 
 	for (i = 0; i != 7; i++) {
-		tm.tm_wday = (i+1) % 7;
+		tm.tm_wday = (i+weekstart) % 7;
 		wcsftime(buf, sizeof(buf), L"%a", &tm);
 		for (len = 2; len > 0; --len) {
 			if ((width = wcswidth(buf, len)) <= 2)
