INSTDEPENDS += debhelper
INSTDEPENDS += gnome-pkg-tools
INSTDEPENDS += gtk-doc-tools
INSTDEPENDS += intltool
INSTDEPENDS += xsltproc
INSTDEPENDS += valac
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libxml2-dev
# Build-Depends-Indep:
INSTDEPENDS += libglib2.0-doc
