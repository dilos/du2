#!/bin/sh

# Verify that third-party main loop integration, as provided by Qt
# and E, can be built against this dbus-python. We simulate a third-party
# main loop by copying the one for dbus-glib from this source tree.

exec 2>&1
set -e
set -x

test_build () {
	PYTHON="$1"

	mkdir "$ADTTMP/$PYTHON"
	# provide some cunningly disguised main-loop glue
	sed \
		-e 's/dbus_glib/dbus_test/g' \
		< _dbus_glib_bindings/module.c \
		> "$ADTTMP/$PYTHON/module.c"

	( cd "$ADTTMP/$PYTHON" && ${CC:-cc} \
		-Wall -Wextra -Wno-error -fPIC -shared \
		-o _dbus_test_bindings$(${PYTHON}-config --extension-suffix) \
		module.c \
		$(${PYTHON}-config --cflags --libs) \
		$(pkg-config --cflags --libs dbus-python dbus-glib-1) )

	PYTHONPATH="$ADTTMP/$PYTHON" $PYTHON -c \
		"from _dbus_test_bindings import DBusGMainLoop"
}

for p in python $(pyversions -s); do
	test_build "$p"
	test_build "$p-dbg"
done
