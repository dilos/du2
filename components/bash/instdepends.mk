INSTDEPENDS += debhelper
INSTDEPENDS += autoconf
INSTDEPENDS += autotools-dev
INSTDEPENDS += bison
INSTDEPENDS += libncurses5-dev
INSTDEPENDS += texinfo
INSTDEPENDS += texi2html
# locales
INSTDEPENDS += gettext
INSTDEPENDS += sharutils
INSTDEPENDS += time
INSTDEPENDS += xz-utils
INSTDEPENDS += dpkg-dev
#Build-Depends-Indep:
INSTDEPENDS += texlive-latex-base
INSTDEPENDS += ghostscript
INSTDEPENDS += texlive-fonts-recommended

# buster
#INSTDEPENDS += locales
INSTDEPENDS += man2html
