libsass (3.4.3-1+dilos2) unstable; urgency=medium

  * build for dilos

 -- Igor Kozhukhov <igor@dilos.org>  Sat, 22 Sep 2018 13:25:33 +0300

libsass (3.4.3-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Modernize Vcs-Browse field: Use git subdir.
  * Stop override lintian for
    package-needs-versioned-debhelper-build-depends: Fixed in lintian.
  * Update copyright info: Extend coverage of Debian packaging.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 21 Jan 2017 20:12:01 +0100

libsass (3.4.0-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Update homepage URL.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 11 Dec 2016 11:32:01 +0100

libsass (3.3.6-2) unstable; urgency=medium

  * Add autopkgtest.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 09 Sep 2016 11:28:10 +0200

libsass (3.3.6-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Fix avoid call uupdate in watch file (counterproductive with
    git-buildpackage).
  * Update watch file:
    + Use substitution string @PACKAGE@.
    + Add usage comment.
  * Modernize git-buildpackage config: Filter any .git* file.
  * Declare compliance with Debian Policy 3.9.8.
  * Update TODO: Testsuite never disabled.
  * Build-depend on licensecheck (not devscripts).

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 09 Sep 2016 03:12:39 +0200

libsass (3.3.4-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    Closes: Bug#818896. Thanks to Frederic Bonnard.

  [ Jonas Smedegaard ]
  * Drop CDBS get-orig-source target: Use "gbp import-orig --uscan"
    instead.
  * Update watch file:
    + Bump file format to version 4.
    + Use github pattern from ducmentation.
  * Update copyright info:
    + Use License-Grant for GPL with exception.
    + Drop unneeded disclaimers.
    + Use license shortname FSFUL~Boost.
    + Change main copyright holder.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 21 Mar 2016 22:09:20 +0100

libsass (3.3.3-1) unstable; urgency=medium

  [ upstream ]
  * New upstream release.
    Thanks to Raphaël (see bug#694733).

  [ Jonas Smedegaard ]
  * Declare compliance with Debian Policy 3.9.7.
  * Modernize Vcs-Git field: Use https protocol.
  * Update copyright info:
    + Extend copyright for main upstream author to cover recent years.
    + Extend copyright of packaging to cover current year.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 08 Mar 2016 01:18:47 +0100

libsass (3.3.2-1) unstable; urgency=medium

  [ upstream ]
  * New upstream release(s).

  [ Jonas Smedegaard ]
  * Update copyright info:
    + Fix typo in License shortname.
    + Cover newly added files.
  * Drop patch 1001: No longer applies.
  * Tidy lintian overrides.
  * Add lintian override regarding debhelper 9.
  * Copy unusually named Makefile.am to help CDBS notice it.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 30 Nov 2015 20:26:27 +0100

libsass (3.2.5-2) unstable; urgency=medium

  * Fix Vcs-* URLs.
  * Modernize git-buildpackage config: Avoid git- prefix.
  * Add patch 1001 to include header file sass_interface.h.
  * Update copyright info: Fix use License-Reference field (not
    pseudo-field within License field).
  * Git-ignore quilt .pc dir.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 30 Nov 2015 19:21:42 +0100

libsass (3.2.5-1) unstable; urgency=medium

  * Update copyright info:
    + Fix use SPDX license shortname BSL.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Fix reference GPL2 license (as pseudo-field: Yet another
      limitation of format 1.0).
  * Add lintian overrides regarding license in License-Reference field.
    See bug#786450.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 20 Jun 2015 13:09:53 -0500

libsass (3.2.4-2) unstable; urgency=medium

  * Bump debhelper compatibility level to 9.
  * Fix set upstream version.
  * Modernize git-buildpackage config: Drop "git-" prefix.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 13 May 2015 23:37:57 +0200

libsass (3.2.4-1) unstable; urgency=medium

  [ upstream ]
  * New upstream release(s).

  [ Jonas Smedegaard ]
  * Update Vcs-Browser URL to use cgit web frontend.
  * Update copyright info: Drop unused GAP license section.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 13 May 2015 23:04:33 +0200

libsass (3.1.0-1) unstable; urgency=low

  * Initial release.
    Closes: bug#694730.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 03 Mar 2015 03:56:22 +0100
