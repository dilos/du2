Source: libsass
Section: libs
Priority: optional
Build-Depends: @cdbs@
Maintainer: Debian Sass team <pkg-sass-devel@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>
Standards-Version: 3.9.8
Homepage: http://sass-lang.com/libsass
Vcs-Git: https://anonscm.debian.org/git/pkg-sass/libsass.git
Vcs-Browser: https://anonscm.debian.org/git/pkg-sass/libsass.git

Package: libsass0
Section: libs
Architecture: any
Depends: ${shlibs:Depends},
 ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends},
 ${cdbs:Pre-Depends}
Multi-Arch: same
Description: C/C++ port of the Sass CSS precompiler
 Sass is a pre-processing language for CSS. It allows you to write
 cleaner stylesheets and makes collaboration on your CSS a breeze.
 .
 LibSass is a C/C++ port of the Sass engine. The point is to be simple,
 fast, and easy to integrate.

Package: libsass0-dbg
Section: debug
Priority: extra
Architecture: fixme-any
Depends: ${misc:Depends},
 libsass0 (= ${binary:Version})
Description: C/C++ port of the Sass CSS precompiler - debugging symbols
 Sass is a pre-processing language for CSS. It allows you to write
 cleaner stylesheets and makes collaboration on your CSS a breeze.
 .
 LibSass is a C/C++ port of the Sass engine. The point is to be simple,
 fast, and easy to integrate.
 .
 This package contains the debugging symbols.

Package: libsass-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libsass0 (= ${binary:Version}),
 ${devlibs:Depends},
 ${misc:Depends}
Suggests: libsass-doc
Description: C/C++ port of the Sass CSS precompiler - development headers
 Sass is a pre-processing language for CSS. It allows you to write
 cleaner stylesheets and makes collaboration on your CSS a breeze.
 .
 LibSass is a C/C++ port of the Sass engine. The point is to be simple,
 fast, and easy to integrate.
 .
 This package provides header files for developing your applications to
 use libsass template engine.
