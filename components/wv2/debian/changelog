wv2 (0.4.2.dfsg.2-2debian9.0.0+0+dilos1) unstable; urgency=low

  * Build for DilOS.

 -- DilOS Team <dilos@dilos.org>  Sat, 02 Mar 2019 21:43:08 +0300

wv2 (0.4.2.dfsg.2-2debian9.0.0+0) stretch; urgency=low

  * Run "make test" directly to avoid differences between when dh_auto_test
    thinks the testsuite should be run and when we do.  (Closes: #718141)
  * Enable LFS so that we can read files of CIFS shares.
  * debian/rules: Simplify - dh compat 9 handles passing CPPFLAGS under
    cmake.
  * Support parsing end notes (new patch support-endnotes.patch from upstream
    SVN).

 -- Olly Betts <olly@survex.com>  Thu, 29 Aug 2013 10:38:30 +1200

wv2 (0.4.2.dfsg.2-1) unstable; urgency=low

  * Repack to remove src/generator/generator_wword{6,8}.htm, which are
    based on documents from Microsoft.  These two files were documented as
    removed in README.Debian, but actually still present.
  * Improve wording in README.Debian.
  * "Standards-Version: 3.9.4": No changes required.

 -- Olly Betts <olly@survex.com>  Thu, 30 May 2013 11:21:57 +0000

wv2 (0.4.2.dfsg.1-10) unstable; urgency=low

  * Fix to use -I options from libgsf's pkg-config so libxml headers are
    found.  (Closes: #707417)
  * Honour DEB_BUILD_OPTIONS nocheck in debian/rules.  (Closes: #685920)
  * Fix double "-l" in output from wv2-config --libs.  (LP: #1017413)

 -- Olly Betts <olly@survex.com>  Fri, 10 May 2013 12:06:41 +1200

wv2 (0.4.2.dfsg.1-9.1) unstable; urgency=low

  * Non-maintainer upload.
  * [SECURITY] Fix "Buffer overflow":
    add patch buffer-overflow.patch, taken from calligra git.
    (Closes: #684078)

 -- gregor herrmann <gregoa@debian.org>  Sun, 26 Aug 2012 15:20:51 +0200

wv2 (0.4.2.dfsg.1-9) unstable; urgency=low

  * Apply hardening to CFLAGS too (patch from Simon Ruderich in private mail).
  * Fix compatibility with glib 2.32 (Closes: #665626)

 -- Olly Betts <olly@survex.com>  Sun, 25 Mar 2012 04:04:44 +0000

wv2 (0.4.2.dfsg.1-8) unstable; urgency=low

  * "Standards-Version: 3.9.3": No changes required.
  * Ensure hardening LDFLAGS are used by updating to debhelper compat level 9.
    (Closes: #662009)

 -- Olly Betts <olly@survex.com>  Fri, 23 Mar 2012 09:30:09 +0000

wv2 (0.4.2.dfsg.1-7) unstable; urgency=low

  * The mips/mipsel issue appears to be due to the wv2 code assuming it knows
    the bit pattern for NaN and Inf, so revert the change to disable
    optimisation (which didn't actually work anyway) and add new patch
    fix-nan-and-inf-for-mips.patch.

 -- Olly Betts <olly@survex.com>  Thu, 05 Jan 2012 05:14:37 +0000

wv2 (0.4.2.dfsg.1-6) unstable; urgency=low

  * Enabling hardening build flags also turned on optimisation, which results
    in a bus error in handlertest on mips and mipsel.  The backtrace is useless
    so just disable optimisation again for these two archs.

 -- Olly Betts <olly@survex.com>  Sun, 01 Jan 2012 11:15:19 +0000

wv2 (0.4.2.dfsg.1-5) unstable; urgency=low

  * Enable hardened build flags.

 -- Olly Betts <olly@survex.com>  Wed, 28 Dec 2011 23:24:21 +0000

wv2 (0.4.2.dfsg.1-4) unstable; urgency=low

  * Fix avoid-writing-after-structures.patch to actually work.

 -- Olly Betts <olly@survex.com>  Thu, 01 Dec 2011 05:12:36 +0000

wv2 (0.4.2.dfsg.1-3) unstable; urgency=low

  * New patch avoid-writing-after-structures.patch to fix word95test and
    word97test not to write after the end of word-aligned structures, fixing
    FTBFS on sparc.

 -- Olly Betts <olly@survex.com>  Wed, 30 Nov 2011 14:23:07 +0000

wv2 (0.4.2.dfsg.1-2) unstable; urgency=low

  * New maintainer.

 -- Olly Betts <olly@survex.com>  Wed, 30 Nov 2011 13:01:02 +0000

wv2 (0.4.2.dfsg.1-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Add patch fix-ftbfs-with-gold.patch to fix FTBFS with binutils-gold.
    (Closes: #556686)
  * Add patch fix-handling-empty-associatedstrings.patch to fix handling of
    .doc files with an empty associated strings section. (Closes: #603868)
  * Build with debugging messages disabled. (Closes: #603871)
  * Drop leading article from short description.
  * Actually run the testsuite, don't just build it (needs new patch
    fix-tests.patch)
  * "Standards-Version: 3.9.2":
    + Include the BSD licence in debian/copyright rather than referring to
      /usr/share/common-licenses/BSD.

 -- Olly Betts <olly@survex.com>  Wed, 30 Nov 2011 12:04:22 +0000

wv2 (0.4.2.dfsg.1-1) unstable; urgency=low

  * New upstream release.
  * Switch to 3.0 (quilt) source format. No changes required further adding
    debian/source/format.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Sat, 14 Nov 2009 00:28:32 +0100

wv2 (0.4.1.dfsg-1) unstable; urgency=low

  * New upstream release. "Another release, another bump soname".
  * Update from upstream to build with GCC 4.4. (Closes: #548708)
  * Bump soname fom libwv2-3 to libwv2-4, make changes accordingly.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Sat, 10 Oct 2009 18:10:30 +0200

wv2 (0.4.0.dfsg-1) unstable; urgency=low

  * New upstream release.

  +++ Changes by Pino Toscano:

  * Bump SONAME.
  * Switch to dh, and bump debhelper requirements & compatibility to 7.
  * wv2's build system is cmake now, so build-depend on it.
  * Add dependency on ${misc:Depends}, given debhelper is used.
  * Bump Standards-Version to 3.8.3, no changes required.
  * Remove duplicate section from binary libwv2-3 in control.
  * Update copyright.

 -- Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>  Fri, 18 Sep 2009 17:59:51 +0200

wv2 (0.3.1.dfsg-1) unstable; urgency=low

  * Repack tarball and remove unlicesed documentation. Document this in
    Debian.README.
  * Update debian/copyright.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Fri, 07 Aug 2009 16:32:59 +0200

wv2 (0.3.1-1) unstable; urgency=low

  * New upstream release.
  * Remove patches: 
    - gcc_4.3_fixes, merged upstream. 
    - 05_relibtoolize.dpatch, 50_autogen.dpatch, 10_wv2-config_static.dpatch,
      obsolete.
    - remove quilt patching stuff.
  * Remove deprecated libstdc++-dev, it is being updated in build-essential.
  * Rename libwv2-1c2 -> libwv2-2.
  * Update to debhelper 6. 
  * Add Homepage field.
  * Update debian/copyright.
  * Update to Standards-Version: 3.8.2, no changes required.
  * Update uploaders, remove Isaac.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Fri, 07 Aug 2009 00:31:33 +0200

wv2 (0.2.3-2) unstable; urgency=low

  * Update Standards-Version to 3.7.3.
  * Update uploaders.
  * Move patch system to quilt.
  * Add gcc_4.3_fixes to fix problems with GCC 4.3. This patch adds a missing
    include and hack away the #warning stuff.  (Closes: #441614)
  * Replace  ${source:Version}) with (= ${binary:Version}) to make package 
    binNMUable.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Wed, 26 Dec 2007 08:32:36 +0100

wv2 (0.2.3-1) unstable; urgency=low

  * New upstream release.

  +++ Changes by Christopher Martin:

  * Drop the libwv2.la file from libwv2-dev, at the request of
    J.H.M. Dassen (Ray). (Closes: #374332)

 -- Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>  Sun, 18 Jun 2006 15:28:58 -0400

wv2 (0.2.2-6) unstable; urgency=medium

  +++ Changes by Christopher Martin:

  * Add a patch which fixes CVE-2006-2197, missing boundary checks which could
    allow the execution of arbitrary code. Urgency medium.

  * Revamp patch system; should make further updates easier.

  * Set Uploaders to the standard Qt/KDE team list.

  +++ Changes by Pierre Habouzit:

  * Add -DNDEBUG in the CFLAGS (no more horribly verbose debug output on
    stderr; see wvlog.h). (Closes: #329109)

 -- Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>  Sat, 17 Jun 2006 19:00:35 -0400

wv2 (0.2.2-5) unstable; urgency=low

  * wv2-config: introduce a "--static" option to provide options suitable
    for static linking and have "--libs" output just "-lwv2", closes: #342889
  * Update libtool (required to fix FTBFS in GNU/k*BSD), closes: #311988

 -- Isaac Clerencia <isaac@debian.org>  Mon, 12 Dec 2005 08:36:00 +0100

wv2 (0.2.2-4) unstable; urgency=low

  +++ Changes by Isaac Clerencia:

  * Rebuild against new libgsf-1-dev, making libwv2-1c2 installable in sid
    again, closes: #338712

 -- Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>  Sat, 12 Nov 2005 11:27:27 +0100

wv2 (0.2.2-3) unstable; urgency=low

  * Changed maintainer to the Debian Qt/KDE group (in coordination with
    koffice, which has also changed maintainer to the Debian Qt/KDE group).

 -- Ben Burton <bab@debian.org>  Sun, 16 Oct 2005 02:07:54 +1000

wv2 (0.2.2-2) unstable; urgency=low

  * C++ ABI transition (g++-3.3 -> g++-4.0).
  * Library package renamed from libwv2-1 to libwv2-1c2.
  * Build-depends on c++abi2-dev, since the binaries do not depend on libstdc++.
  * Made libwv2-dev depend on libstdc++-dev since it imports libstdc++ headers.
  * Removed -pedantic from compilation flags to avoid HUGE_VAL error (#319553).
  * Bumped standards-version to 3.6.2.1 (no changes required).

 -- Ben Burton <bab@debian.org>  Sat, 23 Jul 2005 10:20:38 +1000

wv2 (0.2.2-1) unstable; urgency=low

  * New upstream bugfix release.
  * Builds under gcc 3.4 (closes: #274051).

 -- Ben Burton <bab@debian.org>  Thu, 14 Oct 2004 19:08:15 +1000

wv2 (0.2.1-2) unstable; urgency=low

  * Build-depends on zlib1g-dev, which no longer seems to be provided by
    libxml2-dev (closes: #242971).

 -- Ben Burton <bab@debian.org>  Sun, 11 Apr 2004 07:36:57 +1000

wv2 (0.2.1-1) unstable; urgency=low

  * New upstream release.

 -- Ben Burton <bab@debian.org>  Sun,  7 Dec 2003 20:37:52 +1100

wv2 (0.1.9-1) unstable; urgency=low

  * New upstream release.
  * Bumped standards-version to 3.6.1.

 -- Ben Burton <bab@debian.org>  Mon, 29 Sep 2003 10:57:45 +1000

wv2 (0.1.8-1) unstable; urgency=low

  * New upstream release.
  * Bumped standards-version to 3.6.0.

 -- Ben Burton <bab@debian.org>  Fri,  8 Aug 2003 08:58:07 +1000

wv2 (0.0.9-1) unstable; urgency=low

  * Initial Release (closes: #196455).
  * Patched olestorage.cpp for type correctness with newer versions of
    libgfs-1.
  * Using AM_MAINTAINER_MODE.
  * Updated config.{sub,guess}.

 -- Ben Burton <bab@debian.org>  Sat,  7 Jun 2003 15:30:53 +1000

