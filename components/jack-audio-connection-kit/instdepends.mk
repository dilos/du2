INSTDEPENDS += debhelper
INSTDEPENDS += cdbs
INSTDEPENDS += libtool
INSTDEPENDS += automake1.11
INSTDEPENDS += autoconf
INSTDEPENDS += bash-completion
INSTDEPENDS += d-shlibs
INSTDEPENDS += doxygen
#INSTDEPENDS += libasound2-dev [linux-any]
#INSTDEPENDS += libffado-dev [linux-any]
#INSTDEPENDS += libraw1394-dev [linux-any]
#INSTDEPENDS += libzita-resampler-dev [linux-any]
#INSTDEPENDS += libzita-alsa-pcmi-dev [linux-any]
INSTDEPENDS += libreadline-dev
INSTDEPENDS += libsamplerate-dev
INSTDEPENDS += libsndfile1-dev
INSTDEPENDS += libtool
INSTDEPENDS += libdb-dev
INSTDEPENDS += uuid-dev
INSTDEPENDS += patchutils
INSTDEPENDS += po-debconf
