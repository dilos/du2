ruby-hpricot (0.8.6-6+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- DilOS Team <dilos@dilos.org>  Fri, 12 Apr 2019 11:35:06 +0300

ruby-hpricot (0.8.6-6) unstable; urgency=medium

  * Team upload.
  * Always remove unnecessary binary extension (Closes: #806191)
    - move its removal from debian/ruby-tests.rb, what is utterly wrong, from
      override_dh_auto_install in debian/fules

 -- Antonio Terceiro <terceiro@debian.org>  Tue, 12 Jan 2016 17:37:19 -0200

ruby-hpricot (0.8.6-5) unstable; urgency=medium

  * Team upload.
  * Disable tests that fail on armel, armhf, mipsel

 -- Christian Hofstaedtler <zeha@debian.org>  Wed, 15 Jan 2014 13:21:54 +0100

ruby-hpricot (0.8.6-4) unstable; urgency=low

  * Team upload.

  [ Cédric Boutillier ]
  * debian/control:
    - remove obsolete DM-Upload-Allowed flag
    - use canonical URI in Vcs-* fields
  * debian/copyright: use DEP5 copyright-format/1.0 official URL
    for Format field

  [ Christian Hofstaedtler ]
  * Remove transitional packages
  * Bump Standards-Version to 3.9.5 (no changes)
  * Update Build-Depends for ruby2.0, drop ruby1.8

  [ Jonas Genannt ]
  * d/ruby-hpricot.NEWS added NEWS file about hpricot EOL
  * d/ruby-tests.rb: force encoding to UTF-8, to make tests work

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Tue, 31 Dec 2013 11:59:31 +0100

ruby-hpricot (0.8.6-3) unstable; urgency=low

  * Stop shipping fast_xs and depend on it instead.  Closes: 679606

 -- Tollef Fog Heen <tfheen@debian.org>  Sat, 30 Jun 2012 20:58:04 +0200

ruby-hpricot (0.8.6-2) unstable; urgency=low

  * Bump build dependency on gem2deb to >= 0.3.0~

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Tue, 26 Jun 2012 10:21:21 +0200

ruby-hpricot (0.8.6-1) unstable; urgency=low

  * New upstream version
  * Ignore test results on ia64 for now. See #653582.

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Wed, 25 Jan 2012 22:45:51 +0100

ruby-hpricot (0.8.5-1) unstable; urgency=low

  * New upstream version
  * Priority of transitional packages set to extra
  * Override lintian warnings about duplicate description of transitional
    packages

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Wed, 30 Nov 2011 13:02:54 +0100

ruby-hpricot (0.8.4-1) unstable; urgency=low

  * New upstream release
  * Migrated to gem2deb packaging
    + rename binary and source packages
  * Add myself to uploaders

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Fri, 19 Aug 2011 16:56:30 +0200

libhpricot-ruby (0.8.2-1) unstable; urgency=low

  * New upstream release
  * switch to ruby 1.9.1 (Closes: #565837)
  * update email address

 -- Ryan Niebur <ryan@debian.org>  Fri, 29 Jan 2010 23:53:30 -0800

libhpricot-ruby (0.8.1-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version (no changes)
  * Add myself to uploaders

 -- Micah Anderson <micah@debian.org>  Wed, 19 Aug 2009 17:09:56 -0400

libhpricot-ruby (0.8-2) unstable; urgency=low

  * build ext/fast_xs too (Closes: #532949)

 -- Ryan Niebur <ryanryan52@gmail.com>  Fri, 12 Jun 2009 22:55:53 -0700

libhpricot-ruby (0.8-1) unstable; urgency=low

  * fix watch file
  * New upstream release
  * remove patching, it's fixed upstream
  * take over package with permission from Ari
  * update standards version to 3.8.1
  * add DMUA field
  * improve short descriptions
  * add ${misc:Depends}
  * fix d/copyright
  * update Homepage
  * remove debian/*.dirs, they are not needed

 -- Ryan Niebur <ryanryan52@gmail.com>  Mon, 08 Jun 2009 00:15:45 -0700

libhpricot-ruby (0.6-2) unstable; urgency=low

  * Use new Homepage dpkg header.
  * Bump build-dep on r-p-t to >= 0.14, fixes the ruby1.9 install
    problem.
  * Install to /usr/lib/ruby/1.9.0, not /usr/lib/ruby/1.9.
    Closes: #484615.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Mon, 16 Jun 2008 15:17:57 +0200

libhpricot-ruby (0.6-1) unstable; urgency=low

  * New upstream version
  * Fix FTBFS with new ruby (Closes: #427463)

 -- Ari Pollak <ari@debian.org>  Thu, 21 Jun 2007 11:40:50 -0400

libhpricot-ruby (0.5-2) unstable; urgency=low

  * Upload to unstable
  * Remove pkg-ruby-extras Uploaders rule as part of migration

 -- Ari Pollak <ari@debian.org>  Wed, 25 Apr 2007 11:05:01 -0400

libhpricot-ruby (0.5-1) experimental; urgency=low

  * New upstream version (Closes: #410458)
  * Update watch file to only track stable versions

 -- Ari Pollak <ari@debian.org>  Mon, 20 Nov 2006 15:33:32 -0500

libhpricot-ruby (0.4-1) unstable; urgency=low

  * Initial release (Closes: #391329)
  * Added note to debian/copyright about license inconsistency

 -- Ari Pollak <ari@debian.org>  Sat, 21 Oct 2006 19:22:48 -0400

