Source: patchutils
Section: text
Priority: optional
Maintainer: Laszlo Boszormenyi (GCS) <gcs@debian.org>
# dh-autoreconf because we patch Makefile.am
# rpm includes gendiff
Build-Depends: debhelper (>= 9), xmlto, dh-autoreconf, gawk
Standards-Version: 3.9.8
Vcs-Git: git://git.debian.org/collab-maint/patchutils.git
Vcs-Browser: https://alioth.debian.org/anonscm/git/collab-maint/patchutils.git
Homepage: http://cyberelk.net/tim/patchutils/index.html

Package: patchutils
Architecture: any
Depends: ${shlibs:Depends}, ${perl:Depends}, ${misc:Depends}, patch, debianutils (>= 1.16)
Multi-Arch: foreign
Description: Utilities to work with patches
 This package includes the following utilities:
  - combinediff creates a cumulative patch from two incremental patches
  - dehtmldiff extracts a diff from an HTML page
  - filterdiff extracts or excludes diffs from a diff file
  - fixcvsdiff fixes diff files created by CVS that "patch" mis-interprets
  - flipdiff exchanges the order of two patches
  - grepdiff shows which files are modified by a patch matching a regex
  - interdiff shows differences between two unified diff files
  - lsdiff shows which files are modified by a patch
  - recountdiff recomputes counts and offsets in unified context diffs
  - rediff and editdiff fix offsets and counts of a hand-edited diff
  - splitdiff separates out incremental patches
  - unwrapdiff demangles patches that have been word-wrapped
