INSTDEPENDS += debhelper
INSTDEPENDS += nettle-dev
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += libtasn1-6-dev
INSTDEPENDS += autotools-dev
# datefudge
INSTDEPENDS += dpkg-dev
INSTDEPENDS += libp11-kit-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += chrpath
INSTDEPENDS += libidn11-dev
INSTDEPENDS += autogen
INSTDEPENDS += bison
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += libcmocka-dev
INSTDEPENDS += libgmp-dev
INSTDEPENDS += libopts25-dev
INSTDEPENDS += automake
INSTDEPENDS += libunbound-dev
INSTDEPENDS += libssl-dev
# openssl <!nocheck>, softhsm2 <!nocheck>
#Build-Depends-Indep:
INSTDEPENDS += gtk-doc-tools
INSTDEPENDS += texinfo
