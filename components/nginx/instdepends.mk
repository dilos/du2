INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
INSTDEPENDS += po-debconf
INSTDEPENDS += dpkg-dev
INSTDEPENDS += libexpat-dev
INSTDEPENDS += libgd-dev
INSTDEPENDS += libgeoip-dev
INSTDEPENDS += libhiredis-dev
INSTDEPENDS += liblua5.1-0-dev
# [!i386 !amd64 !kfreebsd-i386 !armel !armhf !powerpc !powerpcspe !mips !mipsel],
#INSTDEPENDS += libluajit-5.1-dev
# [i386 amd64 kfreebsd-i386 armel armhf powerpc powerpcspe mips mipsel],
INSTDEPENDS += libmhash-dev
# libpam0g-dev
INSTDEPENDS += libpam-dev
INSTDEPENDS += libpcre3-dev
INSTDEPENDS += libperl-dev
INSTDEPENDS += libssl-dev
INSTDEPENDS += libxslt1-dev
INSTDEPENDS += po-debconf
INSTDEPENDS += quilt
INSTDEPENDS += zlib1g-dev
