Although by specification stemv values can be in principle floats/reals,
pdftoepdf.cc/pdftex expects ints, and used getInt. Now, getInt in
poppler breaks out if the value is not a real integer, but a float, while
the getInt function in xpdf returns the (int)f instead.
This patch uses round to obtain the closes value in case it is a float.
---
 texk/web2c/pdftexdir/ChangeLog    |    6 ++++++
 texk/web2c/pdftexdir/pdftoepdf.cc |    3 ++-
 2 files changed, 8 insertions(+), 1 deletion(-)

--- texlive-bin.orig/texk/web2c/pdftexdir/ChangeLog
+++ texlive-bin/texk/web2c/pdftexdir/ChangeLog
@@ -1,3 +1,9 @@
+2016-07-16  Akira Kakuto  <kakuto@fuk.kindai.ac.jp>
+
+	* pdftoepdf.cc: Use zround(stemV->getNum()) instead of stemV->getInt()
+	in epdf_create_fontdescriptor(), since the value of stemV may be real
+	in general.
+
 2016-06-12  Karl Berry  <karl@freefriends.org>
         and Akira Kakuto  <kakuto@fuk.kindai.ac.jp>
 
--- texlive-bin.orig/texk/web2c/pdftexdir/pdftoepdf.cc
+++ texlive-bin/texk/web2c/pdftexdir/pdftoepdf.cc
@@ -71,6 +71,7 @@
 // does not allow it.
 extern int getpdfsuppresswarningpagegroup(void);
 extern integer getpdfsuppressptexinfo(void);
+extern integer zround(double);
 }
 
 // The prefix "PTEX" for the PDF keys is special to pdfTeX;
@@ -427,7 +428,7 @@
         && (fontmap = lookup_fontmap(basefont->getName())) != NULL) {
         // copy the value of /StemV
         fontdesc->dictLookup("StemV", &stemV);
-        fd = epdf_create_fontdescriptor(fontmap, stemV->getInt());
+        fd = epdf_create_fontdescriptor(fontmap, zround(stemV->getNum()));
         if (fontdesc->dictLookup("CharSet", &charset) &&
             charset->isString() && is_subsetable(fontmap))
             epdf_mark_glyphs(fd, charset->getString()->getCString());
