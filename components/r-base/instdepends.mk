INSTDEPENDS += debhelper
#INSTDEPENDS += gcc
INSTDEPENDS += g++
INSTDEPENDS += gfortran
INSTDEPENDS += libblas-dev
INSTDEPENDS += liblapack-dev
INSTDEPENDS += tcl8.6-dev
INSTDEPENDS += tk8.6-dev
INSTDEPENDS += bison
INSTDEPENDS += groff-base
INSTDEPENDS += libncurses5-dev
INSTDEPENDS += libreadline-dev
INSTDEPENDS += texinfo
INSTDEPENDS += libbz2-dev
INSTDEPENDS += liblzma-dev
INSTDEPENDS += libpcre3-dev
INSTDEPENDS += libcurl4-openssl-dev
INSTDEPENDS += xdg-utils
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += libpng-dev
INSTDEPENDS += libjpeg-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += libxt-dev
INSTDEPENDS += x11proto-core-dev
INSTDEPENDS += libpango1.0-dev
INSTDEPENDS += libcairo2-dev
INSTDEPENDS += libtiff5-dev
INSTDEPENDS += xvfb
INSTDEPENDS += xauth
INSTDEPENDS += xfonts-base
INSTDEPENDS += texlive-base
INSTDEPENDS += texlive-latex-base
INSTDEPENDS += texlive-generic-recommended
INSTDEPENDS += texlive-fonts-recommended
INSTDEPENDS += texlive-fonts-extra
INSTDEPENDS += texlive-extra-utils
INSTDEPENDS += texlive-latex-recommended
INSTDEPENDS += texlive-latex-extra
INSTDEPENDS += default-jdk
INSTDEPENDS += mpack
INSTDEPENDS += bash-completion
INSTDEPENDS += libnghttp2-dev
INSTDEPENDS += libidn2-0-dev
INSTDEPENDS += librtmp-dev
INSTDEPENDS += libssh2-1-dev
INSTDEPENDS += libpsl-dev
INSTDEPENDS += libssl-dev
INSTDEPENDS += libldap2-dev
