Source: bison
Section: devel
Priority: optional
Maintainer: Chuan-kai Lin <cklin@debian.org>
Homepage: http://www.gnu.org/software/bison/
Standards-Version: 3.9.6.0
Build-Depends: debhelper (>= 9), gettext, m4 (>= 1.4-14), autotools-dev

Package: bison
Section: devel
Priority: optional
Architecture: any
Depends: m4, ${shlibs:Depends}, ${misc:Depends}, libbison-dev (= ${binary:Version})
Multi-Arch: foreign
Suggests: bison-doc
Description: YACC-compatible parser generator
 Bison is a general-purpose parser generator that converts a
 grammar description for an LALR(1) context-free grammar into a C
 program to parse that grammar.  Once you are proficient with Bison, you
 may use it to develop a wide range of language parsers, from those used
 in simple desk calculators to complex programming languages.
 .
 Bison is upward compatible with Yacc: all properly-written Yacc
 grammars ought to work with Bison with no change.  Anyone familiar with
 Yacc should be able to use Bison with little trouble.  Documentation of
 the program is in the bison-doc package.

Package: libbison-dev
Section: libdevel
Priority: optional
Architecture: any
Depends: ${misc:Depends}
Recommends: bison
Multi-Arch: same
Description: YACC-compatible parser generator - development library
 Bison is a general-purpose parser generator that converts a
 grammar description for an LALR(1) context-free grammar into a C
 program to parse that grammar.  Once you are proficient with Bison, you
 may use it to develop a wide range of language parsers, from those used
 in simple desk calculators to complex programming languages.
 .
 This package provides the liby.a file including functions needed by
 yacc parsers.

#Package: developer-parser-bison
#Depends: bison
#Priority: optional
#Section: oldlibs
#Architecture: all
#Description: YACC-compatible parser generator
# Empty package to facilitate upgrades, can be safely removed.

#Package: system-library-bison-runtime
#Depends: bison
#Priority: optional
#Section: oldlibs
#Architecture: all
#Description: YACC-compatible parser generator
# Empty package to facilitate upgrades, can be safely removed.
