INSTDEPENDS += debhelper
# perl
INSTDEPENDS += libssl-dev
INSTDEPENDS += libtest-exception-perl
INSTDEPENDS += libtest-nowarnings-perl
INSTDEPENDS += libtest-pod-perl
INSTDEPENDS += libtest-warn-perl
# openssl
INSTDEPENDS += perl-openssl-defaults
