Source: speech-dispatcher
Section: sound
Priority: optional
Maintainer: Debian TTS Team <tts-project@lists.alioth.debian.org>
Uploaders:
 Paul Gevers <elbrus@debian.org>, Samuel Thibault <sthibault@debian.org>
Build-Depends:
 automake,
 debhelper (>= 11.1), dh-exec,
 dh-python,
 python3-xdg,
 flite1-dev (>= 1.4),
 gettext,
 libao-dev,
 libasound2-dev [linux-any],
 libaudio-dev,
 libdotconf-dev (>= 1.3),
 libespeak-dev, libespeak-ng-dev,
 libglib2.0-dev (>= 2.36),
 libltdl-dev,
 libpulse-dev,
 libtool,
 libxau-dev,
 libsndfile1-dev,
# libttspico-dev,
 texinfo,
 systemd [linux-any],
 help2man
Build-Depends-Indep:
 python3
Vcs-Browser: https://anonscm.debian.org/git/tts/speech-dispatcher.git
Vcs-Git: https://anonscm.debian.org/git/tts/speech-dispatcher.git
Homepage: http://devel.freebsoft.org/speechd
XS-Autobuild: yes
Standards-Version: 4.2.0

##start-contrib
#Package: speech-dispatcher-pico
#Architecture: any
#Multi-arch: foreign
#Depends:
# ${shlibs:Depends},
# ${misc:Depends},
# speech-dispatcher (>= ${source:Upstream-Version}),
# speech-dispatcher (<< ${source:Upstream-Version}.0~)
#Breaks: speech-dispatcher-contrib (<< 0.8.5-5)
#Replaces: speech-dispatcher-contrib (<< 0.8.5-5)
#Description: Speech Dispatcher: Pico output module
# Speech Dispatcher provides a device independent layer for speech synthesis.
# It supports various software and hardware speech synthesizers as
# backends and provides a generic layer for synthesizing speech and
# playing back PCM data via those different backends to applications.
# .
# Various high level concepts like enqueueing vs. interrupting speech and
# application specific user configurations are implemented in a device
# independent way, therefore freeing the application programmer from
# having to yet again reinvent the wheel.
# .
# This package contains the output module for the pico speech synthesizer.
#
#Package: speech-dispatcher-baratinoo
#Architecture: any
#Multi-arch: foreign
#Section: contrib/sound
#Depends:
# ${shlibs:Depends},
# ${misc:Depends},
# speech-dispatcher (>= ${source:Upstream-Version}),
# speech-dispatcher (<< ${source:Upstream-Version}.0~)
#Recommends:
# voxygen
#Description: Speech Dispatcher: Baratinoo (VoxyGen) output module
# Speech Dispatcher provides a device independent layer for speech synthesis.
# It supports various software and hardware speech synthesizers as
# backends and provides a generic layer for synthesizing speech and
# playing back PCM data via those different backends to applications.
# .
# Various high level concepts like enqueueing vs. interrupting speech and
# application specific user configurations are implemented in a device
# independent way, therefore freeing the application programmer from
# having to yet again reinvent the wheel.
# .
# This package contains the output module for the Baratinoo speech synthesizer,
# also called VoxyGen, which needs to be installed separately.
#
#Package: speech-dispatcher-kali
#Architecture: any
#Multi-arch: foreign
#Section: contrib/sound
#Depends:
# ${shlibs:Depends},
# ${misc:Depends},
# speech-dispatcher (>= ${source:Upstream-Version}),
# speech-dispatcher (<< ${source:Upstream-Version}.0~)
#Recommends:
# kali-tts
#Description: Speech Dispatcher: Kali output module
# Speech Dispatcher provides a device independent layer for speech synthesis.
# It supports various software and hardware speech synthesizers as
# backends and provides a generic layer for synthesizing speech and
# playing back PCM data via those different backends to applications.
# .
# Various high level concepts like enqueueing vs. interrupting speech and
# application specific user configurations are implemented in a device
# independent way, therefore freeing the application programmer from
# having to yet again reinvent the wheel.
# .
# This package contains the output module for the Baratinoo speech synthesizer,
# also called Kali, which needs to be installed separately.
#
#Package: speech-dispatcher-ibmtts
#Architecture: i386
#Multi-arch: foreign
#Section: contrib/sound
#Depends:
# ${shlibs:Depends},
# ${misc:Depends},
# speech-dispatcher-audio-plugins (>= ${source:Upstream-Version}),
# speech-dispatcher-audio-plugins (<< ${source:Upstream-Version}),
# speech-dispatcher (>= ${source:Upstream-Version}),
# speech-dispatcher (<< ${source:Upstream-Version}.0~)
#Description: Speech Dispatcher: IBM TTS output module
# Speech Dispatcher provides a device independent layer for speech synthesis.
# It supports various software and hardware speech synthesizers as
# backends and provides a generic layer for synthesizing speech and
# playing back PCM data via those different backends to applications.
# .
# Various high level concepts like enqueueing vs. interrupting speech and
# application specific user configurations are implemented in a device
# independent way, therefore freeing the application programmer from
# having to yet again reinvent the wheel.
# .
# This package contains the output module for the Voxin speech synthesizer,
# also called IBM TTS or Eloquence, which needs to be installed separately.
##end-contrib

Package: speech-dispatcher
Architecture: any
Multi-arch: foreign
Depends:
 adduser,
 lsb-base (>= 3.0-10),
 speech-dispatcher-audio-plugins (= ${binary:Version}),
 init-system-helpers (>= 1.51),
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 pulseaudio,
 speech-dispatcher-espeak-ng,
 sound-icons
Suggests:
 libttspico-utils,
 espeak,
 mbrola,
 speech-dispatcher-doc-cs,
 speech-dispatcher-festival,
 speech-dispatcher-cicero,
 speech-dispatcher-flite,
 speech-dispatcher-espeak
Breaks:
 libspeechd2 (<< 0.9~),
 python-speechd (<< 0.9~),
 python3-speechd (<< 0.9~),
 cl-speech-dispatcher (<< 0.9~),
Description: Common interface to speech synthesizers
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains Speech Dispatcher itself.

Package: libspeechd2
Architecture: any
Multi-arch: same
Section: libs
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Pre-Depends:
 ${misc:Pre-Depends}
Description: Speech Dispatcher: Shared libraries
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains a shared library needed for C programs linked with it.

Package: libspeechd-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends:
 libspeechd2 (= ${binary:Version}),
 ${misc:Depends}
Suggests:
 speech-dispatcher
Description: Speech Dispatcher: Development libraries and header files
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains static library, and C headers needed for programs
 linked with the shared or static library.

Package: cl-speech-dispatcher
Architecture: all
Section: lisp
Depends:
 cl-regex,
 ${misc:Depends}
Description: Common Lisp interface to Speech Dispatcher
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains a Common Lisp library for communication with Speech
 Dispatcher.

Package: python3-speechd
Architecture: all
Section: python
Breaks:
 python-speechd
Replaces:
 python-speechd
Depends:
 python3-xdg,
 ${misc:Depends},
 ${python3:Depends}
Description: Python interface to Speech Dispatcher
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains a Python library for communication with Speech
 Dispatcher.

Package: speech-dispatcher-festival
Architecture: any
Multi-arch: foreign
Depends:
 festival,
 festival-freebsoft-utils (>= 0.6),
 speech-dispatcher (>= 0.6),
 ${misc:Depends},
 ${shlibs:Depends}
Breaks:
 speech-dispatcher (<< 0.8)
Replaces:
 speech-dispatcher (<< 0.8)
Recommends:
 sound-icons
Description: Festival support for Speech Dispatcher
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains dependencies on packages necessary for running Speech
 Dispatcher with Festival.

Package: speech-dispatcher-doc-cs
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 ${misc:Depends}
Suggests:
 speech-dispatcher
Description: Speech Dispatcher documentation in Czech
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains Czech documentation of Speech Dispatcher.

Package: speech-dispatcher-audio-plugins
Architecture: any
Multi-arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Pre-Depends:
 ${misc:Pre-Depends}
Breaks:
 speech-dispatcher (<< 0.8-1)
Replaces:
 speech-dispatcher (<< 0.8-1)
Description: Speech Dispatcher: Audio output plugins
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains all the speech-dispatcher audio output plugins.

Package: speech-dispatcher-flite
Architecture: any
Multi-arch: foreign
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 speech-dispatcher (= ${binary:Version}),
Breaks: speech-dispatcher (<< 0.8.5-5)
Replaces: speech-dispatcher (<< 0.8.5-5)
Description: Speech Dispatcher: Flite output module
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains the output module for the flite speech synthesizer.

Package: speech-dispatcher-cicero
Architecture: any
Multi-arch: foreign
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 speech-dispatcher (= ${binary:Version}),
Breaks: speech-dispatcher (<< 0.8.5-5)
Replaces: speech-dispatcher (<< 0.8.5-5)
Description: Speech Dispatcher: Cicero output module
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains the output module for the cicero speech synthesizer.

Package: speech-dispatcher-espeak
Architecture: any
Multi-arch: foreign
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 speech-dispatcher (= ${binary:Version}),
Breaks: speech-dispatcher (<< 0.8.5-5)
Replaces: speech-dispatcher (<< 0.8.5-5)
Description: Speech Dispatcher: Espeak output module
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains the output module for the espeak speech synthesizer.

Package: speech-dispatcher-espeak-ng
Architecture: any
Multi-arch: foreign
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 speech-dispatcher (= ${binary:Version}),
Breaks: speech-dispatcher (<< 0.8.5-5)
Replaces: speech-dispatcher (<< 0.8.5-5)
Description: Speech Dispatcher: Espeak-ng output module
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains the output module for the espeak-ng speech synthesizer.
