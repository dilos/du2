#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS=hardening=+all
include /usr/share/dpkg/pkg-info.mk

# NAS is in universe in Ubuntu
ifeq ($(shell dpkg-vendor --derives-from Ubuntu && echo yes),yes)
	NAS = --with-nas=no
endif


%:
	dh $@ --with python3

override_dh_auto_configure:
	for i in espeak-ng-mbrola-generic.conf \
	         espeak-mbrola-generic.conf ; do \
		sed < config/modules/$$i.in > config/modules/$$i -e "s/\$$DEB_HOST_MULTIARCH/$(DEB_HOST_MULTIARCH)/" ; \
	done
	dh_auto_configure -- --with-module-bindir=\$${prefix}/lib/speech-dispatcher-modules \
	                     pidpath=/var/run/speech-dispatcher/ $(NAS)

override_dh_auto_install:
	dh_auto_install --destdir=debian/tmp

ifeq ($(DEB_SOURCE),speech-dispatcher)
override_dh_strip:
	dh_strip --ddeb-migration='speech-dispatcher-dbg (<< 0.8.3-2~)'

override_dh_installinit:
#	dh_installinit -a --error-handler=true --no-enable --no-start

override_dh_fixperms-arch:
	dh_fixperms
	chmod 700 $(CURDIR)/debian/speech-dispatcher/var/log/speech-dispatcher

override_dh_makeshlibs:
	dh_makeshlibs -plibspeechd2
endif

override_dh_shlibdeps:
	dh_shlibdeps --dpkg-shlibdeps-params=--ignore-missing-info

override_dh_systemd_enable:
#	dh_systemd_enable --no-enable

# Switch to main rules
main:
	sed -i 's/^speech-dispatcher-contrib /speech-dispatcher /' debian/changelog
	sed -i '1s/^Source: speech-dispatcher-contrib$$/Source: speech-dispatcher/' debian/control
	sed -i 's/^ libttspico-dev/# libttspico-dev/' debian/control
	sed -i '/^#start-contrib$$/,/^#end-contrib$$/s/^/#/' debian/control
	sed -i '/Package: speech-dispatcher$$/,$$s/^#*//' debian/control
	sed -i '1,/^$$/s_^Section: contrib/_Section: _' debian/control

# Switch to contrib rules
contrib:
	sed -i 's/^speech-dispatcher /speech-dispatcher-contrib /' debian/changelog
	sed -i '1s/^Source: speech-dispatcher$$/Source: speech-dispatcher-contrib/' debian/control
	sed -i 's/^#* libttspico-dev/ libttspico-dev/' debian/control
	sed -i '/^##start-contrib$$/,/^##end-contrib$$/s/^#//' debian/control
	sed -i '/Package: speech-dispatcher$$/,$$s/^/#/' debian/control
	sed -i '1,/^$$/s_^Section: \([^/]*\)$$_Section: contrib/\1_' debian/control
