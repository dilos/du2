#!/usr/bin/make -f

DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

export XDG_RUNTIME_DIR=/tmp

PY2VERS := $(shell pyversions -v -r debian/control)
PY3VERS := $(shell py3versions -v -r debian/control)
DEFPY := $(shell pyversions -v -d)
PY_PLATFORM := $(shell python -c 'from distutils.util import get_platform; print get_platform()')
pd := python-matplotlib-doc
p := python-matplotlib
p3 := python3-matplotlib
pdata := python-matplotlib-data
pdbg := python-matplotlib-dbg
p3dbg := python3-matplotlib-dbg

# build
build: build-arch build-indep

build-indep: build-indep-stamp
build-indep-stamp: $(DEFPY:%=build-%-stamp)
	dh_testdir

	# build the doc
# fixme:
#	( cd doc ; MPLCONFIGDIR=. MATPLOTLIBDATA=$(CURDIR)/lib/matplotlib/mpl-data/ \
#		PYTHONPATH=$(CURDIR)/build/lib.$(PY_PLATFORM)-$(DEFPY) ./make.py --small html )

	touch $@

build-arch: $(PY2VERS:%=build-%-stamp) $(PY3VERS:%=build-%-stamp)

build-%-stamp:
	dh_testdir

	cp -f debian/setup.cfg setup.cfg
	python$* ./setup.py build $(PY_BUILD_FLAGS)
#	python$*-dbg ./setup.py build $(PY_BUILD_FLAGS)

ifeq (,$(findstring nocheck,$(DEB_BUILD_OPTIONS)))
	echo "backend      : TkAgg" > matplotlibrc
	-PYTHONPATH=$(shell python$* -c "from distutils.command.build import build ; \
	                                from distutils.core import Distribution ; \
					b = build(Distribution()) ; \
					b.finalize_options() ; \
					print(b.build_platlib)") \
	MATPLOTLIBDATA=$(CURDIR)/lib/matplotlib/mpl-data/ \
	MPLCONFIGDIR=. \
		xvfb-run -a python$* tests.py --eval-attr="not network" --verbosity=2

#	-PYTHONPATH=$(shell python$*-dbg -c "from distutils.command.build import build ; \
#	                                    from distutils.core import Distribution ; \
#					    b = build(Distribution()) ; \
#					    b.finalize_options() ; \
#					    print(b.build_platlib)") \
#	MATPLOTLIBDATA=$(CURDIR)/lib/matplotlib/mpl-data/ \
#	MPLCONFIGDIR=. \
#		xvfb-run -a python$*-dbg tests.py --eval-attr="not network" --verbosity=2
	rm -f matplotlibrc
endif

	touch $@


# clean
clean:
	dh_testdir
	dh_testroot
	
	for i in $(PY2VERS) $(PY3VERS); do \
		python$(i) ./setup.py clean --all; \
	done
#		python$(i)-dbg ./setup.py clean --all; 

	# clean the doc
	-( cd doc ; ./make.py clean )

	# workaround for cleaning the doc ("doc/make.py clean" only runs useless svn-clean)
	rm -rf doc/build doc/examples
	rm -f doc/mpl_examples/api/*.png
	rm -f doc/mpl_examples/pylab_examples/*.png
	rm -f doc/mpl_examples/pylab_examples/*.pdf
	rm -f doc/mpl_examples/units/*.png
	rm -f doc/pyplots/tex_demo.png
	rm -f doc/_static/matplotlibrc
	rm -f doc/_templates/gallery.html

	find . -name '*.pyc' -exec rm {} \;

	dh_clean lib/matplotlib/mpl-data/matplotlib.conf \
		lib/matplotlib/mpl-data/matplotlibrc setup.cfg
	rm -rf build
	rm -rf lib/matplotlib.egg-info

	# restore "pristine" doc/matplotlibrc
	sed -i '/^examples\./d' $(CURDIR)/doc/matplotlibrc

	# remove doc symlinks to examples directory
	rm -rf doc/mpl_examples
	rm -rf doc/mpl_toolkits/axes_grid/examples

	# build twice in a row
	rm -rf doc/tex.cache/ fontList.py3k.cache result_images/ test_cache/ tex.cache/ examples/pylab_examples/test.xls
	rm -rf doc/finance.cache/ doc/fontList.cache fontList.cache

	# remove embedded CXX to prevent it to be picked up
	rm -rf extern/CXX/

# install
install-arch: build-arch $(PY2VERS:%=install-%-stamp) $(PY3VERS:%=install-py3-%-stamp)
	dh_testdir
	dh_testroot
	dh_numpy
	dh_numpy3

install-%-stamp: build-%-stamp
	dh_testdir
	dh_testroot

	python$* ./setup.py install_lib \
		-d $(CURDIR)/debian/$(p)/usr/lib/python$*/site-packages/ --no-compile
	python$* ./setup.py install_egg_info \
		-d $(CURDIR)/debian/$(p)/usr/lib/python$*/site-packages/
#	python$*-dbg ./setup.py install_lib \
#		-d $(CURDIR)/debian/$(pdbg)/usr/lib/python$*/site-packages/ --no-compile

#	find debian/$(pdbg) ! -type d ! -name '*.so' | xargs rm -f
#	find debian/$(pdbg) -depth -empty -exec rmdir {} \;

	# don't install baseline_images, needed for tests only, but huge
	# leave the dir tho, as matplotlib/tests/__init__.py checks for it
	rm -rf $(CURDIR)/debian/$(p)/usr/lib/python$*/*-packages/matplotlib/tests/baseline_images/*
	for d in $(CURDIR)/debian/$(p)/usr/lib/python$*/dist-packages/matplotlib/tests/baseline_images/; do \
	    mkdir -p $$d; \
	    touch $$d/.keep; \
	done

	rm -rf $(CURDIR)/debian/$(p)/usr/lib/python$*/site-packages/matplotlib/mpl-data/
	rm -rf $(CURDIR)/debian/$(p)/usr/lib/python$*/site-packages/matplotlib/backends/Matplotlib.nib/

	rm -rf $(CURDIR)/debian/$(pdbg)/usr/lib/python$*/site-packages/matplotlib/mpl-data/
	rm -rf $(CURDIR)/debian/$(pdbg)/usr/lib/python$*/site-packages/matplotlib/backends/Matplotlib.nib/

	# replace minified jquery with packaged version and hope it works
	for i in $(PY2VERS); do \
	    dh_link -ppython-matplotlib usr/share/javascript/jquery/jquery.min.js \
	      usr/lib/python$$i/site-packages/matplotlib/backends/web_backend/jquery/js/jquery-1.7.1.min.js; \
	    dh_link -ppython-matplotlib usr/share/javascript/jquery-ui/jquery-ui.min.js \
	      usr/lib/python$$i/site-packages/matplotlib/backends/web_backend/jquery/js/jquery-ui.min.js; \
	    dh_link -ppython-matplotlib usr/share/javascript/jquery-ui/css/smoothness/jquery-ui.min.css \
	      usr/lib/python$$i/site-packages/matplotlib/backends/web_backend/jquery/css/themes/base/jquery-ui.min.css; \
	done

	touch $@

install-py3-%-stamp: build-%-stamp
	dh_testdir
	dh_testroot

	-rm $(CURDIR)/debian/$(p3)/usr/lib/python3/dist-packages/matplotlib/backends/web_backend/jquery/js/jquery-1.7.1.min.js
	-rm $(CURDIR)/debian/$(p3)/usr/lib/python3/dist-packages/matplotlib/backends/web_backend/jquery/js/jquery-ui.min.js
	-rm $(CURDIR)/debian/$(p3)/usr/lib/python3/dist-packages/matplotlib/backends/web_backend/jquery/css/themes/base/jquery-ui.min.css


	python$* ./setup.py install_lib \
		-d $(CURDIR)/debian/$(p3)/usr/lib/python3/dist-packages/ --no-compile
	python$* ./setup.py install_egg_info \
		-d $(CURDIR)/debian/$(p3)/usr/lib/python3/dist-packages/
#	python$*-dbg ./setup.py install_lib \
#		-d $(CURDIR)/debian/$(p3dbg)/usr/lib/python3/dist-packages/ --no-compile

#	find debian/$(p3dbg) ! -type d ! -name '*.so' | xargs rm -f
#	find debian/$(p3dbg) -depth -empty -exec rmdir {} \;

	# don't install baseline_images, needed for tests only, but huge
	# leave the dir tho, as matplotlib/tests/__init__.py checks for it
	rm -rf $(CURDIR)/debian/$(p3)/usr/lib/python3/*-packages/matplotlib/tests/baseline_images/*
	for d in $(CURDIR)/debian/$(p3)/usr/lib/python3/dist-packages/matplotlib/tests/baseline_images/; do \
	    mkdir -p $$d; \
	    touch $$d/.keep; \
	done

	rm -rf $(CURDIR)/debian/$(p3)/usr/lib/python3/dist-packages/matplotlib/mpl-data/
	rm -rf $(CURDIR)/debian/$(p3)/usr/lib/python3/dist-packages/matplotlib/backends/Matplotlib.nib/

	rm -rf $(CURDIR)/debian/$(p3dbg)/usr/lib/python3/dist-packages/matplotlib/mpl-data/
	rm -rf $(CURDIR)/debian/$(p3dbg)/usr/lib/python3/dist-packages/matplotlib/backends/Matplotlib.nib/

	# replace minified jquery with packaged version and hope it works
	for i in $(PY3VERS); do \
	    dh_link -ppython3-matplotlib usr/share/javascript/jquery/jquery.min.js \
	      usr/lib/python3/dist-packages/matplotlib/backends/web_backend/jquery/js/jquery-1.7.1.min.js; \
	    dh_link -ppython3-matplotlib usr/share/javascript/jquery-ui/jquery-ui.min.js \
	      usr/lib/python3/dist-packages/matplotlib/backends/web_backend/jquery/js/jquery-ui.min.js; \
	    dh_link  -ppython3-matplotlib usr/share/javascript/jquery-ui/css/smoothness/jquery-ui.min.css \
	      usr/lib/python3/dist-packages/matplotlib/backends/web_backend/jquery/css/themes/base/jquery-ui.min.css; \
	done

	touch $@

install-indep: build-indep
	dh_testdir -i
	dh_testroot -i
	dh_prep
	dh_install -p $(pdata)
	rm -fr $(CURDIR)/debian/$(pdata)/usr/share/matplotlib/mpl-data/fonts/ttf/Vera*.ttf
	rm -fr $(CURDIR)/debian/$(pdata)/usr/share/matplotlib/mpl-data/fonts/ttf/*.TXT
	rm -fr $(CURDIR)/debian/$(pdata)/usr/share/matplotlib/mpl-data/fonts/ttf/local.conf
	rm -fr $(CURDIR)/debian/$(pdata)/usr/share/matplotlib/mpl-data/fonts/pdfcorefonts/readme.txt
	chmod 644 $(CURDIR)/debian/$(pdata)/usr/share/matplotlib/mpl-data/images/*.svg

# binary
binary-indep: build-indep install-indep
	dh_testdir -i
	dh_testroot -i
	dh_installchangelogs -i CHANGELOG
# fixme:
#	dh_installdocs -p$(pd) -i doc/build/html/
	# remove pyc file from doc
	dh_installexamples -p$(pd) -i examples/*
	dh_compress -i -Xexamples -Xexamples/data -Xpdf -X.js -Xobjects.inv
	dh_link -i
	dh_sphinxdoc
	find $(CURDIR)/debian/$(pd)/ -type f -name '*.pyc' | xargs --no-run-if-empty rm
	dh_lintian -i
	dh_fixperms -i
	dh_installdeb -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

binary-arch: build-arch install-arch
	dh_testdir -a
	dh_testroot -a
	dh_installchangelogs -a CHANGELOG
	dh_installdocs -p$(p) -a README.rst
	# make python scripts starting with '#!' executable
	for i in `find debian -mindepth 2 -type f ! -perm 755`; do \
		if head -1 $$i | grep -q '^#!'; then \
			chmod 755 $$i; \
			echo "made executable: $$i"; \
		fi; \
	done
	dh_python2 -a
	dh_python3 -a
	dh_link -a
	dh_lintian -a
	dh_strip -a -p$(p) --dbg-package=$(pdbg)
	dh_strip -a -p$(p3) --dbg-package=$(p3dbg)
	dh_compress -a
	dh_fixperms -a
	dh_installdeb -a
	dh_shlibdeps -a
	dh_gencontrol -a
	dh_md5sums -a
	dh_builddeb -a

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install
