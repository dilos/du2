Source: lua-sandbox
Section: interpreters
Priority: optional
Maintainer: Heka Maintainers <pkg-heka-maint@lists.alioth.debian.org>
Uploaders: Raphaël Hertzog <hertzog@debian.org>, Mathieu Parent <sathieu@debian.org>
Build-Depends: debhelper (>= 9), cmake
Standards-Version: 3.9.8
Homepage: https://github.com/mozilla-services/lua_sandbox
Vcs-Git: https://anonscm.debian.org/pkg-heka/lua-sandbox.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-heka/lua-sandbox.git

Package: libluasandbox0
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
# Upstream package name
Conflicts: luasandbox
Description: Generic Lua sandbox library for dynamic data analysis — dynamic library
 The Lua sandbox is a library allowing customized control over the Lua
 execution environment including functionality like global data
 preservation/restoration on shutdown/startup, output collection in textual or
 binary formats and an array of parsers for various data types (Nginx, Apache,
 Syslog, MySQL and many RFC grammars).
 .
 Sandboxes provide a dynamic and isolated execution environment for data
 parsing, transformation, and analysis. They allow access to data without
 jeopardizing the integrity or performance of the processing infrastructure.
 This broadens the audience that the data can be exposed to and facilitates new
 uses of the data (i.e. debugging, monitoring, dynamic provisioning, SLA
 analysis, intrusion detection, ad-hoc reporting, etc.)
 .
 Features of lua-sandbox:
  - small: memory requirements are as little as 8 KiB for a basic sandbox
  - fast: microsecond execution times
  - stateful: ability to resume where it left off after a restart/reboot
  - isolated: failures are contained and malfunctioning sandboxes are
    terminated. Containment is defined in terms of restriction to the operating
    system, file system, libraries, memory use, Lua instruction use, and output
    size.

Package: libluasandbox-dev
Architecture: any
Section: libdevel
Depends: libluasandbox0 (= ${binary:Version}), ${misc:Depends}
Description: Generic Lua sandbox library for dynamic data analysis – development files
 The Lua sandbox is a library allowing customized control over the Lua
 execution environment including functionality like global data
 preservation/restoration on shutdown/startup, output collection in textual or
 binary formats and an array of parsers for various data types (Nginx, Apache,
 Syslog, MySQL and many RFC grammars).
 .
 This package contains the C headers and other development files.

Package: libluasandbox-bin
Section: utils
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Generic Lua sandbox library for dynamic data analysis – utilities
 The Lua sandbox is a library allowing customized control over the Lua
 execution environment including functionality like global data
 preservation/restoration on shutdown/startup, output collection in textual or
 binary formats and an array of parsers for various data types (Nginx, Apache,
 Syslog, MySQL and many RFC grammars).
 .
 This package contains the command line utilities.
