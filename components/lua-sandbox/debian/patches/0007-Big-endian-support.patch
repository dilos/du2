From: Mathieu Parent <math.parent@gmail.com>
Date: Sat, 10 Dec 2016 22:45:34 +0100
Subject: Big endian support

Bug: https://github.com/mozilla-services/lua_sandbox/pull/173
---
 include/luasandbox/util/heka_message.h |  1 +
 src/heka/message.c                     | 17 ++++++++++++-----
 src/util/heka_message.c                |  2 ++
 src/util/protobuf.c                    | 10 ++++++++--
 src/util/test/test_protobuf.c          |  9 ++++++++-
 5 files changed, 31 insertions(+), 8 deletions(-)

diff --git a/include/luasandbox/util/heka_message.h b/include/luasandbox/util/heka_message.h
index b6b4aab..8f41130 100644
--- a/include/luasandbox/util/heka_message.h
+++ b/include/luasandbox/util/heka_message.h
@@ -108,6 +108,7 @@ typedef struct {
   {
     lsb_const_string  s;
     double            d;
+    long long         ll;
   } u;
   lsb_read_type type;
 } lsb_read_value;
diff --git a/src/heka/message.c b/src/heka/message.c
index ef8b96b..3f423fa 100644
--- a/src/heka/message.c
+++ b/src/heka/message.c
@@ -12,6 +12,7 @@
 #include <stdlib.h>
 #include <string.h>
 #include <time.h>
+#include <endian.h>
 
 #include "../luasandbox_impl.h" // todo the API should change so this doesn't
 // need access to the impl
@@ -166,16 +167,21 @@ static const char* process_fields(lua_State *lua, const char *p, const char *e)
 
     case 7: // value_double
       {
-        double val = 0;
+        union {
+          double    d;
+          long long ll;
+        } val;
+        val.d = 0;
         switch (wiretype) {
         case 1:
           if (p + sizeof(double) > e) {
             p = NULL;
             break;
           }
-          memcpy(&val, p, sizeof(double));
+          memcpy(&val.d, p, sizeof(double));
+          val.ll = le64toh(val.ll);
           p += sizeof(double);
-          lua_pushnumber(lua, val);
+          lua_pushnumber(lua, val.d);
           lua_rawseti(lua, 5, ++value_count);
           break;
         case 2:
@@ -185,9 +191,10 @@ static const char* process_fields(lua_State *lua, const char *p, const char *e)
             break;
           }
           do {
-            memcpy(&val, p, sizeof(double));
+            memcpy(&val.d, p, sizeof(double));
+            val.ll = le64toh(val.ll);
             p += sizeof(double);
-            lua_pushnumber(lua, val);
+            lua_pushnumber(lua, val.d);
             lua_rawseti(lua, 5, ++value_count);
           } while (p < e);
           break;
diff --git a/src/util/heka_message.c b/src/util/heka_message.c
index 64398e6..13ec401 100644
--- a/src/util/heka_message.c
+++ b/src/util/heka_message.c
@@ -8,6 +8,7 @@
 
 #include "luasandbox/util/heka_message.h"
 
+#include <endian.h>
 #include <limits.h>
 #include <stdio.h>
 #include <stdlib.h>
@@ -112,6 +113,7 @@ read_double_value(const char *p, const char *e, int ai, lsb_read_value *val)
   val->type = LSB_READ_NUMERIC;
   p += sizeof(double) * ai;
   memcpy(&val->u.d, p, sizeof(double));
+  val->u.ll = le64toh(val->u.ll);
   return true;
 }
 
diff --git a/src/util/protobuf.c b/src/util/protobuf.c
index d510586..ee26b83 100644
--- a/src/util/protobuf.c
+++ b/src/util/protobuf.c
@@ -8,6 +8,7 @@
 
 #include "luasandbox/util/protobuf.h"
 
+#include <endian.h>
 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>
@@ -102,8 +103,13 @@ lsb_err_value lsb_pb_write_double(lsb_output_buffer *ob, double i)
 
   lsb_err_value ret = lsb_expand_output_buffer(ob, needed);
   if (!ret) {
-    // todo add big endian support if necessary
-    memcpy(&ob->buf[ob->pos], &i, needed);
+    union {
+      double    d;
+      long long ll;
+    } tmp;
+    tmp.d = i;
+    tmp.ll = htole64(tmp.ll);
+    memcpy(&ob->buf[ob->pos], &tmp.ll, needed);
     ob->pos += needed;
   }
   return ret;
diff --git a/src/util/test/test_protobuf.c b/src/util/test/test_protobuf.c
index 38b2eb6..22a80db 100644
--- a/src/util/test/test_protobuf.c
+++ b/src/util/test/test_protobuf.c
@@ -8,6 +8,7 @@
 
 #include <stdio.h>
 #include <string.h>
+#include <endian.h>
 
 #include "luasandbox/error.h"
 #include "luasandbox/test/mu_test.h"
@@ -143,11 +144,17 @@ static char* test_lsb_pb_write_bool()
 static char* test_lsb_pb_write_double()
 {
   double d = 7.13;
+  union {
+    double d;
+    long long ll;
+  } d_le;
+  d_le.d = d;
+  d_le.ll = htole64(d_le.ll);
   lsb_output_buffer ob;
   lsb_init_output_buffer(&ob, sizeof d);
   lsb_err_value ret = lsb_pb_write_double(&ob, d);
   mu_assert(!ret, "received %s", ret);
-  mu_assert(memcmp(ob.buf, &d, sizeof d) == 0, "received: %g",
+  mu_assert(memcmp(ob.buf, &d_le.d, sizeof d_le.d) == 0, "received: %g",
             *((double *)ob.buf));
   ret = lsb_pb_write_double(&ob, d);
   mu_assert(ret == LSB_ERR_UTIL_FULL, "received %s", lsb_err_string(ret));
