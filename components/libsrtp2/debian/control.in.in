Source: libsrtp2
Priority: optional
Maintainer: Debian VoIP Team <pkg-voip-maintainers@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>,
 Daniel Pocock <daniel@pocock.com.au>
Build-Depends: @cdbs@
Build-Depends-Indep: doxygen
Standards-Version: 4.1.4
Section: libs
Vcs-Git: https://salsa.debian.org/pkg-voip-team/libsrtp2.git
Vcs-Browser: https://salsa.debian.org/pkg-voip-team/libsrtp2.git
Homepage: https://github.com/cisco/libsrtp

Package: __DEVPKGNAME__
Section: libdevel
Depends: ${cdbs:Depends},
 ${devlibs:Depends},
 ${misc:Depends},
 __LIBPKGNAME__ (= ${binary:Version})
Suggests: __DOCPKGNAME__
Architecture: any
Description: Secure RTP (SRTP) and UST Reference Implementations - development files
 SRTP is a security profile for RTP that adds confidentiality, message
 authentication, and replay protection to that protocol. It is specified
 in RFC 3711.
 .
 LibSRTP provides an implementation of the Secure Real-time Transport
 Protocol (SRTP), the Universal Security Transform (UST), and a
 supporting cryptographic kernel.
 .
 This package contains the development headers and static libraries.

Package: __LIBPKGNAME__
Section: libs
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
 ${shlibs:Depends}
Architecture: any
Multi-Arch: same
Description: Secure RTP (SRTP) and UST Reference Implementations - shared library
 SRTP is a security profile for RTP that adds confidentiality, message
 authentication, and replay protection to that protocol. It is specified
 in RFC 3711.
 .
 LibSRTP provides an implementation of the Secure Real-time Transport
 Protocol (SRTP), the Universal Security Transform (UST), and a
 supporting cryptographic kernel.
 .
 This package contains the shared libraries.

Package: __LIBPKGNAME__-dbg
Section: debug
Depends: ${misc:Depends},
 __LIBPKGNAME__ (= ${binary:Version})
Architecture: linux-any
Multi-Arch: same
Description: Secure RTP (SRTP) and UST Reference Implementations - debugging symbols
 SRTP is a security profile for RTP that adds confidentiality, message
 authentication, and replay protection to that protocol. It is specified
 in RFC 3711.
 .
 LibSRTP provides an implementation of the Secure Real-time Transport
 Protocol (SRTP), the Universal Security Transform (UST), and a
 supporting cryptographic kernel.
 .
 This package contains the debugging symbols.

Package: __DOCPKGNAME__
Section: doc
Depends: ${misc:Depends}
Architecture: all
Description: Secure RTP (SRTP) and UST Reference Implementations - documentation
 SRTP is a security profile for RTP that adds confidentiality, message
 authentication, and replay protection to that protocol. It is specified
 in RFC 3711.
 .
 LibSRTP provides an implementation of the Secure Real-time Transport
 Protocol (SRTP), the Universal Security Transform (UST), and a
 supporting cryptographic kernel.
 .
 This package contains API documentation.
