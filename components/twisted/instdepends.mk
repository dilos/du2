INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += patch
INSTDEPENDS += python-all-dev
# python-zope.interface-dbg (>= 4.0.2),
INSTDEPENDS += python-zope.interface
INSTDEPENDS += python-setuptools
INSTDEPENDS += python-incremental
INSTDEPENDS += python-constantly
INSTDEPENDS += python-doc
INSTDEPENDS += python3-all-dev
INSTDEPENDS += python3-zope.interface
INSTDEPENDS += python3-setuptools
INSTDEPENDS += python3-incremental
INSTDEPENDS += python3-constantly
INSTDEPENDS += python3-doc
#Build-Depends-Indep:
INSTDEPENDS += python3-sphinx
