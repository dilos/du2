Source: libkate
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Petter Reinholdtsen <pere@debian.org>
 , Martin Steghöfer <martin@steghoefer.eu>
 , Ralph Giles <giles@thaumas.net>
Build-Depends: dh-exec (>=0.3), debhelper (>= 11), python-all-dev:any, libpython-all-dev, dh-python, liboggz-dev, pkg-config, libpng-dev, doxygen, oggz-tools [solaris-any]
Standards-Version: 4.3.0
Section: libs
Homepage: https://code.google.com/p/libkate/
Vcs-Git: https://salsa.debian.org/multimedia-team/libkate.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/libkate

Package: libkate-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libkate1 (= ${binary:Version}), ${misc:Depends}
Description: Codec for karaoke and text encapsulation (dev)
 Kate is meant to be used for karaoke alongside audio/video streams (typically
 Vorbis and Theora), movie subtitles, song lyrics, and anything that needs text
 data at arbitrary time intervals.
 .
 libkate provides an API for the encoding and decoding of kate files.
 This package contains the development libraries.

Package: libkate1
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Codec for karaoke and text encapsulation
 Kate is meant to be used for karaoke alongside audio/video streams (typically
 Vorbis and Theora), movie subtitles, song lyrics, and anything that needs text
 data at arbitrary time intervals.
 .
 libkate provides an API for the encoding and decoding of kate files.

Package: liboggkate-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: liboggkate1 (= ${binary:Version}), ${misc:Depends}
Description: Codec for karaoke and text encapsulation for Ogg (dev)
 Kate is meant to be used for karaoke alongside audio/video streams (typically
 Vorbis and Theora), movie subtitles, song lyrics, and anything that needs text
 data at arbitrary time intervals.
 .
 liboggkate provides an API for the encapsulation of kate streams into Ogg.
 This package contains the development libraries.

Package: liboggkate1
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Codec for karaoke and text encapsulation for Ogg
 Kate is meant to be used for karaoke alongside audio/video streams (typically
 Vorbis and Theora), movie subtitles, song lyrics, and anything that needs text
 data at arbitrary time intervals.
 .
 liboggkate provides an API for the encapsulation of kate streams into Ogg.

Package: libkate-tools
Section: utils
Architecture: any
Multi-Arch: foreign
XB-Python-Version: ${python:Versions}
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python:Depends}, python-pythoncard, oggz-tools
Provides: ${python:Provides}
Description: Utilities for mangling kate Ogg files
 Kate is meant to be used for karaoke alongside audio/video streams (typically
 Vorbis and Theora), movie subtitles, song lyrics, and anything that needs text
 data at arbitrary time intervals.
 .
 This package contains some utilities useful for debugging and tweaking
 Kate files, using libkate
