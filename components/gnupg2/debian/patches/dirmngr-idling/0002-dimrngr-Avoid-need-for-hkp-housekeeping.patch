From: Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Date: Sat, 29 Oct 2016 02:00:50 -0400
Subject: dimrngr: Avoid need for hkp housekeeping.

* dirmngr/ks-engine-hkp.c (host_is_alive): New function.  Test whether
host is alive and resurrects it if it has been dead long enough.
(select_random_host, map_host, ks_hkp_mark_host): Use host_is_alive
instead of testing hostinfo_t->dead directly.
(ks_hkp_housekeeping): Remove function, no longer needed.
* dirmngr/dirmngr.c (housekeeping_thread): Remove call to
ks_hkp_housekeeping.

--

Rather than resurrecting hosts upon scheduled resurrection times, test
whether hosts should be resurrected as they're inspected for being
dead.  This removes the need for explicit housekeeping, and makes host
resurrections happen "just in time", rather than being clustered on
HOUSEKEEPING_INTERVAL seconds.

Signed-off-by: Daniel Kahn Gillmor <dkg@fifthhorseman.net>
---
 dirmngr/dirmngr.c       |  3 --
 dirmngr/dirmngr.h       |  1 -
 dirmngr/ks-engine-hkp.c | 73 ++++++++++++++++++++++++-------------------------
 3 files changed, 36 insertions(+), 41 deletions(-)

diff --git a/dirmngr/dirmngr.c b/dirmngr/dirmngr.c
index 061cfc300..a8be56fa4 100644
--- a/dirmngr/dirmngr.c
+++ b/dirmngr/dirmngr.c
@@ -1771,12 +1771,10 @@ static void *
 housekeeping_thread (void *arg)
 {
   static int sentinel;
-  time_t curtime;
   struct server_control_s ctrlbuf;
 
   (void)arg;
 
-  curtime = gnupg_get_time ();
   if (sentinel)
     {
       log_info ("housekeeping is already going on\n");
@@ -1789,7 +1787,6 @@ housekeeping_thread (void *arg)
   memset (&ctrlbuf, 0, sizeof ctrlbuf);
   dirmngr_init_default_ctrl (&ctrlbuf);
 
-  ks_hkp_housekeeping (curtime);
   if (network_activity_seen)
     {
       network_activity_seen = 0;
diff --git a/dirmngr/dirmngr.h b/dirmngr/dirmngr.h
index 35bc000fb..acd4c636d 100644
--- a/dirmngr/dirmngr.h
+++ b/dirmngr/dirmngr.h
@@ -193,7 +193,6 @@ const char* dirmngr_get_current_socket_name (void);
 
 
 /*-- Various housekeeping functions.  --*/
-void ks_hkp_housekeeping (time_t curtime);
 void ks_hkp_reload (void);
 
 
diff --git a/dirmngr/ks-engine-hkp.c b/dirmngr/ks-engine-hkp.c
index 57e7529f3..2b90441e2 100644
--- a/dirmngr/ks-engine-hkp.c
+++ b/dirmngr/ks-engine-hkp.c
@@ -203,6 +203,25 @@ host_in_pool_p (int *pool, int tblidx)
 }
 
 
+static int
+host_is_alive (hostinfo_t hi, time_t curtime)
+{
+  if (!hi)
+    return 0;
+  if (!hi->dead)
+    return 1;
+  if (!hi->died_at)
+    return 0; /* manually marked dead */
+  if (hi->died_at + RESURRECT_INTERVAL <= curtime
+      || hi->died_at > curtime)
+    {
+      hi->dead = 0;
+      log_info ("resurrected host '%s'", hi->name);
+      return 1;
+    }
+  return 0;
+}
+
 /* Select a random host.  Consult TABLE which indices into the global
    hosttable.  Returns index into TABLE or -1 if no host could be
    selected.  */
@@ -212,11 +231,13 @@ select_random_host (int *table)
   int *tbl = NULL;
   size_t tblsize = 0;
   int pidx, idx;
+  time_t curtime;
 
+  curtime = gnupg_get_time ();
   /* We create a new table so that we randomly select only from
      currently alive hosts.  */
   for (idx=0; (pidx = table[idx]) != -1; idx++)
-    if (hosttable[pidx] && !hosttable[pidx]->dead)
+    if (hosttable[pidx] && host_is_alive (hosttable[pidx], curtime))
       {
         tblsize++;
         tbl = xtryrealloc(tbl, tblsize * sizeof *tbl);
@@ -395,6 +416,7 @@ map_host (ctrl_t ctrl, const char *name, const char *srvtag, int force_reselect,
   gpg_error_t err = 0;
   hostinfo_t hi;
   int idx;
+  time_t curtime;
 
   *r_host = NULL;
   if (r_httpflags)
@@ -531,6 +553,7 @@ map_host (ctrl_t ctrl, const char *name, const char *srvtag, int force_reselect,
         xfree (reftbl);
     }
 
+  curtime = gnupg_get_time ();
   hi = hosttable[idx];
   if (hi->pool)
     {
@@ -547,7 +570,7 @@ map_host (ctrl_t ctrl, const char *name, const char *srvtag, int force_reselect,
       if (force_reselect)
         hi->poolidx = -1;
       else if (hi->poolidx >= 0 && hi->poolidx < hosttable_size
-               && hosttable[hi->poolidx] && hosttable[hi->poolidx]->dead)
+               && hosttable[hi->poolidx] && !host_is_alive (hosttable[hi->poolidx], curtime))
         hi->poolidx = -1;
 
       /* Select a host if needed.  */
@@ -599,7 +622,7 @@ map_host (ctrl_t ctrl, const char *name, const char *srvtag, int force_reselect,
       free_dns_addrinfo (aibuf);
     }
 
-  if (hi->dead)
+  if (!host_is_alive (hi, curtime))
     {
       log_error ("host '%s' marked as dead\n", hi->name);
       if (r_httphost)
@@ -704,7 +727,8 @@ ks_hkp_mark_host (ctrl_t ctrl, const char *name, int alive)
 {
   gpg_error_t err = 0;
   hostinfo_t hi, hi2;
-  int idx, idx2, idx3, n;
+  int idx, idx2, idx3, n, is_alive;
+  time_t curtime;
 
   if (!name || !*name || !strcmp (name, "localhost"))
     return 0;
@@ -713,13 +737,15 @@ ks_hkp_mark_host (ctrl_t ctrl, const char *name, int alive)
   if (idx == -1)
     return gpg_error (GPG_ERR_NOT_FOUND);
 
+  curtime = gnupg_get_time ();
   hi = hosttable[idx];
-  if (alive && hi->dead)
+  is_alive = host_is_alive (hi, curtime);
+  if (alive && !is_alive)
     {
       hi->dead = 0;
       err = ks_printf_help (ctrl, "marking '%s' as alive", name);
     }
-  else if (!alive && !hi->dead)
+  else if (!alive && is_alive)
     {
       hi->dead = 1;
       hi->died_at = 0; /* Manually set dead.  */
@@ -751,14 +777,15 @@ ks_hkp_mark_host (ctrl_t ctrl, const char *name, int alive)
 
           hi2 = hosttable[n];
           if (!hi2)
-            ;
-          else if (alive && hi2->dead)
+            continue;
+          is_alive = host_is_alive (hi2, curtime);
+          if (alive && !is_alive)
             {
               hi2->dead = 0;
               err = ks_printf_help (ctrl, "marking '%s' as alive",
                                     hi2->name);
             }
-          else if (!alive && !hi2->dead)
+          else if (!alive && is_alive)
             {
               hi2->dead = 1;
               hi2->died_at = 0; /* Manually set dead. */
@@ -974,34 +1001,6 @@ ks_hkp_resolve (ctrl_t ctrl, parsed_uri_t uri)
 }
 
 
-/* Housekeeping function called from the housekeeping thread.  It is
-   used to mark dead hosts alive so that they may be tried again after
-   some time.  */
-void
-ks_hkp_housekeeping (time_t curtime)
-{
-  int idx;
-  hostinfo_t hi;
-
-  for (idx=0; idx < hosttable_size; idx++)
-    {
-      hi = hosttable[idx];
-      if (!hi)
-        continue;
-      if (!hi->dead)
-        continue;
-      if (!hi->died_at)
-        continue; /* Do not resurrect manually shot hosts.  */
-      if (hi->died_at + RESURRECT_INTERVAL <= curtime
-          || hi->died_at > curtime)
-        {
-          hi->dead = 0;
-          log_info ("resurrected host '%s'", hi->name);
-        }
-    }
-}
-
-
 /* Reload (SIGHUP) action for this module.  We mark all host alive
  * even those which have been manually shot.  */
 void
