From: Werner Koch <wk@gnupg.org>
Date: Tue, 24 Jan 2017 16:36:28 +0100
Subject: dirmngr: New option --disable-ipv4.

* dirmngr/dirmngr.c (oDisableIPv4): New const.
(opts): New option --disable-ipv4.
(parse_rereadable_options): Set that option.
* dirmngr/dirmngr.h (opt): New field 'disable_ipv4'.
* dirmngr/dns-stuff.c (opt_disable_ipv4): bew var.
(set_dns_disable_ipv4): New.
(resolve_name_standard): Skip v4 addresses when OPT_DISABLE_IPV4 is
set.
* dirmngr/ks-engine-hkp.c (map_host): Ditto.
(send_request): Pass HTTP_FLAG_IGNORE_IPv4 if opt.disable_v4 is set.
* dirmngr/crlfetch.c (crl_fetch): Ditto.
* dirmngr/ks-engine-finger.c (ks_finger_fetch): Ditto.
* dirmngr/ks-engine-http.c (ks_http_fetch): Ditto.
* dirmngr/ocsp.c (do_ocsp_request): Ditto.

Signed-off-by: Werner Koch <wk@gnupg.org>
(cherry picked from commit 72736af86a501592d974d46ff754a63959e183bd)
---
 dirmngr/crlfetch.c         |  4 +++-
 dirmngr/dirmngr.c          |  5 +++++
 dirmngr/dirmngr.h          |  1 +
 dirmngr/dns-stuff.c        | 15 +++++++++++++++
 dirmngr/dns-stuff.h        |  4 ++++
 dirmngr/ks-engine-finger.c |  4 +++-
 dirmngr/ks-engine-hkp.c    |  8 ++++++--
 dirmngr/ks-engine-http.c   |  3 ++-
 dirmngr/ocsp.c             |  3 ++-
 doc/dirmngr.texi           |  5 +++++
 10 files changed, 46 insertions(+), 6 deletions(-)

diff --git a/dirmngr/crlfetch.c b/dirmngr/crlfetch.c
index 8fe6e0b1b..aa82137f7 100644
--- a/dirmngr/crlfetch.c
+++ b/dirmngr/crlfetch.c
@@ -198,7 +198,9 @@ crl_fetch (ctrl_t ctrl, const char *url, ksba_reader_t *reader)
         err = http_open_document (&hd, url, NULL,
                                   ((opt.honor_http_proxy? HTTP_FLAG_TRY_PROXY:0)
                                    |(DBG_LOOKUP? HTTP_FLAG_LOG_RESP:0)
-                                   |(opt.use_tor? HTTP_FLAG_FORCE_TOR:0)),
+                                   |(opt.use_tor? HTTP_FLAG_FORCE_TOR:0)
+                                   |(opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4:0)
+                                   ),
                                   ctrl->http_proxy, NULL, NULL, NULL);
 
       switch ( err? 99999 : http_get_status_code (hd) )
diff --git a/dirmngr/dirmngr.c b/dirmngr/dirmngr.c
index 8d9de9e5a..83356c94c 100644
--- a/dirmngr/dirmngr.c
+++ b/dirmngr/dirmngr.c
@@ -111,6 +111,7 @@ enum cmd_and_opt_values {
   oBatch,
   oDisableHTTP,
   oDisableLDAP,
+  oDisableIPv4,
   oIgnoreLDAPDP,
   oIgnoreHTTPDP,
   oIgnoreOCSPSvcUrl,
@@ -224,6 +225,8 @@ static ARGPARSE_OPTS opts[] = {
 
   ARGPARSE_s_n (oUseTor, "use-tor", N_("route all network traffic via Tor")),
 
+  ARGPARSE_s_n (oDisableIPv4, "disable-ipv4", "@"),
+
   ARGPARSE_s_s (oSocketName, "socket-name", "@"),  /* Only for debugging.  */
 
   ARGPARSE_s_u (oFakedSystemTime, "faked-system-time", "@"), /*(epoch time)*/
@@ -586,6 +589,7 @@ parse_rereadable_options (ARGPARSE_ARGS *pargs, int reread)
 
     case oDisableHTTP: opt.disable_http = 1; break;
     case oDisableLDAP: opt.disable_ldap = 1; break;
+    case oDisableIPv4: opt.disable_ipv4 = 1; break;
     case oHonorHTTPProxy: opt.honor_http_proxy = 1; break;
     case oHTTPProxy: opt.http_proxy = pargs->r.ret_str; break;
     case oLDAPProxy: opt.ldap_proxy = pargs->r.ret_str; break;
@@ -645,6 +649,7 @@ parse_rereadable_options (ARGPARSE_ARGS *pargs, int reread)
 
   set_dns_verbose (opt.verbose, !!DBG_DNS);
   http_set_verbose (opt.verbose, !!DBG_NETWORK);
+  set_dns_disable_ipv4 (opt.disable_ipv4);
 
   return 1; /* Handled. */
 }
diff --git a/dirmngr/dirmngr.h b/dirmngr/dirmngr.h
index acd4c636d..fd80d7237 100644
--- a/dirmngr/dirmngr.h
+++ b/dirmngr/dirmngr.h
@@ -98,6 +98,7 @@ struct
 
   int disable_http;       /* Do not use HTTP at all.  */
   int disable_ldap;       /* Do not use LDAP at all.  */
+  int disable_ipv4;       /* Do not use leagacy IP addresses.  */
   int honor_http_proxy;   /* Honor the http_proxy env variable. */
   const char *http_proxy; /* The default HTTP proxy.  */
   const char *ldap_proxy; /* Use given LDAP proxy.  */
diff --git a/dirmngr/dns-stuff.c b/dirmngr/dns-stuff.c
index 9347196b3..ad19fc2ce 100644
--- a/dirmngr/dns-stuff.c
+++ b/dirmngr/dns-stuff.c
@@ -119,6 +119,10 @@ static int opt_debug;
 /* The timeout in seconds for libdns requests.  */
 static int opt_timeout;
 
+/* The flag to disable IPv4 access - right now this only skips
+ * returned A records.  */
+static int opt_disable_ipv4;
+
 /* If set force the use of the standard resolver.  */
 static int standard_resolver;
 
@@ -227,6 +231,15 @@ set_dns_verbose (int verbose, int debug)
 }
 
 
+/* Set the Disable-IPv4 flag so that the name resolver does not return
+ * A addresses.  */
+void
+set_dns_disable_ipv4 (int yes)
+{
+  opt_disable_ipv4 = !!yes;
+}
+
+
 /* Set the timeout for libdns requests to SECONDS.  A value of 0 sets
  * the default timeout and values are capped at 10 minutes.  */
 void
@@ -873,6 +886,8 @@ resolve_name_standard (const char *name, unsigned short port,
     {
       if (ai->ai_family != AF_INET6 && ai->ai_family != AF_INET)
         continue;
+      if (opt_disable_ipv4 && ai->ai_family == AF_INET)
+        continue;
 
       dai = xtrymalloc (sizeof *dai + ai->ai_addrlen - 1);
       dai->family = ai->ai_family;
diff --git a/dirmngr/dns-stuff.h b/dirmngr/dns-stuff.h
index d68dd1728..9eb97fd6a 100644
--- a/dirmngr/dns-stuff.h
+++ b/dirmngr/dns-stuff.h
@@ -95,6 +95,10 @@ struct srventry
 /* Set verbosity and debug mode for this module. */
 void set_dns_verbose (int verbose, int debug);
 
+/* Set the Disable-IPv4 flag so that the name resolver does not return
+ * A addresses.  */
+void set_dns_disable_ipv4 (int yes);
+
 /* Set the timeout for libdns requests to SECONDS.  */
 void set_dns_timeout (int seconds);
 
diff --git a/dirmngr/ks-engine-finger.c b/dirmngr/ks-engine-finger.c
index b1f02ad7d..114f2e9ac 100644
--- a/dirmngr/ks-engine-finger.c
+++ b/dirmngr/ks-engine-finger.c
@@ -83,7 +83,9 @@ ks_finger_fetch (ctrl_t ctrl, parsed_uri_t uri, estream_t *r_fp)
   *server++ = 0;
 
   err = http_raw_connect (&http, server, 79,
-                          (opt.use_tor? HTTP_FLAG_FORCE_TOR : 0), NULL);
+                          ((opt.use_tor? HTTP_FLAG_FORCE_TOR : 0)
+                           | (opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4 : 0)),
+                          NULL);
   if (err)
     {
       xfree (name);
diff --git a/dirmngr/ks-engine-hkp.c b/dirmngr/ks-engine-hkp.c
index 2b90441e2..dad83efcd 100644
--- a/dirmngr/ks-engine-hkp.c
+++ b/dirmngr/ks-engine-hkp.c
@@ -526,6 +526,8 @@ map_host (ctrl_t ctrl, const char *name, const char *srvtag, int force_reselect,
             {
               if (ai->family != AF_INET && ai->family != AF_INET6)
                 continue;
+              if (opt.disable_ipv4 && ai->family == AF_INET)
+                continue;
               dirmngr_tick (ctrl);
 
               add_host (name, is_pool, ai, 0, reftbl, reftblsize, &refidx);
@@ -607,7 +609,8 @@ map_host (ctrl_t ctrl, const char *name, const char *srvtag, int force_reselect,
         {
           for (ai = aibuf; ai; ai = ai->next)
             {
-              if (ai->family == AF_INET6 || ai->family == AF_INET)
+              if (ai->family == AF_INET6
+                  || (!opt.disable_ipv4 && ai->family == AF_INET))
                 {
                   err = resolve_dns_addr (ai->addr, ai->addrlen, 0, &host);
                   if (!err)
@@ -1058,7 +1061,8 @@ send_request (ctrl_t ctrl, const char *request, const char *hostportstr,
                    /* fixme: AUTH */ NULL,
                    (httpflags
                     |(opt.honor_http_proxy? HTTP_FLAG_TRY_PROXY:0)
-                    |(opt.use_tor? HTTP_FLAG_FORCE_TOR:0)),
+                    |(opt.use_tor? HTTP_FLAG_FORCE_TOR:0)
+                    |(opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4 : 0)),
                    ctrl->http_proxy,
                    session,
                    NULL,
diff --git a/dirmngr/ks-engine-http.c b/dirmngr/ks-engine-http.c
index 858c943ea..dbbf4bb79 100644
--- a/dirmngr/ks-engine-http.c
+++ b/dirmngr/ks-engine-http.c
@@ -88,7 +88,8 @@ ks_http_fetch (ctrl_t ctrl, const char *url, estream_t *r_fp)
                    /* httphost */ NULL,
                    /* fixme: AUTH */ NULL,
                    ((opt.honor_http_proxy? HTTP_FLAG_TRY_PROXY:0)
-                    | (opt.use_tor? HTTP_FLAG_FORCE_TOR:0)),
+                    | (opt.use_tor? HTTP_FLAG_FORCE_TOR:0)
+                    | (opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4 : 0)),
                    ctrl->http_proxy,
                    session,
                    NULL,
diff --git a/dirmngr/ocsp.c b/dirmngr/ocsp.c
index 9127cf754..b46c78567 100644
--- a/dirmngr/ocsp.c
+++ b/dirmngr/ocsp.c
@@ -174,7 +174,8 @@ do_ocsp_request (ctrl_t ctrl, ksba_ocsp_t ocsp, gcry_md_hd_t md,
  once_more:
   err = http_open (&http, HTTP_REQ_POST, url, NULL, NULL,
                    ((opt.honor_http_proxy? HTTP_FLAG_TRY_PROXY:0)
-                    | (opt.use_tor? HTTP_FLAG_FORCE_TOR:0)),
+                    | (opt.use_tor? HTTP_FLAG_FORCE_TOR:0)
+                    | (opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4 : 0)),
                    ctrl->http_proxy, NULL, NULL, NULL);
   if (err)
     {
diff --git a/doc/dirmngr.texi b/doc/dirmngr.texi
index dd104273d..b00c2d377 100644
--- a/doc/dirmngr.texi
+++ b/doc/dirmngr.texi
@@ -312,6 +312,11 @@ not be used a different one can be given using this option.  Note that
 a numerical IP address must be given (IPv6 or IPv4) and that no error
 checking is done for @var{ipaddr}.
 
+@item --disable-ipv4
+@opindex disable-ipv4
+Disable the use of all IPv4 addresses.  This option is mainly useful
+for debugging.
+
 @item --disable-ldap
 @opindex disable-ldap
 Entirely disables the use of LDAP.
