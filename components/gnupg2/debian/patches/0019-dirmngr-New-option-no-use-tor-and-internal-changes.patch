From: Werner Koch <wk@gnupg.org>
Date: Wed, 1 Feb 2017 17:54:14 +0100
Subject: dirmngr: New option --no-use-tor and internal changes.

* dirmngr/dns-stuff.c (disable_dns_tormode): New.
* dirmngr/dirmngr.c (oNoUseTor): New const.
(opts): New option --no-use-tor.
(tor_mode): New var.
(parse_rereadable_options): Change to use TOR_MODE.
(dirmngr_use_tor): New.
(set_tor_mode): Call disable_dns_tormode.  Implement oNoUseTor.
* dirmngr/dirmngr.h (opt): Remove field 'use_tor'.  Replace all
references by a call to dirmngr_use_tor().
* dirmngr/server.c (cmd_getinfo): Distinguish between default and
enforced TOR_MODE.
--

This patch replaces the global variable opt.use_tar by a function
testing a file local mode flag.  This patch prepares for a
use-tor-if-available mode.

GnuPG-bug-id: 2935
Signed-off-by: Werner Koch <wk@gnupg.org>
(cherry picked from commit 7440119e729d3fdedda8a9b44b70f8959beea8d7)
---
 dirmngr/crlfetch.c         | 10 +++++-----
 dirmngr/dirmngr.c          | 46 +++++++++++++++++++++++++++++++++++++++++++---
 dirmngr/dirmngr.h          |  3 +--
 dirmngr/dns-stuff.c        |  8 ++++++++
 dirmngr/dns-stuff.h        |  1 +
 dirmngr/ks-engine-finger.c |  2 +-
 dirmngr/ks-engine-hkp.c    |  6 +++---
 dirmngr/ks-engine-http.c   |  2 +-
 dirmngr/ks-engine-ldap.c   |  6 +++---
 dirmngr/ocsp.c             |  4 ++--
 dirmngr/server.c           | 10 +++++++---
 11 files changed, 75 insertions(+), 23 deletions(-)

diff --git a/dirmngr/crlfetch.c b/dirmngr/crlfetch.c
index aa82137f7..337fe6e4d 100644
--- a/dirmngr/crlfetch.c
+++ b/dirmngr/crlfetch.c
@@ -198,7 +198,7 @@ crl_fetch (ctrl_t ctrl, const char *url, ksba_reader_t *reader)
         err = http_open_document (&hd, url, NULL,
                                   ((opt.honor_http_proxy? HTTP_FLAG_TRY_PROXY:0)
                                    |(DBG_LOOKUP? HTTP_FLAG_LOG_RESP:0)
-                                   |(opt.use_tor? HTTP_FLAG_FORCE_TOR:0)
+                                   |(dirmngr_use_tor()? HTTP_FLAG_FORCE_TOR:0)
                                    |(opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4:0)
                                    ),
                                   ctrl->http_proxy, NULL, NULL, NULL);
@@ -292,7 +292,7 @@ crl_fetch (ctrl_t ctrl, const char *url, ksba_reader_t *reader)
                      "LDAP");
           err = gpg_error (GPG_ERR_NOT_SUPPORTED);
         }
-      else if (opt.use_tor)
+      else if (dirmngr_use_tor ())
         {
           /* For now we do not support LDAP over Tor.  */
           log_error (_("CRL access not possible due to Tor mode\n"));
@@ -318,7 +318,7 @@ crl_fetch (ctrl_t ctrl, const char *url, ksba_reader_t *reader)
 gpg_error_t
 crl_fetch_default (ctrl_t ctrl, const char *issuer, ksba_reader_t *reader)
 {
-  if (opt.use_tor)
+  if (dirmngr_use_tor ())
     {
       /* For now we do not support LDAP over Tor.  */
       log_error (_("CRL access not possible due to Tor mode\n"));
@@ -350,7 +350,7 @@ crl_fetch_default (ctrl_t ctrl, const char *issuer, ksba_reader_t *reader)
 gpg_error_t
 ca_cert_fetch (ctrl_t ctrl, cert_fetch_context_t *context, const char *dn)
 {
-  if (opt.use_tor)
+  if (dirmngr_use_tor ())
     {
       /* For now we do not support LDAP over Tor.  */
       log_error (_("CRL access not possible due to Tor mode\n"));
@@ -377,7 +377,7 @@ gpg_error_t
 start_cert_fetch (ctrl_t ctrl, cert_fetch_context_t *context,
                   strlist_t patterns, const ldap_server_t server)
 {
-  if (opt.use_tor)
+  if (dirmngr_use_tor ())
     {
       /* For now we do not support LDAP over Tor.  */
       log_error (_("CRL access not possible due to Tor mode\n"));
diff --git a/dirmngr/dirmngr.c b/dirmngr/dirmngr.c
index 83356c94c..43e9cbd07 100644
--- a/dirmngr/dirmngr.c
+++ b/dirmngr/dirmngr.c
@@ -138,6 +138,7 @@ enum cmd_and_opt_values {
   oHTTPWrapperProgram,
   oIgnoreCertExtension,
   oUseTor,
+  oNoUseTor,
   oKeyServer,
   oNameServer,
   oDisableCheckOwnSocket,
@@ -224,6 +225,7 @@ static ARGPARSE_OPTS opts[] = {
                 N_("|FILE|use the CA certificates in FILE for HKP over TLS")),
 
   ARGPARSE_s_n (oUseTor, "use-tor", N_("route all network traffic via Tor")),
+  ARGPARSE_s_n (oNoUseTor, "no-use-tor", "@"),
 
   ARGPARSE_s_n (oDisableIPv4, "disable-ipv4", "@"),
 
@@ -300,6 +302,16 @@ static volatile int shutdown_pending;
 /* Flags to indicate that we shall not watch our own socket. */
 static int disable_check_own_socket;
 
+/* Flag to control the Tor mode.  */
+static enum
+  { TOR_MODE_AUTO = 0,  /* Switch to NO or YES         */
+    TOR_MODE_NEVER,     /* Never use Tor.              */
+    TOR_MODE_NO,        /* Do not use Tor              */
+    TOR_MODE_YES,       /* Use Tor                     */
+    TOR_MODE_FORCE      /* Force using Tor             */
+  } tor_mode;
+
+
 /* Counter for the active connections.  */
 static int active_connections;
 
@@ -475,7 +487,7 @@ set_debug (void)
 static void
 set_tor_mode (void)
 {
-  if (opt.use_tor)
+  if (dirmngr_use_tor ())
     {
       /* Enable Tor mode and when called again force a new curcuit
        * (e.g. on SIGHUP).  */
@@ -486,6 +498,26 @@ set_tor_mode (void)
           log_info ("(is your Libassuan recent enough?)\n");
         }
     }
+  else
+    disable_dns_tormode ();
+}
+
+
+/* Return true if Tor shall be used.  */
+int
+dirmngr_use_tor (void)
+{
+  if (tor_mode == TOR_MODE_AUTO)
+    {
+      /* FIXME: Figure out whether Tor is running.  */
+    }
+
+  if (tor_mode == TOR_MODE_FORCE)
+    return 2; /* Use Tor (using 2 to indicate force mode) */
+  else if (tor_mode == TOR_MODE_YES)
+    return 1; /* Use Tor */
+  else
+    return 0; /* Do not use Tor.  */
 }
 
 
@@ -548,7 +580,9 @@ parse_rereadable_options (ARGPARSE_ARGS *pargs, int reread)
       FREE_STRLIST (opt.ignored_cert_extensions);
       http_register_tls_ca (NULL);
       FREE_STRLIST (opt.keyserver);
-      /* Note: We do not allow resetting of opt.use_tor at runtime.  */
+      /* Note: We do not allow resetting of TOR_MODE_FORCE at runtime.  */
+      if (tor_mode != TOR_MODE_FORCE)
+        tor_mode = TOR_MODE_AUTO;
       disable_check_own_socket = 0;
       enable_standard_resolver (0);
       set_dns_timeout (0);
@@ -625,7 +659,13 @@ parse_rereadable_options (ARGPARSE_ARGS *pargs, int reread)
       add_to_strlist (&opt.ignored_cert_extensions, pargs->r.ret_str);
       break;
 
-    case oUseTor: opt.use_tor = 1; break;
+    case oUseTor:
+      tor_mode = TOR_MODE_FORCE;
+      break;
+    case oNoUseTor:
+      if (tor_mode != TOR_MODE_FORCE)
+        tor_mode = TOR_MODE_NEVER;
+      break;
 
     case oStandardResolver: enable_standard_resolver (1); break;
     case oRecursiveResolver: enable_recursive_resolver (1); break;
diff --git a/dirmngr/dirmngr.h b/dirmngr/dirmngr.h
index fd80d7237..6a4fd003f 100644
--- a/dirmngr/dirmngr.h
+++ b/dirmngr/dirmngr.h
@@ -91,7 +91,6 @@ struct
                                  program.  */
 
   int running_detached; /* We are running in detached mode.  */
-  int use_tor;          /* Tor mode has been enabled.  */
   int allow_version_check; /* --allow-version-check is active.  */
 
   int force;          /* Force loading outdated CRLs. */
@@ -191,7 +190,7 @@ void dirmngr_init_default_ctrl (ctrl_t ctrl);
 void dirmngr_deinit_default_ctrl (ctrl_t ctrl);
 void dirmngr_sighup_action (void);
 const char* dirmngr_get_current_socket_name (void);
-
+int dirmngr_use_tor (void);
 
 /*-- Various housekeeping functions.  --*/
 void ks_hkp_reload (void);
diff --git a/dirmngr/dns-stuff.c b/dirmngr/dns-stuff.c
index ad19fc2ce..52f011a00 100644
--- a/dirmngr/dns-stuff.c
+++ b/dirmngr/dns-stuff.c
@@ -222,6 +222,14 @@ enable_dns_tormode (int new_circuit)
 }
 
 
+/* Disable tor mode.  */
+void
+disable_dns_tormode (void)
+{
+  tor_mode = 0;
+}
+
+
 /* Set verbosity and debug mode for this module. */
 void
 set_dns_verbose (int verbose, int debug)
diff --git a/dirmngr/dns-stuff.h b/dirmngr/dns-stuff.h
index 9eb97fd6a..9b8303c3b 100644
--- a/dirmngr/dns-stuff.h
+++ b/dirmngr/dns-stuff.h
@@ -120,6 +120,7 @@ int recursive_resolver_p (void);
 /* Put this module eternally into Tor mode.  When called agained with
  * NEW_CIRCUIT request a new TOR circuit for the next DNS query.  */
 void enable_dns_tormode (int new_circuit);
+void disable_dns_tormode (void);
 
 /* Change the default IP address of the nameserver to IPADDR.  The
    address needs to be a numerical IP address and will be used for the
diff --git a/dirmngr/ks-engine-finger.c b/dirmngr/ks-engine-finger.c
index 114f2e9ac..811b72de4 100644
--- a/dirmngr/ks-engine-finger.c
+++ b/dirmngr/ks-engine-finger.c
@@ -83,7 +83,7 @@ ks_finger_fetch (ctrl_t ctrl, parsed_uri_t uri, estream_t *r_fp)
   *server++ = 0;
 
   err = http_raw_connect (&http, server, 79,
-                          ((opt.use_tor? HTTP_FLAG_FORCE_TOR : 0)
+                          ((dirmngr_use_tor ()? HTTP_FLAG_FORCE_TOR : 0)
                            | (opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4 : 0)),
                           NULL);
   if (err)
diff --git a/dirmngr/ks-engine-hkp.c b/dirmngr/ks-engine-hkp.c
index 858cd2f26..be8b08333 100644
--- a/dirmngr/ks-engine-hkp.c
+++ b/dirmngr/ks-engine-hkp.c
@@ -285,7 +285,7 @@ tor_not_running_p (ctrl_t ctrl)
 {
   assuan_fd_t sock;
 
-  if (!opt.use_tor)
+  if (!dirmngr_use_tor ())
     return 0;
 
   sock = assuan_sock_connect_byname (NULL, 0, 0, NULL, ASSUAN_SOCK_TOR);
@@ -1088,7 +1088,7 @@ send_request (ctrl_t ctrl, const char *request, const char *hostportstr,
                    /* fixme: AUTH */ NULL,
                    (httpflags
                     |(opt.honor_http_proxy? HTTP_FLAG_TRY_PROXY:0)
-                    |(opt.use_tor? HTTP_FLAG_FORCE_TOR:0)
+                    |(dirmngr_use_tor ()? HTTP_FLAG_FORCE_TOR:0)
                     |(opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4 : 0)),
                    ctrl->http_proxy,
                    session,
@@ -1245,7 +1245,7 @@ handle_send_request_error (ctrl_t ctrl, gpg_error_t err, const char *request,
       break;
 
     case GPG_ERR_EACCES:
-      if (opt.use_tor)
+      if (dirmngr_use_tor ())
         {
           log_info ("(Tor configuration problem)\n");
           dirmngr_status (ctrl, "WARNING", "tor_config_problem 0",
diff --git a/dirmngr/ks-engine-http.c b/dirmngr/ks-engine-http.c
index dbbf4bb79..69642ff98 100644
--- a/dirmngr/ks-engine-http.c
+++ b/dirmngr/ks-engine-http.c
@@ -88,7 +88,7 @@ ks_http_fetch (ctrl_t ctrl, const char *url, estream_t *r_fp)
                    /* httphost */ NULL,
                    /* fixme: AUTH */ NULL,
                    ((opt.honor_http_proxy? HTTP_FLAG_TRY_PROXY:0)
-                    | (opt.use_tor? HTTP_FLAG_FORCE_TOR:0)
+                    | (dirmngr_use_tor ()? HTTP_FLAG_FORCE_TOR:0)
                     | (opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4 : 0)),
                    ctrl->http_proxy,
                    session,
diff --git a/dirmngr/ks-engine-ldap.c b/dirmngr/ks-engine-ldap.c
index 6d520e98e..b7aa7cc65 100644
--- a/dirmngr/ks-engine-ldap.c
+++ b/dirmngr/ks-engine-ldap.c
@@ -850,7 +850,7 @@ ks_ldap_get (ctrl_t ctrl, parsed_uri_t uri, const char *keyspec,
 
   (void) ctrl;
 
-  if (opt.use_tor)
+  if (dirmngr_use_tor ())
     {
       /* For now we do not support LDAP over Tor.  */
       log_error (_("LDAP access not possible due to Tor mode\n"));
@@ -1033,7 +1033,7 @@ ks_ldap_search (ctrl_t ctrl, parsed_uri_t uri, const char *pattern,
 
   (void) ctrl;
 
-  if (opt.use_tor)
+  if (dirmngr_use_tor ())
     {
       /* For now we do not support LDAP over Tor.  */
       log_error (_("LDAP access not possible due to Tor mode\n"));
@@ -1909,7 +1909,7 @@ ks_ldap_put (ctrl_t ctrl, parsed_uri_t uri,
   /* Elide a warning.  */
   (void) ctrl;
 
-  if (opt.use_tor)
+  if (dirmngr_use_tor ())
     {
       /* For now we do not support LDAP over Tor.  */
       log_error (_("LDAP access not possible due to Tor mode\n"));
diff --git a/dirmngr/ocsp.c b/dirmngr/ocsp.c
index b46c78567..aff8e3288 100644
--- a/dirmngr/ocsp.c
+++ b/dirmngr/ocsp.c
@@ -132,7 +132,7 @@ do_ocsp_request (ctrl_t ctrl, ksba_ocsp_t ocsp, gcry_md_hd_t md,
 
   (void)ctrl;
 
-  if (opt.use_tor)
+  if (dirmngr_use_tor ())
     {
       /* For now we do not allow OCSP via Tor due to possible privacy
          concerns.  Needs further research.  */
@@ -174,7 +174,7 @@ do_ocsp_request (ctrl_t ctrl, ksba_ocsp_t ocsp, gcry_md_hd_t md,
  once_more:
   err = http_open (&http, HTTP_REQ_POST, url, NULL, NULL,
                    ((opt.honor_http_proxy? HTTP_FLAG_TRY_PROXY:0)
-                    | (opt.use_tor? HTTP_FLAG_FORCE_TOR:0)
+                    | (dirmngr_use_tor ()? HTTP_FLAG_FORCE_TOR:0)
                     | (opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4 : 0)),
                    ctrl->http_proxy, NULL, NULL, NULL);
   if (err)
diff --git a/dirmngr/server.c b/dirmngr/server.c
index c9c4ad437..bca3a61e4 100644
--- a/dirmngr/server.c
+++ b/dirmngr/server.c
@@ -625,7 +625,7 @@ option_handler (assuan_context_t ctx, const char *key, const char *value)
   else if (!strcmp (key, "honor-keyserver-url-used"))
     {
       /* Return an error if we are running in Tor mode.  */
-      if (opt.use_tor)
+      if (dirmngr_use_tor ())
         err = gpg_error (GPG_ERR_FORBIDDEN);
     }
   else
@@ -2338,14 +2338,18 @@ cmd_getinfo (assuan_context_t ctx, char *line)
     }
   else if (!strcmp (line, "tor"))
     {
-      if (opt.use_tor)
+      int use_tor;
+
+      use_tor = dirmngr_use_tor ();
+      if (use_tor)
         {
           if (!is_tor_running (ctrl))
             err = assuan_write_status (ctx, "NO_TOR", "Tor not running");
           else
             err = 0;
           if (!err)
-            assuan_set_okay_line (ctx, "- Tor mode is enabled");
+            assuan_set_okay_line (ctx, use_tor == 1 ? "- Tor mode is enabled"
+                                  /**/              : "- Tor mode is enforced");
         }
       else
         err = set_error (GPG_ERR_FALSE, "Tor mode is NOT enabled");
