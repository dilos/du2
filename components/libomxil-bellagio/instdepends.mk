INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += autotools-dev
# libasound2-dev - linux
INSTDEPENDS += libmad0-dev
INSTDEPENDS += libvorbis-dev
INSTDEPENDS += doxygen
INSTDEPENDS += libjs-jquery
