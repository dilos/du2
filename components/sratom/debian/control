Source: sratom
Section: libs
Priority: optional
Maintainer: Debian Multimedia Maintainers <pkg-multimedia-maintainers@lists.alioth.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>,
 Jaromír Mikeš <mira.mikes@seznam.cz>
Build-Depends:
 debhelper (>= 9),
 libserd-dev (>= 0.24.0~),
 libsord-dev (>= 0.12.0~dfsg0),
 lv2-dev,
 pkg-config,
 python
Build-Depends-Indep:
 doxygen,
 graphviz
Standards-Version: 3.9.8
Homepage: http://drobilla.net/software/sratom/
Vcs-Git: https://anonscm.debian.org/git/pkg-multimedia/sratom.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-multimedia/sratom.git

Package: libsratom-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libserd-dev (>= 0.18.0~),
 libsord-dev (>= 0.12.0~dfsg0),
 libsratom-0-0 (= ${binary:Version}),
 lv2-dev,
 ${misc:Depends}
Recommends:
 pkg-config
Suggests:
 libsratom-doc
Description: library for serialising LV2 atoms to/from Turtle - development files
 Sratom is a new C library for serialising LV2 atoms to/from Turtle.
 It is intended to be a full serialisation solution for LV2 atoms,
 allowing implementations to serialise binary atoms to strings and
 read them back again. This is particularly useful for saving plugin
 state, or implementing plugin control with network transparency.
 Sratom uses Serd and Sord to do the work, it is a small library
 implemented in a single source file, suitable for direct inclusion
 in projects if avoiding a dependency is desired.
 .
 This package provides the development files for Sratom.

Package: libsratom-0-0
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: library for serialising LV2 atoms to/from Turtle
 Sratom is a new C library for serialising LV2 atoms to/from Turtle.
 It is intended to be a full serialisation solution for LV2 atoms,
 allowing implementations to serialise binary atoms to strings and
 read them back again. This is particularly useful for saving plugin
 state, or implementing plugin control with network transparency.
 Sratom uses Serd and Sord to do the work, it is a small library
 implemented in a single source file, suitable for direct inclusion
 in projects if avoiding a dependency is desired.

Package: libsratom-doc
Section: doc
Architecture: all
Enhances:
 libsratom-dev
Depends:
 ${misc:Depends}
Description: library for serialising LV2 atoms to/from Turtle - documentation
 Sratom is a new C library for serialising LV2 atoms to/from Turtle.
 It is intended to be a full serialisation solution for LV2 atoms,
 allowing implementations to serialise binary atoms to strings and
 read them back again. This is particularly useful for saving plugin
 state, or implementing plugin control with network transparency.
 Sratom uses Serd and Sord to do the work, it is a small library
 implemented in a single source file, suitable for direct inclusion
 in projects if avoiding a dependency is desired.
 .
 This package provides the developer's reference for sratom.
