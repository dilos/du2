Source: swt-gtk
Section: libs
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Adrian Perez <blackxored@debian.org>,
           أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@sabily.org>,
           Jakub Adam <jakub.adam@ktknet.cz>,
           Niels Thykier <niels@thykier.net>
Build-Depends: default-jdk, gcj-native-helper [linux-any], libgnomeui-dev, libxtst-dev,
 libxcb-render-util0-dev, libxt-dev, libglib2.0-dev (>= 2.35),
 libgl1-mesa-dev | libgl-dev, libglu1-mesa-dev | libglu-dev,
 fastjar, cdbs, patchutils, debhelper (>= 9), libwebkitgtk-dev, javahelper,
 dpkg-dev (>= 1.16.1~)
Standards-Version: 3.9.4
Vcs-Git: git://anonscm.debian.org/pkg-java/swt-gtk.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-java/swt-gtk.git
Homepage: http://www.eclipse.org/swt/

Package: libswt-gtk-3-java
Architecture: any
Section: java
Depends: libswt-gtk-3-jni (= ${binary:Version}), ${misc:Depends}
Suggests: libswt-gtk-3-java-gcj
Conflicts: libswt3.2-gtk-jni,
           libswt3.2-gtk-java,
           libswt-gtk-3.4-java,
           libswt-gtk-3.4-jni,
           libswt-gtk-3.5-jni,
           libswt-gnome-gtk-3.5-jni,
           libswt-cairo-gtk-3.5-jni,
           libswt-mozilla-gtk-3.5-jni
Breaks: libswt-gnome-gtk-3-jni (<< 3.8.0~rc2-2~),
        libswt-cairo-gtk-3-jni (<< 3.8.0~rc2-2~),
        libswt-glx-gtk-3-jni (<< 3.8.0~rc2-2~),
        libswt-webkit-gtk-3-jni (<< 3.8.0~rc2-2~),
        eclipse-rcp (<< 3.8)
Description: Standard Widget Toolkit for GTK+ Java library
 The Standard Widget Toolkit (SWT) is a fast and rich Java GUI toolkit.
 For platforms with existing SWT implementations it provides efficient, portable
 and fast access to native controls and user interface facilities.

Package: libswt-gtk-3-jni
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: libswt-gnome-gtk-3-jni
Breaks: libswt-gtk-3-java (<< ${binary:Version}), libswt-gtk-3-java (>> ${binary:Version})
Description: Standard Widget Toolkit for GTK+ JNI library
 The Standard Widget Toolkit (SWT) is a fast and rich Java GUI toolkit.
 For platforms with existing SWT implementations it provides efficient, portable
 and fast access to native controls and user interface facilities.
 .
 This package includes the JNI libraries (atk, awt, gtk, pi).

Package: libswt-gnome-gtk-3-jni
Architecture: any
Depends: libswt-gtk-3-jni (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: Standard Widget Toolkit for GTK+ GNOME JNI library
 The Standard Widget Toolkit (SWT) is a fast and rich Java GUI toolkit.
 For platforms with existing SWT implementations it provides efficient, portable
 and fast access to native controls and user interface facilities.
 .
 This package includes the GNOME JNI library.

Package: libswt-cairo-gtk-3-jni
Architecture: any
Depends: libswt-gtk-3-jni (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: Standard Widget Toolkit for GTK+ Cairo JNI library
 The Standard Widget Toolkit (SWT) is a fast and rich Java GUI toolkit.
 For platforms with existing SWT implementations it provides efficient, portable
 and fast access to native controls and user interface facilities.
 .
 This package includes the Cairo JNI libraries.

Package: libswt-glx-gtk-3-jni
Architecture: any
Depends: libswt-gtk-3-jni (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: Standard Widget Toolkit for GTK+ GLX JNI library
 The Standard Widget Toolkit (SWT) is a fast and rich Java GUI toolkit.
 For platforms with existing SWT implementations it provides efficient, portable
 and fast access to native controls and user interface facilities.
 .
 This package includes the GLX JNI libraries.

Package: libswt-webkit-gtk-3-jni
Architecture: any
Depends: libswt-gtk-3-jni (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Breaks: libswt-gtk-3-java (<< ${binary:Version}), libswt-gtk-3-java (>> ${binary:Version})
Description: Standard Widget Toolkit for GTK+ WebKit JNI library
 The Standard Widget Toolkit (SWT) is a fast and rich Java GUI toolkit.
 For platforms with existing SWT implementations it provides efficient, portable
 and fast access to native controls and user interface facilities.
 .
 This package includes the WebKit JNI libraries.

Package: libswt-gtk-3-java-gcj
Architecture: any
Section: java
Depends: ${misc:Depends}, ${shlibs:Depends},
	  libswt-cairo-gtk-3-jni (= ${binary:Version}),
	  libswt-glx-gtk-3-jni (= ${binary:Version}),
	  libswt-gnome-gtk-3-jni (= ${binary:Version}),
	  libswt-gtk-3-jni (= ${binary:Version}),
	  libswt-webkit-gtk-3-jni (= ${binary:Version})
Description: Standard Widget Toolkit for GTK+ native library
 The Standard Widget Toolkit (SWT) is a fast and rich Java GUI toolkit.
 For platforms with existing SWT implementations it provides efficient, portable
 and fast access to native controls and user interface facilities.
 .
 This package contains a native library built using gcj.
