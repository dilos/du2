#!/usr/bin/make -f

#export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
dpkg_buildflags = DEB_BUILD_MAINT_OPTIONS=$(DEB_BUILD_MAINT_OPTIONS) dpkg-buildflags
CFLAGS=$(shell $(dpkg_buildflags) --get CFLAGS) $(shell getconf LFS_CFLAGS) 
CPPFLAGS=$(shell $(dpkg_buildflags) --get CPPFLAGS)
LDFLAGS=$(shell $(dpkg_buildflags) --get LDFLAGS)

ifneq (,$(findstring stage1,$(DEB_BUILD_PROFILES)))
  DEVS=no
  bootstrap_dh_flags=-Nw3m-img
else ifeq ($(DEB_BUILD_ARCH_OS),linux)
  DEVS=x11,fb+s
else
  DEVS=x11
endif

confargs := --prefix=/usr --sysconfdir=/etc --libexecdir=/usr/lib \
	--mandir=/usr/share/man \
	--with-gc --with-ssl \
	--with-imagelib=imlib2 \
	--with-migemo="cmigemo -q -d /usr/share/cmigemo/utf-8/migemo-dict" \
	--with-editor=/usr/bin/sensible-editor \
	--with-browser=/usr/bin/sensible-browser \
	--enable-gopher \
	--enable-image=$(DEVS) \
	--enable-m17n --enable-unicode --enable-nls

builddir := .

build-indep:

build-arch: build-stamp
build-stamp:
	dh_testdir
	cd $(builddir) && dh_autotools-dev_updateconfig
	test ! -f $(builddir)/entity.h || test -f $(builddir)/entity.h.debian-bak || cp $(builddir)/entity.h $(builddir)/entity.h.debian-bak
	cd $(builddir) && LC_ALL=C CFLAGS="$(CFLAGS)" CPPFLAGS="$(CPPFLAGS)" LDFLAGS="$(LDFLAGS)" ./configure $(confargs)
	cd $(builddir) && LC_ALL=C $(MAKE) OPTS="-Wall -g -DDEBIAN"
	cd $(builddir)/po && LC_ALL=C $(MAKE) update-gmo
	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	if test -f $(builddir)/Makefile; then cd $(builddir) && $(MAKE) distclean; fi
	rm -f $(builddir)/po/*.gmo
	rm -f $(builddir)/po/stamp-po
	rm -f extract-stamp patch-stamp configure-stamp build-stamp install-stamp
	test ! -f $(builddir)/entity.h.debian-bak || mv -f $(builddir)/entity.h.debian-bak $(builddir)/entity.h
	cd $(builddir) && dh_autotools-dev_restoreconfig
	dh_clean

install: install-stamp
install-stamp: build-stamp
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs $(bootstrap_dh_flags)

#	cd $(builddir) && $(MAKE) install DESTDIR=$(CURDIR)/debian/w3m INSTALL_W3MIMGDISPLAY="install -g video -m 2755"
	cd $(builddir) && $(MAKE) install DESTDIR=$(CURDIR)/debian/w3m INSTALL_W3MIMGDISPLAY="install -g bin -m 2755"
	install -m 644 debian/w3mconfig $(CURDIR)/debian/w3m/etc/w3m/config
	install -m 644 debian/mailcap $(CURDIR)/debian/w3m/etc/w3m/mailcap
ifeq (,$(findstring stage1,$(DEB_BUILD_PROFILES)))
	mv -f debian/w3m/usr/lib/w3m/w3mimgdisplay debian/w3m-img/usr/lib/w3m/w3mimgdisplay
endif
	touch install-stamp

binary-indep:

binary-arch: install
	dh_testdir -a
	dh_testroot -a
	dh_installdocs -a $(bootstrap_dh_flags)
	-rm -f debian/w3m/usr/share/doc/w3m/README.cygwin
	-mv -f debian/w3m/usr/share/doc/w3m/README.img debian/w3m-img/usr/share/doc/w3m-img/README.img
	cp -a $(builddir)/doc-jp/* debian/w3m/usr/share/doc/w3m/ja/
	-mv -f debian/w3m/usr/share/doc/w3m/ja/README.img debian/w3m-img/usr/share/doc/w3m-img/README.img.ja
	-rm -f debian/w3m/usr/share/doc/w3m/ja/w3m.1
	-rm -f debian/w3m/usr/share/doc/w3m/ja/README.cygwin
	-rm -rf debian/w3m/usr/share/doc/w3m/ja/CVS
	cp -a $(builddir)/doc-de/* debian/w3m/usr/share/doc/w3m/de/
	-rm -f debian/w3m/usr/share/doc/w3m/de/w3m.1
	dh_installexamples -a $(bootstrap_dh_flags)
	cp -a $(builddir)/Bonus/* debian/w3m/usr/share/doc/w3m/examples/Bonus/
	-mv -f debian/w3m/usr/share/doc/w3m/examples/Bonus/README debian/w3m/usr/share/doc/w3m/examples/Bonus/README.ja
	-rm -rf debian/w3m/usr/share/doc/w3m/examples/Bonus/CVS
	cd debian/w3m/usr/share/doc/w3m/examples/Bonus && find . -type f -print | \
	while read f; do \
		mv -f $$f $$f.tmp; \
		LC_ALL=C sed -e 's:/usr/local/bin/:/usr/bin/:' \
			-e 's:/bin/env:/usr/bin/env:' \
			$$f.tmp > $$f; \
		rm -f $$f.tmp; \
	done
	cd debian/w3m/usr/share/doc/w3m && \
		mv ja/keymap.* ja/examples; mv ja/menu.* ja/examples
	dh_link usr/share/doc/w3m/examples/keymap.default usr/share/doc/w3m/keymap.default \
		usr/share/doc/w3m/examples/keymap.lynx usr/share/doc/w3m/keymap.lynx \
		usr/share/doc/w3m/ja/examples/keymap.default usr/share/doc/w3m/ja/keymap.default \
		usr/share/doc/w3m/ja/examples/keymap.lynx usr/share/doc/w3m/ja/keymap.lynx
	dh_installmenu $(bootstrap_dh_flags)
	dh_installmime $(bootstrap_dh_flags)
#	dh_installman $(bootstrap_dh_flags)
	dh_installchangelogs -a $(bootstrap_dh_flags) $(builddir)/ChangeLog
	for f in \
		debian/w3m/usr/share/doc/w3m/examples/Bonus/README.ja \
		debian/w3m/usr/share/doc/w3m/ja/FAQ.html \
		debian/w3m/usr/share/doc/w3m/ja/HISTORY \
		debian/w3m/usr/share/doc/w3m/ja/MANUAL.html \
		debian/w3m/usr/share/doc/w3m/ja/README \
		debian/w3m/usr/share/doc/w3m/ja/README.SSL \
		debian/w3m/usr/share/doc/w3m/ja/README.cookie \
		debian/w3m/usr/share/doc/w3m/ja/README.dict \
		debian/w3m/usr/share/doc/w3m/ja/README.func \
		debian/w3m/usr/share/doc/w3m/ja/README.keymap \
		debian/w3m/usr/share/doc/w3m/ja/README.m17n \
		debian/w3m/usr/share/doc/w3m/ja/README.mailcap \
		debian/w3m/usr/share/doc/w3m/ja/README.menu \
		debian/w3m/usr/share/doc/w3m/ja/README.migemo \
		debian/w3m/usr/share/doc/w3m/ja/README.mouse \
		debian/w3m/usr/share/doc/w3m/ja/README.siteconf \
		debian/w3m/usr/share/doc/w3m/ja/STORY.html \
		debian/w3m/usr/share/doc/w3m/ja/examples/menu.default \
		debian/w3m/usr/share/doc/w3m/ja/examples/menu.submenu \
		debian/w3m/usr/share/man/ja/man1/w3m.1; do \
		iconv -f euc-jp -t utf-8 "$$f" > "$$f".tmp && mv -f "$$f".tmp "$$f" || rm -f "$$f".tmp; \
	done
ifeq (,$(findstring stage1,$(DEB_BUILD_PROFILES)))
	for f in \
		debian/w3m/usr/share/doc/w3m-img/README.img.ja; do \
		iconv -f euc-jp -t utf-8 "$$f" > "$$f".tmp && mv -f "$$f".tmp "$$f" || rm -f "$$f".tmp; \
	done
endif
	for f in \
		debian/w3m/usr/share/doc/w3m/ja/README.passwd \
		debian/w3m/usr/share/doc/w3m/ja/README.pre_form \
		debian/w3m/usr/share/doc/w3m/ja/README.tab; do \
		iconv -f iso-2022-jp -t utf-8 "$$f" > "$$f".tmp && mv -f "$$f".tmp "$$f" || rm -f "$$f".tmp; \
	done
	for f in \
		debian/w3m/usr/share/doc/w3m/examples/Bonus/2ch.cgi; do \
		iconv -f cp932 -t utf-8 "$$f" > "$$f".tmp && mv -f "$$f".tmp "$$f" || rm -f "$$f".tmp; \
	done
	dh_strip -a $(bootstrap_dh_flags)
	dh_compress -a $(bootstrap_dh_flags) -XREADME.func
	dh_lintian -a $(bootstrap_dh_flags)
	dh_fixperms -a $(bootstrap_dh_flags) -Xw3mimgdisplay
	dh_installdeb -a $(bootstrap_dh_flags)
	dh_shlibdeps -a $(bootstrap_dh_flags)
	dh_gencontrol -a $(bootstrap_dh_flags)
#	dh_makeshlibs -a $(bootstrap_dh_flags)
	dh_md5sums -a $(bootstrap_dh_flags)
	dh_builddeb -a $(bootstrap_dh_flags)

source diff:                                                                  
	@echo >&2 'source and diff are obsolete - use dpkg-source -b'; false

build: build-indep build-arch
binary: binary-indep binary-arch
.PHONY: build-indep build-arch build install clean binary-indep binary-arch binary
