Source: sord
Section: libs
Priority: optional
Maintainer: Debian Multimedia Maintainers <pkg-multimedia-maintainers@lists.alioth.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>,
 Jaromír Mikeš <mira.mikes@seznam.cz>
Build-Depends:
 debhelper (>= 9),
 doxygen,
 graphviz,
 libserd-dev (>= 0.24.0~dfsg0),
 libpcre++-dev,
 pkg-config,
 python
Standards-Version: 3.9.8
Homepage: http://drobilla.net/software/sord/
Vcs-Git: https://anonscm.debian.org/git/pkg-multimedia/sord.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-multimedia/sord.git

Package: libsord-0-0
Section: utils
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Suggests:
 sordi
Description: library for storing RDF data in memory
 Sord is a lightweight C library for storing Resource
 Description Framework (RDF) data in memory.
 .
 Sord includes man pages for the library (man sord) and a
 simple command line utility (man sordi).

Package: libsord-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libserd-dev (>= 0.18.0~dfsg0),
 libsord-0-0 (= ${binary:Version}),
 ${misc:Depends}
Suggests:
 libsord-doc
Recommends:
 pkg-config
Description: library for storing RDF data in memory (development files)
 Sord is a lightweight C library for storing Resource
 Description Framework (RDF) data in memory.
 .
 This package provides the development headers.

Package: sordi
Architecture: any
Section: text
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: library for storing RDF data in memory - utilities
 Sord is a lightweight C library for storing Resource
 Description Framework (RDF) data in memory.
 .
 This package provides the 'sordi' and 'sord_validate' utilities.

Package: libsord-doc
Section: doc
Architecture: all
Enhances:
 libsord-dev
Depends:
 ${misc:Depends}
Recommends:
 libjs-jquery
Description: library for storing RDF data in memory (documentation)
 Sord is a lightweight C library for storing Resource
 Description Framework (RDF) data in memory.
 .
 This package provides the developer's reference for sord.
