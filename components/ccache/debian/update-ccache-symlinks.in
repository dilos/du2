#!/usr/bin/perl

use strict;
use warnings FATAL => "all";

my $basedir = '';
$basedir = $ENV{'BASEDIR'} if (defined($ENV{'BASEDIR'}));
$basedir = '' if ($basedir eq "/");

my $ccache_dir = "$basedir/usr/lib/ccache";
my @gcc_dirs = (
    "$basedir/usr/gcc",
    "$basedir/usr/lib/gcc",
    "$basedir/usr/lib/gcc-cross",
    "$basedir/usr/lib/%DEB_HOST_MULTIARCH%/gcc",
);
my %old_symlinks; # Current compiler names in /usr/lib/ccache
my %new_symlinks; # Compiler names that should be in /usr/lib/ccache
my @standard_names = qw(cc c++);
my $verbose = 0;

sub consider {
    my ($name) = @_;
    if (-x "$basedir/usr/bin/$name") {
        $new_symlinks{$name} = 1;
    }
}

sub consider_gcc {
    my ($prefix, $suffix) = @_;
    consider "${prefix}gcc${suffix}";
    consider "${prefix}g++${suffix}";
}

# Find existing standard compiler names.
foreach (@standard_names) {
    consider $_;
}

# Find existing GCC variants.
consider_gcc "", "";
consider_gcc "", "-4";
consider_gcc "", "-4i";
consider_gcc "", "-4.4";
consider_gcc "", "-4.4i";
consider_gcc "", "-5";
consider_gcc "", "-6";
consider_gcc "", "-6i";
consider_gcc "", "-6s";
consider_gcc "", "-6g";
consider_gcc "", "-7";
consider_gcc "", "-8";
consider_gcc "", "-8i";
consider_gcc "", "-9";
consider_gcc "", "-9i";
consider_gcc "", "-10";
consider_gcc "", "-10i";
consider_gcc "c89-", "";
consider_gcc "c99-", "";
foreach my $gcc_dir (@gcc_dirs) {
    foreach my $dir (<$gcc_dir/*>) {
        (my $kind = $dir) =~ s|.*/||;
        consider_gcc "$kind-", "";
        foreach (<$dir/*>) {
            if (! -l $_ and -d $_) {
                s|.*/||;
                consider_gcc "", "-$_";
                consider_gcc "$kind-", "-$_";
            }
        }
    }
}

# Find existing clang variants.
consider "clang";
consider "clang++";
consider "llvm-clang";

# Find existing symlinks.
foreach (<$ccache_dir/*>) {
    if (-l) {
        s|.*/||;
        $old_symlinks{$_} = 1;
    }
}

# Remove obsolete symlinks.
foreach (keys %old_symlinks) {
    if (! exists $new_symlinks{$_}) {
        print "Removing $ccache_dir/$_\n" if $verbose;
        unlink "$ccache_dir/$_";
    }
}

# Add missing symlinks.
foreach (keys %new_symlinks) {
    if (! exists $old_symlinks{$_}) {
        print "Adding $ccache_dir/$_\n" if $verbose;
        symlink "../../bin/ccache", "$ccache_dir/$_";
    }
}
