INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += libxext-dev
INSTDEPENDS += x11proto-resource-dev
INSTDEPENDS += x11proto-xext-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += quilt
INSTDEPENDS += automake
INSTDEPENDS += libtool
INSTDEPENDS += xutils-dev
