Source: vala
Section: devel
Priority: optional
Maintainer: Maintainers of Vala packages <pkg-vala-maintainers@lists.alioth.debian.org>
Uploaders: Marc-Andre Lureau <marcandre.lureau@gmail.com>,
           Loic Minier <lool@dooz.org>,
           Sebastian Dröge <slomo@debian.org>,
           Emilio Pozuelo Monfort <pochu@debian.org>,
           Sjoerd Simons <sjoerd@debian.org>,
           Sebastian Reichel <sre@debian.org>,
           Michael Biebl <biebl@debian.org>,
           Andreas Henriksson <andreas@fatal.se>
Build-Depends: debhelper (>= 9.20160114),
               dh-autoreconf,
               libglib2.0-dev (>= 2.32),
               bison (>= 2.3),
               autotools-dev,
               flex,
               gnome-pkg-tools,
               help2man,
               xsltproc,
               libdbus-1-dev,
               dbus (>= 1.8),
               libgirepository1.0-dev
Standards-Version: 3.9.8
Vcs-Git: https://anonscm.debian.org/git/pkg-vala/vala.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-vala/vala.git
Homepage: https://wiki.gnome.org/Projects/Vala/

Package: valac
Architecture: any
Depends: ${shlibs:Depends},
         libvala-0.34-0 (= ${binary:Version}),
         libglib2.0-dev (>= 2.32),
         valac-0.34-vapi,
         ${misc:Depends}
Recommends: gcc
Conflicts: valac-0.12, valac-0.14, valac-0.16, valac-0.18, valac-0.20,
           valac-0.22, valac-0.24, valac-0.26, valac-0.28, valac-0.30
Description: C# like language for the GObject system
 Vala is a new programming language that aims to bring modern programming
 language features to GNOME developers without imposing any additional
 runtime requirements and without using a different ABI compared to
 applications and libraries written in C.
 .
 valac, the Vala compiler, is a self-hosting compiler that translates
 Vala source code into C source and header files.  It uses the GObject
 type system to create classes and interfaces declared in the Vala
 source code. This package also contains the vala-gen-introspect and
 vapigen binaries that will automatically generate Vala bindings.

Package: valac-0.34-vapi
Architecture: all
Depends: ${misc:Depends}
Provides: valac-vapi
Description: C# like language for the GObject system - vapi files
 Vala is a new programming language that aims to bring modern programming
 language features to GNOME developers without imposing any additional
 runtime requirements and without using a different ABI compared to
 applications and libraries written in C.
 .
 valac, the Vala compiler, is a self-hosting compiler that translates
 Vala source code into C source and header files.  It uses the GObject
 type system to create classes and interfaces declared in the Vala
 source code. This package also contains the vala-gen-introspect and
 vapigen binaries that will automatically generate Vala bindings.
 .
 This package contains the bundled vapi files, which make some
 common libraries available for vala development.

Package: vala-0.34-doc
Architecture: all
Depends: ${misc:Depends}
Suggests: valac,
          devhelp
Provides: vala-doc
Section: doc
Multi-Arch: foreign
Description: C# like language for the GObject system - documentation
 Vala is a new programming language that aims to bring modern programming
 language features to GNOME developers without imposing any additional
 runtime requirements and without using a different ABI compared to
 applications and libraries written in C.
 .
 This package contains the Vala Reference Manual.

Package: libvala-0.34-0
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends},
         ${misc:Depends}
Section: libs
Multi-Arch: same
Description: C# like language for the GObject system - library
 Vala is a new programming language that aims to bring modern programming
 language features to GNOME developers without imposing any additional
 runtime requirements and without using a different ABI compared to
 applications and libraries written in C.
 .
 This library contains the parser used by valac. It can be used for
 building tools around Vala.

Package: libvala-0.34-dev
Architecture: any
Depends: ${misc:Depends},
         libvala-0.34-0 (= ${binary:Version}),
         libglib2.0-dev (>= 2.32)
Provides: libvala-dev
Section: libdevel
Multi-Arch: same
Description: C# like language for the GObject system - development headers
 Vala is a new programming language that aims to bring modern
 programming language features to GNOME developers without imposing
 any additional runtime requirements and without using a different ABI
 compared to applications and libraries written in C.
 .
 This package has the development library and headers for valac. These
 headers can be used for building tools around Vala.
