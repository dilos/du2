Source: python-certbot-nginx
Section: python
Priority: optional
Maintainer: Debian Let's Encrypt <team+letsencrypt@tracker.debian.org>
Uploaders: Harlan Lieberman-Berg <hlieberman@debian.org>,
           Francois Marier <francois@debian.org>
Build-Depends: debhelper (>= 11~),
               dh-python,
               python-sphinx,
               python-sphinx-rtd-theme,
               python3,
	       python3-acme (>= 0.26.0~),
               python3-certbot (>= 0.25.0~),
               python3-configargparse (>= 0.10.0),
               python3-mock,
               python3-openssl (>= 0.13),
               python3-parsedatetime (>= 1.3),
               python3-pyparsing (>= 1.5.5),
               python3-requests,
               python3-rfc3339,
               python3-setuptools (>= 1.0),
               python3-six,
               python3-sphinx (>= 1.3.1-1~),
               python3-sphinx-rtd-theme,
               python3-tz,
               python3-zope.component,
               python3-zope.interface
Standards-Version: 4.3.0
Homepage: https://letsencrypt.org/
Vcs-Git: https://salsa.debian.org/letsencrypt-team/certbot/certbot-nginx.git
Vcs-Browser: https://salsa.debian.org/letsencrypt-team/certbot/certbot-nginx
#Rules-Requires-Root: no

Package: python3-certbot-nginx
Architecture: all
Depends: nginx,
	 certbot (>= 0.25.0~),
	 ${misc:Depends},
	 ${python3:Depends}
Breaks: python-certbot-nginx (<< 0.20.0~)
Replaces: python-certbot-nginx (<< 0.20.0~)
Suggests: python-certbot-nginx-doc
Description: Nginx plugin for Certbot
 The objective of Certbot, Let's Encrypt, and the ACME (Automated
 Certificate Management Environment) protocol is to make it possible
 to set up an HTTPS server and have it automatically obtain a
 browser-trusted certificate, without any human intervention. This is
 accomplished by running a certificate management agent on the web
 server.
 .
 This agent is used to:
 .
   - Automatically prove to the Let's Encrypt CA that you control the website
   - Obtain a browser-trusted certificate and set it up on your web server
   - Keep track of when your certificate is going to expire, and renew it
   - Help you revoke the certificate if that ever becomes necessary.
 .
 This package contains the Nginx plugin to the main application.

Package: python-certbot-nginx-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: Nginx plugin documentation for Certbot
 The objective of Certbot, Let's Encrypt, and the ACME (Automated
 Certificate Management Environment) protocol is to make it possible
 to set up an HTTPS server and have it automatically obtain a
 browser-trusted certificate, without any human intervention. This is
 accomplished by running a certificate management agent on the web
 server.
 .
 This agent is used to:
 .
   - Automatically prove to the Let's Encrypt CA that you control the website
   - Obtain a browser-trusted certificate and set it up on your web server
   - Keep track of when your certificate is going to expire, and renew it
   - Help you revoke the certificate if that ever becomes necessary.
 .
 This package contains the documentation for the Nginx plugin.

Package: python-certbot-nginx
Section: oldlibs
Architecture: all
Depends: python3-certbot-nginx, ${misc:Depends}
Description: transitional dummy package
 This is a transitional dummy package for the migration of certbot
 from python2 to python3.  It can be safely removed.