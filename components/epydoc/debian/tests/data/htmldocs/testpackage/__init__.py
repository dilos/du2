"""
This is an important module that really needs to be documented.

It has a bunch of stuff in it that is not only really important, but also
really interesting too.  Because of that, we really want this information to
show up in the generated Epydoc documentation.

Besides being a pile of completely useless text, this is used as a test case
for Epydoc, to make sure that it's working.

@author: Kenneth J. Pronovici <pronovic@debian.org>
"""

__all__ = [ 'util', ]

