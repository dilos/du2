#!/usr/bin/make -f

include /usr/share/cdbs/1/rules/debhelper.mk
include /usr/share/cdbs/1/class/gnome.mk
include /usr/share/cdbs/1/rules/utils.mk
include /usr/share/gnome-pkg-tools/1/rules/uploaders.mk
include /usr/share/cdbs/1/rules/autoreconf.mk

#DEB_DH_AUTORECONF_ARGS = --as-needed

# Ensure at build time that the library has no dependencies on undefined
# symbols, and speed up loading.
#DEB_LDFLAGS_MAINT_APPEND=-Wl,-z,defs -Wl,-O1 -Wl,--as-needed
DEB_LDFLAGS_MAINT_APPEND=-Wl,-z,defs
include /usr/share/dpkg/default.mk

DEB_HOST_ARCH_OS ?= $(shell dpkg-architecture -qDEB_HOST_ARCH_OS)

common-install-arch::
	find $(DEB_DESTDIR) -name "*.la" | xargs rm -f

common-binary-post-install-arch:: list-missing

ifneq ($(DEB_HOST_ARCH_OS), hurd)
ENABLE_TDB=--enable-tdb
endif

DEB_CONFIGURE_EXTRA_FLAGS = 			\
	--libdir=\$${prefix}/lib/$(DEB_HOST_MULTIARCH) \
	--enable-pulse				\
	--enable-gstreamer			\
	--enable-gtk				\
	--enable-gtk-doc 			\
	$(ENABLE_TDB) 				\
	--with-builtin=dso

# Linux-specific flags:
ifeq ($(DEB_HOST_ARCH_OS), linux)
DEB_CONFIGURE_EXTRA_FLAGS += --enable-alsa --disable-oss
else
DEB_CONFIGURE_EXTRA_FLAGS += --disable-alsa --enable-oss
endif

DEB_DH_MAKESHLIBS_ARGS_ALL += --exclude=usr/lib/$(DEB_HOST_MULTIARCH)/gtk-2.0/modules
DEB_DH_MAKESHLIBS_ARGS_ALL += --exclude=usr/lib/$(DEB_HOST_MULTIARCH)/gtk-3.0/modules
DEB_DH_MAKESHLIBS_ARGS_ALL += --exclude=usr/lib/$(DEB_HOST_MULTIARCH)/libcanberra-0.30
DEB_DH_MAKESHLIBS_ARGS_libcanberra0 += -- -c4
DEB_DH_MAKESHLIBS_ARGS_libcanberra-gtk0 += -- -c4
DEB_DH_MAKESHLIBS_ARGS_libcanberra-gtk3-0 += -- -c4

DEB_DH_STRIP_ARGS_libcanberra0 = --dbgsym-migration='libcanberra0-dbg (<< 0.30-3~)'
DEB_DH_STRIP_ARGS_libcanberra-pulse = --dbgsym-migration='libcanberra-pulse-dbg (<< 0.30-3~)'
DEB_DH_STRIP_ARGS_libcanberra-gstreamer = --dbgsym-migration='libcanberra-gstreamer-dbg (<< 0.30-3~)'
DEB_DH_STRIP_ARGS_libcanberra-gtk0 = --dbgsym-migration='libcanberra-gtk0-dbg (<< 0.30-3~)'
DEB_DH_STRIP_ARGS_libcanberra-gtk-module = --dbgsym-migration='libcanberra-gtk-module-dbg (<< 0.30-3~)'
DEB_DH_STRIP_ARGS_libcanberra-gtk3-0 = --dbgsym-migration='libcanberra-gtk3-0-dbg (<< 0.30-3~)'
DEB_DH_STRIP_ARGS_libcanberra-gtk3-module = --dbgsym-migration='libcanberra-gtk3-module-dbg (<< 0.30-3~)'

install/libcanberra-gtk3-module::
	cp -a src/canberra-gtk-module.desktop \
	      debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gnome-settings-daemon-3.0/gtk-modules/canberra-gtk3-module.desktop
