INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += gnome-common
INSTDEPENDS += gnome-pkg-tools
INSTDEPENDS += python-all-dev
INSTDEPENDS += python3-all-dev
INSTDEPENDS += python-setuptools
INSTDEPENDS += python3-setuptools
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libcairo2-dev
INSTDEPENDS += libffi-dev
INSTDEPENDS += gobject-introspection
INSTDEPENDS += libgirepository1.0-dev
INSTDEPENDS += python-cairo-dev
INSTDEPENDS += python3-cairo-dev
INSTDEPENDS += xsltproc
# xvfb,
INSTDEPENDS += xauth
INSTDEPENDS += dbus
INSTDEPENDS += at-spi2-core
# locales
INSTDEPENDS += docbook-xsl
# see https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=844924
#               pep8,
INSTDEPENDS += pyflakes
# python-all-dbg
# python3-all-dbg
# python-apt-dbg
# python-cairo-dbg
INSTDEPENDS += gir1.2-glib-2.0
INSTDEPENDS += gir1.2-freedesktop
INSTDEPENDS += gir1.2-gtk-3.0
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dh-python
