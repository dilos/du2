Source: libkml
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Francesco Paolo Lovergine <frankie@debian.org>,
           Bas Couwenberg <sebastic@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper (>= 9),
               dh-python,
               cmake,
               default-jdk,
               libboost-dev,
               libcurl4-gnutls-dev | libcurl-ssl-dev,
               libexpat1-dev,
               libminizip-dev,
               liburiparser-dev (>= 0.7.1),
               pkg-kde-tools,
               python-dev (>= 2.6.6-3~),
               python-all-dev (>= 2.6.6-3~),
               swig,
               zlib1g-dev
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-grass/libkml.git
Vcs-Git: https://anonscm.debian.org/git/pkg-grass/libkml.git
Homepage: https://github.com/libkml/libkml
X-Python-Version: 2.7

Package: libkmlbase1
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Library to manipulate KML 2.2 OGC standard files - libkmlbase
 This is a library for use with applications that want to parse,
 generate and operate on KML, a geo-data XML variant. It is an
 implementation of the OGC KML 2.2 standard. It is written in C++ and
 bindings are available via SWIG to Java and Python.
 .
 This package contains the libkmlbase shared library.

Package: libkmlconvenience1
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Library to manipulate KML 2.2 OGC standard files - libkmlconvenience
 This is a library for use with applications that want to parse,
 generate and operate on KML, a geo-data XML variant. It is an
 implementation of the OGC KML 2.2 standard. It is written in C++ and
 bindings are available via SWIG to Java and Python.
 .
 This package contains the libkmlconvenience shared library.

Package: libkmldom1
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Library to manipulate KML 2.2 OGC standard files - libkmldom
 This is a library for use with applications that want to parse,
 generate and operate on KML, a geo-data XML variant. It is an
 implementation of the OGC KML 2.2 standard. It is written in C++ and
 bindings are available via SWIG to Java and Python.
 .
 This package contains the libkmldom shared library.

Package: libkmlengine1
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Library to manipulate KML 2.2 OGC standard files - libkmlengine
 This is a library for use with applications that want to parse,
 generate and operate on KML, a geo-data XML variant. It is an
 implementation of the OGC KML 2.2 standard. It is written in C++ and
 bindings are available via SWIG to Java and Python.
 .
 This package contains the libkmlengine shared library.

Package: libkmlregionator1
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Library to manipulate KML 2.2 OGC standard files - libkmlregionator
 This is a library for use with applications that want to parse,
 generate and operate on KML, a geo-data XML variant. It is an
 implementation of the OGC KML 2.2 standard. It is written in C++ and
 bindings are available via SWIG to Java and Python.
 .
 This package contains the libkmlregionator shared library.

Package: libkmlxsd1
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Library to manipulate KML 2.2 OGC standard files - libkmlxsd
 This is a library for use with applications that want to parse,
 generate and operate on KML, a geo-data XML variant. It is an
 implementation of the OGC KML 2.2 standard. It is written in C++ and
 bindings are available via SWIG to Java and Python.
 .
 This package contains the libkmlxsd shared library.

Package: libkml-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libkmlbase1 (= ${binary:Version}),
         libkmlconvenience1 (= ${binary:Version}),
         libkmldom1 (= ${binary:Version}),
         libkmlengine1 (= ${binary:Version}),
         libkmlregionator1 (= ${binary:Version}),
         libkmlxsd1 (= ${binary:Version}),
         libminizip-dev,
         liburiparser-dev (>= 0.7.1),
         libboost-dev,
         libexpat1-dev,
         zlib1g-dev,
         ${misc:Depends}
Description: Library to manipulate KML 2.2 OGC standard files - development files
 This is a library for use with applications that want to parse,
 generate and operate on KML, a geo-data XML variant. It is an
 implementation of the OGC KML 2.2 standard. It is written in C++ and
 bindings are available via SWIG to Java and Python.
 .
 This package contains files required to build C/C++ programs which use
 the KML library.


Package: libkml-java
Architecture: any
Section: java
Depends: libkmlbase1 (= ${binary:Version}),
         libkmlconvenience1 (= ${binary:Version}),
         libkmldom1 (= ${binary:Version}),
         libkmlengine1 (= ${binary:Version}),
         libkmlregionator1 (= ${binary:Version}),
         libkmlxsd1 (= ${binary:Version}),
         ${misc:Depends}
Pre-Depends: ${shlibs:Depends},
             ${misc:Pre-Depends}
Description: Library to manipulate KML 2.2 OGC standard files - Java package
 This is a library for use with applications that want to parse,
 generate and operate on KML, a geo-data XML variant. It is an
 implementation of the OGC KML 2.2 standard. It is written in C++ and
 bindings are available via SWIG to Java and Python.
 .
 This package contains the required packages for Java applications.

Package: python-kml
Architecture: any
Section: python
Depends: libkmlbase1 (= ${binary:Version}),
         libkmlconvenience1 (= ${binary:Version}),
         libkmldom1 (= ${binary:Version}),
         libkmlengine1 (= ${binary:Version}),
         libkmlregionator1 (= ${binary:Version}),
         libkmlxsd1 (= ${binary:Version}),
         ${shlibs:Depends},
         ${python:Depends},
         ${misc:Depends}
Provides: ${python:Provides}
Description: Library to manipulate KML 2.2 OGC standard files - Python extension
 This is a library for use with applications that want to parse,
 generate and operate on KML, a geo-data XML variant. It is an
 implementation of the OGC KML 2.2 standard. It is written in C++ and
 bindings are available via SWIG to Java and Python.
 .
 This package contains required extensions for Python applications.

