libotf (0.9.13-3+dilos1) unstable; urgency=low

  * Build for DilOS.

 -- Denis Kozadaev <denis@tambov.ru>  Mon, 08 Jan 2018 10:59:07 +0000

libotf (0.9.13-3) unstable; urgency=low

  * Enable Multi-arch support
  * debian/control:
    - Update Standards-Version to Debian Policy 3.9.6. (No changes)

 -- Harshula Jayasuriya <harshula@debian.org>  Mon, 11 Jan 2016 01:28:43 +1100

libotf (0.9.13-2) unstable; urgency=low

  * debian/control:
    - Remove DM-Upload-Allowed: yes
    - Add Vcs-Git & Vcs-Browser
    - Update Standards-Version to Debian Policy 3.9.5. (No changes)
  * Fix debian-changelog-has-wrong-weekday
  * Use dh-autoreconf (Closes: #749530).

 -- Harshula Jayasuriya <harshula@debian.org>  Tue, 09 Sep 2014 01:13:30 +1000

libotf (0.9.13-1) unstable; urgency=low

  * New upstream release 0.9.13.
  * Upstream has moved to https://savannah.nongnu.org/projects/m17n.
  * Updated to debhelper 9.
  * debian/copyright: update tarball location, homepage, copyright dates.
  * debian/rules: use dpkg-buildflags

 -- Harshula Jayasuriya <harshula@debian.org>  Sat, 15 Dec 2012 00:47:08 +1100

libotf (0.9.12-2) unstable; urgency=low

  * Update maintainer email address.
  * Update Standards-Version to Debian Policy 3.9.3. (No changes)
  * debian/control: add libotf-bin conflicts with otf-trace (#673744).
  * Fix description-synopsis-starts-with-article.
  * Fix debian-rules-missing-recommended-target.
  * Add libotf-config man page.

 -- Harshula Jayasuriya <harshula@debian.org>  Sun, 27 May 2012 16:56:42 +1000

libotf (0.9.12-1) unstable; urgency=low

  * New upstream release 0.9.12.
  * Update Standards-Version to Debian Policy 3.9.1. (no changes)
  * debian/copyright: update dates
  * debian/libotf-dev.install: don't include .la files in -dev.

 -- Harshula Jayasuriya <harshula@gmail.com>  Fri, 12 Nov 2010 01:43:46 +1100

libotf (0.9.11-1) unstable; urgency=low

  * New upstream release 0.9.11.
  * Update Standards-Version to Debian Policy 3.8.4. (no changes)
  * Update debian/libotf0.symbols.
  * debian/copyright: update dates
  * debian/source/format: 3.0 (quilt)
  * debian/libotf0.symbols: update symbols

 -- Harshula Jayasuriya <harshula@gmail.com>  Sat, 26 Jun 2010 00:19:39 +1000

libotf (0.9.10-1) unstable; urgency=low

  * New upstream release 0.9.10.
  * Update Standards-Version to Debian Policy 3.8.3. (no changes)
  * debian/control:
    - move package libotf0-dbg to section debug.
    - DM-Upload-Allowed: yes
  * debian/copyright: specify the version of the license file.
  * Add debian/libotf0.symbols (Closes: #547185).

 -- Harshula Jayasuriya <harshula@gmail.com>  Mon, 26 Oct 2009 00:49:48 +1100

libotf (0.9.9-1) unstable; urgency=low

  * New upstream release 0.9.9.
  * debian/copyright: update the copyright year.
  * debian/control: inherit Priority and Section.

 -- Harshula Jayasuriya <harshula@gmail.com>  Tue, 03 Mar 2009 21:02:52 +1100

libotf (0.9.8-1) unstable; urgency=low

  * New upstream release 0.9.8. (Closes: #466815, #476625)
  * New Maintainer. (Closes: #512068)
  * Update Standards-Version to Debian Policy 3.8.0. (no changes)
  * debian/compat: update to compat level 7.
  * debian/control:
    - Replace ${Source-Version} with ${binary:Version}.
    - Add ${misc:Depends}.
    - Add the Homepage field.
  * debian/copyright: updated to LGPL 2.1 and added the copyright message.
  * debian/rules: stop using CDBS.
  * Add debian/{docs,watch}.
  * Add the pkg-config file to the package.

 -- Harshula Jayasuriya <harshula@gmail.com>  Thu, 19 Feb 2009 16:22:31 +1100

libotf (0.9.4-1) unstable; urgency=low

  * New upstream release
  * Previous NMU acknowledged (Closes: #335135).
  * Added a new package for debugging; libotf0-dbg.
  * Updated standards version to 3.6.2.

 -- Hidetaka Iwai <tyuyu@debian.or.jp>  Sat, 28 Jan 2006 08:02:14 +0900

libotf (0.9.3-1.1) unstable; urgency=high

  * Non-maintainer upload.
  * Update automake build-dep to automake1.9 (Closes: #335135).

 -- Luk Claes <luk@debian.org>  Fri, 30 Dec 2005 16:10:36 +0100

libotf (0.9.3-1) unstable; urgency=low

  * New upstream release

 -- Hidetaka Iwai <tyuyu@debian.or.jp>  Tue, 28 Dec 2004 21:17:55 +0900

libotf (0.9.2-2) unstable; urgency=low

  * debian/control: Added build dependencies on automake and libtool
  (closes: #268571)

 -- Hidetaka Iwai <tyuyu@debian.or.jp>  Sat, 28 Aug 2004 22:13:11 +0900

libotf (0.9.2-1) unstable; urgency=low

  * New upstream release

 -- Hidetaka Iwai <tyuyu@debian.or.jp>  Thu, 26 Aug 2004 23:34:50 +0900

libotf (0.9.1-1) unstable; urgency=low

  * New upstream release

 -- Hidetaka Iwai <tyuyu@debian.or.jp>  Thu, 26 Aug 2004 23:34:19 +0900

libotf (0.9-3) unstable; urgency=low

  * debian/control: Added build dependencies on xfree86 development 
  packages (closes: #249848).

 -- Hidetaka Iwai <tyuyu@debian.or.jp>  Thu, 20 May 2004 22:08:55 +0900

libotf (0.9-2) unstable; urgency=low

  * Initial upload to debian.  Close wnpp bug (closes: #244372)

 -- Hidetaka Iwai <tyuyu@debian.or.jp>  Mon, 19 Apr 2004 20:38:16 +0900

libotf (0.9-1) unstable; urgency=low

  * Initial Release.

 -- Hidetaka Iwai <tyuyu@debian.or.jp>  Sat, 10 Apr 2004 13:47:38 +0900

