INSTDEPENDS += debhelper
INSTDEPENDS += chrpath
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += gfortran
#INSTDEPENDS += mpi-default-dev
INSTDEPENDS += libopenmpi-dev
INSTDEPENDS += texinfo
# Build-Depends-Indep:
INSTDEPENDS += ghostscript
INSTDEPENDS += transfig
