INSTDEPENDS += debhelper
INSTDEPENDS += autoconf
INSTDEPENDS += dpkg-dev
INSTDEPENDS += bison
INSTDEPENDS += flex
INSTDEPENDS += gettext
INSTDEPENDS += texinfo
INSTDEPENDS += dejagnu
INSTDEPENDS += quilt
INSTDEPENDS += chrpath
INSTDEPENDS += python3
INSTDEPENDS += file
INSTDEPENDS += xz-utils
INSTDEPENDS += lsb-release
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += lib32z1-dev
#
INSTDEPENDS += dh-exec
