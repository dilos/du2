INSTDEPENDS += debhelper
#INSTDEPENDS += libnss-wrapper
INSTDEPENDS += autoconf-archive
INSTDEPENDS += automake
INSTDEPENDS += autotools-dev
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dh-exec
INSTDEPENDS += dpkg-dev
INSTDEPENDS += libexpat-dev
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += python
INSTDEPENDS += python-dbus
INSTDEPENDS += python-gobject
INSTDEPENDS += xmlto
INSTDEPENDS += xsltproc

INSTDEPENDS += libuuid-dev
INSTDEPENDS += libbsm-dev

INSTDEPENDS += doxygen
