#!/usr/bin/make -f
#export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS := hardening=+all
DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
DEB_BUILD_ARCH_OS ?= $(shell dpkg-architecture -qDEB_BUILD_ARCH_OS)
DEB_BUILD_ARCH ?= $(shell dpkg-architecture -qDEB_BUILD_ARCH)
export CPPFLAGS += -DFALSE_SHARING_ALIGN=64

LIB_VERSION = 30
UPSTREAM_VERSION = $(shell dpkg-parsechangelog | egrep '^Version:' | cut -f 2 -d ':' | sed 's/ //' | sed 's/~dfsg.*$$//')
COMPAT_VERSION = $(UPSTREAM_VERSION)~dfsg
PYTHON_VERSION = 1.0a1

MIB_MODULES = smux ucd-snmp/dlmod mibII/mta_sendmail disman/event-mib
EXCL_MIB_MODULES =

OLD_MIBS_DIR="/usr/share/mibs/site:/usr/share/snmp/mibs:/usr/share/mibs/iana:/usr/share/mibs/ietf:/usr/share/mibs/netsnmp"
MIBS_DIR="/usr/share/snmp/mibs:/usr/share/snmp/mibs/iana:/usr/share/snmp/mibs/ietf"

ifeq (linux,$(DEB_BUILD_ARCH_OS))
MIB_MODULES += ucd-snmp/diskio ucd-snmp/lmsensorsMib etherlike-mib/dot3StatsTable
IPV6 = --enable-ipv6
DEB_DH_GENCONTROL_ARGS=-- -Vos-specific-dev="libsensors4-dev"
else
ifeq (kfreebsd,$(DEB_BUILD_ARCH_OS))
IPV6 = --enable-ipv6
DEB_DH_GENCONTROL_ARGS=-- -Vos-specific-dev="libkvm-dev"
CFLAGS += $(shell pkg-config --cflags libbsd-overlay)
LDFLAGS += $(shell pkg-config --libs libbsd-overlay)
ifeq (solaris,$(DEB_BUILD_ARCH_OS))
MIB_MODULES += disman/event-mib ucd-snmp/diskio udp-mib tcp-mib if-mib
IPV6 = --enable-ipv6
  ifeq (solaris-sparc,$(DEB_BUILD_ARCH))
    MIB_MODULES += ucd-snmp/lmSensors
  endif
endif
endif
endif
ifeq (hurd,$(DEB_BUILD_ARCH_OS))
IPV6 = --enable-ipv6
EXCL_MIB_MODULES += mibII
else
MIB_MODULES += host
endif

%:
	dh $@ --with autotools-dev,autoreconf,python2
#	dh $@ --with autotools-dev,autoreconf,python2,systemd

override_dh_auto_configure:
	dh_auto_configure -- --prefix=/usr --sysconfdir=/etc --mandir=/usr/share/man \
	  --with-persistent-directory=/var/lib/snmp \
	  --enable-ucd-snmp-compatibility \
	  --enable-shared --with-cflags="$(CFLAGS) -DNETSNMP_USE_INLINE" \
	  --with-ldflags="$(LDFLAGS)" \
	  --with-perl-modules="INSTALLDIRS=vendor" \
	  $(IPV6) --with-logfile=none \
	  --without-rpm --with-libwrap --with-openssl \
	  --without-dmalloc --without-efence --without-rsaref \
	  --with-sys-contact="root" --with-sys-location="Unknown" \
	  --with-mib-modules="$(MIB_MODULES)" \
	  --with-out-mib-modules="$(EXCL_MIB_MODULES)" \
	  --enable-mfd-rewrites \
	  --with-mibdirs="\$$HOME/.snmp/mibs:$(MIBS_DIR):$(OLD_MIBS_DIR)" \
	  --with-mnttab=/etc/mnttab \
	  --with-mysql \
	  --with-defaults \
	  --enable-agentx-dom-sock-only

#	  --with-mnttab=/etc/mtab \
#	  --with-mysql \
# --with-python-modules=""  # don't specify it, since dh_python2 cannot handle it.
# --with-dnssec-local-validation  # not enabled since libval doesn't exist in Debian yet

override_dh_auto_build:
	dh_auto_build
	cd python; python setup.py build --basedir=$(CURDIR)

override_dh_auto_install:
	dh_auto_install
	cd python; python setup.py install --root=$(CURDIR)/debian/tmp \
		--install-layout=deb --basedir=$(CURDIR) ; \
		find $(CURDIR) -name *.pyc -delete

override_dh_clean:
	dh_clean
	rm -rf `find . -name .libs` \
	       python/build         \
	       python/netsnmp_python.egg-info \
	       dist/generation-scripts/gen-variables \
	       perl/SNMP/t/snmptest.cmd \
	       perl/TrapReceiver/const-c.inc \
	       perl/TrapReceiver/const-xs.inc

override_dh_strip:
	dh_strip -plibsnmp$(LIB_VERSION) --dbg-package=libsnmp$(LIB_VERSION)-dbg

override_dh_makeshlibs:
	dh_makeshlibs -plibsnmp$(LIB_VERSION) -V"libsnmp$(LIB_VERSION) (>= $(COMPAT_VERSION))"

override_dh_install-arch:
	dh_install
	install $(CURDIR)/EXAMPLE.conf $(CURDIR)/debian/snmpd/etc/snmp/snmpd.conf.sample
	# SMF service
	cp -f debian/svc/snmpd.xml $(CURDIR)/debian/snmpd/var/svc/manifest/network/
	cp -f debian/svc/snmpd $(CURDIR)/debian/snmpd/var/svc/method/
	chmod 755 $(CURDIR)/debian/snmpd/var/svc/method/snmpd

override_dh_installdocs:
	dh_installdocs -plibsnmp-perl -ptkmib -plibsnmp-base -plibsnmp$(LIB_VERSION)
	dh_installdocs --link-doc=libsnmp$(LIB_VERSION) \
		       -plibsnmp-dev \
		       -ppython-netsnmp \
		       -psnmpd \
		       -psnmptrapd \
		       -psnmp

override_dh_fixperms-arch:
	dh_fixperms
	chmod -x debian/libsnmp-dev/etc/snmp/*.conf
	chmod 600 debian/snmpd/etc/snmp/*.sample

override_dh_auto_test:
	# prevent test since it fails with network configuration under pbuilder/etc.
	true

#override_dh_systemd_enable:
#	dh_systemd_enable --package=snmptrapd --no-enable
#	dh_systemd_enable --remaining-packages

get-orig-source:
	@uscan --download-version $(UPSTREAM_VERSION) --destdir /tmp --force-download --no-symlink
	@tar xf /tmp/net-snmp-$(UPSTREAM_VERSION).tar.gz -C /tmp
	@rm -rf /tmp/net-snmp-$(UPSTREAM_VERSION)/doc
	@cd /tmp/net-snmp-$(UPSTREAM_VERSION)/mibs; \
	 grep ^iana ianalist | while read iana mibs; do rm -f `echo $$mibs | sed -e s/$$/.txt/`; done; \
	 cat rfclist | while read rfc mibs; do rm -f `echo $$mibs | sed 's/:/.txt /g' | sed 's/$$/.txt/'`; done
	@rm -f /tmp/net-snmp-$(UPSTREAM_VERSION)/mibs/RFC-1215.txt
	@cd /tmp; tar cJf net-snmp_$(COMPAT_VERSION).orig.tar.xz net-snmp-$(UPSTREAM_VERSION)
	@rm -rf /tmp/net-snmp-$(UPSTREAM_VERSION)
