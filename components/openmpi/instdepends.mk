INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += automake
INSTDEPENDS += autoconf
INSTDEPENDS += libtool
INSTDEPENDS += libltdl-dev
INSTDEPENDS += autotools-dev
INSTDEPENDS += chrpath
INSTDEPENDS += gfortran
INSTDEPENDS += libhwloc-dev
INSTDEPENDS += pkg-config
#INSTDEPENDS += libibverbs-dev
#INSTDEPENDS += libfabric-dev
#INSTDEPENDS += libpsm-infinipath1-dev
#INSTDEPENDS += libcr-dev [linux-any]
#INSTDEPENDS += libnuma-dev [linux-any]
INSTDEPENDS += flex
