Source: poppler
Section: devel
Priority: optional
Maintainer: Loic Minier <lool@dooz.org>
Uploaders: Josselin Mouette <joss@debian.org>,
           Pino Toscano <pino@debian.org>
Build-Depends: debhelper (>= 9),
               dpkg (>= 1.16.1),
               dh-autoreconf,
               libglib2.0-dev (>= 2.41),
               libfontconfig1-dev,
               libqt4-dev (>= 4:4.7.0),
               libcairo2-dev (>= 1.10.0),
               libopenjp2-7-dev,
               libjpeg-dev,
               libpng-dev,
               libtiff-dev,
               liblcms2-dev,
               libfreetype6-dev,
               gtk-doc-tools (>= 1.14),
               pkg-config (>= 0.18),
               libgirepository1.0-dev (>= 1.42.0-2~),
               gobject-introspection (>= 1.42.0-2~),
               qtbase5-dev,
               zlib1g-dev,
               libnss3-dev,
               libglib2.0-doc,
               libcairo2-doc,
               libiconv-dev [solaris-any]
Standards-Version: 3.9.8
Homepage: http://poppler.freedesktop.org/
Vcs-Git: https://alioth.debian.org/anonscm/git/pkg-freedesktop/poppler.git
Vcs-Browser: https://anonscm.debian.org/gitweb/?p=pkg-freedesktop/poppler.git
Testsuite: autopkgtest

Package: libpoppler64
Architecture: any
Section: libs
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends},
         ${misc:Depends}
Recommends: poppler-data
Description: PDF rendering library
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains the shared core library.

Package: libpoppler-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends: libpoppler64 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Description: PDF rendering library -- development files
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains the development libraries needed to build applications
 using Poppler.

Package: libpoppler-private-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends: libpoppler-dev (= ${binary:Version}),
         ${misc:Depends}
Suggests: libfreetype6-dev,
Breaks: libpoppler-dev (<< 0.20.2)
Replaces: libpoppler-dev (<< 0.20.2)
Description: PDF rendering library -- private development files
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains the private and unstable headers needed to build
 applications using the private Poppler core library.

Package: libpoppler-glib8
Architecture: any
Section: libs
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: libpoppler64 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Description: PDF rendering library (GLib-based shared library)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package provides the GLib-based shared library for applications
 using the GLib interface to Poppler.

Package: libpoppler-glib-dev
Architecture: any
Section: libdevel
Depends: libpoppler-glib8 (= ${binary:Version}),
         libpoppler-dev (= ${binary:Version}),
         gir1.2-poppler-0.18 (= ${binary:Version}),
         libglib2.0-dev (>= 2.41),
         libcairo2-dev (>= 1.10.0),
         ${shlibs:Depends},
         ${misc:Depends}
Suggests: libpoppler-glib-doc
Description: PDF rendering library -- development files (GLib interface)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains the headers and development libraries needed to
 build applications using the GLib-based Poppler interface.

Package: libpoppler-glib-doc
Architecture: all
Section: doc
Depends: libglib2.0-doc,
         libcairo2-doc,
         ${misc:Depends}
Breaks: libpoppler-glib-dev (<< 0.22.5)
Replaces: libpoppler-glib-dev (<< 0.22.5)
Description: PDF rendering library -- documentation for the GLib interface
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains the API documentation of the GLib-based Poppler
 interface.

Package: gir1.2-poppler-0.18
Architecture: any
Section: introspection
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${gir:Depends}
Description: GObject introspection data for poppler-glib
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains introspection data for poppler-glib.
 .
 It can be used by packages using the GIRepository format to generate
 dynamic bindings.

Package: libpoppler-qt4-4
Architecture: any
Section: libs
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: libpoppler64 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Description: PDF rendering library (Qt 4 based shared library)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package provides the Qt 4 based shared library for applications
 using the Qt 4 interface to Poppler.

Package: libpoppler-qt4-dev
Architecture: any
Section: libdevel
Depends: libpoppler-qt4-4 (= ${binary:Version}),
         libpoppler-dev (= ${binary:Version}),
         libqt4-dev (>= 4:4.7.0),
         ${shlibs:Depends},
         ${misc:Depends}
Description: PDF rendering library -- development files (Qt 4 interface)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains the headers and development libraries needed to
 build applications using the Qt 4-based Poppler interface.

Package: libpoppler-qt5-1
Architecture: any
Section: libs
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: libpoppler64 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Description: PDF rendering library (Qt 5 based shared library)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package provides the Qt 5 based shared library for applications
 using the Qt 5 interface to Poppler.

Package: libpoppler-qt5-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends: libpoppler-qt5-1 (= ${binary:Version}),
         libpoppler-dev (= ${binary:Version}),
         qtbase5-dev,
         ${shlibs:Depends},
         ${misc:Depends}
Description: PDF rendering library -- development files (Qt 5 interface)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains the headers and development libraries needed to
 build applications using the Qt 5-based Poppler interface.

Package: libpoppler-cpp0v5
Architecture: any
Section: libs
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: libpoppler64 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Conflicts: libpoppler-cpp0
Replaces: libpoppler-cpp0
Description: PDF rendering library (CPP shared library)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package provides the CPP shared library for applications
 using a simple C++ interface (using STL, and no other dependency) to Poppler.

Package: libpoppler-cpp-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends: libpoppler-cpp0v5 (= ${binary:Version}),
         libpoppler-dev (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Description: PDF rendering library -- development files (CPP interface)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains the headers and development libraries needed to
 build applications using the C++ Poppler interface.

Package: poppler-utils
Architecture: any
Section: utils
Multi-Arch: foreign
Depends: libpoppler64 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
# should really be just the following:
#Breaks: xpdf-utils (<< 3.02-2~)
# ... but because of #774949 (remove after Stretch/9.0) instead is:
Breaks: xpdf-utils (<< 1:0), xpdf-common
Conflicts: pdftohtml
Replaces: xpdf-utils (<< 3.02-2~),
          pdftohtml,
          xpdf-reader
Provides: xpdf-utils,
          pdftohtml
Description: PDF utilities (based on Poppler)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains command line utilities (based on Poppler) for getting
 information of PDF documents, convert them to other formats, or manipulate
 them:
  * pdfdetach -- lists or extracts embedded files (attachments)
  * pdffonts -- font analyzer
  * pdfimages -- image extractor
  * pdfinfo -- document information
  * pdfseparate -- page extraction tool
  * pdfsig -- verifies digital signatures
  * pdftocairo -- PDF to PNG/JPEG/PDF/PS/EPS/SVG converter using Cairo
  * pdftohtml -- PDF to HTML converter
  * pdftoppm -- PDF to PPM/PNG/JPEG image converter
  * pdftops -- PDF to PostScript (PS) converter
  * pdftotext -- text extraction
  * pdfunite -- document merging tool

Package: poppler-dbg
Architecture: linux-any
Section: debug
Priority: extra
Multi-Arch: same
Depends: ${misc:Depends},
         libpoppler64 (= ${binary:Version})
Description: PDF rendering library -- debugging symbols
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains the debugging symbols for all the Poppler libraries
 and the utilities.

