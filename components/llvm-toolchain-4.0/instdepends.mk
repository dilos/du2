INSTDEPENDS += debhelper
INSTDEPENDS += flex
INSTDEPENDS += bison
INSTDEPENDS += dejagnu
INSTDEPENDS += tcl
INSTDEPENDS += expect
INSTDEPENDS += cmake
# perl
INSTDEPENDS += libtool
# chrpath
INSTDEPENDS += texinfo
INSTDEPENDS += sharutils
INSTDEPENDS += libffi-dev
INSTDEPENDS += lsb-release
INSTDEPENDS += patchutils
INSTDEPENDS += diffstat
INSTDEPENDS += xz-utils
INSTDEPENDS += python-dev
INSTDEPENDS += libedit-dev
INSTDEPENDS += libncurses5-dev
INSTDEPENDS += swig
INSTDEPENDS += python-six
INSTDEPENDS += python-sphinx
# binutils-dev
INSTDEPENDS += libjsoncpp-dev
INSTDEPENDS += lcov
# procps
INSTDEPENDS += help2man
INSTDEPENDS += zlib1g-dev
# g++-multilib [amd64 i386 kfreebsd-amd64 mips mips64 mips64el mipsel powerpc ppc64 s390 s390x sparc sparc64 x32],
#    ocaml-nox [amd64 arm64 armel armhf i386],
#    ocaml-findlib [amd64 arm64 armel armhf i386],
#    libctypes-ocaml-dev [amd64 arm64 armel armhf i386],
#    dh-ocaml [amd64 arm64 armel armhf i386],
