INSTDEPENDS += debhelper
INSTDEPENDS += check
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dh-exec
INSTDEPENDS += dpkg-dev
INSTDEPENDS += intltool
#libasound2-dev (>= 1.0.24) [linux-any],
INSTDEPENDS += libasyncns-dev
INSTDEPENDS += libatomic-ops-dev
INSTDEPENDS += libavahi-client-dev
#libbluetooth-dev (>= 4.99) [linux-any] <!stage1>,
#libsbc-dev [linux-any],
#libcap-dev [linux-any],
INSTDEPENDS += libfftw3-dev
INSTDEPENDS += libgconf2-dev
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libgtk-3-dev
INSTDEPENDS += libice-dev
INSTDEPENDS += libjack-dev
INSTDEPENDS += libjson-c-dev
#INSTDEPENDS += liblircclient-dev
INSTDEPENDS += libltdl-dev
INSTDEPENDS += liborc-0.4-dev
INSTDEPENDS += libsamplerate0-dev
INSTDEPENDS += libsndfile1-dev
INSTDEPENDS += libsoxr-dev
INSTDEPENDS += libspeexdsp-dev
INSTDEPENDS += libssl-dev
#libsystemd-dev [linux-any],
INSTDEPENDS += libtdb-dev
#libudev-dev (>= 143) [linux-any],
#libwebrtc-audio-processing-dev (>= 0.2) [linux-any],
INSTDEPENDS += libwrap0-dev
INSTDEPENDS += libx11-xcb-dev
INSTDEPENDS += libxcb1-dev
INSTDEPENDS += libxtst-dev
