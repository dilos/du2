confuse (3.0+dfsg-2+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- Denis Kozadaev <denis@tambov.ru>  Fri, 08 Dec 2017 20:01:09 +0300

confuse (3.0+dfsg-2) unstable; urgency=medium

  * Upload to unstable. 

 -- Aurelien Jarno <aurel32@debian.org>  Mon, 06 Jun 2016 21:41:34 +0200

confuse (3.0+dfsg-1) experimental; urgency=medium

  * New upstream version (closes: #824697)
    - Rename libconfuse0 into libconfuse1 to match the ABI change.
    - Drop debian/patches/02-doxygen-dotfont.patch (obsolete)
  * Upload to experimental.

 -- Aurelien Jarno <aurel32@debian.org>  Sun, 29 May 2016 16:14:54 +0000

confuse (2.8+dfsg-1) unstable; urgency=medium

  * New upstream version: 
    - Drop debian/patches/01-german-translation.patch (merged upstream).
  * Update upstream URL to the new site.
  * Update symbols file.
  * Bumped Standards-Version to 3.9.8.

 -- Aurelien Jarno <aurel32@debian.org>  Tue, 24 May 2016 21:20:46 +0200

confuse (2.7-5) unstable; urgency=low

  * Switch to dpkg-source 3.0 (quilt) format.
  * Switch to dh.
  * Rebuild the documentation in the build process.
  * Pass --with autoreconf to dh (closes: #749051).
  * Bumped Standards-Version to 3.9.5.

 -- Aurelien Jarno <aurel32@debian.org>  Sun, 25 May 2014 21:56:23 +0200

confuse (2.7-4) unstable; urgency=low

  * Run the testsuite during build (closes: #635915). 
  * Add build-arch and build-indep targets. 

 -- Aurelien Jarno <aurel32@debian.org>  Sun, 31 Jul 2011 14:14:18 +0200

confuse (2.7-3) unstable; urgency=low

  * Add a replaces on libconfuse0 (<< 2.7-2) in libconfuse-common. Closes;
    #630862).

 -- Aurelien Jarno <aurel32@debian.org>  Sat, 18 Jun 2011 19:22:33 +0200

confuse (2.7-2) unstable; urgency=low

  * Bumped Standards-Version to 3.9.2 (no changes).
  * Convert to multiarch:
    - Bump debhelper build-dep to >= 8.1.3 for multiarch.
    - Add Pre-Depends: ${misc:Pre-Depends} to libconfuse0 and it 
      Multi-Arch: same.
    - Install the library in the multiarch path.
    - Add a libconfuse-common package containing the translations.
  * Dropped .la file from -dev packages since it is not used by any
    depending package. 

 -- Aurelien Jarno <aurel32@debian.org>  Wed, 15 Jun 2011 21:59:29 +0200

confuse (2.7-1) unstable; urgency=low

  * New upstream version.
  * Bump Standards-Version to 3.8.4 (no changes).
  * Add symbol files.
  * Switch to debhelper 5.
  * libconfuse-dev.doc-base.doc, libconfuse-dev.doc-base.tutorial:
    move to section Programming/C. 
  * Add German translation, by Martin Hedenfalk (closes: #532848).

 -- Aurelien Jarno <aurel32@debian.org>  Mon, 22 Feb 2010 22:34:23 +0100

confuse (2.6-2) unstable; urgency=low

  * Use -Wno-usused to get this package buildable with gcc-4.3 (closes:
    bug#462697).

 -- Aurelien Jarno <aurel32@debian.org>  Sun, 27 Jan 2008 00:54:53 +0100

confuse (2.6-1) unstable; urgency=low

  * New upstream version. 

 -- Aurelien Jarno <aurel32@debian.org>  Sat, 29 Dec 2007 21:33:17 +0100

confuse (2.5-3) unstable; urgency=low

  * Make the package binNMU safe. 

 -- Aurelien Jarno <aurel32@debian.org>  Mon,  5 Mar 2007 21:24:10 +0100

confuse (2.5-2) unstable; urgency=low

  * Correctly handle parameters with double quotes and backslash.  Thanks
    to Raphaël Hertzog for the patch (closes: bug#374875).
  * Bumped Standards-Version to 3.7.2 (no changes).
  * debian/copyright: updated FSF address.

 -- Aurelien Jarno <aurel32@debian.org>  Sun,  9 Jul 2006 13:01:58 +0200

confuse (2.5-1) unstable; urgency=low

  * First upload of the year. Happy New Year!
  * New upstream version.

 -- Aurelien Jarno <aurel32@debian.org>  Sat,  1 Jan 2005 13:20:44 +0100

confuse (2.4-1) unstable; urgency=low

  * New upstream version.

 -- Aurelien Jarno <aurel32@debian.org>  Sun, 26 Sep 2004 21:52:20 +0200

confuse (2.3-1) unstable; urgency=low

  * New upstream version.
  * Bumped Standards-Version to 3.6.1 (no changes).

 -- Aurelien Jarno <aurel32@debian.org>  Mon,  2 Aug 2004 15:02:25 +0200

confuse (2.2-1) unstable; urgency=low

  * Initial Release (closes: bug#241095).

 -- Aurelien Jarno <aurel32@debian.org>  Wed, 31 Mar 2004 00:24:58 +0200

