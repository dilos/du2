INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
INSTDEPENDS += bison
INSTDEPENDS += chrpath
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += file
INSTDEPENDS += libffi-dev
INSTDEPENDS += libgdbm-dev
INSTDEPENDS += libgmp-dev
INSTDEPENDS += libncurses5-dev
INSTDEPENDS += libncursesw5-dev
INSTDEPENDS += libreadline-dev
INSTDEPENDS += libssl1.0-dev
INSTDEPENDS += libyaml-dev
# netbase
# openssl
# procps
# ruby | ruby-interpreter | ruby1.8,
INSTDEPENDS += ruby
INSTDEPENDS += rubygems-integration
# systemtap-sdt-dev [linux-any],
INSTDEPENDS += tcl8.6-dev
INSTDEPENDS += tk8.6-dev
INSTDEPENDS += zlib1g-dev
#
INSTDEPENDS += dtrace
INSTDEPENDS += libcpp
