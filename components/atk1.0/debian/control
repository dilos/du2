# This file is autogenerated. DO NOT EDIT!
# 
# Modifications should be made to debian/control.in instead.
# This file is regenerated automatically in the clean target.
Source: atk1.0
Section: libs
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Andreas Henriksson <andreas@fatal.se>, Emilio Pozuelo Monfort <pochu@debian.org>, Iain Lane <laney@debian.org>, Mario Lang <mlang@debian.org>, Michael Biebl <biebl@debian.org>, Sjoerd Simons <sjoerd@debian.org>
Vcs-Browser: https://anonscm.debian.org/viewvc/pkg-gnome/desktop/unstable/atk1.0
Vcs-Svn: svn://anonscm.debian.org/pkg-gnome/desktop/unstable/atk1.0
Build-Depends: debhelper (>= 10),
               pkg-config,
               libglib2.0-dev (>= 2.31.2),
               gnome-pkg-tools (>= 0.10),
               gtk-doc-tools (>= 1.13),
               docbook-xml,
               autotools-dev,
               gobject-introspection (>= 0.9.12-4~),
               libgirepository1.0-dev (>= 0.10.7-1~)
Build-Depends-Indep: libglib2.0-doc
Standards-Version: 3.9.8
Homepage: https://wiki.gnome.org/Accessibility

Package: libatk1.0-0
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libatk1.0-data (= ${source:Version})
Pre-Depends: ${misc:Pre-Depends}
Description: ATK accessibility toolkit
 ATK is a toolkit providing accessibility interfaces for applications or
 other toolkits. By implementing these interfaces, those other toolkits or
 applications can be used with tools such as screen readers, magnifiers, and
 other alternative input devices.
 .
 This is the runtime part of ATK, needed to run applications built with it.

Package: libatk1.0-udeb
Package-Type: udeb
Section: debian-installer
Architecture: linux-any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: ATK accessibility toolkit
 This is a udeb, or a microdeb, for the debian-installer.
 .
 ATK is a toolkit providing accessibility interfaces for applications or
 other toolkits. By implementing these interfaces, those other toolkits or
 applications can be used with tools such as screen readers, magnifiers, and
 other alternative input devices.
 .
 This is a stripped down version of the runtime part of ATK.

Package: libatk1.0-data
Section: misc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Common files for the ATK accessibility toolkit
 ATK is a toolkit providing accessibility interfaces for applications or
 other toolkits. By implementing these interfaces, those other toolkits or
 applications can be used with tools such as screen readers, magnifiers, and
 other alternative input devices.
 .
 This contains the common files which the runtime libraries need.

Package: libatk1.0-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         libatk1.0-0 (= ${binary:Version}),
         gir1.2-atk-1.0 (= ${binary:Version}),
         pkg-config,
         libglib2.0-dev (>= 2.31.2)
Replaces: gir-repository-dev
Description: Development files for the ATK accessibility toolkit
 ATK is a toolkit providing accessibility interfaces for applications or
 other toolkits. By implementing these interfaces, those other toolkits or
 applications can be used with tools such as screen readers, magnifiers, and
 other alternative input devices.
 .
 These are the development files for ATK, needed for compilation of
 programs or toolkits which use it.

Package: libatk1.0-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: libglib2.0-doc,
          libgtk2.0-doc,
          devhelp
Description: Documentation files for the ATK toolkit
 ATK is a toolkit providing accessibility interfaces for applications or
 other toolkits. By implementing these interfaces, those other toolkits or
 applications can be used with tools such as screen readers, magnifiers, and
 other alternative input devices.
 .
 This contains the HTML documentation for the ATK library in
 /usr/share/doc/libatk1.0-doc/ .

Package: gir1.2-atk-1.0
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         ${gir:Depends}
Description: ATK accessibility toolkit (GObject introspection)
 ATK is a toolkit providing accessibility interfaces for applications or
 other toolkits. By implementing these interfaces, those other toolkits or
 applications can be used with tools such as screen readers, magnifiers, and
 other alternative input devices.
 .
 This package can be used by other packages using the GIRepository format to
 generate dynamic bindings
