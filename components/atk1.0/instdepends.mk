INSTDEPENDS += debhelper
INSTDEPENDS += pkg-config
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += gnome-pkg-tools
INSTDEPENDS += gtk-doc-tools
INSTDEPENDS += docbook-xml
INSTDEPENDS += autotools-dev
INSTDEPENDS += gobject-introspection
INSTDEPENDS += libgirepository1.0-dev
# Build-Depends-Indep:
INSTDEPENDS += libglib2.0-doc
