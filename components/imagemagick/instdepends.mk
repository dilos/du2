INSTDEPENDS += debhelper
# for improving build
INSTDEPENDS += dh-exec
INSTDEPENDS += dh-autoreconf
# for linking compiling ...
INSTDEPENDS += pkg-config
INSTDEPENDS += libltdl-dev
# for libtool does not link to depends lib
INSTDEPENDS += chrpath
# for special function
INSTDEPENDS += libfftw3-dev
INSTDEPENDS += liblcms2-dev
INSTDEPENDS += liblqr-1-0-dev
# for fonts
INSTDEPENDS += libfreetype6-dev
INSTDEPENDS += libfontconfig1-dev
INSTDEPENDS += gsfonts
# for compression
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += liblzma-dev
INSTDEPENDS += libbz2-dev
# for X
INSTDEPENDS += libx11-dev
INSTDEPENDS += libxext-dev
INSTDEPENDS += libxt-dev
# for plugins
INSTDEPENDS += ghostscript
INSTDEPENDS += libdjvulibre-dev
INSTDEPENDS += libexif-dev
INSTDEPENDS += libjpeg-dev
INSTDEPENDS += libopenjp2-7-dev
INSTDEPENDS += libopenexr-dev
INSTDEPENDS += libperl-dev
INSTDEPENDS += libpng-dev
INSTDEPENDS += libtiff-dev
INSTDEPENDS += libwmf-dev
# libgraphviz-dev, incompatible license against fftw
# for converting svg
INSTDEPENDS += libpango1.0-dev
INSTDEPENDS += librsvg2-bin
INSTDEPENDS += librsvg2-dev
INSTDEPENDS += libxml2-dev
# for easy symbols of c++ lib
INSTDEPENDS += pkg-kde-tools
# for libgomp symbols
INSTDEPENDS += dpkg-dev
# for test of poc
INSTDEPENDS += libtool-bin
# for documentation
#Build-Depends-Indep:
INSTDEPENDS += doxygen
INSTDEPENDS += doxygen-latex
INSTDEPENDS += graphviz
INSTDEPENDS += libxml2-utils
INSTDEPENDS += xsltproc
