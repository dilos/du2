INSTDEPENDS += debhelper
INSTDEPENDS += quilt
# Build-Depends-Indep:
INSTDEPENDS += docbook-xml
INSTDEPENDS += docbook-dsssl
INSTDEPENDS += openjade
INSTDEPENDS += w3m
INSTDEPENDS += slice
INSTDEPENDS += autoconf
INSTDEPENDS += recode
