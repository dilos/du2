INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
# pypy
INSTDEPENDS += python-all
INSTDEPENDS += python3-all
INSTDEPENDS += python3-sphinx
INSTDEPENDS += python3-wheel
