#!/usr/bin/make -f

# Uncomment this to turn on verbose mode. 
#export DH_VERBOSE=1

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

DEB_DH_BUILDDEB_ARGS += -- -Z$(shell dpkg-deb --help | grep -q ":.* xz[,.]" \
                               && echo xz || echo bzip2)

# This has to be exported to make some magic below work.
export DH_OPTIONS

export CFLAGS = -g

ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
CFLAGS += -O0
else
CFLAGS += -O3
endif

# Suppress accidental execution of the auto-* tools, c.f.
# http://lists.debian.org/debian-devel/2001/debian-devel-200111/msg01416.html
no_auto_tools = ACLOCAL="`pwd`/missing aclocal" \
	AUTOCONF="`pwd`/missing autoconf" \
	AUTOMAKE="`pwd`/missing automake" \
	AUTOHEADER="`pwd`/missing autoheader"

make_flags = moduledir=/usr/lib/gdk-imlib2 $(no_auto_tools)

build build-arch: $(QUILT_STAMPFN) build-stamp
build-indep:

build-stamp:
	dh_testdir
ifneq "$(wildcard /usr/share/misc/config.sub)" ""
	cp -f /usr/share/misc/config.sub config.sub
endif
ifneq "$(wildcard /usr/share/misc/config.guess)" ""
	cp -f /usr/share/misc/config.guess config.guess
endif
ifneq "$(wildcard /usr/share/libtool/ltmain.sh)" ""
	cp -f /usr/share/libtool/ltmain.sh ltmain.sh
endif
ifneq "$(wildcard /usr/share/libtool/config/ltmain.sh)" ""
	cp -f /usr/share/libtool/config/ltmain.sh ltmain.sh
endif
ifneq "$(wildcard /usr/share/libtool/build-aux/ltmain.sh)" ""
	cp -f /usr/share/libtool/build-aux/ltmain.sh ltmain.sh
endif

	autoreconf -vi && \
	./configure --prefix=/usr --with-gtk-prefix=/usr \
		--mandir=/usr/share/man --sysconfdir=/etc/imlib \
		--x-includes=/usr/include \
		--x-libraries=/usr/lib/$(DEB_HOST_MULTIARCH)\
		--libdir=\$${prefix}/lib/$(DEB_HOST_MULTIARCH)
	$(MAKE) $(make_flags)

	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp
ifneq "$(wildcard /usr/share/misc/config.sub)" ""
	rm -f config.sub
endif
ifneq "$(wildcard /usr/share/misc/config.guess)" ""
	rm -f config.guess
endif

	[ ! -f Makefile ] || $(MAKE) clean
	[ ! -f Makefile ] || $(MAKE) distclean

	dh_clean

install: DH_OPTIONS=
install: build
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs

	$(MAKE) $(make_flags) DESTDIR=`pwd`/debian/tmp install

	dh_movefiles

	# Library package
	dh_install -pimlib11 --autodest debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libImlib.so.*

	# Developer package
	dh_install -pimlib11-dev --autodest debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libImlib.so
	dh_install -pimlib11-dev --autodest debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libImlib.a
	dh_install -pimlib11-dev --autodest debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libImlib.la
	dh_install -pimlib11-dev --autodest debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig/imlib.pc
	dh_install -pimlib11-dev --autodest debian/tmp/usr/include/Imlib*

#	# GDK library package
#	dh_install -pgdk-imlib11 --autodest debian/tmp/usr/lib/libgdk_imlib.so.*
#	dh_install -pgdk-imlib11 --autodest debian/tmp/usr/lib/gdk-imlib2/libimlib*.so

#	# GDK developer package
#	dh_install -pgdk-imlib11-dev --autodest debian/tmp/usr/lib/libgdk_imlib.a
#	dh_install -pgdk-imlib11-dev --autodest debian/tmp/usr/lib/libgdk_imlib.la
#	dh_install -pgdk-imlib11-dev --autodest debian/tmp/usr/lib/libgdk_imlib.so
#	dh_install -pgdk-imlib11-dev --autodest debian/tmp/usr/include/gdk_imlib*
#	dh_install -pgdk-imlib11-dev --autodest debian/tmp/usr/lib/pkgconfig/imlibgdk.pc

	# Extra stuff
	dh_install -pimlib-base --autodest debian/tmp/etc/imlib/*
	dh_install -pimlib-base --autodest debian/tmp/usr/bin/imlib-config
	dh_install -pimlib-base --autodest debian/tmp/usr/share/man/man1/imlib-config.1
	dh_install -pimlib-base --autodest debian/tmp/usr/share/aclocal/*

#	# Compat package
#	mkdir debian/gdk-imlib1/usr
#	mkdir debian/gdk-imlib1/usr/lib
#	ln -s gdk-imlib2 debian/gdk-imlib1/usr/lib/gdk-imlib1
#	ln -s libgdk_imlib.so.2 debian/gdk-imlib1/usr/lib/libgdk_imlib.so.1
#	ln -s libgdk_imlib.so.2 debian/gdk-imlib1/usr/lib/libgdk_imlib.so.9.14

# This single target is used to build all the packages, all at once, or
# one at a time. So keep in mind: any options passed to commands here will
# affect _all_ packages. Anything you want to only affect one package
# should be put in another target, such as the install target.
binary-common:
	dh_testdir
	dh_testroot
	dh_installdocs -A debian/README.Debian
	dh_installexamples
	dh_installmenu
	dh_installcron
	dh_installinfo
	dh_installchangelogs ChangeLog
	dh_link
	dh_compress
	dh_fixperms
	dh_strip --ctfconvert-flags=" "
	dh_makeshlibs
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb $(DEB_DH_BUILDDEB_ARGS)

# Build architecture independent packages using the common target.
binary-indep: build install
	 $(MAKE) -f debian/rules DH_OPTIONS=-i binary-common

# Build architecture dependent packages using the common target.
binary-arch: build install
	$(MAKE) -f debian/rules DH_OPTIONS=-a binary-common

# Any other binary targets build just one binary package at a time.
binary-%: build install
	make -f debian/rules binary-common DH_OPTIONS=-p$*

binary: binary-indep binary-arch
.PHONY: build build-arch clean binary-indep binary-arch binary install
