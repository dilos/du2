From de38df51c67399cf52a7b174aab76bcda2ef8ba8 Mon Sep 17 00:00:00 2001
From: Niko Tyni <ntyni@debian.org>
Date: Mon, 25 Apr 2016 16:31:00 +0300
Subject: perlbug: wrap overly long lines

Mail transport agents limit the length of message lines at SMTP time.
One observed limit is 1000 characters per line. Mail user agents typically
work around these limits by MIME-encoding the message. Since perlbug
doesn't do that, it needs to limit the length of its lines manually to
make sure bug reports get delivered.

The longest lines in perlbug reports normally come from Config::myconfig
output, particularly 'config_args', which has been observed to exceed
1000 characters on some configurations, causing report rejection. While
less likely, the list of local patches is another potential source of
overly long lines.

Use Text::Wrap (if available) to wrap the body of the report at an
arbitrarily chosen and hopefully safe limit of 900 characters. No
indentation or continuation line markers are added, though it would
be easy to add those if desired. Attachments and mail headers are not
wrapped.

(Backported for Debian 5.24 by removing lib/perlbug.t changes)

Origin: backport, http://perl5.git.perl.org/perl.git/bd18aea6d95681e1bd5c2cdfd894bd3706e4b819
Bug: https://rt.perl.org/Ticket/Display.html?id=128020
Bug-Debian: https://bugs.debian.org/822463
Patch-Name: fixes/perlbug-linewrap.diff
---
 utils/perlbug.PL | 13 ++++++++++++-
 1 file changed, 12 insertions(+), 1 deletion(-)

diff --git a/utils/perlbug.PL b/utils/perlbug.PL
index 100bc02..235313b 100644
--- a/utils/perlbug.PL
+++ b/utils/perlbug.PL
@@ -77,6 +77,8 @@ BEGIN {
     $::HaveTemp = ($@ eq "");
     eval { require Module::CoreList; };
     $::HaveCoreList = ($@ eq "");
+    eval { require Text::Wrap; };
+    $::HaveWrap = ($@ eq "");
 };
 
 my $Version = "1.40";
@@ -1083,7 +1085,16 @@ sub _read_report {
     my $content;
     open( REP, "<:raw", $fname ) or die "Couldn't open file '$fname': $!\n";
     binmode(REP, ':raw :crlf') if $Is_MSWin32;
-    while (<REP>) { $content .= $_; }
+    # wrap long lines to make sure the report gets delivered
+    local $Text::Wrap::columns = 900;
+    local $Text::Wrap::huge = 'overflow';
+    while (<REP>) {
+        if ($::HaveWrap && /\S/) { # wrap() would remove empty lines
+            $content .= Text::Wrap::wrap(undef, undef, $_);
+        } else {
+            $content .= $_;
+        }
+    }
     close(REP) or die "Error closing report file '$fname': $!";
     return $content;
 }
