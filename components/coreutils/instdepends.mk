INSTDEPENDS += debhelper
INSTDEPENDS += gettext
INSTDEPENDS += autotools-dev
INSTDEPENDS += texinfo
INSTDEPENDS += groff
#INSTDEPENDS += libattr1-dev [linux-any]
#INSTDEPENDS += libacl1-dev [linux-any]
#INSTDEPENDS += libselinux1-dev [linux-any]
INSTDEPENDS += gperf
INSTDEPENDS += bison
#
INSTDEPENDS += libnvpair-dev
