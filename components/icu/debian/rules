#!/usr/bin/make -f
# -*- makefile -*-

include /usr/share/dpkg/architecture.mk

DEB_HOST_MULTIARCH ?= $(dpkg-architecture -qDEB_HOST_MULTIARCH)
export DEB_HOST_MULTIARCH
CFLAGS += -std=gnu99 -D__EXTENSIONS__ -g -gdwarf-2 -fno-omit-frame-pointer
CXXFLAGS += -D__EXTENSIONS__ -g -gdwarf-2 -fno-omit-frame-pointer
ifeq ($(DEB_HOST_ARCH),solaris-i386)
CFLAGS += -msave-args
CXXFLAGS += -msave-args
endif

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export CC=gcc
export CXX=g++

l_SONAME=57

override_dh_clean:
	dh_clean
	find $(CURDIR)/source/ \( -name Makefile -o -name pkgdataMakefile \) \
		-exec rm {} \;
	rm -Rf build-native

override_dh_auto_configure:
ifeq ($(DEB_BUILD_ARCH),$(DEB_HOST_ARCH))
	dh_auto_configure -- --enable-static
else
	dh_auto_configure -B $(CURDIR)/build-native -- --host=$(DEB_BUILD_GNU_TYPE)
	dh_auto_build -B $(CURDIR)/build-native
	dh_auto_configure -- --enable-static --with-cross-build=$(CURDIR)/build-native
endif

override_dh_auto_build:
	dh_auto_build --parallel
	$(MAKE) -C $(CURDIR)/source/ doc

override_dh_auto_install:
	dh_auto_install
	$(MAKE) -C $(CURDIR)/source/ install-doc DESTDIR=$(CURDIR)/debian/tmp/
	# delete extra license file
	rm  $(CURDIR)/debian/tmp/usr/share/icu/57.1/LICENSE

override_dh_installdocs-indep:
	dh_installdocs -i
	# symlink duplicated files
	for file in `find debian/icu-doc/usr/share/doc/icu-doc/html/ -name \*icu_1_1\*`; do \
		normal=`echo $$file | sed s/icu_1_1//`; \
		if [ -f $$normal ]; then \
			rm $$file; \
			ln -s `basename $$normal` $$file; \
		fi; \
	done

override_dh_strip:
#	dh_strip -plibicu$(l_SONAME) --dbg-package=libicu$(l_SONAME)-dbg
#	dh_strip -picu-devtools --dbg-package=icu-devtools-dbg
#	# libicudata library doesn't contain any debug symbols, remove that
#	for file in `find debian/libicu$(l_SONAME)-dbg/usr/lib/debug -type f -print`; do \
#		if ! nm -a $$file | grep -q debug; then rm $$file; fi; \
#	done
	dh_strip --ctfconvert-flags="-i"

%:
	dh $@ --sourcedirectory=$(CURDIR/)source/ --parallel

.PHONY: override_dh_clean override_dh_auto_configure override_dh_auto_build \
	override_dh_auto_install override_dh_installdocs-indep \
	override_dh_strip
