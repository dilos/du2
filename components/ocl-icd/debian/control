Source: ocl-icd
Maintainer: Vincent Danjean <vdanjean@debian.org>
Section: libs
Priority: extra
Build-Depends: debhelper (>= 9.0.0),
               ruby,
               dh-autoreconf,
               faketime,
               autoconf (>= 2.68),
               automake,
               libtool,
               asciidoc,
               xmlto,
               dpkg-dev (>= 1.17.0)
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/cgit/collab-maint/ocl-icd.git
Vcs-Git: https://anonscm.debian.org/git/collab-maint/ocl-icd.git
Homepage: https://forge.imag.fr/projects/ocl-icd/

Package: ocl-icd-libopencl1
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
# To promote to an Recommends once an ICD is in main
Suggests: opencl-icd
Pre-Depends: ${misc:Pre-Depends}
Conflicts: libopencl1,
           amd-app,
           nvidia-libopencl1-dev
Provides: libopencl1,
          libopencl-1.1-1,
          libopencl-1.2-1,
          libopencl-2.0-1,
          libopencl-2.1-1
Replaces: libopencl1,
          amd-app,
          nvidia-libopencl1-dev
Description: Generic OpenCL ICD Loader
 OpenCL (Open Computing Language) is a multivendor open standard for
 general-purpose parallel programming of heterogeneous systems that include
 CPUs, GPUs and other processors.
 .
 This package contains an installable client driver loader (ICD Loader)
 library that can be used to load any (free or non-free) installable client
 driver (ICD) for OpenCL. It acts as a demultiplexer so several ICD can
 be installed and used together.

Package: ocl-icd-opencl-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${shlibs:Depends},
         ${misc:Depends},
         opencl-c-headers | opencl-headers,
         ocl-icd-libopencl1 (= ${binary:Version})
Recommends: libgl1-mesa-dev | libgl-dev,
            libpoclu-dev
Pre-Depends: ${misc:Pre-Depends}
Breaks: ocl-icd-libopencl1 (<< 2.1.3-5~),
        nvidia-libopencl1 (<< 305~),
        amd-libopencl1 (<< 1:13.4-4~)
Conflicts: opencl-dev
Provides: opencl-dev
Replaces: opencl-dev,
          ocl-icd-libopencl1 (<< 2.1.3-5~),
          nvidia-libopencl1 (<< 305~),
          amd-libopencl1 (<< 1:13.4-4~)
Description: OpenCL development files
 OpenCL (Open Computing Language) is a multivendor open standard for
 general-purpose parallel programming of heterogeneous systems that include
 CPUs, GPUs and other processors.
 .
 This package provides the development files: headers and libraries.
 .
 It also ensures that the ocl-icd ICD loader is installed so its additional
 features (compared to the OpenCL norm) can be used: .pc file, avaibility to
 select an ICD without root privilege, etc.

Package: ocl-icd-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${misc:Depends}
Description: Development files to build a ICD Loader
 OpenCL (Open Computing Language) is a multivendor open standard for
 general-purpose parallel programming of heterogeneous systems that include
 CPUs, GPUs and other processors.
 .
 This package provides a header file that allows a OpenCL implementation
 to build a installable client driver (ICD). With a ICD, an OpenCL
 implementation can be used by any OpenCL program without the need
 to link the program to the specific OpenCL implementation.
 .
 For building OpenCL applications, installs the ocl-icd-opencl-dev package
 instead.
