#!/sbin/sh

#
# Copyright (c) 2007, 2012, Oracle and/or its affiliates. All rights reserved.
# Copyright 2016 Hans Rosenfeld <rosenfeld@grumpf.hope-2000.org>
#
# smf_method(5) start/stop script required for server DNS

. /lib/svc/share/smf_include.sh

get_config ()
{
    configuration_file=/etc/lwresd.conf
    cmdopts=""
    properties="debug_level configuration_file server"

    for prop in $properties
    do
	value=`/usr/bin/svcprop -p options/${prop} ${SMF_FMRI}`
	if [ -z "${value}" -o "${value}" = '""' ]; then
	    continue;
	fi

	case $prop in
	'debug_level')
	    if [ ${value} -gt 0 ]; then
		cmdopts="${cmdopts} -d ${value}"
	    fi
	    ;;
	'configuration_file')
	    cmdopts="${cmdopts} -c ${value}"
	    configuration_file=${value};
	    ;;
	'server')
	    set -- `echo ${value} | /usr/bin/sed -e  's/\\\\//g'`
	    server=$@
	    ;;
	esac
    done

    configuration_dir=$(sed -n -e \
	's,^[[:space:]]*directory.*"\(.*\)";,\1,p' \
	${configuration_file})
    [ -z "${configuration_dir}" ] && configuration_dir=/etc

    configuration_files=$(sed -n -e \
        "s,^[[:space:]]*file.*\"\(.*\)\";,${configuration_dir}/\1,p" \
        ${configuration_file} | sort -u)
    configuration_files="${configuration_files} ${configuration_file}"    
}

result=${SMF_EXIT_OK}

# Read command line arguments
method="$1"		# %m
instance="$2" 		# %i
contract="$3"		# %{restarter/contract}

# Set defaults; SMF_FMRI should have been set, but just in case.
if [ -z "$SMF_FMRI" ]; then
    SMF_FMRI="svc:/network/lwresd:${instance}"
fi
server="/usr/sbin/lwresd"
I=`/usr/bin/basename $0`
cmduser=bind

case "$method" in
'start')
    get_config

    cmdopts="-u ${cmduser} ${cmdopts}"
    if [ ${result} = ${SMF_EXIT_OK} ]; then
	echo "$I: Executing: ${server} ${cmdopts}"
	ppriv -s A-all -s A+basic,net_privaddr,file_dac_read,file_dac_search,sys_resource,proc_chroot,proc_setid -e ${server} ${cmdopts}
	result=$?
	if [ $result -ne 0 ]; then
	    echo "$I : start failed! Check syslog for further information." >&2
        fi
    fi
    ;;
'stop')
    get_config

    smf_kill_contract ${contract} TERM 1
    [ $? -ne 0 ] && exit 1

    ;;
*)
    echo "Usage: $I [stop|start] <instance>" >&2
    exit 1
    ;;
esac
exit ${result}
