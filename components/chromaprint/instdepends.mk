INSTDEPENDS += debhelper
INSTDEPENDS += cmake
INSTDEPENDS += libavcodec-dev
INSTDEPENDS += libavformat-dev
INSTDEPENDS += libswresample-dev
INSTDEPENDS += python-docutils
INSTDEPENDS += libfftw3-dev
